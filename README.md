# Climprod 5

Climprod project with Pierre FREON and Julien VEYSSIER.

Experimental interactive graphical software for choosing and fitting surplus production
models including environmental variables

Climprod is written in java and distributed in form of a .jar file.

## Usage

files in example_data/ are directly loadable in Climprod.

Here is a list of answers to go to the end of "select the appropriate model and fit it" with the
data file "EXAMPLE.CLI" :

* Have there been changes in the fishing pattern during the period (effort allocation quota mesh-size ...)?__No__
* Is the fishing effort unit standardized and is the CPUE proportional to abundance?__Yes__
* Do time-lags and deviations from the stable age structure have negligible effects on production rate?__Yes__
* Does the data-set apply to a single stock?__No__
* Does the data-set apply to a sub-stock?__Yes__
* Is the sub-stock well isolated (i.e. with few exchanges) from others?__Yes__
* Do you think that the data-set covers periods both of overexploitation and of underexploitation?__No__
* Do you think that the data-set cover periods both of underexploitation and optimal exploitation?__Yes__
* Do you see any abnormal statistics in the statistical data table?__(see the second table in the Climprod frame) No__
* Is interannual variability too large?__No__
* Do you see outlier points?__No__
* Do you see outlier points?__No__
* Constantly increasing effort?__Yes__
* Are the two variables dependent?__No__
* Number of significantly exploited year-classes __1__
* Is the influence of fishing effort on CPUE more important than environmental influence?__Yes__
* Does this plot appear to be decreasing?__Yes__
* Does this plot look obviously linear?__No__
* Do you have any (additional) reason to expect highly unstable behaviour or collapse of the stock?__No__
* Did the stock already collapse or exhibit drastic decrease(s) in catches?__No__
* What is the life span of the species?__6__
* Is the ratio (lifespan/number of exploited year-classes) lower than 2?__No__
* Is the single stock subdivided into various geographical sub-stocks (all must be exploited by the fleet)?__Don't know__
* Are there natural protected areas for the stock or constantly inacessible adult biomass?__Don't know__
* Are there one or several non negligible spawnings before recruitment?__Don't know__
* Is the fecundity of the species very low (sharks mammals etc.)?__No__
* Is there a strong instability in the cpue time series?__No__
* Does the environment influence: __abundance__
* May the stock present large fluctuations in CPUE when overexploited?__No__
* Does this plot look linear __No__
* Does this plot look monotonic?__Yes__
* Age at recruitment __1__
* Age at the begining of environmental influence __0__
* Age at the end of environmental influence __1__
* Is this an acceptable model?__Yes__
* Are there a good fit and no trend or strong autocorrelation in residuals?__Yes__
* Are there reasonable jackknife coefficient R2 (>65% recommended) no extreme yearly coefficient and acceptable MSY graph __Yes__

## TO DO

* find out why "MSY and MS-E graphs" cannot be plotted when Number of significantly exploited year-classes = 1; Age at recruitment = 1; Age at the beginning of environmental influence = 1; Age at the end of environmental influence = 1.
* identify the infinite loop and fix it. Elements of answer in SARDFAO2.CLI .

