/**
 * Titre : Climprod<p>
 * Gere fenetre xx setTitle("Climprod : Results tables (model fitted :"
 */
package fr.ird.climprod;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumn;

public class Cadre_Resultats extends JFrame {

    JTabbedPane jTabbedPane = new JTabbedPane();
    JTable[] jTableValidation = new JTable[4];
    JTable[] jTableModelisation = new JTable[2];
    JPanel jPanelValidation = new JPanel();
    JPanel jPanelModelisation = new JPanel();
    JPanel jPanel1 = new JPanel();

    JScrollPane jScrollPane1 = new JScrollPane();
    JScrollPane jScrollPane3 = new JScrollPane();
    JScrollPane jScrollPane2 = new JScrollPane();

    JScrollPane jScrollPane4 = new JScrollPane();
    JScrollPane jScrollPane5 = new JScrollPane();
    JScrollPane jScrollPane6 = new JScrollPane();

    GridLayout gridLayout1 = new GridLayout();

    public Cadre_Resultats() {
        try {
            initWindow();
            UtilCadre.Size(this, 80, 85);
            System.out.println(" Facteurs reduction fenetre par rapport a max, sx et sy ligne 35 Cadre_Resultats.java = " + 80 + " " + 85); // Test 2020
            UtilCadre.Centrer(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initWindow() throws Exception {
        this.setIconImage(Toolkit.getDefaultToolkit().createImage(Cadre_Resultats.class.getResource("resources/images/Climprod.jpg")));
        this.addWindowListener(new java.awt.event.WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                this_windowClosing(e);
            }
        });
        this.getContentPane().add(jTabbedPane, BorderLayout.CENTER);
        jPanelValidation.setLayout(gridLayout1);
        jPanel1.setLayout(new GridLayout(3, 1, 5, 5));
   // gridLayout1.setRows(3);
        // gridLayout1.setVgap(5);
        jScrollPane1.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)), "Final Jackknife results"));
        jScrollPane3.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)), "Noteworthy jackknife values for MSY"));
        jScrollPane2.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)), "Noteworthy jackknife values for MSE"));
        gridLayout1.setColumns(2);
        jScrollPane4.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)), "Intermediate Jackknife results"));
        jPanelValidation.add(jPanel1, null);
        jPanel1.add(jScrollPane1, null);
        jPanel1.add(jScrollPane2, null);
        jPanel1.add(jScrollPane3, null);
        
        final JFrame jj = this;
        int condition = JComponent.WHEN_IN_FOCUSED_WINDOW;
        KeyStroke keyStroke2 = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
        InputMap inputMap2 = jTabbedPane.getInputMap(condition);
        ActionMap actionMap2 = jTabbedPane.getActionMap();
        inputMap2.put(keyStroke2, keyStroke2.toString());
        actionMap2.put(keyStroke2.toString(), new AbstractAction() {            
            @Override
            public void actionPerformed(ActionEvent arg0) {
                jj.dispatchEvent(new WindowEvent(jj, WindowEvent.WINDOW_CLOSING));
            }
        });
        
        jPanelValidation.add(jScrollPane4, null);

        jPanelModelisation.setLayout(gridLayout1);
        jScrollPane5.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)), "Main results"));
        jScrollPane6.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)), "Detailled results"));
        jPanelModelisation.add(jScrollPane5, null);
        jPanelModelisation.add(jScrollPane6, null);
        if (Global.modelisationOk) {
            jTableModelisation[0] = new JTable();
            //renseignerTable(jTableModelisation[0],Data.getStatistics());
            renseignerTable(jTableModelisation[0], Modele.getResult());
            jScrollPane5.getViewport().add(jTableModelisation[0], null);
            jTableModelisation[1] = new JTable();
            renseignerTable(jTableModelisation[1], Modele.getYearResult());
            jScrollPane6.getViewport().add(jTableModelisation[1], null);
            jTabbedPane.add("Modelization", jPanelModelisation);
            jTableCopyPaste[] ModToClipboard;
            ModToClipboard = new jTableCopyPaste[2];
            for (int i = 0; i < 2; i++) {
                jTableModelisation[i].setCellSelectionEnabled(true);
                jTableModelisation[i].setRowSelectionAllowed(false);
                ModToClipboard[i] = new jTableCopyPaste(jTableModelisation[i], true, false);
            }
        }
        if (Global.validationOk) {
            jTableValidation[0] = new JTable();
            renseignerTable(jTableValidation[0], Validation.getParamResult());
            jTableValidation[1] = new JTable();
            renseignerTable(jTableValidation[1], Validation.getMS_EResult());
            jTableValidation[2] = new JTable();
            renseignerTable(jTableValidation[2], Validation.getMS_YResult());
            jTableValidation[3] = new JTable();
            renseignerTable(jTableValidation[3], Validation.getYearResult());

            // jTabbedPane.add(new DispositionTables(jTableValidation));
            jScrollPane1.getViewport().add(jTableValidation[0], null);
            jScrollPane2.getViewport().add(jTableValidation[1], null);
            jScrollPane3.getViewport().add(jTableValidation[2], null);
            jScrollPane4.getViewport().add(jTableValidation[3], null);
            jTabbedPane.add("Validation", jPanelValidation);
            jTableCopyPaste[] ValToClipboard = new jTableCopyPaste[4];
            for (int i = 0; i < 4; i++) {
                jTableValidation[i].setCellSelectionEnabled(true);
                jTableValidation[i].setRowSelectionAllowed(false);
                ValToClipboard[i] = new jTableCopyPaste(jTableValidation[i], true, false);
            }
        }
        this.setTitle("Climprod : Results tables (model fitted :" + RechercheModele.getEquation() + ")");
    }

    void this_windowClosing(WindowEvent e) {
        this.dispose();
    }

    @Override
    public void dispose() {
        super.dispose();
        Global.CadreResultats = null;
    }

    private void renseignerTable(JTable t, Object[][] res) {
        if (res == null) {
            return;
        }
        int c = res[0].length;
        int l = res.length - 1;
        String[] column = new String[c];
        for (int j = 0; j < c; j++) {
            column[j] = (String) res[0][j];
        }

        Object[][] dat = new Object[l][c];
        for (int i = 0; i < l; i++) {
            System.arraycopy(res[i + 1], 0, dat[i], 0, c);

        }
        ModeleDeTableStat mdt = new ModeleDeTableStat(dat, column);
        t.setModel(mdt);
        TableColumn tmcolumn;
        int nbc = t.getColumnModel().getColumnCount();
        for (int i = 0; i < nbc; i++) {
            tmcolumn = t.getColumnModel().getColumn(i);
            tmcolumn.setMinWidth(50);
        }

    }

}

class DispositionTables extends JPanel {

    Box boxV;
    Box boxH;
    JScrollPane[] jScrollPane1;
    JTable[] jTable1;

    public DispositionTables(JTable[] t) {
        if (t.length != 3) {
            return;
        }
        jScrollPane1 = new JScrollPane[3];
        for (int i = 0; i < 3; i++) {
            jScrollPane1[i] = new JScrollPane();
        }
        jTable1 = t;
        try {
            initWindow();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initWindow() throws Exception {
        boxV = Box.createHorizontalBox();
        boxH = Box.createVerticalBox();
        jScrollPane1[0].setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)), "Mains results"));
        jScrollPane1[1].setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)), "Noteworthy values"));
        jScrollPane1[2].setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)), "Detailled values"));
        this.add(boxV, null);
        boxV.add(boxH, null);
        boxH.add(jScrollPane1[0], null);
        boxH.add(jScrollPane1[1], null);
        boxV.add(jScrollPane1[2], null);
        jScrollPane1[0].getViewport().add(jTable1[0], null);
        jScrollPane1[1].getViewport().add(jTable1[1], null);
        jScrollPane1[2].getViewport().add(jTable1[2], null);
    }

}
