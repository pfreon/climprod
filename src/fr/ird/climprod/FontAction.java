/**
 * En lien avec PlotPreferences.java. Commentaire 2020.
 */
package fr.ird.climprod;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

class FontAction extends AbstractAction {

    public FontAction(JComboBox comb1, JComboBox comb2, JComboBox comb3, JLabel comp) {
        target = comp;
        name = comb1;
        style = comb2;
        size = comb3;

    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        
        String Name = (String) name.getSelectedItem();
        String SizeString = (String) size.getSelectedItem();
        int Size = Integer.parseInt(SizeString);
        String Style = (String) style.getSelectedItem();
        
        Font f = new Font(Name, Font.BOLD + Font.ITALIC, Size);
        if (null != Style) switch (Style) {
            case "Plain":
                f = new Font(Name, Font.PLAIN, Size);
                break;
            case "Bold":
                f = new Font(Name, Font.BOLD, Size);
                break;
            case "Italic":
                f = new Font(Name, Font.ITALIC, Size);
                break;
        }
        target.setFont(f);
        target.setText(target.getText());
        target.repaint();
    }

    private final JLabel target;
    private final JComboBox name, style, size;

}
