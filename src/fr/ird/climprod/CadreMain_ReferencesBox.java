// Fenetre Help ->References du menu principal pour présenter la référence du logiciel pour citation.
package fr.ird.climprod;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

public class CadreMain_ReferencesBox extends JDialog implements ActionListener {


  JPanel jPanReferences = new JPanel();
  JPanel jPanCmd = new JPanel();
  JScrollPane jScrollPanText = new JScrollPane();
  JButton cmdOK = new JButton();
  JTextArea jTextAreaReferences = new JTextArea();



  BorderLayout borderLayout1 = new BorderLayout();
  FlowLayout flowLayout1 = new FlowLayout();
  FlowLayout flowLayout2 = new FlowLayout();
  GridLayout gridLayout1 = new GridLayout();


  public CadreMain_ReferencesBox(Frame parent) {
    super(parent);
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try {
      jbInit();
    }
    catch(Exception e) {
      e.printStackTrace();
    }
   // imageIcone.setIcon(imageIcon);
    pack();
  }

  private void jbInit() throws Exception  {
    //imageIcon = new ImageIcon(getClass().getResource("Climprod.jpg"));
    this.setTitle("References");
    setResizable(false);

    jPanReferences.setLayout(borderLayout1);
    jPanCmd.setLayout(flowLayout1);
    //jPanImage.setLayout(flowLayout1);
   // jPanImage.setBorder(new EmptyBorder(10, 10, 10, 10));
   // gridLayout1.setRows(4);
   // gridLayout1.setColumns(1);

    jTextAreaReferences.setText(
   "HOW TO CITE THIS SOFTWARE, ITS EQUATIONS & AND THE PAST-ENVIRONMENT-AVERAGING APPROCH:\n\n" +
   "The software itself and the all the equations of the mixed models where CPUE = f(E,V) " +
   "can be cited as follows:\n" +
   "Fréon, P., Mullon, C. and Pichon, G. 1993. CLIMPROD: experimental interactive software " + 
   "for choosing and fitting surplus production models including environmental variables. " +
   "FAO Computerized Information Series Fisheries, 5. 76p.\n" +
   "Regarding equations of the mixed models # 13, 21, 26 and 31 the following text must be " +
   "added to the previous reference: 'Model(s) available from version 5.0 of the CLIMPROD software'.\n\n" +
   "OTHER USEFUL REFERENCES ABOUT THE CLIMPROD APPROACH:\n\n" +
   "Fréon, P. 1988. Introduction of climatic variables into global production models. in: " +
   "International Symposium on Long Term Changes in Marine Fish Populations, Vigo (Espange) 1986. " +
   "Larrañeta, M.G. and Wyatt, T. (Eds). Consejo Superior de Investigaciones Cientificas: 481-528.\n" +
   "Fréon, P. 1991. L'introduction d'une variable climatique dans les modèles globaux de " +
   "production. In P. Cury and C. Roy. (Eds). Pêcheries Ouest-Africaines : variabilité, " +
   "instabilité et changements. ORSTOM Editions, Paris. pp. 395-424. NOTE: Translation into " +
   "French of the above paper.\n" +
   "Fréon, P., Mullon, C. and Pichon, G. 1991. CLIMPROD: a fully interactive expert-system " +
   "for choosing and adjusting global production models which accounts for changes in " +
   "environmental factors. In T. Kawasaki, S. Tanaka, Y. Toba and A. Taniguchi. (Eds). " +
   "Long-term variability of pelagic fish populations and their environment. " +
   "Pergamon Press. pp. 347-357.\n" +
   "Fréon, P., Kiewcinski, B. and Lopez, D. 1992. Utilité du système expert CLIMPROD pour " +
   "la connaissance du déterminisme du recrutement. Ann. Inst. Océanogr., 68: 193-210.\n" + 
   "Fréon, P. and Yáñez, E. 1995. Influencia del medio ambiente en evaluación de stock: " +
   "una aproximación con modelos de producción. Invest. Mar., Chili 23: 25-47.");
    jTextAreaReferences.setLineWrap(true);
    jTextAreaReferences.setWrapStyleWord(true);
    jTextAreaReferences.setPreferredSize(new Dimension(550, 600));
    jTextAreaReferences.setEditable(false);
    jTextAreaReferences.setMargin(new Insets(5, 5, 5, 5));
    jTextAreaReferences.setFont(new java.awt.Font("Serif", 0, 14)); // Fenetre Help ->References du menu principal. Avant 0, 11. Modif 2020.
//    jPanText.setLayout(flowLayout1);
//    jPanText.setBorder(new EmptyBorder(10, 10, 10, 10));
//    jPanText.add(jTextAreaReferences, null);


	jScrollPanText.setBorder(new EmptyBorder(10, 10, 10, 10));
	jScrollPanText.add(jTextAreaReferences, null);

	jScrollPanText.getViewport().add(jTextAreaReferences, null);

    cmdOK.setText("Ok");
    int condition = JComponent.WHEN_IN_FOCUSED_WINDOW;
    KeyStroke keyStrokep = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
    InputMap inputMapp = cmdOK.getInputMap(condition);
    ActionMap actionMapp = cmdOK.getActionMap();
    inputMapp.put(keyStrokep, keyStrokep.toString());
    actionMapp.put(keyStrokep.toString(), new AbstractAction() {            
        @Override
        public void actionPerformed(ActionEvent arg0) {
            cmdOK.doClick();
        }
    });
    cmdOK.addActionListener(this);
    jPanCmd.add(cmdOK, null);

	jPanReferences.add(jScrollPanText, BorderLayout.CENTER);

    jPanReferences.add(jPanCmd, BorderLayout.SOUTH);
    this.getContentPane().add(jPanReferences, null);
  }

  protected void processWindowEvent(WindowEvent e) {
    if (e.getID() == WindowEvent.WINDOW_CLOSING) {
      cancel();
    }
    super.processWindowEvent(e);
  }

  void cancel() {
    dispose();
  }

  public void actionPerformed(ActionEvent e) {
    if (e.getSource() == cmdOK) {
      cancel();
    }
  }
}