package fr.ird.climprod;

/* ****************************************************************** 
   *******Definition of the Main() method (Java entry point) ********
   *******and size of the main frame ******************************** 
   ******************************************************************
*/

import static fr.ird.climprod.AppelNavigateur.isMacPlatform; //  isMacPlatform returns whether this is a Mac OS X platform
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class Main {

    boolean packFrame = false;
    
    private void show() {
        CadreMain frame = new CadreMain();
        if (packFrame) {
            frame.pack();
        } else {
            frame.validate();
        }

        UtilCadre.leftResize(frame, 95, 95); // Resize of the main frame (cadre principal).
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        // Set Look&Feel
        try {
            // IF MACOS !!!!!!!!
            //UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            if (! isMacPlatform()){
                UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
            }
            //UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
            //UIManager.setLookAndFeel("it.unitn.ing.swing.plaf.macos.MacOSLookAndFeel");
            System.out.println("Theme FOUND");
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
        }
        // Show main frame
        Main main = new Main();
        main.show();
    }
}
