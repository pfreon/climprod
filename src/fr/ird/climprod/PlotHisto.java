/*
 * Titre : Climprod<p>
 * Permet de tracer des histogrammes, toutes les séries doivent avoir la même
 * taille.
 */
package fr.ird.climprod;
//import java.awt.event.*;

public class PlotHisto extends PlotModal {

    public PlotHisto() {
        super();
        super.typePlot = 3;
    }

    /*
     * Ajoute une série de données au graphique représentant des valeurs. La
     * série est ajoutée au vecteur ListeSerie. Après l'ajout la méthode
     * détermine les nouveaux extremas du graphique
     *
     * @param PlotSerie s.
     */
    @Override
    public void setValeurs(PlotSerie s) {

        if (nbSerie == 0) {
            nbBarPlot = s.getSize();
        }
        if ((nbSerie == 0) || (nbBarPlot == s.getSize())) {
            super.setValeurs(s);
        }
        if (extremasAxes[2] > 0 && nbSerie == 1) {
            extremasAxes[2] = 0;
        }

    }
    /*
     * Trace les axes du graphique.
     *
     * @param : Graphics g, le contexte graphique.
     */
    /*
     * public void TraceAxes(Graphics g){
     *
     * float x1,y1,xg,px; float x2,y2,yg,py; float maxx; int grad; float nb;
     * Line2D.Float line; String eti; Font fcurrent=g.getFont(); FontMetrics fm;
     * Dimension d=panel.getSize(); Graphics2D g2 = (Graphics2D) g; Shape[]
     * shapes= new Shape[1];
     *
     * if (d.width<d.height) {	grad=(int) (d.width*0.01);} else {grad=(int)
     * (d.height*0.01);}
     *
     * x1= (extremasAxes[0]*paramEchelle[0]-paramEchelle[2]); x2=
     * (extremasAxes[1]*paramEchelle[0]-	paramEchelle[2]); y1=
     * ((extremasAxes[2]+origine[0])*paramEchelle[1]+	paramEchelle[3]); y2=
     * ((extremasAxes[3]+origine[0])*paramEchelle[1]+	paramEchelle[3]); line=new
     * Line2D.Float(x1,y1,x2,y1); g2.draw(line); fm=g.getFontMetrics(feti);
     * py=fm.getHeight(); float py2=py/2; float dec=0;
     *
     * nb=extremasAxes[0]+pas[0]; g.setFont(feti); for(int i=0;i<nbBarPlot;i++)
     * { xg= (nb*paramEchelle[0]-	paramEchelle[2]); line=new
     * Line2D.Float(xg,y1-grad,xg,y1+grad); g2.draw(line); eti=etiquettes[i];
     * px=fm.stringWidth(eti)/2;
     *
     * g.drawString(eti,(int)(xg-px),(int)(y1+grad+py+dec)); nb=nb+pas[0];
     * dec=py2-dec; } TextLayout textTl = new TextLayout(titre[0],
     * getPoliceTitre('x'), new FontRenderContext(null, false, false));
     * textTl.draw(g2,x1+(x2-x1)/2-(float)(textTl.getBounds().getWidth())/2,
     * y1+grad+2*py+(float)(textTl.getBounds().getHeight())); textTl = new
     * TextLayout(titre[2], getPoliceTitre('g'), new FontRenderContext(null,
     * false, false));
     * textTl.draw(g2,x1+(x2-x1)/2-(float)(textTl.getBounds().getWidth())/2,
     * y2-2*(float)(textTl.getBounds().getHeight()));
     *
     *
     * x1= ((extremasAxes[0]+origine[1])*paramEchelle[0]-paramEchelle[2]); x2=
     * ((extremasAxes[1]+origine[1])*paramEchelle[0]-	paramEchelle[2]); y1=
     * (extremasAxes[2]*paramEchelle[1]+	paramEchelle[3]); y2=
     * (extremasAxes[3]*paramEchelle[1]+	paramEchelle[3]); line=new
     * Line2D.Float(x1,y1,x1,y2); g2.draw(line); nb=extremasAxes[2]+pas[1];
     * maxx=0; while (nb<=extremasAxes[3]+0.0001)
     * {
     * yg=  (nb*paramEchelle[1]+	paramEchelle[3]);
     * line=new Line2D.Float(x1-grad,yg,x1+grad,yg);
     * g2.draw(line);
     * eti=getFormat(nb,'y');
     * px=fm.stringWidth(eti)+2;
     * if (px>maxx) maxx=px;
     *
     * g.drawString(eti,(int)(x1-grad-px),(int)(yg)); nb=nb+pas[1]; } int w =
     * d.width; textTl = new TextLayout(titre[1], getPoliceTitre('y'), new
     * FontRenderContext(null, false, false)); AffineTransform textAt = new
     * AffineTransform(); g2.setFont(getPoliceTitre('y'));
     * textAt.setToTranslation(x1-grad-maxx-(float)textTl.getBounds().getHeight(),(y2+(y1-y2+(float)textTl.getBounds().getWidth())/2));
     * g2.transform(textAt ); textAt.setToRotation(3*Math.PI/2.0);
     * g2.transform(textAt); g2.drawString(titre[1],0,0); g.setFont(fcurrent); }
     */
    private String[] etiquettes;
    private int nbBarPlot;
}
