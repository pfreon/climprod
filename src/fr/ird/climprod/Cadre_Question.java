/*****
 * Titre : Climprod<p>
 * Gère les fenêtres de "Select the appropriate model and fit it" (=Questions)
 * y compris les messages de résultats de la sélection, dont ceux
 * correspondant aux décisions négatives (-1, -2, -3; lignes 410-441).
 */
package fr.ird.climprod;
import static fr.ird.climprod.UtilCadre.Centrer;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.border.*;
import javax.swing.event.*;

public class Cadre_Question extends JFrame 
{
    JPanel jpanBouton = new JPanel();
    JButton cmdPrevious = new JButton();
    JButton cmdNext = new JButton();
    JPanel jpanHelp = new JPanel();
    JButton cmdHelp = new JButton();
    GridBagLayout gridBagLayout1 = new GridBagLayout();
    GridLayout gridLayout1 = new GridLayout();
    JButton cmdStop = new JButton();
    GridLayout gridLayout2 = new GridLayout();
    DefaultListModel<String> modele = new DefaultListModel<String>();
    JPanel jpanRegle = new JPanel();
    JScrollPane jScrollPane1 = new JScrollPane();
    JTextArea jTextAreaRegle = new JTextArea();
    BorderLayout borderLayout1 = new BorderLayout();
    TitledBorder titledBorder1;
    JPanel jpanQuestion = new JPanel();
    JScrollPane jScrollPane2 = new JScrollPane();
    JScrollPane jScrollPane4 = new JScrollPane();
    JTextArea jTextAreaQuestion = new JTextArea();
    JList<String> jlstItem = new JList<String>();
    GridLayout gridLayout3 = new GridLayout();
    JScrollPane jScrollPane3 = new JScrollPane();
    JTextArea jTextAreaMessage = new JTextArea();
    Cadre_SplitPlot dlgSp;
    public static int  nim, i, jk;
 
    private JFrame parent;

    public Cadre_Question(JFrame parent) 
  {
        try {
            this.parent = parent;
            initWindow();
            UtilCadre.Size(this, 60, 60); // remplace 60, 60 par 30, 30. Cadre Questions non-graphiques Test 2020   	    
    	    //System.out.println("Définition dans ligne 47 de Cadre_Question.java des Facteurs de réduction (30, 30)(JFrame) par rapport à max (sx et sy ligne 73 UtilCadre.java");// Test 2020
            UtilCadre.Centrer(this);
            QuestionReponse.reset();
            this.loadQuestion(true);
            } 
	   catch (Exception e) {e.printStackTrace();
        }
    }

    private void initWindow() throws Exception {
        // TODO mettre en toFront la win main quand this est fermé, plus bas

        this.setIconImage(Toolkit.getDefaultToolkit().createImage(Cadre_Question.class.getResource("resources/images/Climprod.jpg")));
        this.addWindowListener(new java.awt.event.WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                Global.CadreQuestion = null;
                this_windowClosing(e);
            }
        });
        titledBorder1 = new TitledBorder("");
        this.getContentPane().setLayout(gridBagLayout1);
        this.setTitle("Climprod: Select the appropriate model and fit it");
        cmdPrevious.setEnabled(false);
        cmdPrevious.setText("Previous (P)");
        int condition = JComponent.WHEN_IN_FOCUSED_WINDOW;
        KeyStroke keyStrokep = KeyStroke.getKeyStroke(KeyEvent.VK_P, 0);
        InputMap inputMapp = cmdPrevious.getInputMap(condition);
        ActionMap actionMapp = cmdPrevious.getActionMap();
        inputMapp.put(keyStrokep, keyStrokep.toString());
        actionMapp.put(keyStrokep.toString(), new AbstractAction() {            
            @Override
            public void actionPerformed(ActionEvent arg0) {
                cmdPrevious.doClick();
            }
        });
        cmdPrevious.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                cmdPrevious_actionPerformed(e);
            }
        });
        cmdNext.setText("Next (N)");
        // type N key to simulate a click on the "Next" button
        KeyStroke keyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_N, 0);
        InputMap inputMap = cmdNext.getInputMap(condition);
        ActionMap actionMap = cmdNext.getActionMap();
        inputMap.put(keyStroke, keyStroke.toString());
        actionMap.put(keyStroke.toString(), new AbstractAction() {            
            @Override
            public void actionPerformed(ActionEvent arg0) {
                cmdNext.doClick();
            }
        });
        cmdNext.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                cmdNext_actionPerformed(e);
            }
        });
        cmdHelp.setText("Help (H)");
        KeyStroke keyStroke3 = KeyStroke.getKeyStroke(KeyEvent.VK_H, 0);
        InputMap inputMap3 = cmdHelp.getInputMap(condition);
        ActionMap actionMap3 = cmdHelp.getActionMap();
        inputMap3.put(keyStroke3, keyStroke3.toString());
        actionMap3.put(keyStroke3.toString(), new AbstractAction() {            
            @Override
            public void actionPerformed(ActionEvent arg0) {
                cmdHelp.doClick();
            }
        });
        cmdHelp.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                cmdHelp_actionPerformed(e);
            }
        });
        jpanHelp.setLayout(gridLayout2);
        jpanBouton.setLayout(gridLayout1);
        cmdStop.setText("Stop (Esc)");
        KeyStroke keyStroke2 = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
        InputMap inputMap2 = cmdStop.getInputMap(condition);
        ActionMap actionMap2 = cmdStop.getActionMap();
        inputMap2.put(keyStroke2, keyStroke2.toString());
        actionMap2.put(keyStroke2.toString(), new AbstractAction() {            
            @Override
            public void actionPerformed(ActionEvent arg0) {
                cmdStop.doClick();
            }
        });
        cmdStop.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                cmdStop_actionPerformed(e);
            }
        });
        gridLayout2.setHgap(3);
        gridLayout1.setHgap(3);
        jScrollPane1.setBorder(new TitledBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED, Color.white, Color.white, new Color(142, 142, 142), new Color(99, 99, 99)), "Current rule"));
        jScrollPane1.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        jTextAreaRegle.setLineWrap(true);
        jTextAreaRegle.setWrapStyleWord(true);
        jTextAreaRegle.setMargin(new Insets(2, 2, 2, 2));
        jTextAreaRegle.setEditable(false);
        // jpanRegle.setLayout(borderLayout1);
        //  jpanRegle.setBorder(new TitledBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED,Color.white,Color.white,new Color(99, 99, 99),new Color(142, 142, 142)),"Current rule"));
        jScrollPane2.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        jTextAreaQuestion.setLineWrap(true);
        jTextAreaQuestion.setWrapStyleWord(true);
        jTextAreaQuestion.setMargin(new Insets(2, 2, 2, 2));
        jTextAreaQuestion.setText("");
        jTextAreaQuestion.setEditable(false);
        jpanQuestion.setLayout(gridLayout3);
        gridLayout3.setRows(2);
        gridLayout3.setVgap(5);
        jpanQuestion.setBorder(new TitledBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED, Color.white, Color.white, new Color(142, 142, 142), new Color(99, 99, 99)), "Question"));
        jScrollPane3.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        jScrollPane3.setBorder(new TitledBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED, Color.white, Color.white, new Color(142, 142, 142), new Color(99, 99, 99)), "Message"));
        jTextAreaMessage.setLineWrap(true);
        jTextAreaMessage.setWrapStyleWord(true);
        jTextAreaMessage.setText("");
        jTextAreaMessage.setEditable(false);
        jlstItem.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                jlstItem_valueChanged(e);
            }
        });
        jlstItem.addMouseListener(new javax.swing.event.MouseInputListener() {
            @Override            
            public void mouseClicked(MouseEvent e) {
                //System.out.println("Mouse clicked (# of clicks: " + e.getClickCount() + ")");
                if (e.getClickCount() == 2){
                    cmdNext.doClick();
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                //System.out.println("Mouse clicked (# of clicks: "
                //    + e.getClickCount() + ")");
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void mouseExited(MouseEvent e) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void mouseMoved(MouseEvent e) {
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
        jlstItem.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        //KeyStroke keyStroke4 = KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD0, 0);
        KeyStroke keyStroke411 = KeyStroke.getKeyStroke(KeyEvent.VK_A, 0);
        KeyStroke keyStroke422 = KeyStroke.getKeyStroke(KeyEvent.VK_Z, 0);
        KeyStroke keyStroke433 = KeyStroke.getKeyStroke(KeyEvent.VK_E, 0);
        KeyStroke keyStroke444 = KeyStroke.getKeyStroke(KeyEvent.VK_R, 0);
        KeyStroke keyStroke455 = KeyStroke.getKeyStroke(KeyEvent.VK_T, 0);
        KeyStroke keyStroke466 = KeyStroke.getKeyStroke(KeyEvent.VK_Y, 0);
        KeyStroke keyStroke41 = KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD1, 0);
        KeyStroke keyStroke42 = KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD2, 0);
        KeyStroke keyStroke43 = KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD3, 0);
        KeyStroke keyStroke44 = KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD4, 0);
        KeyStroke keyStroke45 = KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD5, 0);
        KeyStroke keyStroke46 = KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD6, 0);
        KeyStroke keyStroke47 = KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD7, 0);
        KeyStroke keyStroke48 = KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD8, 0);
        KeyStroke keyStroke49 = KeyStroke.getKeyStroke(KeyEvent.VK_NUMPAD9, 0);
        InputMap inputMap4 = jlstItem.getInputMap(condition);
        ActionMap actionMap4 = jlstItem.getActionMap();
        //inputMap4.put(keyStroke4, keyStroke4.toString());
        inputMap4.put(keyStroke41, keyStroke41.toString());
        inputMap4.put(keyStroke411, keyStroke411.toString());
        inputMap4.put(keyStroke422, keyStroke422.toString());
        inputMap4.put(keyStroke433, keyStroke433.toString());
        inputMap4.put(keyStroke444, keyStroke444.toString());
        inputMap4.put(keyStroke455, keyStroke455.toString());
        inputMap4.put(keyStroke466, keyStroke466.toString());
        inputMap4.put(keyStroke42, keyStroke42.toString());
        inputMap4.put(keyStroke43, keyStroke43.toString());
        inputMap4.put(keyStroke44, keyStroke44.toString());
        inputMap4.put(keyStroke45, keyStroke45.toString());
        inputMap4.put(keyStroke46, keyStroke46.toString());
        inputMap4.put(keyStroke47, keyStroke47.toString());
        inputMap4.put(keyStroke48, keyStroke48.toString());
        inputMap4.put(keyStroke49, keyStroke49.toString());
        AbstractAction aa = new AbstractAction() {            
            @Override
            public void actionPerformed(ActionEvent arg0) {
                //System.out.println("COMMMMM : "+arg0.getActionCommand());
                //System.out.println("paramstring : "+arg0.paramString());
                jlstItem.setSelectedIndex(Integer.parseInt(arg0.getActionCommand()
                        .replace('a', '1')
                        .replace('z', '2')
                        .replace('e', '3')
                        .replace('r', '4')
                        .replace('t', '5')
                        .replace('y', '6')
                )-1);
            }
        };
        //actionMap4.put(keyStroke4.toString(), aa);
        actionMap4.put(keyStroke411.toString(), aa);
        actionMap4.put(keyStroke422.toString(), aa);
        actionMap4.put(keyStroke433.toString(), aa);
        actionMap4.put(keyStroke444.toString(), aa);
        actionMap4.put(keyStroke455.toString(), aa);
        actionMap4.put(keyStroke466.toString(), aa);
        actionMap4.put(keyStroke41.toString(), aa);
        actionMap4.put(keyStroke42.toString(), aa);
        actionMap4.put(keyStroke43.toString(), aa);
        actionMap4.put(keyStroke44.toString(), aa);
        actionMap4.put(keyStroke45.toString(), aa);
        actionMap4.put(keyStroke46.toString(), aa);
        actionMap4.put(keyStroke47.toString(), aa);
        actionMap4.put(keyStroke48.toString(), aa);
        actionMap4.put(keyStroke49.toString(), aa);
                
        jpanBouton.add(cmdPrevious, null);
        jpanBouton.add(cmdNext, null);

        jpanHelp.add(cmdStop, null);
        jpanHelp.add(cmdHelp, null);
        jScrollPane3.getViewport().add(jTextAreaMessage, null);
        jScrollPane2.getViewport().add(jTextAreaQuestion, null);
        jScrollPane1.getViewport().add(jTextAreaRegle, null);
        jScrollPane4.getViewport().add(jlstItem, null);
        jpanQuestion.add(jScrollPane2, null);
        jpanQuestion.add(jScrollPane4, null);
        if (Global.bavard) {
            this.getContentPane().add(jScrollPane1, new GridBagConstraints(0, 0, 4, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            this.getContentPane().add(jpanQuestion, new GridBagConstraints(0, 1, 4, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            this.getContentPane().add(jScrollPane3, new GridBagConstraints(1, 3, 3, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            this.getContentPane().add(jpanBouton, new GridBagConstraints(0, 4, 2, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(5, 11, 10, 0), 0, 0));
            this.getContentPane().add(jpanHelp, new GridBagConstraints(2, 4, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(5, 11, 10, 6), 0, 0));
        } else {
            this.getContentPane().add(jpanQuestion, new GridBagConstraints(0, 0, 4, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            this.getContentPane().add(jScrollPane3, new GridBagConstraints(0, 1, 4, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            this.getContentPane().add(jpanBouton, new GridBagConstraints(0, 3, 2, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(5, 11, 10, 0), 0, 0));
            this.getContentPane().add(jpanHelp, new GridBagConstraints(2, 3, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(5, 11, 10, 6), 0, 0));
        }
        
    }

    void cmdPrevious_actionPerformed(ActionEvent e) {
        jTextAreaMessage.setText("");
        int p = QuestionReponse.previousQuestion();
        if (p == 1 && Global.bavard == false) {
            p = 0;   //gestion affichage premiere question
        }
        cmdPrevious.setEnabled(p > 0);
        if (Global.etapeEnCours != QuestionReponse.getEtape()) {   //Reponse conduisant au choix du mod�le succeptible de changer.
            if (Global.etapeEnCours == 2) {
                Global.modelisationOk = false;
                Global.numero_modele = -2;
                //System.out.println("Dans Cadre_Question.java: On passe de l'�tape 2 � l'�tape 1");
            } else if (Global.etapeEnCours == 3) {
                Global.validationOk = false;
                //System.out.println("Dans Cadre_Question.java: On passe de l'�tape 3 � l'�tape 2");
            }
            Global.etapeEnCours = QuestionReponse.getEtape();
        }

        this.loadQuestion(false);

    }

    void cmdNext_actionPerformed(ActionEvent e) {
        int item = jlstItem.getSelectedIndex();
        if (modele.getSize() == 0) {
            item = QuestionReponse.getReponse();
        }
        if (item == -1) {
            new MsgDialogBox(0, "Please answer this question", 1, this.parent);
        } else {
            jTextAreaMessage.setText("");
            QuestionReponse.nextQuestion(item);
            Global.etapeEnCours = QuestionReponse.getEtape();
            switch (Global.etapeEnCours) {
                case -1:
                    System.out.println("Current step is -1");
                case Global.questionM:
                    this.loadQuestion(true);
                    break;
                case Global.fixationM:
                    RechercheModele.select();
                    //System.out.println("retour equation " +RechercheModele.getEquation());
                    if (RechercheModele.getType() == -1) {
                        new MsgDialogBox(0, "There is no available model fully appropriate to your case, which is:\n" 
                        + "an environmental influence on: " + Global.environmental_influence
                        + "\na relationship between CPUE and E: " 
                        + CadreMain.cpu_relation_E[Global.relationCPU_E] 
                        + "\nand a relationship between CPUE and V: " 
                        + CadreMain.cpu_relation_V[Global.relationCPU_V]      
                        + ".\nIn order to found an appropriate model, you can revise your \nprevious answers. \nIt is not recommended to modify your answers too much upward \ndue to re-initialization issues.", 1, this.parent);
                        return;

                    }
                    if (Global.numero_modele != RechercheModele.getNumero()) {
                        Global.numero_modele = RechercheModele.getNumero();
                        Global.typeModele = RechercheModele.getType();
                        if (RechercheModele.getType() != -1) {
                            Modele.Estimer();
                        }
                        Cadre_Modele dlg = new Cadre_Modele(this);
                        dlg.setTitle("Climprod: The model " + RechercheModele.getEquation() + " is selected and fitted");
                        // deprecated
                        //dlg.show();
                        dlg.setVisible(true);
                        this.setVisible(false);
                    }
                    this.loadQuestion(true);

                    break;

                case Global.validationM:
                    this.loadQuestion(true);
                    if (!Global.validationOk) {
                        Validation.valide_modele();
                    }
                    break;

            }
            if (Global.bavard) {
                cmdPrevious.setEnabled(true);
            } else {
                cmdPrevious.setEnabled(QuestionReponse.getNbQuestion() > 1);
            }
        }

    }
    private void loadQuestion(boolean next) {

        int numQ, numR;
        String Script;
        String[] item;
        String message;
        modele = new DefaultListModel<String>();
        numQ = QuestionReponse.getNum();       // Numéro de question vers laquelle la règle renvoi (2ème et 3ème colonne de fichier Arbre_decisions.csv)
        numR = QuestionReponse.getNumRegle();  // Numéro de règle appliquée (4ème colonne de fichier Arbre_decisions.csv).
        //System.out.println("Dans Cadre_Question ligne 406: n° de question numQ = " + numQ + "  n° de règle numR = " + numR);
        if (numQ == -1) // End of the model selection procedure due to basic assumptions not fulfilled. 
           {
		   String m$ = "Your data set is not appropriate.";           	
		if (numR > 6) // Modif. 2020
	        m$ = "Your data set is not appropriate for this model (see statistical results in the 'Current Known facts' table).\nI stop here the procedure.";
	    	if (!Global.bavard) 
				{
                m$ = "\n" + TexteRegles.getComment();
            	}
            new MsgDialogBox(0, m$, 0, this.parent);
            stopProcess();
            return;
           } 
		else 
          if (numQ == -2) // End of the model selection procedure due to R2 < .40 for models CPUE=f(E) or CPUE=f(V). Comment 2021
	    	 {
		        new MsgDialogBox(0, "The fit of the simple selected model CPUE = f(E) or CPUE = f(V) \nis not good enough (R2 < 0.40) to be retained in a final model \nor in a mixed model where the considerd variable is most influent.\nI stop here the procedure.", 0, this.parent);
		        //System.out.println("Texte_erreur_jackknife$1 = "); // + Texte_erreur_jackknife$[1]
           QuestionReponse.reset();
            this.dispose();
            Global.CadreQuestion = null;
            if (dlgSp != null)
		{
                dlgSp.setVisible(false);
                dlgSp = null;
            	}
            return;
            } 
		else
          if (numQ == -3) // End of the validation procedure due to R2 < .70 for all mixed models CPUE=f(E,V)	    {
	    {
		        new MsgDialogBox(0, "The selected model is not validated \ndue to R2 < .70 for a mixed model CPUE=f(E,V).\nI stop here the procedure.", 0, this.parent);
           QuestionReponse.reset();
            this.dispose();
            Global.CadreQuestion = null;
            if (dlgSp != null)
		{
                dlgSp.setVisible(false);
                dlgSp = null;
            	}
            return;
            } 
		else
          if (numQ == -4) // End of the validation procedure due to the last answer related to plots at the end of the validation procedure
					      // Addition 2021.
	    {
		        new MsgDialogBox(0, "The selected model is not validated \ndue to your last answer.\nI stop here the procedure.", 0, this.parent);
           QuestionReponse.reset();
            this.dispose();
            Global.CadreQuestion = null;
            if (dlgSp != null)
		{
                dlgSp.setVisible(false);
                dlgSp = null;
            	}
            return;
            } 
	   else 
	  		if (numQ == -5) // Happy end of the validation procedure
	    {
            if (dlgSp != null)
				{
                dlgSp.setVisible(false);
                dlgSp = null;
            	}
			if ((Global.numero_modele < 6 && Global.numero_modele > 1) || Global.numero_modele == 20 || Global.numero_modele == 33) // Modèles CPUE=f(V) et modèle exponentiel additif
				new MsgDialogBox(0, "MSY and MS-E graphs are not justified or available for this model.\nPlease continue.", 0, this);
            new MsgDialogBox(0, "The model is validated. \nYou can use it for prediction, display result tables through \nthe 'Modelization' menu and built a directory containing .html and \n.jpg files of the history of all results (graphs & tables) and \nanswers to questions through the 'File' menu.", 1, this.parent);

            QuestionReponse.reset();
            this.dispose();
            Global.CadreQuestion = null;
            return;
            }
        TexteRegles.loadRegles(numR);
            //System.out.println("Dans Cadre_Question ligne 468: n° de question numQ = " + numQ + "  n° de règle numR = " + numR);        
            jTextAreaRegle.setText(TexteRegles.getScript());
            String[] itemR = TexteRegles.getItem();
            for (int i = 0; i < itemR.length; i++)  // Item.length = nombre d'item (questions) associés à la règle. Commentaire 2020.
			{
                jTextAreaRegle.append("\n\t");
                jTextAreaRegle.append(itemR[i]);
            }
            jTextAreaRegle.setCaretPosition(0);

        QuestionReponse.loadQuestion();
        Script = QuestionReponse.getScript();  // Script = texte question (if any). Commentaire 2020.
        if (Script != null) {
            jTextAreaQuestion.setText(QuestionReponse.getScript());
        } else {
            jTextAreaQuestion.setText("");
        }
        jTextAreaQuestion.setCaretPosition(0);

        item = QuestionReponse.getItem();  // Contenu question codé (ex: [Ljava.lang.String;@7ba4500a)
        jlstItem.setModel(modele);
        if (item != null) {
            for (int i = 0; i < item.length; i++) {
                modele.addElement(item[i]);
            }
            //jlstItem.setModel(modele); (movido hacia arriba)
            int rep = QuestionReponse.getReponse();
            if (rep != -1) {
                jlstItem.setSelectedIndex(rep);
            }            
        } else //pas d'item on provoque la réponse
        {

            QuestionReponse.setReponse();
            if (Global.bavard == false && QuestionReponse.displayQuestion() == false)//quand pas bavard
            {

                if (next) {
                    cmdNext.doClick();
                } else {
                    cmdPrevious.doClick();
                }
				// Passe ici uniquement si pas de commentaire
                return;
            }
        }

        message = TexteRegles.getComment();

        jTextAreaMessage.setText(message); //Message �ventuel
        jTextAreaMessage.setCaretPosition(0);

        if (QuestionReponse.getHelp() == null) {
            this.cmdHelp.setEnabled(false);
        } else {
            this.cmdHelp.setEnabled(true);
        }

        if (dlgSp != null) {
            dlgSp.dispose();
        }
        dlgSp = null;
        Global.JackknifePlot = false;
        switch (numQ) {  // correspond en fait au n° de décision dans Arbre_Decision.csv
            case 12:
                dlgSp = new Cadre_SplitPlot(Global.timePlot);
                break;
            case 13:
                dlgSp = new Cadre_SplitPlot(Global.distriPlot);
                break;
            case 14:
                dlgSp = new Cadre_SplitPlot(Global.scatterPlot);
                break;
            case 15:
                dlgSp = new Cadre_SplitPlot(Global.timePlot[2]);
                break;
            case 16:
                dlgSp = new Cadre_SplitPlot(Global.indePlot);
                break;
            case 18:
                Modele.Flag_additive_model_fitted = false;
                TexteRegles.Flag_Modele_Exponentiel_deja_recherche = false;
                TexteRegles.Flag_Modele_Lineaire_deja_recherche = false;
                break;
            case 19:
            case 20:
                //dlgSp=new Cadre_SplitPlot(Global.scatterPlot[2]);
                dlgSp = new Cadre_SplitPlot(Global.lagPlot[0]);
                break;
			case 46:
				Modele.Flag_additive_model_fitted = false;
				Global.relationCPU_V=0;
				break;
            case 48:
            case 49:
            case 50:
                dlgSp = new Cadre_SplitPlot(Global.residualsPlot[1]);
                break;
            case 59:
            case 60:
                //dlgSp=new Cadre_SplitPlot(Global.scatterPlot[3]);
                dlgSp = new Cadre_SplitPlot(Global.lagPlot[1]);
                break;
			case 66:
				Global.relationCPU_E=0;
				break;
            case 68:
                dlgSp = new Cadre_SplitPlot(Global.residualsPlot[0]);
                break;
            case 95:
                dlgSp = new Cadre_SplitPlot(Global.variatePlot);
                break;
            case 96:
                dlgSp = new Cadre_SplitPlot(Global.fittedCpuePlot, true);
                break;
            case 97:
				dlgSp = new Cadre_SplitPlot(Global.jackknifePlot, false); // Complété ci-dessous par instructions extraites de CadreMain.java lignes 681 etc pour affichage onglets jackknife de menu principal Plots si écran trop petit.
                GraphicsDevice gd = Global.CadreMain.getGraphicsConfiguration().getDevice();
          		int xscr = gd.getDisplayMode().getWidth();  // Obtient la résolution en largeur de l'écran de l'ordi utilisé.
          		if (xscr < 1800) {
                	Global.JackknifePlot = true;
                	Cadre_Plot dlg;
                	dlg = new Cadre_Plot(Global.jackknifePlot);
                	dlg.setTitle("Climprod: Jackknife plots");
                	dlg.setVisible(true);
                	Global.CadreJackniffePlots = dlg;
                }      
                break;
            case 99:
                dlgSp = new Cadre_SplitPlot(Global.msyPlot);
                break;
        }
        if (dlgSp != null || Global.JackknifePlot == true) {  					  // Vérifie si existe ou non fenêtre graphique à droite de fenêtre Question.
            UtilCadre.leftResize(this, 35, 95);   // 35 -> 13  et 95 ->48 pour réduire largeur fenêtre Question lors de présentation graphiques. Premier chiffre affecte largeur fenetre, second ?? Test 2020
            UtilCadre.rightResize(dlgSp, 65, 95); // 65 -> 40  pour réduire largeur fenêtre Graphique lors de présentation graphiques. Premier chiffre affecte largeur fenetre, second ?? Test 2020
			//System.out.println("Définition dans ligne 554-5 de Cadre_Question.java des Facteurs de réduction (13, 48) de leftResize par rapport à max pour questions sur graphiques (sx et sy ligne 73 UtilCadre.java) 35 -> 13 et 95 ->48");// Test 2020
            dlgSp.setVisible(true);
            this.setVisible(true);
            dlgSp.toFront();
            this.toFront();
            // for keys to work even if dlgSp has the focus
            dlgSp.addKeyListener(new KeyListener() {

                @Override
                public void keyTyped(KeyEvent e) {
                    //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                    //System.out.println("PRESSED : "+e.getKeyChar());
                    if (e.getKeyChar() == 'n'){
                        cmdNext.doClick();
                    }
                    else{
                        jlstItem.setSelectedIndex(Integer.parseInt((""+e.getKeyChar())
                            .replace('a', '1')
                            .replace('z', '2')
                            .replace('e', '3')
                            .replace('r', '4')
                            .replace('t', '5')
                            .replace('y', '6')
                        )-1);
                    }
                }

                @Override
                public void keyPressed(KeyEvent e) {
                    //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public void keyReleased(KeyEvent e) {
                    //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }
            });

        } else {
            UtilCadre.Size(this, 60, 60);  // remplace 60, 60 par 30, 30 Test 2020          
            // System.out.println("Définition dans ligne 606 de Cadre_Question.java des Facteurs de réduction (30, 30) (JFrame) par rapport à max (sx et sy) ligne 73 UtilCadre.java. Test 2020");// Test 2020
            UtilCadre.Centrer(this);

        }
        //this.show();
        this.setVisible(true);
        //this.toBack();
        this.toFront();
    }

    void jlstItem_valueChanged(ListSelectionEvent e) {
        int item = jlstItem.getSelectedIndex();
        String itemTxt = jlstItem.getSelectedValue();
        String questTxt = jTextAreaQuestion.getText();
        if (item >= 0) {
            QuestionReponse.setReponse(item, itemTxt, questTxt, TexteRegles.getComment());
            jTextAreaMessage.setText(TexteRegles.getComment());
            jTextAreaMessage.setCaretPosition(0);
        }
    }

    void cmdStop_actionPerformed(ActionEvent e) {
        MsgDialogBox msg = new MsgDialogBox(1, "Do you want to stop the process?", 1, this.parent);
        if (msg.Ok()) {
            stopProcess();
        }
    }

    void cmdHelp_actionPerformed(ActionEvent e) {

        String file = QuestionReponse.getHelp();
        Cadre_Help dlg = new Cadre_Help(this, file);
        //dlg.show();
        dlg.setVisible(true);
    }

    void this_windowClosing(WindowEvent e) {
        stopProcess();
    }

    private void stopProcess() { //à modifier
        //Global.init();
        QuestionReponse.reset();
        if (this.dlgSp != null){
            this.dlgSp.setVisible(false);
        }
        this.dispose();
        Global.CadreQuestion = null;
        this.parent.toFront();
    }

}
