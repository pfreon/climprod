
/**
 * Titre : Climprod<p>

 */
package fr.ird.climprod;
import javax.swing.table.AbstractTableModel;

/**
 * Mod�le de table g�rant l'affichage dans jTabStat.
 */

class ModeleDeTableStat extends AbstractTableModel {

  public ModeleDeTableStat(Object[][]dataTable,String[] col) {
    columnNames = col;
    data = dataTable;

  }

  public int getColumnCount() {
   return columnNames.length;
  }

  public Object getValueAt(int row, int col) {
    return data[row][col];
  }

  public int getRowCount() {
    return data.length;
  }

  public boolean isCellEditable(int row, int col) {
           return false;
  }


public String getColumnName(int col) {
        return columnNames[col];
    }

public Class getColumnClass(int c) {
   return getValueAt(0, c).getClass();
}



final String[] columnNames ;
final Object[][] data ;

}