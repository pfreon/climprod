/**
 * Titre : Climprod<p>
 * Gestion des fenetres du menu principal Plots (Time Plots, Bivariate Plots, etc);
 */
package fr.ird.climprod;

import static fr.ird.climprod.UtilCadre.Centrer;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Arrays;
import java.util.Vector;

public class Cadre_Plot extends JFrame {

    private JTabbedPane jTabbedPanePlot = new JTabbedPane();
    private Vector<PanelPlot> vpanPlot = new Vector<PanelPlot>();
    private Plot[] plots;
    private JToolBar jToolBar1 = new JToolBar();
    private JButton cmdOption = new JButton();
    private JButton cmdSave = new JButton();
    private JButton cmdEnd = new JButton();

    public Cadre_Plot(Plot[] p) {

        try {
            plots = p;
            initWindow();
            UtilCadre.Size(this, 70, 70);
            // System.out.println(" Defintion des facteurs reduction fenetre par rapport a max, sx et sy ligne 29 Cadre_Plot.java = " + 70 + " " + 70 +" -> 30, 40"); // Test 2020

            UtilCadre.Centrer(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initWindow() throws Exception {
        this.setIconImage(Toolkit.getDefaultToolkit().createImage(Cadre_Plot.class.getResource("resources/images/Climprod.jpg")));
        this.addWindowListener(new java.awt.event.WindowAdapter() {

            public void windowClosing(WindowEvent e) {
                this_windowClosing(e);
            }
        });

        for (Plot plot : plots) {
            this.addTab(plot);
        }
        
        // keys for tab moving
        int condition = JComponent.WHEN_IN_FOCUSED_WINDOW;
        KeyStroke keyStroken = KeyStroke.getKeyStroke(KeyEvent.VK_PAGE_DOWN, KeyEvent.CTRL_DOWN_MASK);
        InputMap inputMapn = jTabbedPanePlot.getInputMap(condition);
        ActionMap actionMapn = jTabbedPanePlot.getActionMap();
        inputMapn.put(keyStroken, keyStroken.toString());
        actionMapn.put(keyStroken.toString(), new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                nextTab();
            }
        });
        KeyStroke keyStrokep = KeyStroke.getKeyStroke(KeyEvent.VK_PAGE_UP, KeyEvent.CTRL_DOWN_MASK);
        InputMap inputMapp = jTabbedPanePlot.getInputMap(condition);
        ActionMap actionMapp = jTabbedPanePlot.getActionMap();
        inputMapp.put(keyStrokep, keyStrokep.toString());
        actionMapp.put(keyStrokep.toString(), new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                previousTab();
            }
        });
        
        this.getContentPane().add(jTabbedPanePlot, BorderLayout.CENTER);

        /**
         * ************** Toolbar*********************************************
         */
        this.getContentPane().add(jToolBar1, BorderLayout.NORTH);
        jToolBar1.add(cmdOption);
        jToolBar1.add(cmdSave);
        jToolBar1.addSeparator();
        jToolBar1.add(cmdEnd);
        // jToolBar1.setMargin(new Insets(0,0,0,0));

        cmdOption.setToolTipText("Display chart preferences (P)");
        KeyStroke keyStrokea = KeyStroke.getKeyStroke(KeyEvent.VK_P, 0);
        InputMap inputMapa = cmdOption.getInputMap(condition);
        ActionMap actionMapa = cmdOption.getActionMap();
        inputMapa.put(keyStrokea, keyStrokea.toString());
        actionMapa.put(keyStrokea.toString(), new AbstractAction() {            
            @Override
            public void actionPerformed(ActionEvent arg0) {
                cmdOption.doClick();
            }
        });
        cmdSave.setToolTipText("Save image plot (S)");
        KeyStroke keyStrokes = KeyStroke.getKeyStroke(KeyEvent.VK_S, 0);
        InputMap inputMaps = cmdSave.getInputMap(condition);
        ActionMap actionMaps = cmdSave.getActionMap();
        inputMaps.put(keyStrokes, keyStrokes.toString());
        actionMaps.put(keyStrokes.toString(), new AbstractAction() {            
            @Override
            public void actionPerformed(ActionEvent arg0) {
                cmdSave.doClick();
            }
        });
        cmdEnd.setToolTipText("Close (ESC)");
        KeyStroke keyStroke2 = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
        InputMap inputMap2 = cmdEnd.getInputMap(condition);
        ActionMap actionMap2 = cmdEnd.getActionMap();
        inputMap2.put(keyStroke2, keyStroke2.toString());
        actionMap2.put(keyStroke2.toString(), new AbstractAction() {            
            @Override
            public void actionPerformed(ActionEvent arg0) {
                cmdEnd.doClick();
            }
        });
        cmdOption.setIcon(new ImageIcon(Cadre_SplitPlot.class.getResource("resources/images/BuildPlot.gif")));
        cmdSave.setIcon(new ImageIcon(Cadre_SplitPlot.class.getResource("resources/images/savePlot.gif")));
        cmdEnd.setIcon(new ImageIcon(Cadre_SplitPlot.class.getResource("resources/images/End.gif")));

        cmdOption.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cmdOption_actionPerformed(e);
            }
        });
        cmdSave.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cmdSave_actionPerformed(e);
            }
        });
        cmdEnd.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cmdEnd_actionPerformed(e);
            }
        });

        /*this.setJMenuBar(menuBar1);
         menuActivePlot.setText("Active plot");

         menuActivePlotOption.setText("Chart option");
         menuActivePlotOption.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(ActionEvent e) {
         menuActivePlotOption_actionPerformed(e);
         }
         });


         menuActivePlotSave.setText("Save image plot");
         menuActivePlotSave.addActionListener(new java.awt.event.ActionListener() {
         public void actionPerformed(ActionEvent e) {
         menuActivePlotSave_actionPerformed(e);
         }
         });

         menuBar1.add(menuActivePlot);
         menuActivePlot.add(menuActivePlotOption);
         menuActivePlot.addSeparator();
         menuActivePlot.add(menuActivePlotSave);*/
    }
    
    private void previousTab(){
        int current = jTabbedPanePlot.getSelectedIndex();
        int size = jTabbedPanePlot.getTabCount();
        int previous = (current - 1) % size;
        // previous line should work but modulus is different in java
        if (previous < 0) previous = size-1;
        jTabbedPanePlot.setSelectedIndex(previous);
    }
    
    private void nextTab(){
        int current = jTabbedPanePlot.getSelectedIndex();
        int size = jTabbedPanePlot.getTabCount();
        int next = (current + 1) % size;
        jTabbedPanePlot.setSelectedIndex(next);
    }

    private void addTab(Plot plot) {
        if (plot == null) {
            return;
        }
        PanelPlot pp = new PanelPlot();
        pp.setPlot(plot);
        jTabbedPanePlot.addTab(plot.getTitreGraphique(), pp);
        vpanPlot.add(pp);
    }

    void cmdOption_actionPerformed(ActionEvent e) {
        int i = jTabbedPanePlot.getSelectedIndex();
        PanelPlot pp = (PanelPlot) vpanPlot.get(i);
        if (i != -1) {
            if (pp.getPlot() != null) {
                PlotPreferences dlg = new PlotPreferences(this, pp.getPlot());
                plots[i] = dlg.getNewPreferences();
                pp.setPlot(plots[i]);
                pp.getPlot().Echelle(pp);
                pp.update();
            }
        }

    }

    void cmdSave_actionPerformed(ActionEvent e) {
        int i = jTabbedPanePlot.getSelectedIndex();
        PanelPlot pp = (PanelPlot) vpanPlot.get(i);
        pp.saveImage();
    }

    void cmdEnd_actionPerformed(ActionEvent e) {
        this.dispose();
    }

    void this_windowClosing(WindowEvent e) {
        this.dispose();
    }

    @Override
    public void dispose() {
        super.dispose();
        if (Arrays.equals(plots, Global.timePlot)) {
            Global.CadreTimePlots = null;
		} else if (Arrays.equals(plots, Global.distriPlot)) {
            Global.CadrePlotHistogr = null;            
        } else if (Arrays.equals(plots, Global.scatterPlot)) {
            Global.CadreBiVariatePlots = null;
        } else if (Arrays.equals(plots, Global.fittedCpuePlot)) {
            Global.CadreFittedPlots = null;
        } else if (Arrays.equals(plots, Global.jackknifePlot)) {
            Global.CadreJackniffePlots = null;
        } else if (Arrays.equals(plots, Global.msyPlot)) {
            Global.CadreMSPlots = null;
        } else if (Arrays.equals(plots, Global.residualsPlot)) {
            Global.CadreResidualPlots = null;
        } else if (Arrays.equals(plots, Global.variatePlot)) {
            Global.CadreThreeVariatePlots = null;
        }
    }
}
