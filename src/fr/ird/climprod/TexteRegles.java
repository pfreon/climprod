/**
 * Titre : Climprod<p>
 * Gestion des messages de la fenetre "Climprod: select the appropriate model and fit it". Commentaire 2020.
 * en relation avec regles.csv et comment.csv
 */
package fr.ird.climprod;
import java.io.InputStream;
import java.util.StringTokenizer;
import java.util.Hashtable;
import java.util.HashMap;
import java.text.DecimalFormat;
import java.awt.*;
import javax.swing.*;
public class TexteRegles {

    private static String [] script;
    private static String [][] item;
    private static HashMap<String, String> htNum=new HashMap<String, String>();
    private static HashMap<String, String> htComment=new HashMap<String, String>();
    private static boolean[] lethal;
    private static boolean[] posee;
    private static int numEnCours;
    private static int indexEnCours;
    public static boolean Flag_Modele_Lineaire_deja_recherche;
	public static boolean Flag_Modele_Exponentiel_deja_recherche;
	public static boolean Flag_Q_cpue_sous_sur_production_asked;
    private static int Flag_Modele_General_deja_recherche=0;
    private static String scriptEnCours;
    private static String[] itemEnCours;
    private static String commentaireEnCours="";

  //public static void initScript(String file,String fileComment)
  public static void initScript(InputStream file,InputStream fileComment) //Lecture des fichiers regles.csv puis Comment.csv.
  {
      try
      {
	int nbData=0;
        String[] dataLine;
		Modele.Flag_additive_model_fitted = false;
		Global.environmental_influence = "";
		Flag_Modele_Exponentiel_deja_recherche = false;
		Flag_Modele_Lineaire_deja_recherche = false;
		Modele.Flag_additive_model_fitted = false;
        ReadFileText rft=new ReadFileText(file); // Fichier regles.csv
        dataLine=rft.getLines();
        nbData=dataLine.length;
        script=new String[nbData];
        item=new String[nbData][];
        lethal=new boolean[nbData];
        posee=new boolean[nbData];

        initRegles();
        for (int i=0;i<dataLine.length;i++)
        {
             StringTokenizer d=new StringTokenizer (dataLine[i],";");
             htNum.put(d.nextToken().trim(),Integer.toString(i));
             if(d.nextToken().trim().equals("0"))
                lethal[i]=false;
             else
                lethal[i]=true;
             script[i]=d.nextToken().trim();
             int k=d.countTokens();
             if (k!=0)
             {
                item[i]=new String[k];
                for (int j=0;j<k;j++)
                      item[i][j]=d.nextToken().trim();  // Texte des conditions des règles (sans l'énoncé ".. if:").
             }

			/*System.out.println(script[i]);
             for(int l=0;l<k;l++)
               System.out.print(item[i][l]+"  ");
             System.out.println("******************");*/
        }
        rft=new ReadFileText(fileComment); // Fichier Comment.cs
        dataLine=rft.getLines();
        for (int i=0;i<dataLine.length;i++)
        {
             String c$="";
             String n$;
             StringTokenizer d=new StringTokenizer (dataLine[i],";");
             n$=d.nextToken().trim();
             while(d.hasMoreTokens())
                  c$=c$+d.nextToken().trim()+"\n";
             htComment.put(n$,c$);
             //htComment.put(d.nextToken().trim(),d.nextToken());
        }
      }
      catch(Exception e)
      {
         MsgDialogBox msg=new MsgDialogBox(0,e.getMessage(),0, Global.CadreMain);
      }
}

public static void loadRegles(int number)
{
    numEnCours=number; // N° de règle.
    System.out.println("on charge la règle (dans TexteRegles, ligne 96) " +  numEnCours );
    commentaireEnCours="";
	if (numEnCours ==-1) numEnCours = 2; // No rule to apply -> numEnCours = 2 (free number after elimination of rule 2)  
    {
         indexEnCours=Integer.parseInt((String)htNum.get(Integer.toString(numEnCours)));  
         // indexEnCours = Indice (n° de ligne de 0 à n) de la règle dans la table regles.csv. 
         // Attention different de indexEnCours de QuestionReponse.java.
         // La fonction parseInt() analyse une chaîne de caractère fournie en argument et renvoie un entier exprimé dans une base donnée.
         // Integer.toString returns a string representation of the first argument i.
		 // System.out.println("indexEnCours dans TexteRegles.java = " + indexEnCours + " numEnCours = " + numEnCours);
         scriptEnCours=script[indexEnCours]; // Ennoncé de la condition de la règle (se termine par "if:"; 3ème champ de regles.csv).
		 //System.out.println("scriptEnCours = " + scriptEnCours);
         itemEnCours=new String[item[indexEnCours].length]; // Number of conditions in the rule. 
		 //System.out.println("item[indexEnCours].length = " + item[indexEnCours].length);
         for (int i=0;i<itemEnCours.length;i++)        // itemEnCours.length = number of non-empty text items in the file regles.csv
					{
	                itemEnCours[i]=item[indexEnCours][i]; // itemEnCours[i] = non-empty text items in the file regles.csv
				    // System.out.println("i = " + i + " itemEnCours[i] = " + itemEnCours[i]);
				    }
    }
}

public static String getScript(){
    //if(posee[numEnCours]==true) return "";
    posee[indexEnCours]=true;
    return scriptEnCours;
}

public static String[] getItem(){

    return itemEnCours;
}

public static void initRegles()
{
      for(int i=0;i<posee.length;i++)
            posee[i]=false;

}

/*
    Teste la règle
    @return boolean true si règle vraie.
    IMPOSSIBLE DE PRESENTER MESSAGE DE WARNING (lu dans Comments.csv) DANS FENETRE QUESTION SI:
       1) Pas de règle associée (ex: décision n°5;
       2) result (boléen) = true;
       3) si décision aboutit sur -1 (on a alors un message d'arrêt de la procedure de sélection STOP).
    Seule solution trouvée (uniquement lorsque result != true): rajouter un if et donner ici le commentaire.
*/
public static boolean isTrue(){
          boolean result=false;
          commentaireEnCours="";
          DecimalFormat nf= new DecimalFormat(" 0.00");
          switch(numEnCours)
          {
          	  case 30: // Averstissement usage double click. Rajout 2020.
					if (Global.double_click.compareTo("ShowWarningDoubleClick") != 0) Global.dbl_click = 2; 
					else Global.dbl_click = 1;
					//System.out.println(" TexteRegles case 30: Global.double_click = " + Global.double_click + "; Global.dbl_click = " + Global.dbl_click);	
					result =  (Global.dbl_click == 1); 
                    break;
              case 41:
                    result =  Data.getNbYears()<12;
                    break;
              case 47:
					Global.temp=Global.RangeEffort();
                    result =  Global.temp<1.0d;  // Avant <0.4d. Modif. 2020.
                    break;
              case 42:
                    result =  (Global.changement_exploitation!=2);
                    if(Global.changement_exploitation==3)
                        commentaireEnCours="It is compulsory to know the answer to this question. \nPlease have a look at the help file using the 'Help (H)' button \nif necessary";
                    break;
              case 43:
                    result =  (Global.unite_standardisee!=1);
                    if(Global.unite_standardisee==3)
                        commentaireEnCours="It is compulsory to know the answer to this question. \nPlease have a look at the help file using the 'Help (H)' button \nif necessary";
                    break;
              case 44:
                    result =  (Global.effet_delais_abundance_negligeable!=1);
                    if(Global.effet_delais_abundance_negligeable==3)
                        commentaireEnCours="It is compulsory to know the answer to this question. \nPlease have a look at the help file using the 'Help (H)' button \nif necessary";
                    break;
              case 46:
                    result =  (Global.stock_unique!=1 && Global.metapopulation!=1); // Changement de nom variable. Modif. 2020
                    if(!result){
                    commentaireEnCours="Metapopulations are special cases that difficult the application of any kind of population dynamics model.\nAccording to the level connectivity between sub-stocks, modelization might be borderline or not recommended as you will see after answering the next question.";
                    }
                    break;
              case 45:
                    result =  (Global.stock_unique!=1 && Global.metapopulation==1 && Global.sous_stock_isole!=1); // Global.metapopulation==1
                    if(!result){
                    commentaireEnCours="Your case is a borderline one either because you deal only with a sub-stock or with a full metapopulation (and the lower is the connectivity between sub-stocks, the more borderline you are).\nYou must not extrapolate your results beyond the interval of observation of the different variables (effort and/or environment) when using the model for predictions.\nMoreover, any surplus production model using effort will implicitly make the assumption that no variation in recruitment due to other sub-stocks will occur.";
                    }
                    break;
              case 35:
                    result = (Global.under_and_over_exploited!=1);// Avant Global.under_and_over_exploited!=1  && Global.under_and_optimaly==1). Modif 2020.
                    break;
              case 49:
                    result = (Global.under_and_optimaly==2); // Avant (Global.under_and_over_exploited!=1 && Global.under_and_optimaly!=1). Modif 2020.
					if(Global.under_and_optimaly==1){
                    commentaireEnCours="You can still go on but your case is a borderline one. Model results will be uncertain \n(particularly maximum sustainable production that could be underestimated) owing to \nthe lack of exploitation above MSY/MSE levels.";
                    }
                   break;
              case 37:
                    result =  (Global.statistiques_anormales==1);
                    break;
              case 36:
                    result =  (Global.unstability==1);
                    break;
              case 38:
                    result = (Global.abnormal_points_dist==1);
                    break;
              case 39:
                    result = (Global.abnormal_points_scatt==1);
                    break;
              case 50:
                    result = (Global.effort_increasing==1);
                    break;
              case 40:
                    result = (Global.independance==1);           
                    break;
              case 48:
                    result = (Global.envir_preponderant==1 && Global.decreasing_relationship==2); 
                    break;
              case 10:
                    //System.out.println(Global.relationCPU_E+" "+Global.pessimiste+" "+Global.stock_deja_effondre+" "+Global.lifespan+" "+Global.fecondite_faible);
                    result = (Global.relationCPU_E==RechercheModele.exponentiel && Global.pessimiste!=1 && Global.stock_deja_effondre!=1 && Global.lifespan>10 && Global.fecondite_faible!=1);
					if (Global.envir_preponderant == 1) Global.environmental_influence = "";
		    		if(result) Flag_Modele_Exponentiel_deja_recherche=true;
                    break;
              case 11:
                    result = (Global.relationCPU_E==RechercheModele.exponentiel && Global.pessimiste!=1 && Global.stock_deja_effondre!=1 && Global.lifespan>4 && Global.lifespan<11 && Global.fecondite_faible!=1 && Global.rapport_vie_exploitee_inferieur_deux==2);
					//System.out.println("Global.relationCPU_E = " + Global.relationCPU_E + " Global.pessimiste = " + Global.pessimiste + " Global.stock_deja_effondre = " + Global.stock_deja_effondre + " Global.lifespan = " + Global.lifespan + " Global.fecondite_faible = " + Global.fecondite_faible + " Global.rapport_vie_exploitee_inferieur_deux = " + Global.rapport_vie_exploitee_inferieur_deux);
		    		if(result) Flag_Modele_Exponentiel_deja_recherche=true;
                    break;
              case 12:
                    result = (Global.relationCPU_E==RechercheModele.exponentiel && Global.pessimiste!=1 && Global.stock_deja_effondre!=1 && Global.lifespan>4 && Global.lifespan<11 && Global.fecondite_faible!=1 && Global.premiere_reproduction_avant_recrutement==1);
		    		if(result) Flag_Modele_Exponentiel_deja_recherche=true;
                    break;
              case 13:
                    result = (Global.relationCPU_E==RechercheModele.exponentiel && Global.pessimiste!=1 && Global.stock_deja_effondre!=1 && Global.lifespan>4 && Global.lifespan<11 && Global.fecondite_faible!=1 && Global.reserves_naturelles==1);
		    		if(result) Flag_Modele_Exponentiel_deja_recherche=true;
                    break;
              case 14:
                    result = (Global.relationCPU_E==RechercheModele.exponentiel && Global.pessimiste!=1 && Global.stock_deja_effondre!=1 && Global.lifespan>4 && Global.lifespan<11 && Global.fecondite_faible!=1 && Global.metapopulation==1); // metapopulation remplace ici stock_subdivise car redondant. Modif 2020.
		    		if(result) Flag_Modele_Exponentiel_deja_recherche=true;
                    break;
              case 16:
                    result = (Global.relationCPU_E==RechercheModele.exponentiel && Global.pessimiste!=1 && Global.stock_deja_effondre!=1 && Global.rapport_vie_exploitee_inferieur_deux==2 && Global.premiere_reproduction_avant_recrutement==1 && Global.fecondite_faible!=1 && Global.reserves_naturelles==1 && Global.metapopulation==1); // metapopulation remplace ici stock_subdivise car redondant. Modif 2020.
		    		if(result) Flag_Modele_Exponentiel_deja_recherche=true;
                    break;
              case 17:
                    result = (Global.relationCPU_E==RechercheModele.lineaire);
					//System.out.println("Global.environmental_influence" + Global.environmental_influence + " Global.decreasing_relationship = " + Global.decreasing_relationship);
					if (Global.environmental_influence == "")  // To avoid repetition of comment when V > E in influence of on CPUE)
						commentaireEnCours="From this point onward you can keep on using the 'Previous (P)' button but only for checking you previous answers.\nIt is not recommended to modify them because it may lead to uncertain results due to reinitialization issues";
                    if(result) { 
						Flag_Modele_Lineaire_deja_recherche=true;
					}
                    break;
              case 27:
                    result = (Global.environmental_influence == ""); // Please note that this result is voluntary independent from the asked question. This rule was added to allow a new comment (2021).
					//System.out.println("Global.environmental_influence = " + Global.environmental_influence + " Global.decreasing_relationship = " + Global.decreasing_relationship);
                    if(result) { 
						commentaireEnCours="From this point onward you can keep on using the 'Previous (P)' button but only for checking you previous answers.\nIt is not recommended to modify them because it may lead to uncertain results due to reinitialization issues";
					}
                    break;                    
              case 18:
                    result = (Global.pessimiste==1);
                    if(result)
                    {
                         Flag_Modele_Lineaire_deja_recherche=true;
			 			 Global.relationCPU_E=RechercheModele.lineaire;
                    }
                    else
                    {
		    		if(result) Flag_Modele_Exponentiel_deja_recherche=true;
                    Global.relationCPU_E=RechercheModele.exponentiel;
                    }
                    break;
              case 19:
                    result = (Global.stock_deja_effondre==1);
                    if(result)
                    {
                         Flag_Modele_Lineaire_deja_recherche=true;
			 			 Global.relationCPU_E=RechercheModele.lineaire;
                    }
                    else
		    		{
		    		if(result) Flag_Modele_Exponentiel_deja_recherche=true;
                    	Global.relationCPU_E=RechercheModele.exponentiel;
                    }
                    break;
              case 20:
                    result = (Global.lifespan<5 && Global.rapport_vie_exploitee_inferieur_deux==1 && Global.premiere_reproduction_avant_recrutement==2);
                    if(result)
                    {
                         Flag_Modele_Lineaire_deja_recherche=true;
			 			 Global.relationCPU_E=RechercheModele.lineaire;
                    }
                    else
                    {
		    		if(result) Flag_Modele_Exponentiel_deja_recherche=true;
                    	Global.relationCPU_E=RechercheModele.exponentiel;
                    }
                    break;
              case 21:
                    result = (Global.fecondite_faible==1);
                    if(result)
                    {
                         Flag_Modele_Lineaire_deja_recherche=true;
			 			 Global.relationCPU_E=RechercheModele.lineaire;
                    }
                    else
                    {
		    		if(result) Flag_Modele_Exponentiel_deja_recherche=true;
                    	 Global.relationCPU_E=RechercheModele.exponentiel;
                    }
                    break;
              case 22:
                    result = (Global.lifespan<5 && Global.rapport_vie_exploitee_inferieur_deux==1 && Global.reserves_naturelles==2 && Global.metapopulation==2 && Global.cpue_unstable==2); // metapopulation remplace ici stock_subdivise car redondant. Modif 2020.
                    if(result)
		   			{
                         Flag_Modele_Lineaire_deja_recherche=true;
			 				Global.relationCPU_E=RechercheModele.lineaire;
                    }
                       
                    else
                    {
		    		if(result) Flag_Modele_Exponentiel_deja_recherche=true;
                    	Global.relationCPU_E=RechercheModele.exponentiel;
                    }
                    break;
              case 23:
                    Global.relationCPU_E=RechercheModele.general; 
		    		Flag_Modele_General_deja_recherche=1;
                    result=true;
                    break;
              case 24:
                    result=(Global.relationCPU_E==RechercheModele.lineaire && Global.coeff_determination<0.4);
                    if(result)
                    {
			 		Global.relationCPU_E=RechercheModele.general;
			 		Flag_Modele_General_deja_recherche=1;
					commentaireEnCours="The coefficient of determination of the model " + RechercheModele.getEquation()+ " is " + nf.format(Global.coeff_determination)+"\n";
                         commentaireEnCours=commentaireEnCours+"A generalized model is going to be fitted";
                    }
                    break;
              case 25:
                    result=(Global.relationCPU_E==RechercheModele.exponentiel && Global.coeff_determination<0.4);
                    if(result) 
		    		{
		            Global.relationCPU_E=RechercheModele.general;
			  		//Flag_Modele_General_deja_recherche=1;
			  			  commentaireEnCours="The coefficient of determination of the model " + RechercheModele.getEquation()+ " is " + nf.format(Global.coeff_determination)+"\n";
                          commentaireEnCours=commentaireEnCours+"A generalized model is going to be fitted";
                    }
                    break;
              case 26:
                    result = (Global.linear_relationship==1);
                    break;
              // case 27: Unused rule since CLIMPROD v0
              case 28:
                    result = (Global.linear_relationship==2 && Global.monotonic_relationship==1);
                    break;
              case 29:
                    result = (Global.linear_relationship==2 && Global.monotonic_relationship==2);
                    break;
              case 51:
                    //Global.val_param[2]=2; 
					//System.out.println("Flag_Modele_Lineaire_deja_recherche = " + Flag_Modele_Lineaire_deja_recherche +" Global.relationCPU_E = " + Global.relationCPU_E);
		    		result = (Flag_Modele_Lineaire_deja_recherche=false && Global.relationCPU_E==RechercheModele.general && Global.coeff_determination>0.4 && Global.val_param[2]<2.5d && Global.val_param[2]>1.6d);
                    if (result)
                    {
                        Global.relationCPU_E=RechercheModele.lineaire;
                        commentaireEnCours="Coefficient c (m in the Pella and Tomlinson notation) value = " + nf.format(Global.val_param[2]) + ", which suggest the use of a linear relationship (Schaefer model) between CPUE (or CPUE residual) and E instead of the generalized (Pella & Tomlinson mondel) which requires more parameters.";
                    }
					break;
              case 52:
                    result = (Flag_Modele_Exponentiel_deja_recherche!=true && Global.relationCPU_E==RechercheModele.general && Global.coeff_determination>0.4 && Global.pessimiste!=1 && Global.val_param[2]<1.4d && Global.val_param[2]>0.6d);
                    if (result)
                    {
					//System.out.println("Règle 52 Global.relationCPU_E=RechercheModele.exponentiel");
						Flag_Modele_Exponentiel_deja_recherche=true;
                        Global.relationCPU_E=RechercheModele.exponentiel;
                        commentaireEnCours="Coefficient c (m in the Pella and Tomlinson notation) value = " + nf.format(Global.val_param[2]) + ", which suggest the use of an exponential relationship (Fox model) between CPUE (or CPUE residual) and E instead of the generalized (Pella & Tomlinson mondel) which requires more parameters.";
                    }
					break;
              case 53:
                    // Pour cas modèle simple influence Environnement (V) > Effort (E)
                    result=(Global.coeff_determination<0.4);
					if (Global.envir_preponderant == 1) Global.environmental_influence = "";
                    if(result)
                    {
                      commentaireEnCours="The coefficient of determination of the model " + RechercheModele.getEquation()+ " is " +nf.format(Global.coeff_determination)+".\n";
                      commentaireEnCours=commentaireEnCours+ "From the current rule this simple empirical model CPUE = f(V) is not convenient (R² < 0.40).";
                      commentaireEnCours=commentaireEnCours+"\nPlease make sure that your answer to the question 'Is the influence of fishing \neffort on CPUE more important than the environmental influence?' \nwas the correct one.";
                     }
                    break;
              case 54:
                    // Pour cas modèle simple influence Effort (E) > Environnement (V)
                    result=(Global.coeff_determination<0.4);
                    if(result)
                    {
                      commentaireEnCours="The coefficient of determination of the model " + RechercheModele.getEquation()+ " is " +nf.format(Global.coeff_determination)+".\n";
                      commentaireEnCours=commentaireEnCours+ "From the current rule this simple empirical model CPUE = f(E) is not convenient (R2 < 0.40).";
                 	  commentaireEnCours=commentaireEnCours+"\nPlease make sure that your answer to the question 'Is the influence of fishing \neffort on CPUE more important than the environmental influence?' \nwas the correct one.";
                     }
                    //result=(Global.typeModele ==  RechercheModele.mixte)  ;
                    break;
              case 55:
                    result=(Global.acceptable_graphs!=1);
                    if(result)
                       commentaireEnCours="From the current rule and according to your last answer, the model " + RechercheModele.getEquation()+ " is not validated.";
					else {
					  double[] Catches=Data.getCatches();
                      double[] ExtremaCatches=Stat1.Extremas(Catches);
					  double[] f=Data.getF();
                      double[] Extremaf=Stat1.Extremas(f);
                      if(Global.under_and_over_exploited == 1 && Validation.y_ms_m2[0] > ExtremaCatches[1] && Validation.y_ms_m2[1] > ExtremaCatches[1] && Validation.y_ms_m2[2] > ExtremaCatches[1] && Validation.y_ms_m2[3] > ExtremaCatches[1])
                         commentaireEnCours="There is a contradiction between: \n1) your assumption that the data-set covers periods of both overexploitation and underexploitation and\n2) the fact that the maximum observed catch does not overpass any of the MSY central values for noteworthy V values.\nPlease consider revising eithier your last answer or your previous answer(s)\nregarding exploitation levels.\n\n";
                      else
                         if(Global.under_and_optimaly == 1 && Validation.y_ms_m2[0] > ExtremaCatches[1] && Validation.y_ms_m2[1] > ExtremaCatches[1] && Validation.y_ms_m2[2] > ExtremaCatches[1] && Validation.y_ms_m2[3] > ExtremaCatches[1]) 
                         	commentaireEnCours="There is a contradiction between: \n1) your assumption that the data-set covers periods of both underexploitation and optimal exploitation\nand 2) the fact that the maximum observed catch does not overpass any of the MSY central values for noteworthy V values.\nPlease consider revising eithier your last answer or your previous answer(s)\nregarding exploitation levels.\n\n";
                      if(Global.under_and_over_exploited == 1 && Validation.f_ms_m2[0] > Extremaf[1] && Validation.f_ms_m2[1] > Extremaf[1] && Validation.f_ms_m2[2] > Extremaf[1] && Validation.f_ms_m2[3] > Extremaf[1])
                         commentaireEnCours=commentaireEnCours+"There is a contradiction between: \n1) your assumption that the data-set covers periods of both overexploitation and underexploitation and\n2) the fact that the maximum observed weighted effort does not overpass any of the MSE central values for noteworthy V values.\nPlease consider revising eithier your last answer or your previous answer(s)\nregarding exploitation levels.\n\n";
                      else if(Global.under_and_optimaly == 1 && Validation.f_ms_m2[0] > Extremaf[1] && Validation.f_ms_m2[1] > Extremaf[1] && Validation.f_ms_m2[2] > Extremaf[1] && Validation.f_ms_m2[3] > Extremaf[1]) 
                         	commentaireEnCours=commentaireEnCours+"There is a contradiction between: \n1) your assumption that the data-set covers periods of both underexploitation and optimal exploitation and\n2) the fact that the maximum observed weighted effort does not overpass any of the MSE central values for noteworthy V values.\nPlease consider revising eithier your last answer or your previous answer(s)\nregarding exploitation levels.\n\n";
					  if (Global.message$[6] != "") // At least one of the central values of MSE is larger than 1.0E10 for noteworthy V values. 
			              commentaireEnCours=commentaireEnCours + Global.message$[6] + "Please consider revising your answer.\n\n";
			          else if (Global.message$[10] != "") // At least one of the central values of MSE is negative for noteworthy V values.
			              commentaireEnCours=commentaireEnCours + Global.message$[10] + "Please consider revising your answer.\n\n";
					  else if (Global.message$[3] != "") // All noteworthy values of MSY and/or MSE upper limits at 95% are null or negative. 
						  commentaireEnCours=commentaireEnCours + Global.message$[3] + "Please consider revising your answer.\n\n";
					  else	// ici on ne rajoute pas Global.message$[5] != "" car déjà dans règle 0 \\ At least one set of predicted values of CPUE=f(E,V) and Y=f(E,V) corresponding to Vmin or Vmax presents only negative values. 
				      {
					  	if (Global.message$[4] != "") // All noteworthy values of MSY and MSE lower limits at 95% are null or negative. 
							   commentaireEnCours=commentaireEnCours + Global.message$[4] + "Please consider revising your answer.\n\n";
						   else {
								if (Global.message$[1] != "") // All noteworthy values of MSE lower limit at 95% are null or negative. 					
									commentaireEnCours=commentaireEnCours + Global.message$[1] + "Please consider revising your answer.\n\n";
					  			if (Global.message$[7] != "") // Please note that some MSY central values for at least one of the noteworthy V values are unexpectidly negative.
						 			commentaireEnCours=commentaireEnCours + Global.message$[7] + "Please consider revising your answer.\n\n";
            					if (Global.message$[2] != "") // All noteworthy values of MSY lower limit at 95% are null or negative.
									commentaireEnCours=commentaireEnCours + Global.message$[2] + "Please consider revising your answer.\n\n";
		                      	if (Global.message$[8] != "") //Please note that the width of the 95% confidence interval of MSY central values for at least one of the noteworthy V values is larger than the correspondingMSY value.
		                         	commentaireEnCours=commentaireEnCours + Global.message$[8] + "Please consider revising your answer.\n\n";
		                      	if (Global.message$[9] != "") // Please note that the width of the 95% confidence interval of MSE central values for at least one of the noteworthy V values is larger than the corresponding MSE value.					  
		                      	 	commentaireEnCours=commentaireEnCours + Global.message$[9] + "Please consider revising your answer.\n\n";				  
								if (Global.message$[11] != "")// Please note that the MSY central value for the mean V value is 10 times larger than the maximum observed catch value.
									commentaireEnCours=commentaireEnCours + Global.message$[11] + "Please consider revising your answer.\n\n";
								if (Global.message$[12] != "")// Please note that the MSE central value for the mean V value is 5 times larger than the maximum observed fishing effort value.
									commentaireEnCours=commentaireEnCours + Global.message$[12] + "Please consider revising your answer.\n\n";
															}            
    				  }						   
    				}
                    break;   
              case 56:
                    result=(Global.environmental_influence.equals("abundance") && Flag_Modele_Exponentiel_deja_recherche == true && Global.relationCPU_V == 1 && Modele.Flag_additive_model_fitted == false);
					if(result) { Flag_Q_cpue_sous_sur_production_asked = true;
					//System.out.println("Flag_Modele_Exponentiel_deja_recherche: " + Flag_Modele_Exponentiel_deja_recherche + " Modele.Flag_additive_model_fitted = " + Modele.Flag_additive_model_fitted + " Global.relationCPU_V: " + Global.relationCPU_V);
					}
                    break; 
              case 57: 
              		result=(Global.linear_relationship!=1);
					if(result)
                    {
                      commentaireEnCours="There is no available model CPUE = f(E,V) fully appropriate to your case\n";
                      commentaireEnCours=commentaireEnCours+"because you declared that the stock may present large fluctuations in abundance\n";
                      commentaireEnCours=commentaireEnCours+"when overexploited and that there is only one model available in CLIMPROD to\n";
                      commentaireEnCours=commentaireEnCours+"covers this case because other models would be overparameterized.\n";
                      commentaireEnCours=commentaireEnCours+"Since the available model is additive and of exponential form for CPUE = f(E)\n";
                      commentaireEnCours=commentaireEnCours+"and of linear form for CPUE = f(V) it was not selected because\n"; 
                      commentaireEnCours=commentaireEnCours+"from your answers to the questions related to the shape of the\n";
                      commentaireEnCours=commentaireEnCours+"graphical relationship between CPUE residuals and environmental values\n";
                      commentaireEnCours=commentaireEnCours+"CLIMPROD deduced that the relationship CPUE = f(V) is not linear.\n\n"; 
                      commentaireEnCours=commentaireEnCours+"Please make sure that your answer to the question: 'May the stock present\n"; 
                      commentaireEnCours=commentaireEnCours+"large fluctuations in CPUE when overexploited?' was the correct one.\n";
					  commentaireEnCours=commentaireEnCours+"If so, you can try to fit a model CPUE = f(E,V) where the the effect of\n";
					  commentaireEnCours=commentaireEnCours+"fishing effort (E) is linear which would provide a pessimistic response\n";
					  commentaireEnCours=commentaireEnCours+"of the stock to overexploitation.\n";
                     }              
                    break; 
                case 58:
					result=(((Global.numero_modele > 1) && (Global.numero_modele < 6)) || (Global.numero_modele == 20 || (Global.numero_modele == 33))); // Exclusion modèles CPUE=f(V) et modèle exponentiel additif
					if(result)
                    {
                      commentaireEnCours="MSY and MSE graphs are not justified or available for this model.\nPlease continue.\n";
                    } 
                    break;  
                case 59:
                    result=(Global.environmental_influence.equals("catchability"));
                    break; 
				case 60: // Idem case 56 + Flag
    				result=(Flag_Q_cpue_sous_sur_production_asked == false && Global.environmental_influence.equals("abundance") && Flag_Modele_Exponentiel_deja_recherche == true && Global.relationCPU_V == 1 && Modele.Flag_additive_model_fitted == false);
                    break; 
              /*case 56:
                  result = (Global.relationCPU_E==RechercheModele.general && Global.coeff_determination>0.4);
                  Flag_Modele_General_deja_recherche=1;
                    //    commentaireEnCours="Oui le modèle est généralisé";
                    break;
              case 57:
                  result = true;
                  if (Global.CadreJackniffePlots == null) 
                     {
                      Cadre_Plot dlg;
                      dlg = new Cadre_Plot(Global.jackknifePlot);
                      dlg.setTitle("Climprod: Jackknife plots");
                      dlg.setVisible(true);
                      Global.CadreJackniffePlots = dlg;
                     } 
                  else {Global.CadreJackniffePlots.setVisible(true);}
                  break;

              case 58:
                  result = false;
                  break;
				*/                  
              case 4:
                    result = (Global.envir_preponderant==1 && Global.cpue_sous_sur_production!=1 && Global.coeff_determination>0.7);
                    if(result)
                        commentaireEnCours="From the current rule the model " 
                        + RechercheModele.getEquation()+ " is convenient.";
                    break;
              case 5:
                    result = (Global.envir_preponderant==1 && Global.environmental_influence.equals("abundance") && Global.cpue_sous_sur_production==1 && Global.relationCPU_E==6 && Global.coeff_determination>0.7);
                    if(result)
                    {
                        commentaireEnCours="From the current rule the model " 
                        + RechercheModele.getEquation()+ " is convenient.";
                    }
                    else
                    {
		    		commentaireEnCours = "The model is not considered as convenient because R2 is \nlower than the threshold of 0.70";
		    		if (Global.coeff_determination < 0.60 && Data.getNbYears() > 20)
		    			commentaireEnCours = commentaireEnCours + ". (R2 = " + nf.format(Global.coeff_determination) 
		    			+ "\nyou can still build a history & html directory by using"
                    	+ "\nthe corresponding sub-menu in the 'Files' menu.";
                    else commentaireEnCours = commentaireEnCours + ". \nBut please note that this threshold is empirical and that your case"
                    + "\nmight be borderline because R2 = " + nf.format(Global.coeff_determination) + " and because the length of your" 
                    + "\ndataset allows enough degrees of freedom. "
                    + "\nYou may wish to push forward the validation process by using the sub-menu "
                    + "\n'Fit a model directly' in the menu 'Modelisation' and have a look at additional "
                    + "\nstatistical results and graphs in order to make your own decision. But please "
                    + "\nremember that R2 is always overestimated due to the colinearity between CPUE "
                    + "\nand fishing effort."
                    + "\nIn any case you can still build a history & html directory by using "
                    + "\nthe corresponding sub-menu in the 'Files' menu.";
					}
                    break;
              case 6:
                    result = (Global.envir_preponderant==1  && Global.coeff_determination>0.9);
                    if(result)
                        commentaireEnCours="From the current rule the simple model " + RechercheModele.getEquation()
                        + " is convenient \nwith no need of looking for a mixed model because R2 is larger\n"
                        + "than 0.90.\n"
                        + "Nonetheless, if there is a strong colinearity between E and V you can\n"
                        + "intent fitting directely a mixed model but be aware that the results\n"
                        + "will be uncertain.\n"
                        + "In any case please note that any entry of V value in the predictition\n"
                        + "table will not modify the predicted CPUE because CPUE is considered as\n"
                        + "independent from V.";
                    break;
              case 7:
                    result = (Global.envir_preponderant!=1 && Global.cpue_sous_sur_production!=1 && Global.coeff_determination>0.7);
                    if(result)
                        commentaireEnCours="From the current rule the model " + RechercheModele.getEquation()+ " is convenient.";
                    break;
              case 8:
                    result = (Global.envir_preponderant!=1 && Global.environmental_influence.equals("abundance") && Global.cpue_sous_sur_production==1 && Global.relationCPU_E==6 && Global.coeff_determination>0.7);
                    if(result)
                        commentaireEnCours="From the current rule the model " + RechercheModele.getEquation()+ " is convenient.";
                    break;
              case 9:
                    result = (Global.envir_preponderant!=1  && Global.coeff_determination>0.9);
                    if(result)
                        commentaireEnCours="From the current rule the simple model " + RechercheModele.getEquation()
                        + " is convenient \nwith no need of looking for a mixed model because R2 is larger\n"
                        + "than 0.90.\n"
                        + "Nonetheless, if there is a strong colinearity between E and V you can \n"
                        + "intent fitting directely a mixed model but be aware that the results \n"
                        + "will be uncertain.\n"
                        + "In any case please note that any entry of E value in the predictition\n"
                        + "table will not modify the predicted CPUE because CPUE is considered as\n"
                        + "independent from E (which is likely to be unrealistic when the exploitation\n"
                        + "level of the stockis becomes significant).";
                    break;
              case 1:      // Rajout 2020.   		
                    result=(Global.coeff_determination_instable!=1);
          			GraphicsDevice gd = Global.CadreMain.getGraphicsConfiguration().getDevice();
          			int xscr = gd.getDisplayMode().getWidth();  // Obtient la résolution en largeur de l'écran de l'ordi utilisé.
					if (xscr < 1800) {
                    	commentaireEnCours="If the size of your screen is too small to display \ncorrectly the graphs and their full title, please";	
                    	commentaireEnCours=commentaireEnCours+"\nclose the graph window and you will access to a \nmulti-tab window showing separately all graphs.\n\n";
                 	}  
                    if(result)
                      commentaireEnCours=commentaireEnCours+"From the current rule the model and according to your last answer, " + RechercheModele.getEquation()+ " is not validated.";
                    else {
                      if (Global.fFsignif$ ==  " p>0.05") {
                          commentaireEnCours=commentaireEnCours+"Warning: You validated the model although the p-value\n";
                          commentaireEnCours=commentaireEnCours+"of the Fisher test on R² value is larger than 0.05,\n";
                          commentaireEnCours=commentaireEnCours+"which is not recommended.\n\n";
                          }
                      if (Global.test_jackknife == false) {
                          commentaireEnCours=commentaireEnCours+"Warning: You validated the model although one of the p values\n";
                          commentaireEnCours=commentaireEnCours+"of t-ratios of the parameters was higher than 0.05.\n";
                          commentaireEnCours=commentaireEnCours+"This is not recommended except if it concerns\n";
                          commentaireEnCours=commentaireEnCours+"only the intercept of the equation (usually 'a').\n\n";
                          }
                      if (Global.r_jk < 0.6) {
                          commentaireEnCours=commentaireEnCours+"Warning: You validated the model although the jackknife R² value is\n";
                          commentaireEnCours=commentaireEnCours+"lower than 0.60, which is not recommended.\n\n";
                          }
                      if (Global.corrected_R2 < 0.65) {
                          commentaireEnCours=commentaireEnCours+"Warning: You validated the model although the correcte R² value is\n";
                          commentaireEnCours=commentaireEnCours+"lower than 0.65, which is not recommended.\n\n";
                          }
					  if (Global.message$[0] != "") commentaireEnCours=commentaireEnCours+"Warning: "+Global.message$[0]+"\n\n";
                      }
                    break;
              case 0:
                    result=(Global.good_results!=1);
                    if (Global.message$[5] != "") // At least one set of predicted values of CPUE=f(E,V) and Y=f(E,V) corresponding to Vmin or Vmax presents only negative values.
						commentaireEnCours=commentaireEnCours + Global.message$[5] + "Please consider revising your answer.\n\n";
                    if(result)
                      commentaireEnCours="From the current rule and according to your last answer, the model " + RechercheModele.getEquation()+ " is not validated.";
                    break;
              case 15:
                    result=(Global.trend_residuals!=1); 
                    if(result)
                      commentaireEnCours="From the current rule and according to your last answer, the model " + RechercheModele.getEquation()+ " is not validated.";
                    break;
             case 3:
                    result=true;
                    if(result)
                      commentaireEnCours="From the current rule the model " + RechercheModele.getEquation()+ " is  validated.";
                    break;
          }
          
          if (result && commentaireEnCours.equals(""))
             { commentaireEnCours=(String)htComment.get(Integer.toString(numEnCours));
            //System.out.println("On teste la règle (dans TexteRegles.java ligne 529) " + numEnCours + " elle est "+ result );
			if (commentaireEnCours == null) commentaireEnCours = ""; // Patch 2020 pour empêcher plantage écriture questions/réponses dans fichier html
             }
     return result;             
}

public static String getComment(){
    return commentaireEnCours;
}

}