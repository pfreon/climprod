/*
 * Titre : Climprod<p>
 *
 */
package fr.ird.climprod;

import java.awt.*;

public class PlotSerie implements Cloneable {

    public PlotSerie(String nameX, double dataX[], String nameY, double dataY[]) {
        if (dataX.length != dataY.length) {
            return;
        }
        int i;
        pfigure = 1;
        figureLigne = 0;
		pfont = new Font("SansSerif", Font.PLAIN, 11);
        pCouleur = Color.red;
        pDefaultCouleurPoint = Color.black;
        pmark = 0;
        pName[0] = nameX;
        pName[1] = nameY;
        pLegende = nameY;  // Nom légende dans cartouche graphique
        pNbElement = dataX.length;
        pData = new double[pNbElement][2];
        pCouleurPoint = new Color[pNbElement];
		pMin[0] = dataX[0];
        pMax[0] = pMin[0];
        pMin[1] = dataY[0];
        pMax[1] = pMin[1];
	//System.out.println("pMin[0] = " + pMin[0] + " pMax[0] = " + pMax[0] + " pMin[1] = " + pMin[1] + " pMax[1] = " + pMax[1]); 
        for (i = 0; i < pNbElement; i++) {
            pData[i][0] = dataX[i];
            pData[i][1] = dataY[i];
            pCouleurPoint[i] = pCouleur;
            for (int j = 0; j < 2; j++) {
                if (pData[i][j] < pMin[j]) {
                    pMin[j] = pData[i][j];
                } else if (pData[i][j] > pMax[j]) {
                    pMax[j] = pData[i][j];
                }
            }
        }
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        
            PlotSerie sc = (PlotSerie) super.clone();
            sc.pCouleurPoint = (Color[]) pCouleurPoint.clone();
            sc.pMax = (double[]) pMax.clone();
            sc.pMin = (double[]) pMin.clone();
            sc.pData = (double[][]) pData.clone();
            if (pEtiquette != null) {
                sc.pEtiquette = (String[]) pEtiquette.clone();
            } else {
                sc.pEtiquette = null;
            }
            sc.pName = (String[]) pName.clone();
            //System.out.println("Clone de la s�rie dans m�thode clone de la s�rie " +this.toString()+"  "+ sc.toString());
            return sc;
    }

    public double getMaxX() {
        return pMax[0];
    }

    public double getMinX() {
        return pMin[0];
    }

    public double getMaxY() {
        return pMax[1];
    }

    public double getMinY() {
        return pMin[1];
    }

    public String getNameX() {
        return pName[0];
    }

    public String getNameY() {
        return pName[1];
    }

    public double[] getDataX() {
        double[] data = new double[pNbElement];
        for (int i = 0; i < pNbElement; i++) {
            data[i] = pData[i][0];
        }
        return data;
    }

    public int getSize() {
        return pNbElement;
    }

    public void setFigure(int figure) {
        if (figure > 0 && figure < 6) {
            pfigure = figure;
            if (pfigure == 1 && pmark == 0) {
                pmark = 1;
            }
            if ((pfigure == 3 || pfigure == 4) && pEtiquette == null) {
                pEtiquette = new String[pNbElement];
                for (int i = 0; i < pNbElement; i++) {
                    pEtiquette[i] = Double.toString(pData[i][0]);
                }
            }
        }
    }

    public int getFigure() {
        return pfigure;
    }

    public void setMark(int mark) {
        if (mark > 0 && mark < 4) {
            pmark = mark;
        }

    }

    public int getMark() {
        return pmark;
    }

    public double[] getDataY() {
        double[] data = new double[pNbElement];
        for (int i = 0; i < pNbElement; i++) {
            data[i] = pData[i][1];
        }
        return data;
    }

    /*
     * Affecte la couleur sélectionnée à tous les points de la série
     *
     * @param Color couleur, la nouvelle couleur
     */
    public void setCouleur(Color couleur) {
        pCouleur = couleur;
        for (int i = 0; i < pNbElement; i++) {
            pCouleurPoint[i] = pCouleur;
        }
    }

    /*
     * Affecte la couleur s�lectionn�e aux points dont l'index correspond au
     * tableau index(pas de controle) Attention modif acc�s m�thode
     *
     * @param Color couleur, la nouvelle couleur � appliquer sur les points
     * s�lectionn�s
     * @param int[] un tableau d'entiers contenant les index des points �
     * modifi�s.
     */
    public void setCouleurPoints(Color couleur, int[] index) {
        for (int i = 0; i < index.length; i++) {
            pCouleurPoint[index[i]] = couleur;
        }

    }

    /*
     * Affecte la meme couleur � tous les points
     *
     * @param Color couleur, la nouvelle couleur � appliquer sur les points
     * s�lectionn�s
     */
    public void setCouleurPoints(Color couleur) {
        pDefaultCouleurPoint = couleur;
        for (int i = 0; i < pNbElement; i++) {
            pCouleurPoint[i] = couleur;
        }

    }

    /*
     * Retourne un tableau de couleurs repr�sentant la couleur affect�e � chaque
     * point.
     *
     * @return un tableau de couleurs.
     */
    public Color[] getCouleurPoints() {

        return pCouleurPoint;
    }

    /*
     * Retourne la couluer par d�fault affect�e au point.
     *
     * @return Color.
     */
    public Color getDefaultCouleurPoints() {

        return pDefaultCouleurPoint;
    }

    /*
     * Retourne la couleur affect�e � la s�rie sans tenir compte des couleurs
     * particuli�res affect�es � certains points..
     *
     * @return Color ,la couleur de la série.
     */
    public Color getCouleur() {
        return pCouleur;
    }

    /*
     * Retourne les etiquettes sur l'axe des X si la s�rie est trac�e sous forme
     * d'histogramme, ou sur les points dans le cas d'un nuage de points.
     *
     * @return :String[] les etiquettes .
     */
    public String[] getEtiquettes() {
        //if(pfigure==3)
        return pEtiquette;
  //else
        //  return null;
    }

    /*
     * Affectent les �tiquettes sur l'axe des X. sous forme d'histogramme, null
     * dans le cas contraire.
     *
     * @param :String[] les etiquettes sur l'axe des X ou null.
     */
    public void setEtiquettes(String[] etiquettes) {
        if (etiquettes.length != pNbElement) {
            return;
        }
        pEtiquette = new String[pNbElement];
        System.arraycopy(etiquettes, 0, pEtiquette, 0, pNbElement);
    }

    /*
     * Affectent le nom donn� � la s�rie dans la l�gende.
     *
     * @param :String le nouveau nom.
     */
    public void setLegende(String legende) {
        pLegende = legende;
    }

    /*
     * Retourne le nom donn� � la s�rie dans la l�gende.
     *
     * @return :String le nom de la l�gende.
     */
    public String getLegende() {
        return pLegende;
    }

    /*
     Détermine le tracé de la série
     @return BasicStroke appliqué� au contexte graphique
     */
    public BasicStroke getBasicStroke() {

        BasicStroke stroke;

        switch (figureLigne) {
            case Solid:
                stroke = new BasicStroke(1f);
                break;
            case Dash:
                float[] d1 = {20F, 10F, 20F, 10F};
                stroke = new BasicStroke(1f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 10.0F, d1, 0f);
                break;

            case Dot:
                float[] d2 = {5F, 5F, 5F, 5F};
                stroke = new BasicStroke(1f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 10.0F, d2, 0f);
                break;
            case DashDot:
                float[] d3 = {30F, 10F, 10F, 10F, 30F, 10F, 10F, 10F};
                stroke = new BasicStroke(1f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 10.0F, d3, 0f);
                break;
            case DashDotDot:
                float[] d4 = {30F, 10F, 10F, 10F, 10F, 10F};
                stroke = new BasicStroke(1f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 10.0F, d4, 0f);
                break;
            default:
                stroke = new BasicStroke(1f);
        }
        return stroke;

    }

    /*
     Détermnine le figuré de la courbe
     Solid=0
     Dash=1
     Dot=2
     DashDot=3
     DashDotDot=4
     @param un entier tl ;
     */
    public void setFigureLigne(int tl) {

        if (tl >= 0 && tl < 5) {
            figureLigne = tl;
        }
    }
    /*
     Donne le figuré de la courbe
     Solid=0,     Dash=1,     Dot=2,     DashDot=3,     DashDotDot=4
     @return un entier  ;
     */

    public int getFigureLigne() {

        return figureLigne;
    }

    /*
     * Affecte la nouvelle police de caract�res
     *
     * @param Font, la nouvelle police.
     */
    public void setFont(Font f) {
        pfont = f;
    }

    /*
     * Donne la polices de caract�res utilis�e .
     *
     * @return Font, la police utilis�e.
     */
    public Font getFont() {
        return pfont;
    }
    final int Solid = 0;
    final int Dash = 1;
    final int Dot = 2;
    final int DashDot = 3;
    final int DashDotDot = 4;

    private Color pCouleur;
    private Color pCouleurPoint[];
    private Color pDefaultCouleurPoint;
    private int pNbElement;
    private double[] pMin = new double[2];
    private double[] pMax = new double[2];
    private double[][] pData;
    private String[] pEtiquette;
    private String[] pName = new String[2];
    private String pLegende;
    private int pfigure;
    private int pmark;
    private int figureLigne;
    private Font pfont;

}

class Legende {

    public Legende(String name, Color c, int figure) {
        pfigure = figure;
        pCouleur = c;
        pName = name;

    }

    public String getName() {
        return pName;
    }

    public Color getColor() {
        return pCouleur;
    }

    public int getFigure() {
        return pfigure;
    }

    private final int pfigure;
    private final String pName;
    private final Color pCouleur;

}
