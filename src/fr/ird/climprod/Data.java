/**
 * Titre : Climprod<p>
 * Lecture des données du fichier *.CLI, recheche d'erreurs ou insuffisance,
 * calcul de la CPUE et des statistiques de base (sans décalage),
 * prend en compte les décalages temporels lorsque nécessaire,
 * calcule les valeurs moyennes pondérées de V,
 * affiche les graphiques de base (time plots, bivariate plots) sur les 
 * données brutes avec décalage si nécessaire.
 */
package fr.ird.climprod;

import java.util.StringTokenizer;
import java.text.*;//DecimalFormat;
public class Data {

    private static String fileName = null;
    private static int nbData = 0;
    private static double[][] data;
    private static boolean existFile = false;

    private static double[] fcoeff = new double[13];
    private static double[] vcoeff = new double[13];
	public static double[] year;
    private static double[] yexp;
    private static double[] pue;
    private static double[] f;
    private static double[] v;
    private static double[] vbar;
    private static int decalmax = 0;
    public static double[][] stat = new double[10][4]; 
    private static Object[][] dataTable;
    private static Object[][] dataCorrelation;
    
    public static void readFile(String file) 
    {
        dataTable = null;
        dataCorrelation = null;
        fileName = file;
        existFile = false;
        try {
            String[] dataLine;
            String[] fieldNames = {"Year/season", "Catches", "Effort", "Environment"};
			String[] NextToken$ = {"", "", "", ""}; // In order to get access to t.nextToken().trim() without incrementing t (Modif. 2020) 
            ReadFileText rft = new ReadFileText(file);
            dataLine = rft.getLines();
            nbData = dataLine.length - 1;
            data = new double[nbData][4];
            if (dataLine != null) //Lecture ok
            {
                StringTokenizer t = new StringTokenizer(dataLine[0], ";");
                if (t.countTokens() != 4) {
                    throw new OnError("Invalid data file (incorrect number of columns).");
                }
                for (int i = 0; i < 4; i++) {
					NextToken$[i] = t.nextToken().trim();
					if ((NextToken$[1]).equals("Production")) NextToken$[1] = "Catches"; // In order to avoid error for historical .CLI files where "Production" was used instead of "Catches" (Modif. 2020)
                                        if ((NextToken$[0]).equals("Years")) NextToken$[0] = "Year/season";    // In oreder to make clearer the use of fishing or environmental seasons
                                        if ((NextToken$[i]).equals(fieldNames[i]) == false) {
                        throw new OnError("Invalid data file (incorrect column field name(s)).");
                    }
      			}
                for (int i = 1; i < dataLine.length; i++) {
                    StringTokenizer d = new StringTokenizer(dataLine[i], ";");
                    if (d.countTokens() != 4) {
                        throw new OnError("Invalid data file (incorrect number of data in line " + i + ").");
                    }
                    int k = 0;
                    while (d.hasMoreTokens()) {
                        data[i - 1][k] = Double.parseDouble(d.nextToken().trim());
                        //System.out.print(data[i-1][k]+"  ");
                        k = k + 1;
                    }
                    // System.out.println();
                }
            }
            existFile = true;   
            init_val();
            statistiques();
            makePlot(false);
            makeDataTable();
            makeCorrelationTable(); // Coefficient de correlation sur les rangs de Spearman (Rho)
        } catch (Exception e) {
            nbData = 0;
            MsgDialogBox msg = new MsgDialogBox(0, e.getMessage(), 0, Global.CadreMain);
        }
    }

    /**
     * Retourne le nombre de données
     *
     * @return un entier représentant le nombre d'années de données ou -1 si
     * fichier non valide
     */
    public static int getNbYears() {
        if (existFile) {
            return nbData;
        } else {
            return -1;
        }
    }

    /**
     * Retourne le nom complet du fichier
     *
     * @return un String représentant le nom complet du fichier ou null si
     * fichier non valide
     */
    public static String getFileName() {
        if (existFile) {
            return fileName;
        } else {
            return null;
        }
    }

    /**
     * Retourne les données d'origine
     *
     * @return un double[][] ou null si fichier non valide
     */
    public static double[][] getData() {
        if (existFile) {
            return (double[][]) data.clone();
        } else {
            return null;
        }
    }

    /**
     * Retourne la production yexp aprés prise en cpte du décalage
     *
     * @return un double[], la production ou null si fichier non valide
     */
    public static double[] getYexp() {
        if (existFile) {
            return yexp;
        } else {
            return null;
        }
    }

    /**
     * Retourne la production par unite d'effort de peche aprés prise en cpte du
     * décalage
     *
     * @return un double[], ou null si fichier non valide
     */
    public static double[] getPue() {
        if (existFile) { 
            return pue;
        } else {
            return null;
        }
    }

    /**
     * Retourne l'environnement aprés prise en cpte du décalage
     *
     * @return un double[], ou null si fichier non valide
     */
    public static double[] getV() {
        if (existFile) {
            return v;
        } else {
            return null;
        }
    }

    /**
     * Retourne l'effort de peche aprés prise en cpte du décalage
     *
     * @return un double[], ou null si fichier non valide
     */
    public static double[] getF() {
        if (existFile) {
            return f;
        } else {
            return null;
        }
    }
     /**
	 * Retourne Catches
     *
     * @return un double[], ou null si fichier non valide
     */
    public static double[] getCatches() {
        if (existFile) {
            return yexp;
        } else {
            return null;
        }
    }
    /**
     * Retourne l'effort de peche aprés prise en cpte du décalage
     *
     * @return un double[], ou null si fichier non valide
     */
    public static double[] getVbar() {
        if (existFile) {
            return vbar;
        } else {
            return null;
        }
    }

    /**
     * Retourne la periode d'observation aprés prise en cpte du décalage
     *
     * @return un double[], ou null si fichier non valide
     */
    public static double[] getYears() {
        if (existFile) {
            return year;
        } else {
            return null;
        }
    }

    /**
     * Retourne le nombre de données retenues
     *
     * @return un entier, ou null si fichier non valide
     */
    public static int getNbDataRetenue() {
        if (existFile) {
            return nbData - decalmax;
        } else {
            return -1;
        }
    }
/**
     * Construit le tableau des données à ajuster en fonction des décalages
     * et du calcul des moyennes pondérées de E et de V.
     */
    public static void init_val() {
        int i, d;
        calcul_coeff();
        int nim = nbData - decalmax;
        // System.out.println("nim  " + nim);
        year = new double[nim];
        yexp = new double[nim];  // Catches
        pue = new double[nim];
        f = new double[nim];    // Effort
        v = new double[nim];
        vbar = new double[nim];
        for (i = 0; i < nim; i++) {
            year[i] = data[i + decalmax][0];
            yexp[i] = data[i + decalmax][1];
            v[i] = data[i + decalmax][3];
            f[i] = 0;
            vbar[i] = 0;
            for (d = 0; d <= decalmax; d++) {
                f[i] += data[i + decalmax - d][2] * fcoeff[d];
                vbar[i] += data[i + decalmax - d][3] * vcoeff[d];
            }
            pue[i] = yexp[i] / data[i + decalmax][2];
            //System.out.println(data[i+decalmax][0]+"*"+yexp[i]+"*"+v[i]+"*"+f[i]+"*"+vbar[i]+"*"+ pue[i]);
        }
        makePlot(true);

    }

    /**
     *Calcul des coefficients par classe d'àage et par an servant au calcul
     * des moyennes pondérées de E (fcoeff) et de V (vcoeff)
     */
    private static void calcul_coeff() {
        int nbexploit = Global.nb_classes_exploitees;
        int agerec = Global.recruitment_age;
        int begining = Global.begin_influence_period;
        int ending = Global.end_influence_period;
        if (Global.environmental_influence == "catchability") 
           { begining = agerec;
             ending = agerec + nbexploit -1; 
        }
        //System.out.println(nbexploit+"****"+agerec+ "**********" +begining+"******"+ending);

        /*int nbexploit=Integer.parseInt((String)Global.htVar.get("nb_classes_exploitees"));
         int agerec=Integer.parseInt((String)Global.htVar.get("recruitment_age"));
         int begining=Integer.parseInt((String)Global.htVar.get("begin_influence_period"));
         int ending=Integer.parseInt((String)Global.htVar.get("end_influence_period"));*/
        if (nbexploit < 1) {
            nbexploit = 1;
        }
        if (agerec < 1) {
            agerec = 0;
        }
        double[][] t = new double[16][21];
        int ageder;
        double eps = 1.0E-12;
        double dtot;
        for (int d = 0; d < 13; d++) {
            fcoeff[d] = 0;
        }
        for (int d = 0; d < 13; d++) {
            vcoeff[d] = 0;
        }
        dtot = 0;
        for (int d = 0; d < nbexploit; d++) {
            dtot += (nbexploit - d);
        }
        for (int d = 0; d < nbexploit; d++) {
            fcoeff[d] = (nbexploit - d) / dtot;
        }
        decalmax = nbexploit - 1;
        for (int i = 0; i < 15; i++) {
            for (int j = 0; j < 21; j++) {
                t[i][j] = 0;
            }
        }
        ageder = agerec + nbexploit - 1;
        dtot = 0;
       //System.out.println("In Data.java ligne 298 nbexploit = " + nbexploit + "*** agerec = "+ agerec + "*** begining = " + begining + "****ending = "+ ending);
       //System.out.println("*** ageder =" + ageder + "decalmax = " + decalmax);
       if (begining == -1) begining = 0;
       //System.out.println("begining = " + begining);
        for (int i = 0; i < nbexploit; i++) {
            for (int d = begining; d <= ending; d++) {
                t[i][d + i] = 1;
                //System.out.println(i+" " +(d+i-1) );
            }
        }
        for (int d = 0; d < ageder + 1; d++) {
            for (int i = 0; i < nbexploit; i++) {
                vcoeff[ageder - d] += t[i][d];
        //        System.out.println(ageder-d+"***"+vcoeff[d]);
                dtot += t[i][d];
        //        System.out.println("dtot "+i+" " +d +" " +dtot);
            }
        }
        for (int d = 0; d < ageder + 1; d++) {
            vcoeff[d] /= dtot;
        }
        //for(int d=0;d<=ageder;d++)
        //System.out.println(d+"----"+vcoeff[d]);
        int d = ageder;
        //System.out.println(d+"****ll***"+vcoeff[d]+"*******"+decalmax);
        while (vcoeff[d] < eps) {
            d--;
        }
        if (d > decalmax) {
            decalmax = d;
        }
        //System.out.println(decalmax);
        //decalmax=1;
        //if(Global.numero_modele==-2)
        //      decalmax=0;

    }

    private static void makePlot(boolean lag) {
        if (lag) {
            /* ************** Lag plot *****************************
            ******* CPUE versus E (E weighted if relevant) *********
            *** and CPUE versus V (V lagged if relevant) ***********
            *******************************************************/
            
            for (int i = 0; i < 2; i++) {
                Global.lagPlot[i] = new Plot();
            }
            Global.lagPlot[0].setValeurs(new PlotSerie(" ", Data.f, " ", Data.pue));
            Global.lagPlot[1].setValeurs(new PlotSerie(" ", Data.v, " ", Data.pue));

            for (int i = 0; i < 2; i++) {
                Global.lagPlot[i].setTitreGraphique(Global.titreG[i + 12]);
                Global.lagPlot[i].setTitreX("Weighted " + Global.titreSx[i + 10]);
                Global.lagPlot[i].setTitreY(Global.titreSy[i + 10]);
                if (Data.stat[8][i+2] > 100)  Global.lagPlot[i].setDecimalsurX(0); // si range E ou V > 100. Modif 2021.
                else Global.lagPlot[i].setDecimalsurX(2);
                if (Data.stat[8][1] > 100) Global.lagPlot[i].setDecimalsurY(0);   // si range CPUE > 100. Modif 2021. 
                else Global.lagPlot[i].setDecimalsurY(2);
            }
        } else {
             /**********************************************************
             * ***************** time plot *****************************
             **********************************************************/
             
            double minx = Stat1.min(Data.year);
            Global.TimeStepData = Data.year[1] - Data.year[0]; // Time step of the row data (default Year, decimal intervals mean seasonal data
            //System.out.println("Global.TimeStepData = " +  Global.TimeStepData);
            if (Global.TimeStepData >= 1.0) {
                Global.TimeStepData$ = "Full year or single annual season";
            }
            else { 
                Global.TimeStepData$ = "Season";
                for (int i = 0; i < 5; i++) { Global.titreSx[i] = "Season";}
                for (int i = 13; i < 15; i++) { Global.titreSx[i] = "Season";}
                for (int i = 17; i < 22; i++) { Global.titreSx[i] = "Season";}
            }
        
            PlotSerie[] ps = new PlotSerie[4];
            ps[0] = new PlotSerie("x", Data.year, "y", Data.yexp);
            ps[1] = new PlotSerie("x", Data.year, "y", Data.pue);
            ps[2] = new PlotSerie("x", Data.year, "y", Data.f);
            ps[3] = new PlotSerie("x", Data.year, "y", Data.v);
            double RangeYear=Global.RangeYear();
            //System.out.println("RangeSeason(s) = " + RangeYear); 
            for (int i = 0; i < 4; i++) {
                Global.timePlot[i] = new Plot();
                ps[i].setFigure(2);
                Global.timePlot[i].setValeurs(ps[i]);
                Global.timePlot[i].setTitreGraphique(Global.titreG[i]);
                Global.timePlot[i].setTitreX(Global.titreSx[i]);
                Global.timePlot[i].setTitreY(Global.titreSy[i]);
                if (Global.TimeStepData >= 1.0)	Global.timePlot[i].setDecimalsurX(0);  // Time step of the row data (default Year, decimal intervals mean seasonal data
                else Global.timePlot[i].setDecimalsurX(1);
                Global.timePlot[i].setMinAxeX(minx - 1);
				if (RangeYear < 20) Global.timePlot[i].setpasX(1.0d);
				else if (RangeYear < 30) Global.timePlot[i].setpasX(2.0d);
				else if (RangeYear < 45) Global.timePlot[i].setpasX(3.0d);
				else Global.timePlot[i].setpasX(4.0d);				
				//System.out.println("timePlot i=" + i + " Data.stat[8][i] = " + Data.stat[8][i]);
                if (Data.stat[8][i] > 100) Global.timePlot[i].setDecimalsurY(0);
                else Global.timePlot[i].setDecimalsurY(2);
                //Global.timePlot[i].ShowLegend=true;
            }

            /**
             * ********* Distribution plot (histograms) **************************
             * *****utilisé dans 'Select the appropriate model' ******************
             ****(mais pas ceux du Jackknife qui figurent dans Validation.java****
             * ******************************************************************/
            for (int i = 0; i < 4; i++) {
                double[][] zz = new double[10][2];  // Table des valeurs des X [0] et Y [1] des 10 [0->9] classes des histogrammes.
                double[] val = new double[10];
                double[] etiq = new double[10];
                String[] etiq$ = new String[10];
                for (int k = 0; k < 10; k++) {
                    etiq[k] = k + 1;
                }
                Global.distriPlot[i] = new PlotHisto();
                switch (i) {
                    case 0:
                        zz = Stat1.distribuer(10, Data.f);
                        break;
                    case 1:
                        zz = Stat1.distribuer(10, Data.v);
                        break;
                    case 2:
                        zz = Stat1.distribuer(10, Data.yexp);
                        break;
                    case 3:
                        zz = Stat1.distribuer(10, Data.pue);
                        break;

                }
				//System.out.println("i=" + i + " zz[0][0] = " + zz[0][0] + " zz[0][1] = " + zz[0][1]);
				//System.out.println("zz[9][0] = " + zz[9][0] + " zz[9][1] = " + zz[9][1]);
				DecimalFormat df = new DecimalFormat("0.##"); // Gestion des décimales sur axe X des histogrammes
				double range = Math.abs(zz[9][0] - zz[0][0]);
				if (range > 100) {
                    df = new DecimalFormat("0");
                }
                for (int j = 0; j < zz.length; j++) {
                    etiq$[j] = df.format(zz[j][0]).toString();
                    val[j] = zz[j][1];
                }
                PlotSerie psd = new PlotSerie("x", etiq, "y", val);
                psd.setEtiquettes(etiq$);
                psd.setFigure(3);
                Global.distriPlot[i].setValeurs(psd);
                Global.distriPlot[i].setTitreGraphique(Global.titreG[i + 4]);
                Global.distriPlot[i].setTitreX(Global.titreSx[i + 4]);
                Global.distriPlot[i].setTitreY(Global.titreSy[i + 4]); 
                Global.distriPlot[i].setDecimalsurY(0);
                Global.distriPlot[i].setpasY(1.d);
                Global.distriPlot[i].setMaxAxeY(psd.getMaxY());
            }

            /* ********************** Scatter plot *****************************
             ************* Y vs E (unlagged), Y vs V (unlagged),**************** 
             ************* CPUE vs E (unlagged), CPUE vs V (unlagged) **********
             */
            for (int i = 0; i < 4; i++) {
                Global.scatterPlot[i] = new Plot();
            }

            Global.scatterPlot[0].setValeurs(new PlotSerie(" ", Data.f, " ", Data.yexp));
            Global.scatterPlot[1].setValeurs(new PlotSerie(" ", Data.v, " ", Data.yexp));
            Global.scatterPlot[2].setValeurs(new PlotSerie(" ", Data.f, " ", Data.pue));
            Global.scatterPlot[3].setValeurs(new PlotSerie(" ", Data.v, " ", Data.pue));
			//System.out.print("FLAG SCATTER PLOT DE Data.java LIGNE 440"); // test 2020
            for (int i = 0; i < 4; i++) {
                Global.scatterPlot[i].setTitreGraphique(Global.titreG[i + 8]);
                Global.scatterPlot[i].setTitreX(Global.titreSx[i + 8]);
                Global.scatterPlot[i].setTitreY(Global.titreSy[i + 8]);
				double range = Math.abs(Global.scatterPlot[i].getMaxAxeX() - Global.scatterPlot[i].getMinAxeX());
				if (range > 100) Global.scatterPlot[i].setDecimalsurX(0);
                else Global.scatterPlot[i].setDecimalsurX(2);
                range = Math.abs(Global.scatterPlot[i].getMaxAxeY() - Global.scatterPlot[i].getMinAxeY());
				if (range > 100) Global.scatterPlot[i].setDecimalsurY(0);
                else Global.scatterPlot[i].setDecimalsurY(2);                
            }

            /* ***************** Indépendance plot *************************
             * ********utilisé dans 'Select the appropriate model' *********
             * *************************************************************/

            Global.indePlot = new Plot();
            Global.indePlot.setValeurs(new PlotSerie("Environement (V)", Data.v, "Effort (E)", Data.f));
            Global.indePlot.setTitreGraphique(Global.titreG[14]);
			if (Data.stat[8][3] > 100) Global.indePlot.setDecimalsurX(0);
			else Global.indePlot.setDecimalsurX(2);
            if (Data.stat[8][2] > 100) Global.indePlot.setDecimalsurY(0);
      		else Global.indePlot.setDecimalsurY(2);               	
        }
    }

    /*
     *Calcule les principales statistiques sur les données.
     *Utilisé à la lecture du fichier (à ce stade la variable stat ne tient past cpte du décalage)
     */
    private static void statistiques() 
    {
        double[][] trans = new double[4][];
        trans[0] = yexp;  // Production
        trans[2] = f;	  // Effort
        trans[3] = v;	  // Environnement
        trans[1] = pue;   // CPUE
        for (int j = 0; j < 4; j++) 
        {
            stat[0][j] = nbData;
            stat[1][j] = Stat1.moy(trans[j]);
            stat[2][j] = Math.sqrt(Stat1.var(trans[j], true));
            stat[3][j] = 100 * stat[2][j] / (stat[1][j] + 1.e-20);
            stat[4][j] = Stat1.Kurtosis(trans[j]);
            stat[5][j] = Stat1.Skewness(trans[j]);
            double[] ext = Stat1.Extremas(trans[j]);
            stat[6][j] = ext[0];           // Min.
            stat[7][j] = ext[1];           // Max.
            stat[8][j] = ext[1] - ext[0];  // Range.
            ext = Stat1.quant(trans[j], 3);
            stat[9][j] = ext[1];
        }
        try {
		if (stat[7][1]<1) {throw new OnError("All of your CPUE data is < 1. \nPlease change you unit(s) for catches and/or fishing effort.\nThis will facilitate the readability of your results.");}
		} 
		catch (Exception e) {
            nbData = 0;
            MsgDialogBox msg = new MsgDialogBox(0, e.getMessage(), 0, Global.CadreMain);
        }
        try {
		if (stat[6][0]<0 || stat[6][2]<0 ) {throw new OnError("Invalid data file.  \nAll catches and effort data must be > 0.");}
		} 
		catch (Exception e) {
            nbData = 0;
            MsgDialogBox msg = new MsgDialogBox(0, e.getMessage(), 0, Global.CadreMain);
        }        
    }
 
    /*
     Donne les principaux résultats
     @return Object[][] ou null si fichier invalide
     */
    public static Object[][] getStatistics() {
        if (!existFile) {
            return null;
        }
        String[] param$ = {"Cases",
            "Mean",
            "Stand-dev",
            "Coef.Var",
            "Kurtosis",
            "Skewness",
            "Minimum",
            "Maximum",
            "Range",
            "Median"
        };
        String[] title$ = {"Statistics  ", "Catches", "CPUE", "Effort (E)", "Envir. (V)"};
        Object[][] data$ = new String[11][5];
        // DecimalFormat nf0= new DecimalFormat("0");
        //  DecimalFormat nf= new DecimalFormat(" 0.000000;-0.000000");
        DecimalFormat nf = new DecimalFormat(" 0.#####;-0.#####");
        for (int j = 0; j < 5; j++) {
            data$[0][j] = title$[j];
        }

        for (int i = 0; i < 10; i++) {
            data$[i + 1][0] = param$[i];

            for (int j = 0; j < 4; j++) {
                data$[i + 1][j + 1] = nf.format(stat[i][j]);
            }
            //nf0=nf;
        }
        return data$;
    }


    /*
     Donne le tableau de données (attention ne prend pas en cpte le décalage)
     @return Object[][] ou null si fichier invalide
     */
    public static Object[][] getDataTable() {

        if (!existFile) {
            return null;
        }

        return dataTable;
    }

    /*
     Donne le tableau des coeffeicien de correlation sur les rangs de Spearman (Rho)
     entre variables. Affiché dans menu principal après ouverture du fichier de données.
     @return Object[][] ou null si fichier invalide.
     */
    public static Object[][] getCorrelationTable() {

        if (!existFile) {
            return null;
        }
        return dataCorrelation;

    }

    /*
     *Donne le tableau de données
     *Utilisé à la lecture du fichier (à ce stade la variable stat ne tient pas cpte du décalage)
     */
    private static void makeDataTable() {

        if (!existFile) {
            return;
        }
        String[] title$ = {"Year/season", "Catches", "CPUE", "Effort (E)", "Envir. (V)"};
        dataTable = new String[nbData + 1][5];
        // DecimalFormat nf0= new DecimalFormat(" 0;-0");
        // DecimalFormat nf= new DecimalFormat(" 0.000000;-0.000000");
        DecimalFormat nf = new DecimalFormat(" 0.######;-0.######");
        for (int j = 0; j < 5; j++) {
            dataTable[0][j] = title$[j];
        }
        for (int i = 0; i < nbData; i++) {
            dataTable[i + 1][0] = nf.format(year[i]);
            dataTable[i + 1][1] = nf.format(yexp[i]);
            dataTable[i + 1][2] = nf.format(pue[i]);
            dataTable[i + 1][3] = nf.format(f[i]);
            dataTable[i + 1][4] = nf.format(v[i]);

        }

    }


    /*
     Donne le tableau de données (attention prend en cpte le décalage)
     @return Object[][] ou null si fichier invalide
     * Coefficient de correlation sur les rangs de Spearman (Rho)
     */
    private static void makeCorrelationTable() {

        if (!existFile) {
            return;
        }
        String[] title$ = {"Rank Corr. (Rho)", "Catches", "CPUE", "Effort (E.)", "Envir. (V.)"};
        dataCorrelation = new String[5][5];
        DecimalFormat nf = new DecimalFormat(" 0.000;-0.000");

        for (int i = 1; i < 5; i++) {
            for (int j = 1; j < 5; j++) {
                if (i == j) {
                    dataCorrelation[i][j] = nf.format(1);
                } else {
                    dataCorrelation[i][j] = "";
                }
            }
        }
        for (int i = 1; i < 5; i++) {
            dataCorrelation[i][0] = title$[i];
        }
        for (int j = 0; j < 5; j++) {
            dataCorrelation[0][j] = title$[j];
        }
        dataCorrelation[2][1] = nf.format(Stat1.rho(yexp, pue));
        dataCorrelation[3][1] = nf.format(Stat1.rho(f, yexp));
        dataCorrelation[3][2] = nf.format(Stat1.rho(f, pue));
        dataCorrelation[4][1] = nf.format(Stat1.rho(v, yexp));
        dataCorrelation[4][2] = nf.format(Stat1.rho(v, pue));
        dataCorrelation[4][3] = nf.format(Stat1.rho(v, f));
        
/***** t-test on r Spearman: t = abs(r*sqr(((n-2)/(1-(r1^2)))) */ 

        //System.out.println("t C CPUE = " + Math.abs(Stat1.rho(yexp, pue))*Math.sqrt((nbData-2)/(1-Math.pow(Stat1.rho(yexp, pue),2))));
        if (Math.abs(Stat1.rho(yexp, pue))*Math.sqrt((nbData-2)/(1-Math.pow(Stat1.rho(yexp, pue),2))) < Global.TableStudent[nbData-2][0]) 
            dataCorrelation[1][2] = ">0.5";
        else  if (Math.abs(Stat1.rho(yexp, pue))*Math.sqrt((nbData-2)/(1-Math.pow(Stat1.rho(yexp, pue),2))) < Global.TableStudent[nbData-2][1])
            dataCorrelation[1][2] = "<0.5";
        else 
            dataCorrelation[1][2] = "<0.01";
        
        //System.out.println("t f C = " + Math.abs(Stat1.rho(f, yexp))*Math.sqrt((nbData-2)/(1-Math.pow(Stat1.rho(f, yexp),2))));        
        if (Math.abs(Stat1.rho(f, yexp))*Math.sqrt((nbData-2)/(1-Math.pow(Stat1.rho(f, yexp),2))) < Global.TableStudent[nbData-2][0]) 
            dataCorrelation[1][3] = ">0.5";
        else  if (Math.abs(Stat1.rho(f, yexp))*Math.sqrt((nbData-2)/(1-Math.pow(Stat1.rho(f, yexp),2))) < Global.TableStudent[nbData-2][1])
            dataCorrelation[1][3] = "<0.5";
        else 
            dataCorrelation[1][3] = "<0.01";

        //System.out.println("t f CPUE = " + Math.abs(Stat1.rho(f, pue))*Math.sqrt((nbData-2)/(1-Math.pow(Stat1.rho(f, pue),2))));                
        if (Math.abs(Stat1.rho(f, pue))*Math.sqrt((nbData-2)/(1-Math.pow(Stat1.rho(f, pue),2))) < Global.TableStudent[nbData-2][0]) 
            dataCorrelation[2][3] = ">0.5";
        else  if (Math.abs(Stat1.rho(f, pue))*Math.sqrt((nbData-2)/(1-Math.pow(Stat1.rho(f, pue),2))) < Global.TableStudent[nbData-2][1])
            dataCorrelation[2][3] = "<0.5";
        else 
            dataCorrelation[2][3] = "<0.01";

        //System.out.println("t V C = " + Math.abs(Stat1.rho(v, yexp))*Math.sqrt((nbData-2)/(1-Math.pow(Stat1.rho(v, yexp),2))));                       
        if (Math.abs(Stat1.rho(v, yexp))*Math.sqrt((nbData-2)/(1-Math.pow(Stat1.rho(v, yexp),2))) < Global.TableStudent[nbData-2][0]) 
            dataCorrelation[1][4] = ">0.5";
        else  if (Math.abs(Stat1.rho(v, yexp))*Math.sqrt((nbData-2)/(1-Math.pow(Stat1.rho(v, yexp),2))) < Global.TableStudent[nbData-2][1])
            dataCorrelation[1][4] = "<0.5";
        else 
            dataCorrelation[1][4] = "<0.01";        
                
        //System.out.println("t V CPUE = " + Math.abs(Stat1.rho(v, f))*Math.sqrt((nbData-2)/(1-Math.pow(Stat1.rho(v, f),2))));                       
        if (Math.abs(Stat1.rho(v, f))*Math.sqrt((nbData-2)/(1-Math.pow(Stat1.rho(v, f),2))) < Global.TableStudent[nbData-2][0]) 
            dataCorrelation[2][4] = ">0.5";
        else  if (Math.abs(Stat1.rho(v, f))*Math.sqrt((nbData-2)/(1-Math.pow(Stat1.rho(v, f),2))) < Global.TableStudent[nbData-2][1])
            dataCorrelation[2][4] = "<0.5";
        else 
            dataCorrelation[2][4] = "<0.01";        

        //System.out.println("t V C = " + Math.abs(Stat1.rho(v, yexp))*Math.sqrt((nbData-2)/(1-Math.pow(Stat1.rho(v, yexp),2))));                       
        if (Math.abs(Stat1.rho(v, yexp))*Math.sqrt((nbData-2)/(1-Math.pow(Stat1.rho(v, yexp),2))) < Global.TableStudent[nbData-2][0]) 
            dataCorrelation[3][4] = ">0.5";
        else  if (Math.abs(Stat1.rho(v, yexp))*Math.sqrt((nbData-2)/(1-Math.pow(Stat1.rho(v, yexp),2))) < Global.TableStudent[nbData-2][1])
            dataCorrelation[3][4] = "<0.5";
        else 
            dataCorrelation[3][4] = "<0.01";        
    }
}
