/*
 * Titre : Climprod<p>
 * Manage the options of plot changes from the icon that appear on graphs
 */
package fr.ird.climprod;

import java.awt.*;
import java.awt.event.*;
import java.util.Vector;
import javax.swing.*;
import javax.swing.event.*;

import javax.swing.border.*;

public class PlotPreferences extends JDialog
        implements DocumentListener {

    private JButton cancel;
    
    public PlotPreferences(JFrame f, Plot gra) {
        super(f, "Preferences", true);
        ok = false;
        graOld = gra;
        graNew = (Plot) gra.clone();
        xnonModal = !(gra.getType() == 3 || gra.getType() == 4);
        //System.out.println( xnonModal+"  "+ gra.getType());
        JPanel container = new JPanel();
        container.setLayout(new BorderLayout());
        JTabbedPane tabs = new JTabbedPane();
        JPanel scale = buildScalePanel();
        JPanel title = buildTitlePanel();
        buildPatternPanel bld1 = new buildPatternPanel(graNew);

        buildLegendPanel legend = new buildLegendPanel(graNew);
        //JPanel legend = bld2.getPanel();
        JPanel pattern = bld1.getPanel();

        tabs.addTab("Scale", null, scale);
        tabs.addTab("Titles", null, title);
        tabs.addTab("Patterns", null, pattern);
        tabs.addTab("Legend", null, legend);
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        cancel = new JButton("Cancel (ESC)");
        
        int condition = JComponent.WHEN_IN_FOCUSED_WINDOW;
        KeyStroke keyStrokep = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
        InputMap inputMapp = cancel.getInputMap(condition);
        ActionMap actionMapp = cancel.getActionMap();
        inputMapp.put(keyStrokep, keyStrokep.toString());
        actionMapp.put(keyStrokep.toString(), new AbstractAction() {            
            @Override
            public void actionPerformed(ActionEvent arg0) {
                cancel.doClick();
            }
        });
        
        cancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                CancelPressed();
            }
        });
        buttonPanel.add(cancel);
        JButton ok = new JButton("OK");
        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                OKPressed();
            }
        });
        buttonPanel.add(ok);
        getRootPane().setDefaultButton(ok);
        container.add(tabs, BorderLayout.CENTER);
        container.add(buttonPanel, BorderLayout.SOUTH);
        getContentPane().add(container);
        pack();
        UtilCadre.Size(this, 75, 50, true);
        //System.out.println("Flag definition taille fenetre (75, 50) et apparence (true) dans PlotPreferences.java"); // Test 2020 
        UtilCadre.Centrer(this);
        setVisible(true);
        //		UIManager.addPropertyChangeListener(new UISwitchListener(container));
    }

    /*
     * *********** Onglet Echelle *****************
     */
    public JPanel buildScalePanel() {
        int i;
        JPanel scale = new JPanel();
        scale.setLayout(new GridLayout(3, 2));

        JPanel statPanel = new JPanel();
        statPanel.setLayout(new GridLayout(2, 2, 20, 3));
        statPanel.setBorder(new TitledBorder("Statistics"));

        JLabel lblXmin = new JLabel("Xmin: " + graOld.getMinSerieX(), JLabel.RIGHT);
        JLabel lblXmax = new JLabel("Xmax: " + graOld.getMaxSerieX(), JLabel.RIGHT);
        JLabel lblYmin = new JLabel("Ymin: " + graOld.getMinSerieY(), JLabel.RIGHT);
        JLabel lblYmax = new JLabel("Ymax: " + graOld.getMaxSerieY(), JLabel.RIGHT);

        statPanel.add(lblXmin);
        statPanel.add(lblXmax);
        statPanel.add(lblYmin);
        statPanel.add(lblYmax);
        scale.add(statPanel);

        JPanel axePanel = new JPanel();
        axePanel.setLayout(new GridLayout(2, 4, 20, 3));
        axePanel.setBorder(new TitledBorder("Axes"));

        JLabel lblaxeXmin = new JLabel("Xmin: ", JLabel.RIGHT);
        JLabel lblaxeXmax = new JLabel("Xmax: ", JLabel.RIGHT);
        JLabel lblaxeYmin = new JLabel("Ymin: ", JLabel.RIGHT);
        JLabel lblaxeYmax = new JLabel("Ymax: ", JLabel.RIGHT);

        txtaxeXmin = new JTextField("" + graOld.getMinAxeX(), 10);
        txtaxeXmax = new JTextField("" + graOld.getMaxAxeX(), 10);
        txtaxeYmin = new JTextField("" + graOld.getMinAxeY(), 10);
        txtaxeYmax = new JTextField("" + graOld.getMaxAxeY(), 10);
        txtaxeXmin.setEnabled(xnonModal);
        txtaxeXmax.setEnabled(xnonModal);
        axePanel.add(lblaxeXmin);
        axePanel.add(txtaxeXmin);
        axePanel.add(lblaxeXmax);
        axePanel.add(txtaxeXmax);
        axePanel.add(lblaxeYmin);
        axePanel.add(txtaxeYmin);
        axePanel.add(lblaxeYmax);
        axePanel.add(txtaxeYmax);
        scale.add(axePanel);

        JPanel stepPanel = new JPanel();
        stepPanel.setBorder(new TitledBorder("Scale"));
        Box b = Box.createHorizontalBox();

        JLabel lblonX = new JLabel("On X: ", JLabel.RIGHT);
        JLabel lblonY = new JLabel("On Y: ", JLabel.RIGHT);
        txtonX = new JTextField("" + graOld.getpasX(), 5);
        txtonY = new JTextField("" + graOld.getpasY(), 5);
        txtonX.setEnabled(xnonModal);
        b.add(lblonX);
        b.add(txtonX);
        b.add(Box.createHorizontalStrut(15));
        b.add(lblonY);
        b.add(txtonY);

        stepPanel.add(b, "Center");
        scale.add(stepPanel);
//*********************************************************
        JPanel formatPanel = new JPanel();
        formatPanel.setLayout(new GridLayout(2, 3, 4, 10));
        formatPanel.setBorder(new TitledBorder("Format"));

        JLabel lblfX = new JLabel("Nb decimals on X: ", JLabel.RIGHT);
        JLabel lblfY = new JLabel("Nb decimals on Y: ", JLabel.RIGHT);

        chkscienX = new JCheckBox("Scientific");
        chkscienY = new JCheckBox("Scientific");
        String[] Decimal = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
        lstnbX = new JComboBox<String>(Decimal);
        lstnbY = new JComboBox<String>(Decimal);

        formatPanel.add(lblfX);
        formatPanel.add(lstnbX);
        formatPanel.add(chkscienX);
        formatPanel.add(lblfY);
        formatPanel.add(lstnbY);
        formatPanel.add(chkscienY);
        scale.add(formatPanel);
        chkscienX.setSelected(graOld.getNotationEsurX());
        chkscienY.setSelected(graOld.getNotationEsurY());
        lstnbX.setSelectedIndex(graOld.getDecimalsurX());
        lstnbY.setSelectedIndex(graOld.getDecimalsurY());

//***************************************************
		/*	String[] fontName = GraphicsEnvironment //ca marche mais long
         .getLocalGraphicsEnvironment()
         .getAvailableFontFamilyNames();*/
        String[] fontName = {"Serif", "SansSerif", "Monospaced", "Dialog", "DialogInput"};
        String[] fontStyle = {"Plain", "Bold", "Italic", "Bold+Italic"};
        String[] fontSize = {"5", "8", "9", "10", "11", "12", "14", "16", "18", "20", "22", "24"};
        JPanel fontPanel = new JPanel();
        fontPanel.setLayout(new GridLayout(2, 1, 10, 0));

        Box bh = Box.createHorizontalBox();
        fontPanel.setBorder(new TitledBorder("Font"));
        cboFontName = new JComboBox<String>(fontName);
        cboFontSize = new JComboBox<String>(fontSize);
        cboFontStyle = new JComboBox<String>(fontStyle);
        bh.add(cboFontName);
        bh.add(cboFontSize);
        bh.add(cboFontStyle);
        fontPanel.add(bh);
        scale.add(fontPanel);
        lblExample = new JLabel("123 ", JLabel.CENTER);
        fontPanel.add(lblExample);

        JPanel cutPanel = new JPanel();
        cutPanel.setBorder(new TitledBorder("Intersection"));
        cutPanel.setLayout(new GridLayout(2, 2, 10, 10));

        txtCut[0] = new JTextField("" + graOld.getXcutYat());
        txtCut[1] = new JTextField("" + graOld.getYcutXat());
        JLabel lblCutX = new JLabel("X cuts Y at: ", JLabel.RIGHT);
        JLabel lblCutY = new JLabel("Y cuts X at: ", JLabel.RIGHT);
        cutPanel.add(lblCutX);
        cutPanel.add(txtCut[0]);
        cutPanel.add(lblCutY);
        cutPanel.add(txtCut[1]);

        /*lblExample = new JLabel("123 " ,JLabel.CENTER);

         examplePanel.add(lblExample,"BorderLayout.CENTER");*/
        scale.add(cutPanel);
        Action NameAction = new FontAction(cboFontName, cboFontStyle, cboFontSize, lblExample);

        cboFontName.addActionListener(NameAction);
        cboFontSize.addActionListener(NameAction);
        cboFontStyle.addActionListener(NameAction);

        Font f = graOld.getPoliceGraduation();
        cboFontSize.setSelectedItem(Integer.toString(f.getSize()));
        cboFontStyle.setSelectedIndex(f.getStyle());
        cboFontName.setSelectedItem(f.getName());
        return scale;
    }

    /*
     * ******** Onglet Titre ***********************
     */
    public JPanel buildTitlePanel() {
        int i;
        //Box [] bh=new Box[3];
        JPanel title = new JPanel();
        JPanel[] bh = new JPanel[3];
        title.setLayout(new GridLayout(3, 1));
        String[] titre = new String[3];
        titre[0] = graOld.getTitreGraphique();
        titre[1] = graOld.getTitreX();
        titre[2] = graOld.getTitreY();
        Font[] f = new Font[3];
        f[0] = graOld.getPoliceTitre('g');
        f[1] = graOld.getPoliceTitre('x');
        f[2] = graOld.getPoliceTitre('y');
        String[] titrepanel = {"Plot", "Axe X", "Axe Y"};
        String[] position = {"North", "Center", "	South"};
        String[][] fontCar = {{"Serif", "SansSerif", "Monospaced", "Dialog", "DialogInput"}, {"Plain", "Bold", "Italic", "Bold+Italic"}, {"5", "8", "9", "10", "11", "12", "14", "16", "18", "20", "22", "24"}};
        JPanel fontPanelT, fontPanelX, fontPanelY;

        for (i = 0; i < 3; i++) {
            //bh[i] =Box.createHorizontalBox();
            bh[i] = new JPanel();
            bh[i].setLayout(new GridLayout(1, 3, 8, 5));
            //fontPanel[i].setBorder( new TitledBorder(titrepanel[i]) );
            txtonT[i] = new JTextField(titre[i]);
            txtonT[i].getDocument().addDocumentListener(this);
            cboFontNameT[i] = new JComboBox<String>(fontCar[0]);
            cboFontStyleT[i] = new JComboBox<String>(fontCar[1]);
            cboFontSizeT[i] = new JComboBox<String>(fontCar[2]);
            lblExampleT[i] = new JLabel("Example", JLabel.RIGHT);

            //bh[i].add(txtonT[i]);
            bh[i].add(cboFontNameT[i]);
            bh[i].add(cboFontStyleT[i]);
            bh[i].add(cboFontSizeT[i]);
            Action NameAction = new FontAction(cboFontNameT[i], cboFontStyleT[i], cboFontSizeT[i], lblExampleT[i]);

            cboFontNameT[i].addActionListener(NameAction);
            cboFontSizeT[i].addActionListener(NameAction);
            cboFontStyleT[i].addActionListener(NameAction);
            cboFontSizeT[i].setSelectedItem(Integer.toString(f[i].getSize()));
            cboFontStyleT[i].setSelectedIndex(f[i].getStyle());
            cboFontNameT[i].setSelectedItem(f[i].getName());

        }

        fontPanelT = new JPanel();
        fontPanelT.setBorder(new TitledBorder(titrepanel[0]));
        fontPanelT.setLayout(new GridLayout(2, 2, 8, 5));
        fontPanelT.add(txtonT[0]);//"BorderLayout.WEST");
        fontPanelT.add(bh[0]);//,"BorderLayout.CENTER");
        fontPanelT.add(lblExampleT[0]);//"BorderLayout.EAST");
        title.add(fontPanelT);
        fontPanelX = new JPanel();
        fontPanelX.setBorder(new TitledBorder(titrepanel[1]));
        fontPanelX.setLayout(new GridLayout(2, 2, 8, 5));
        fontPanelX.add(txtonT[1]);
        fontPanelX.add(bh[1]);
        fontPanelX.add(lblExampleT[1]);
        title.add(fontPanelX);
        fontPanelY = new JPanel();
        fontPanelY.setBorder(new TitledBorder(titrepanel[2]));
        fontPanelY.setLayout(new GridLayout(2, 2, 8, 5));
        fontPanelY.add(txtonT[2]);
        fontPanelY.add(bh[2]);
        fontPanelY.add(lblExampleT[2]);
        title.add(fontPanelY);
        return title;
    }

    /*
     * ************ Retourne les nouveaux ********************
     * *********** parametres du graphique *******************
     */
    public Plot getNewPreferences() {
        Plot pr;
        if (ok) {
            pr = (Plot) graNew.clone();
        } else {
            pr = (Plot) graOld.clone();
        }
        dispose();
        return pr;
    }

    /*
     * ********** Ecouteur de document **************
     */
    @Override
    public void insertUpdate(DocumentEvent e) {
        TextChange();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        TextChange();
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
    }

    public void TextChange() {  //Pas de source on r�cup�re le champ de tout les textfield
        lblExampleT[0].setText(txtonT[0].getText());
        lblExampleT[1].setText(txtonT[1].getText());
        lblExampleT[2].setText(txtonT[2].getText());

    }

    /*
     * ************ Traitement des Actions Boutons ***********
     */
    public void CancelPressed() {
        this.setVisible(false);

    }

    @Override
    public void dispose() {
        graNew = null;
        graOld = null;
        super.dispose();
    }

    public void OKPressed() {
        //graNew.setpasX((float) Double.parseDouble(txtonX.getText()));

        graNew.setpasX(ValidationText(txtonX, graNew.getpasX()));
        graNew.setpasY(ValidationText(txtonY, graNew.getpasY()));
        graNew.setMaxAxeX(ValidationText(txtaxeXmax, graNew.getMaxAxeX()));
	graNew.setMinAxeX(ValidationText(txtaxeXmin, graNew.getMinAxeX()));
        graNew.setMaxAxeY(ValidationText(txtaxeYmax, graNew.getMaxAxeY()));
        graNew.setMinAxeY(ValidationText(txtaxeYmin, graNew.getMinAxeY()));
        graNew.setPoliceGraduation(lblExample.getFont());
        graNew.setDecimalsurX(lstnbX.getSelectedIndex());
        graNew.setDecimalsurY(lstnbY.getSelectedIndex());
        graNew.setNotationEsurX(chkscienX.isSelected());
        graNew.setNotationEsurY(chkscienY.isSelected());
        graNew.setTitreGraphique(txtonT[0].getText());
        graNew.setTitreX(txtonT[1].getText());
        graNew.setTitreY(txtonT[2].getText());
        graNew.setPoliceTitre('x', lblExampleT[1].getFont());
        graNew.setPoliceTitre('y', lblExampleT[2].getFont());
        graNew.setPoliceTitre('g', lblExampleT[0].getFont());
        graNew.setXcutYat((ValidationText(txtCut[0], graNew.getXcutYat())));
        graNew.setYcutXat((ValidationText(txtCut[1], graNew.getYcutXat())));
        //  System.out.println("Clone du plot dans Ok " + graNew.toString());
        PlotSerie st = (PlotSerie) graNew.ListeSerie.get(0);

        //System.out.println("Clone du de la s�rie dans Ok " + st.toString());
        // System.out.println("Couleur de la s�rie dans Ok " + st.getNameY());
        ok = true;

        this.setVisible(false);
    }

    /*
     * **************** Fonction utilitaires ******************
     */
    private double ValidationText(JTextField txt, double Old) {
        try {
            Old = Double.parseDouble(txt.getText());
        } catch (RuntimeException ex) {
        }
        return Old;
    }

    private int FontDecomposition(String[] Ensemble, String s) {
        int n = Ensemble.length;
        int i;
        for (i = 0; i < n; i++) {
            if (s.equals(Ensemble[i])) {
                return i;
            }
        }
        return 0;
    }
    /*
     * ********************************************************
     */
    private Plot graOld, graNew;
    private final boolean xnonModal;
    private boolean ok;
    private JTextField txtaxeXmin, txtaxeXmax, txtaxeYmin, txtaxeYmax;
    private JTextField txtonX, txtonY;
    private JCheckBox chkscienX, chkscienY;
    private JComboBox lstnbX, lstnbY;
    private JComboBox cboFontName, cboFontSize, cboFontStyle;
    private JLabel lblExample;

    private final JComboBox[] cboFontNameT = new JComboBox[3];
    private final JComboBox[] cboFontSizeT = new JComboBox[3];
    private final JComboBox[] cboFontStyleT = new JComboBox[3];
    private final JTextField[] txtonT = new JTextField[3];
    private final JTextField[] txtCut = new JTextField[2];

    private final JLabel[] lblExampleT = new JLabel[3];

}

class buildPatternPanel {

    buildPatternPanel(Plot plot) {

        String[] line$ = {"Solid", "Dash", "Dot", "Dash Dot", "Dash Dot Dot"};
        String[] point$ = {"None", "Cross", "Square", "Triangle"};

        int nb = plot.nbSerie;
        listeSeries = plot.getListeSeries();
        for (int i = 0; i < nb; i++) {
            modele.addElement("Series " + (i + 1));
        }
        lstSeries.setModel(modele);
        for (int i = 0; i < line$.length; i++) {
            cboFigureLine.addItem(line$[i]);
        }
        for (int i = 0; i < point$.length; i++) {
            cboFigurePoint.addItem(point$[i]);
        }
        pattern.setLayout(gridBagLayout2);
        jScrollPane1.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)), "Series"));
        jScrollPane1.setBounds(new Rectangle(11, 23, 114, 128));
        jScrollPane1.getViewport().add(lstSeries, null);
        jPanLinePoint.setBounds(new Rectangle(139, 31, 336, 258));
        jPanLinePoint.setLayout(gridLayout1);
        gridLayout1.setRows(3);
        jPanLine.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)), "Line"));
        jPanLine.setLayout(null);
        jPanPoint.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)), "Point"));
        jPanExample.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)), "Example"));

        pattern.add(jPanLinePoint, new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(31, 13, 11, 10), 0, 33));
        pattern.add(jScrollPane1, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(31, 12, 141, 0), 102, 100));

        jPanLinePoint.add(jPanLine, null);

        jPanLinePoint.add(jPanPoint, null);
        jPanLinePoint.add(jPanExample, null);
        jPanLine.setLayout(gridBagLayout1);
        jPanLine.add(cboFigureLine, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(10, 17, 10, 0), 7, 1));
        jPanLine.add(SelectColorLine, new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 20, 10, 13), 3, -24));
        jPanPoint.setLayout(gridBagLayout1);
        jPanPoint.add(cboFigurePoint, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(10, 17, 10, 0), 7, 1));
        jPanPoint.add(SelectColorPoint, new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(10, 20, 10, 13), 3, -24));
        jPanLinePoint.add(jPanExample, null);

        lstSeries.setSelectedIndex(0);
        selectedSerie = (PlotSerie) listeSeries.get(0);
        cboFigureLine.setSelectedIndex(selectedSerie.getFigureLigne());
        cboFigurePoint.setSelectedIndex(selectedSerie.getMark());
        SelectColorLine.setSelectedColor(selectedSerie.getCouleur());
        SelectColorPoint.setSelectedColor(selectedSerie.getDefaultCouleurPoints());

        Action paintExample = new PatternAction(listeSeries, lstSeries, jPanExample, cboFigureLine, SelectColorLine, cboFigurePoint, SelectColorPoint);
        SelectColorLine.cboColor.addActionListener(paintExample);
        SelectColorPoint.cboColor.addActionListener(paintExample);
        cboFigureLine.addActionListener(paintExample);
        cboFigurePoint.addActionListener(paintExample);
        lstSeries.addMouseListener(new java.awt.event.MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                selectedSerie = (PlotSerie) listeSeries.get(lstSeries.getSelectedIndex());
                //System.out.println(selectedSerie.getNameY()+"  "+selectedSerie.getDefaultCouleurPoints().toString());
                cboFigureLine.setSelectedIndex(selectedSerie.getFigureLigne());
                cboFigurePoint.setSelectedIndex(selectedSerie.getMark());
                SelectColorLine.setSelectedColor(selectedSerie.getCouleur());
                SelectColorPoint.setSelectedColor(selectedSerie.getDefaultCouleurPoints());
            }
        });

    }

    public JPanel getPanel() {
        return pattern;
    }
    JPanel pattern = new JPanel();
    JScrollPane jScrollPane1 = new JScrollPane();
    JList<String> lstSeries = new JList<String>();
    DefaultListModel<String> modele = new DefaultListModel<String>();
    JPanel jPanLinePoint = new JPanel();
    GridLayout gridLayout1 = new GridLayout();
    JPanel jPanLine = new JPanel();
    JPanel jPanPoint = new JPanel();

    JComboBox<String> cboFigureLine = new JComboBox<String>();
    SelecteurCouleur SelectColorLine = new SelecteurCouleur();
    SelecteurCouleur SelectColorPoint = new SelecteurCouleur();
    JComboBox<String> cboFigurePoint = new JComboBox<String>();
    GridBagLayout gridBagLayout1 = new GridBagLayout();
    GridBagLayout gridBagLayout2 = new GridBagLayout();
    PanelPlot jPanExample = new PanelPlot();
    PlotSerie selectedSerie;

    private Vector listeSeries;
//Plot plot;
}

class buildLegendPanel extends JPanel
        implements DocumentListener {

    buildLegendPanel(Plot plot) {
        super();

        String[] fontName = {"Serif", "SansSerif", "Monospaced", "Dialog", "DialogInput"};
        String[] fontStyle = {"Plain", "Bold", "Italic", "Bold+Italic"};
        String[] fontSize = {"5", "8", "9", "10", "11", "12", "14", "16", "18", "20", "22", "24"};
        cboFontName = new JComboBox<String>(fontName);
        cboFontSize = new JComboBox<String>(fontSize);
        cboFontStyle = new JComboBox<String>(fontStyle);
        Action NameAction = new FontAction(cboFontName, cboFontStyle, cboFontSize, lblExample);
        cboFontName.addActionListener(NameAction);
        cboFontSize.addActionListener(NameAction);
        cboFontStyle.addActionListener(NameAction);
        int nb = plot.nbSerie;
        listeSeries = plot.getListeSeries();
        for (int i = 0; i < nb; i++) {
            modele.addElement("Series " + (i + 1));
        }
        lstSeries.setModel(modele);
        this.setLayout(gridBagLayout1);
        jScrollPane1.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)), "Series"));
        jPanLegend.setLayout(new GridLayout(2, 1));

        jPanTextFont.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)), "Text & Font"));
        jPanTextFont.setLayout(new GridLayout(2, 1, 1, 5));
        jPanFont.setLayout(new GridLayout(1, 3, 8, 5));

        chkShowLegend.setText("Show Legend");
        txtText.setText("");
        txtText.getDocument().addDocumentListener(this);
        lblExample.setHorizontalAlignment(SwingConstants.CENTER);
        lblExample.setHorizontalTextPosition(SwingConstants.CENTER);
        //lblExample.setText("Example");
        chkShowLegend.setSelected(plot.ShowLegend);
        p = plot;
        chkShowLegend.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                p.ShowLegend = chkShowLegend.isSelected();
            }
        });
        this.add(jPanLegend, new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(31, 0, 81, 6), -71, 6));
        jPanLegend.add(jPanTextFont, null);
        jPanTextFont.add(jPanFont, null);
        jPanFont.add(txtText, null);
        jPanFont.add(cboFontName, null);
        jPanFont.add(cboFontStyle, null);
        jPanFont.add(cboFontSize, null);
        jPanTextFont.add(lblExample, null);
        jPanLegend.add(chkShowLegend, null);
        this.add(jScrollPane1, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(31, 12, 141, 0), -180, -40));
        jScrollPane1.getViewport().add(lstSeries, null);

        lstSeries.addListSelectionListener(new javax.swing.event.ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (selectedSerie != null) {
                    selectedSerie.setFont(lblExample.getFont());
                    selectedSerie.setLegende(lblExample.getText());
                }
                selectedSerie = (PlotSerie) listeSeries.get(lstSeries.getSelectedIndex());
                Font f = selectedSerie.getFont();
                cboFontSize.setSelectedItem(Integer.toString(f.getSize()));
                cboFontStyle.setSelectedIndex(f.getStyle());
                cboFontName.setSelectedItem(f.getName());
                txtText.setText(selectedSerie.getLegende());
            }
        });
        /*
         lblExample.addPropertyChangeListener(new java.beans.PropertyChangeListener() {

         public void propertyChange(PropertyChangeEvent e) {

         if(selectedSerie!=null)
         {
         selectedSerie.setFont(lblExample.getFont());
         selectedSerie.setLegende(lblExample.getText());
         }
         }
         });
         */

        lstSeries.setSelectedIndex(0);
    }

    public void chkShowLegend_actionPerformed(ActionEvent e, Plot p) {
        p.ShowLegend = chkShowLegend.isSelected();
    }

    /*
     * ********** Ecouteur de document **************
     */
    @Override
    public void insertUpdate(DocumentEvent e) {
        TextChange();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        TextChange();
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        TextChange();
    }

    public void TextChange() {  //Pas de source on r�cup�re le champ de tout les textfield
        lblExample.setText(txtText.getText());
        if (selectedSerie != null) {
            selectedSerie.setFont(lblExample.getFont());
            selectedSerie.setLegende(lblExample.getText());
        }

    }

    GridBagLayout gridBagLayout1 = new GridBagLayout();
    JScrollPane jScrollPane1 = new JScrollPane();
    JPanel legend = new JPanel();
    JPanel jPanLegend = new JPanel();
    JPanel jPanTextFont = new JPanel();
    JList<String> lstSeries = new JList<String>();
    JCheckBox chkShowLegend = new JCheckBox();

    JComboBox cboFontName;
    JComboBox cboFontStyle;
    JComboBox cboFontSize;
    JTextField txtText = new JTextField();
    JLabel lblExample = new JLabel();
    JPanel jPanFont = new JPanel();

    PlotSerie selectedSerie;
    Plot p;
    DefaultListModel<String> modele = new DefaultListModel<String>();
    private Vector listeSeries;
}
