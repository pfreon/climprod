/**
 * Titre : Climprod<p>
 * Gestion partie droite de fenetre principale "Current known facts". Comment. 2020. 
 */
package fr.ird.climprod;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


public class Cadre_Faits extends JFrame {
  JScrollPane jScrollPane1 = new JScrollPane();
  JTextArea jTextAreaFaits = new JTextArea();

  public Cadre_Faits() {
    try {
      jbInit();
      UtilCadre.Size(this, 60,60);
      UtilCadre.Centrer(this);
    }
    catch(Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {
    jTextAreaFaits.setLineWrap(true);
    jTextAreaFaits.setWrapStyleWord(true);
    this.addWindowListener(new java.awt.event.WindowAdapter() {

      public void windowActivated(WindowEvent e) {
        this_windowActivated(e);
      }
    });

    this.addWindowListener(new java.awt.event.WindowAdapter() {

      public void windowClosing(WindowEvent e) {
        Global.CadreFaits=null;
      }
    });

    this.getContentPane().add(jScrollPane1, BorderLayout.CENTER);
    jScrollPane1.getViewport().add(jTextAreaFaits, null);
  }

  void this_windowActivated(WindowEvent e) {
      jTextAreaFaits.setText(makeFaits());
  }

  private String makeFaits(){
  String s="\n";
  String faits="";
  if(Global.nom_fichier!=null){


    faits=        "Data file path: " +  Global.nom_fichier+s+s;
    faits=faits + "Number of observed data (years): " +Data.getNbYears() + s+s;
    //System.out.println("Flag ligne 60 Cadre_Faits.java Global.envir_preponderant = " + Global.envir_preponderant);

    if(Global.envir_preponderant !=-1 || Global.environmental_influence!="")
    {
        if(Global.envir_preponderant ==1 )
              faits=faits+ "Influence of fishing effort on CPUE is preponderant"+s;
        else if(Global.envir_preponderant ==2)
              faits=faits+"Influence of Environment on CPUE is preponderant"+s;
        if(Global.nb_classes_exploitees!=-1)
        {
          faits=faits + "Number of significantly exploited year-classes: " +Global.nb_classes_exploitees + s;
          if(Global.environmental_influence!="")
              faits=faits + "Environmental influence: " +Global.environmental_influence + s;
          faits=faits + "Age at recruitment: " +Global.recruitment_age + s;
          faits=faits + "Age at the begining of environmental influence: " +Global.begin_influence_period+ s;
          faits=faits + "Age at the end of environmental influence: " +Global.end_influence_period + s+s;

          String[] cpu_relation={"","Linear", "Exponential","General ","Power","Quadratic","Exponential_Additif","Exponential_Not_Additif"};
          //String[] cpu_e={"Linear [CPUE=a+b.E]", "General [CPUE=(a+b.E)^(1/(c-1))]","Exponential [CPUE=a.exp(b.E)]"};
          if(Global.relationCPU_E!=0)
              faits=faits + "Relation between CPUE and E: " +cpu_relation[Global.relationCPU_E] + s;
          //String[] cpu_v  ={"Linear [CPUE=a+b.V]","General [CPUE=a+b.V^c]","Power [CPUE=a.V^b]","Quadratic: [CPUE=a.V+b.V^2+c]"};
          if(Global.relationCPU_V!=0)
              faits=faits + "Relation between CPUE and V: " +cpu_relation[Global.relationCPU_V] + s;

         }
         if(Global.modelisationOk)
         {
           faits=faits +s+ "Selected model: " +RechercheModele.getEquation() + s;
           faits=faits + "Number of years used to fit the model: " +Data.getNbDataRetenue() + s+s;

         // faits=faits +"Parameters"+"\t"+"Actual value"+"    "+"\t"+"Initial value"+s;
           String[][] res$=(String[][]) Modele.getResult();

           for(int i=0;i<Global.nbre_param+1;i++)
           {
                for(int j=0;j<res$[i].length;j++)
                  //faits=faits+"    "+res$[i][j]+"    "+"\t";
                  faits=faits+res$[i][j]+"\t";
                faits=faits+s;

           }
           faits=faits+s;
           for(int i=Global.nbre_param+1;i<res$.length;i++)
           {
                for(int j=0;j<res$[i].length;j++)
                {
                   if(res$[i][j]!=null)
                       faits=faits+res$[i][j]+"\t";
                }
                faits=faits+s;

           }
           if(Global.validationOk)
           {
           	  if(Global.r_jk != 0.00) 
                faits=faits+s+"Jackknife coefficient of determination R²: " + Global.r_jk+s;
              else 
                faits=faits+s+"Jackknife coefficient of determination R²***: " + Global.r_jk+s; 
        		faits=faits+s+"***Jackknife R² estimate was negative and consequently set to zero.\nThis result is due to poor fitting by the Marquart algorithm\nand it is commonly observed when the model does not include\nan intercept parameter or when the model includes a non-linear function.\nConsequently the display of the jackknife R² graph is not available.";
              if(Global.test_jackknife)
                faits=faits+"T_Jackknife: good"+s;
              else
                faits=faits+"T_Jackknife: bad"+s;

           }
         }

      }
  }
  else
  {
      faits=        "Data file: No selected file.";
  }
  return faits;
  }

  //private String faits="";

}