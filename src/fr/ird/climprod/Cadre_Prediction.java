/**
 * Titre : Climprod<p>
 * Displays and allows changes in the prediction table main menu "Modelization" -> "Use the model for prediction". Comment 2020.
 */
package fr.ird.climprod;

import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.text.*;
import javax.swing.table.*;
import java.awt.event.*;
import javax.swing.text.*;
import java.util.Locale;

public class Cadre_Prediction extends JDialog implements DocumentListener {

    private JFrame parent;
    
    public Cadre_Prediction(JFrame parent) {
        super(parent);
        this.parent = parent;
        enableEvents(AWTEvent.WINDOW_EVENT_MASK);
        nbYears = Data.getNbYears() + 2;
        data = new double[nbYears][4];
        double[][] c = Data.getData();
        for (int i = 0; i < nbYears - 2; i++) {
            System.arraycopy(c[i], 0, data[i], 0, 4);
        }

        data[nbYears - 2][0] = data[nbYears - 3][0] + 1;
        data[nbYears - 1][0] = data[nbYears - 2][0] + 1;
        data[nbYears - 1][2] = data[nbYears - 2][2] = data[nbYears - 3][2];
        data[nbYears - 1][3] = data[nbYears - 2][3] = data[nbYears - 3][3];
        init_val();
        try {
            initWindow();
            renseignerTable();

            this.setSize(800, 500);
            UtilCadre.Centrer(this);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void init_val() {
        int i, d;
        calcul_coeff();
        int nim = nbYears - decalmax;
        year = new double[nim];
        yexp = new double[nim];
        pue = new double[nim];
        f = new double[nim];
        fb = new double[nim];
        v = new double[nim];
        vbar = new double[nim];
        for (i = 0; i < nim; i++) {
            year[i] = data[i + decalmax][0];
            yexp[i] = data[i + decalmax][1]; // Catches
            v[i] = data[i + decalmax][3];
            fb[i] = data[i + decalmax][2];   // E
            f[i] = 0;
            vbar[i] = 0;
            for (d = 0; d <= decalmax; d++) {
                f[i] += data[i + decalmax - d][2] * fcoeff[d];
                vbar[i] += data[i + decalmax - d][3] * vcoeff[d];
            }
            pue[i] = yexp[i] / data[i + decalmax][2];

        }

    }

    /**
     *
     */
    private void calcul_coeff() {
        int nbexploit = Global.nb_classes_exploitees;
        int agerec = Global.recruitment_age;
        int begining = Global.begin_influence_period;
        int ending = Global.end_influence_period;
        //System.out.println(nbexploit+"****"+agerec+ "**********" +begining+"******"+ending);

        if (nbexploit < 1) {
            nbexploit = 1;
        }
        if (agerec < 1) {
            agerec = 0;
        }
        if (agerec > 8) {
            agerec = 8;
        }
        if (ending < begining) {
            begining = ending;
        }

        double[][] t = new double[16][21];
        int ageder;
        double eps = 1.0E-12;
        double dtot;
        for (int d = 0; d < 13; d++) {
            fcoeff[d] = 0;
        }
        for (int d = 0; d < 13; d++) {
            vcoeff[d] = 0;
        }
        dtot = 0;
        for (int d = 0; d < nbexploit; d++) {
            dtot += (nbexploit - d);
        }
        for (int d = 0; d < nbexploit; d++) {
            fcoeff[d] = (nbexploit - d) / dtot;
        }
        decalmax = nbexploit - 1;
        for (int i = 0; i < 15; i++) {
            for (int j = 0; j < 21; j++) {
                t[i][j] = 0;
            }
        }
        ageder = agerec + nbexploit - 1;
        dtot = 0;

        for (int i = 0; i < nbexploit; i++) {
            for (int d = begining; d <= ending; d++) {
                t[i][d + i] = 1;
            }
        }

        for (int d = 0; d < ageder + 1; d++) {
            for (int i = 0; i < nbexploit; i++) {
                vcoeff[ageder - d] += t[i][d];
                dtot += t[i][d];
            }
        }
        for (int d = 0; d < ageder + 1; d++) {
            vcoeff[d] /= dtot;
        }

        int d = ageder;

        while (vcoeff[d] < eps) {
            d--;
        }
        if (d > decalmax) {
            decalmax = d;
        }

        if (Global.numero_modele == -2) {
            decalmax = 0;
        }

    }
    /*
     Donne le tableau de donn�es (attention prend en cpte le d�calage)
     @return Object[]]
     */

    private void renseignerTable() {
        int c = year.length;
        String[] title$ = {"Year/season  ", "Obs. Catches", "Effort (E)", "Environment (V)", "Computed E", "Computed V", "Obs. CPUE", "Pred. CPUE"};
        Object[][] data$ = new String[c][8];
        Locale loc = new Locale("en", "US");
        NumberFormat nf;
        nf = NumberFormat.getNumberInstance(loc);
        nf.setMaximumFractionDigits(4);
        nf.setGroupingUsed(false);
        for (int i = 0; i < c; i++) {         // c = nb of years
            data$[i][0] = nf.format(year[i]);
            data$[i][1] = nf.format(yexp[i]); // Catches
            data$[i][2] = nf.format(fb[i]);   // E
            data$[i][3] = nf.format(v[i]);    // V
            data$[i][4] = nf.format(f[i]);    // Averaged-Weighted E
            data$[i][5] = nf.format(vbar[i]); // Averaged-Weighted V
            data$[i][6] = nf.format(pue[i]);  // Observed CPUE
            double e = EquationModele.fonction_modele(f[i], v[i], vbar[i], Global.val_param);
            data$[i][7] = nf.format(e);       // Predicted CPUE
        }
        data$[c - 2][1] = data$[c - 1][1] = "-";
        data$[c - 2][6] = data$[c - 1][6] = "-";
        if (!prediction) {
            data$[c - 2][4] = data$[c - 1][4] = "-";
            data$[c - 2][5] = data$[c - 1][5] = "-";
            data$[c - 2][7] = data$[c - 1][7] = "-";
        }
        ModeleDeTablePredict mdt = new ModeleDeTablePredict(data$, title$);
        jTablePrediction.setModel(mdt);
        jTablePrediction.setRowHeight(25);
        editorComponent.selectAll();
        TableCellEditor jtce = new DefaultCellEditor(editorComponent);
        TableCellRenderer jtcr = new editTableCellRenderer();
        /*TableColumnModel tcm=jTablePrediction.getColumnModel();

         TableColumn tc=tcm.getColumn(2);
         tc.setCellEditor(jt);
         tc=tcm.getColumn(3);
         tc.setCellEditor(jt);*/
        jTablePrediction.getColumnModel().getColumn(2).setCellEditor(jtce);
        jTablePrediction.getColumnModel().getColumn(3).setCellEditor(jtce);
        jTablePrediction.getColumnModel().getColumn(2).setCellRenderer(jtcr);
        jTablePrediction.getColumnModel().getColumn(3).setCellRenderer(jtcr);
        // jTablePrediction.setDefaultRenderer(jTablePrediction.getColumnClass(2),new editTableCellRenderer());

    }

    private int nbYears;
    private double[][] data;
    private double[] year;
    private double[] yexp;
    private double[] pue;
    private double[] f;
    private double[] fb;
    private double[] v;
    private double[] vbar;
    private int decalmax = 0;

    private double[] fcoeff = new double[13];
    private double[] vcoeff = new double[13];

    private boolean prediction = false;

    JLabel lblInfo = new JLabel();
    JScrollPane jScrollPane1 = new JScrollPane();
    JTable jTablePrediction = new JTable();
    JPanel jPanCmd = new JPanel();
    JButton cmdPredict = new JButton();
    JButton cmdCancel = new JButton();
    private doubleTextField editorComponent = new doubleTextField();

    private void initWindow() throws Exception {
        lblInfo.setText("Please change the latest 2 years values in red characters and click on 'Compute prediction'");
        lblInfo.setForeground(Color.red);
        cmdPredict.setText("Compute prediction");
        jPanCmd.setLayout(new GridLayout(1, 3, 5, 0));
        cmdPredict.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                cmdPredict_actionPerformed(e);
            }
        });
        cmdCancel.setText("Close (ESC)");
        int condition = JComponent.WHEN_IN_FOCUSED_WINDOW;
        KeyStroke keyStroke2 = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
        InputMap inputMap2 = cmdCancel.getInputMap(condition);
        ActionMap actionMap2 = cmdCancel.getActionMap();
        inputMap2.put(keyStroke2, keyStroke2.toString());
        actionMap2.put(keyStroke2.toString(), new AbstractAction() {            
            @Override
            public void actionPerformed(ActionEvent arg0) {
                cmdCancel.doClick();
            }
        });
        cmdCancel.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                cancel();
            }
        });
        jPanCmd.add(cmdPredict, 0);
        jPanCmd.add(cmdCancel, 1);
        jTablePrediction.setCellSelectionEnabled(true);
        jTablePrediction.setRowSelectionAllowed(false);

        this.getContentPane().add(lblInfo, BorderLayout.NORTH);
        this.getContentPane().add(jScrollPane1, BorderLayout.CENTER);
        this.getContentPane().add(jPanCmd, BorderLayout.SOUTH);
        jScrollPane1.getViewport().add(jTablePrediction, null);
        jTableCopyPaste PreToClipboard = new jTableCopyPaste(jTablePrediction, true, false);

    }

    @Override
    protected void processWindowEvent(WindowEvent e) {
        if (e.getID() == WindowEvent.WINDOW_CLOSING) {
            cancel();
        }
        super.processWindowEvent(e);
    }

    void cancel() {
        dispose();
    }

    void cmdPredict_actionPerformed(ActionEvent e) {
        if (jTablePrediction.isEditing()) {
            jTablePrediction.getCellEditor().stopCellEditing(); //Controle l'�dition en cours
        }
        ModeleDeTablePredict mdt = (ModeleDeTablePredict) jTablePrediction.getModel();
        int mdtRowC = mdt.getRowCount();
        for (int i = 0; i < 2; i++) {
            data[nbYears - 2 + i][2] = Double.parseDouble((String) mdt.getValueAt(mdtRowC - 2 + i, 2));
            data[nbYears - 2 + i][3] = Double.parseDouble((String) mdt.getValueAt(mdtRowC - 2 + i, 3));
            //System.out.println(data[nbYears-2+i][2] + "  " +data[nbYears-2+i][3]);
        }

        prediction = true;
        init_val();
        renseignerTable();
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
    }

}

/**
 * Modèle de table gérant l'affichage dans jTabStat.
 */
class ModeleDeTablePredict extends AbstractTableModel {

    public ModeleDeTablePredict(Object[][] dataTable, String[] col) {
        columnNames = col;
        data = dataTable;
        newValueOk = true;

    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public Object getValueAt(int row, int col) {
        return data[row][col];
    }

    @Override
    public int getRowCount() {
        return data.length;
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        return ((row > data.length - 3) && (col == 2 || col == 3));

    }

    @Override
    public String getColumnName(int col) {
        return columnNames[col];
    }

    @Override
    public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

    /**
     * Modifie la valeur dans la table (Seuls les doubles sont admis ).
     */
    @Override
    public void setValueAt(Object value, int row, int col) {
        try {
            double res = Double.parseDouble(value.toString().trim());

            data[row][col] = Double.toString(res);
            fireTableCellUpdated(row, col);
            newValueOk = true;
        } catch (RuntimeException ex) {
            newValueOk = false;
            MsgDialogBox msg = new MsgDialogBox(0, "Invalid format number:" + value, 1, Global.CadreMain);
        }

    }

    final String[] columnNames;
    final Object[][] data;
    private boolean newValueOk;
}

/**
 *
 */
class editTableCellRenderer implements TableCellRenderer {

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int col) {
        TableModel mdt = table.getModel();

        jt.setBorder(null);

        jt.setText((String) value);
        if (mdt.isCellEditable(row, col)) {
            jt.setForeground(Color.red);
        } else {
            jt.setForeground(Color.black);
        }

        return jt;
    }

    private final JTextField jt = new JTextField();

}

/**
 * Permet de valider uniquement les entr�es de nombre dans un composant
 * JTextField
 */
class doubleTextField extends JTextField {

    public doubleTextField() {
        super("");
    }

    @Override
    protected Document createDefaultModel() {
        return new doubleTextDocument();
    }

}

/**
 * Ne valide que les nombres de types double
 */
class doubleTextDocument extends PlainDocument {

    @Override
    public void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
        if (str == null) {
            return;
        }
        String oldString = getText(0, getLength());
        String newString = oldString.substring(0, offs) + str + oldString.substring(offs);
        try {
            if (!newString.equals("-")) {
                Double.parseDouble(newString);
            }
            super.insertString(offs, str, a);

        } catch (NumberFormatException e) {
        }
    }

}
