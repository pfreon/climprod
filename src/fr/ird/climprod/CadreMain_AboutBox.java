// Fenetre Help ->About du menu principal pour présenter copyright FAO-IRD.
package fr.ird.climprod;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

public class CadreMain_AboutBox extends JDialog implements ActionListener {
	
  JPanel panel1 = new JPanel();
  JPanel jPanAbout = new JPanel();
  JPanel jPanCmd = new JPanel();
  JPanel jPanImage = new JPanel();
  JPanel jPanText = new JPanel();
  JButton cmdOK = new JButton();
  JLabel imageIconeClimprod = new JLabel();
  JLabel imageIconeIRD = new JLabel();
  JLabel imageIconeFAO = new JLabel();
  ImageIcon imageIconClimprod;
  ImageIcon imageLogoIRD;
  ImageIcon imageLogoFAO;
  
  BorderLayout borderLayout1 = new BorderLayout();
  BorderLayout borderLayout2 = new BorderLayout();
  FlowLayout flowLayout1 = new FlowLayout();
  FlowLayout flowLayout2 = new FlowLayout();
  GridLayout gridLayout1 = new GridLayout();
 // String product = "Climprod";
 // String version = "1.0";
  JTextArea jTextAreaCopyRight = new JTextArea();


  public CadreMain_AboutBox(Frame parent) {
    super(parent);
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try {
      jbInit();
    }
    catch(Exception e) {
      e.printStackTrace();
    }
    imageIconeClimprod.setIcon(imageIconClimprod);
    imageIconeIRD.setIcon(imageLogoIRD);
    imageIconeFAO.setIcon(imageLogoFAO);
    pack();
  }

  private void jbInit() throws Exception  {
    imageIconClimprod = new ImageIcon(getClass().getResource("resources/images/Climprod2.jpg"));  // Avant .ico
    imageLogoIRD = new ImageIcon(getClass().getResource("resources/images/logo_IRD.jpg"));
    imageLogoFAO = new ImageIcon(getClass().getResource("resources/images/logo_FAO.jpg"));
    this.setTitle("About");
    setResizable(true); // Avant false 2021
    panel1.setLayout(borderLayout1);
    jPanAbout.setLayout(borderLayout2);
    jPanCmd.setLayout(flowLayout1);
    jPanImage.setLayout(flowLayout1);
    jPanImage.setBorder(new EmptyBorder(10, 20, 0, 0)); //1er = haut, 2ème = gauche, 3ème = bas; 4ème chiffre = marge droite
    jPanText.add(jTextAreaCopyRight, null); // Utile? Effet non visible.
    gridLayout1.setRows(4);
    gridLayout1.setColumns(1);
    
    jPanText.setLayout(gridLayout1);
    jPanText.setBorder(new EmptyBorder(10, 10, 10, 10));
    cmdOK.setText("Ok");
    cmdOK.addActionListener(this);
    int condition = JComponent.WHEN_IN_FOCUSED_WINDOW;
    KeyStroke keyStrokep = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
    InputMap inputMapp = cmdOK.getInputMap(condition);
    ActionMap actionMapp = cmdOK.getActionMap();
    inputMapp.put(keyStrokep, keyStrokep.toString());
    actionMapp.put(keyStrokep.toString(), new AbstractAction() {            
        @Override
        public void actionPerformed(ActionEvent arg0) {
            cmdOK.doClick();
        }
    }); 
    jTextAreaCopyRight.setPreferredSize(new Dimension(450, 550));
    jTextAreaCopyRight.setWrapStyleWord(true);
    jTextAreaCopyRight.setRows(20);
    jTextAreaCopyRight.setLineWrap(true);
    jTextAreaCopyRight.setDoubleBuffered(true);
    jTextAreaCopyRight.setText(
    " [©FAO-IRD 2003]\n"+
   "COPYRIGHT: All rights reserved. Reproduction and dissemination " +
   "of material contained on FAO\'s and IRD\'s Web sites for educational " +
   "or other non-commercial purposes are authorized without any prior " +
   "written permission from the copyright holders provided the source " +
   "is fully acknowledged. Reproduction of material for resale or other " +
   "commercial purposes is prohibited without the written permission " +
   "of the copyright holders. Applications for such permission should " +
   "be addressed to the: Chief, Publishing Management Service, FAO, Viale n" +
   "delle Terme di Caracalla 00100 Rome, Italy. e-mail: copyright@fao.org " +
   "and to Service des Editions, IRD, 213 Rue Lafayette, 75480 Paris Cedex 10, " +
   "France.\n\n"+
   "DISCLAIMER:The designations employed and the presentation of material " +
   "in this information product do not imply the expression of any opinion " +
   "whatsoever on the part of the Food and Agriculture Organization of the " +
   "United Nations  (FAO) and of the Institut Français de Recherche  pour " +
   "le Développement (IRD) concerning the legal or development status of any " +
   "country, territory, city or area or of its authorities, or concerning the " +
   "delimitation of its frontiers or boundaries.\n" +
   "FAO and IRD decline all responsibility for errors or deficiencies in the database " +
   "or software or in the documentation accompanying it, for program maintenance " +
   "and upgrading swell as for any damage that may arise from them. " +
   "FAO and IRD also decline any responsibility for updating the data and assumes  " +
   "no responsibility for errors and omissions in the data provided. Users are, " +
   "however, kindly asked to report any errors or deficiencies in this product " +
   "to FAO and IRD.\n\n"+
   "Note: Hyperlinks to non-FAO and non-IRD Internet sites do not imply " +
   "any official endorsement of or responsibility or the opinions, ideas, " +
   "for the opinions, ideas, data or products presented at these locations, " +
   "or guarantee the validity of the information provided. The sole purpose " +
   "of links to non-FAO and non-IRD sites is to indicate further information " +
   "available on related topics.");
   
    jTextAreaCopyRight.setAutoscrolls(true);
    setResizable(true);
    jTextAreaCopyRight.setEditable(false);
    jTextAreaCopyRight.setMargin(new Insets(5, 5, 5, 0));
    jTextAreaCopyRight.setFont(new java.awt.Font("Serif", 0, 13)); // Fenetre Help ->About du menu principal. Avant 0, 11. Modif 2020.
    jPanText.setLayout(flowLayout1);
    cmdOK.addActionListener(this);
    jPanImage.add(imageIconeClimprod, null);
    jPanImage.add(imageIconeFAO, null);
    jPanImage.add(imageIconeIRD, null); 
    jPanAbout.add(jPanImage, BorderLayout.WEST);
    jPanImage.add(jTextAreaCopyRight, null);
    this.getContentPane().add(panel1, null);

    jPanAbout.add(jPanText, BorderLayout.CENTER);
    jPanAbout.add(jPanCmd, BorderLayout.SOUTH);
    panel1.add(jPanCmd, BorderLayout.CENTER);
    jPanCmd.add(cmdOK, null);
    panel1.add(jPanAbout, BorderLayout.NORTH);
    borderLayout2.setHgap(5);
    borderLayout2.setVgap(5);
  }

  protected void processWindowEvent(WindowEvent e) {
    if (e.getID() == WindowEvent.WINDOW_CLOSING) {
      cancel();
    }
    super.processWindowEvent(e);
  }

  void cancel() {
    dispose();
  }

  public void actionPerformed(ActionEvent e) {
    if (e.getSource() == cmdOK) {
      cancel();
    }
  }
}