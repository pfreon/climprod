
/**
 * Titre : InfoBancs<p>
 * Description : <p>
 * Copyright : Copyright (c) <p>
 * Soci�t� : <p>
 * @author
 * @version 1.0
 */
package fr.ird.climprod;


import javax.swing.JTextField;
import java.awt.event.*;
import javax.swing.event.*;
import javax.swing.text.*;

public class integerTextField extends changeTextField {

  public integerTextField(int loverLimit,int upperLimit) {
    super( );
    this.loverLimit=loverLimit;
    this.upperLimit=upperLimit;
  /*  this.addInputMethodListener(new java.awt.event.InputMethodListener() {

      public void caretPositionChanged(InputMethodEvent e) {
      }

      public void inputMethodTextChanged(InputMethodEvent e) {
        change=true;
      }
    });*/
  }


  public integerTextField() {
     this(-Integer.MAX_VALUE,Integer.MAX_VALUE);

  }




  public int getTextToInteger(){
     try
       {
           return Integer.parseInt(this.getText());
       }
       catch(NumberFormatException e)
       {
          return 0;
       }


  }

  protected Document createDefaultModel(){
         return new integerTextDocument();
  }


  /**
  *
   */
  class integerTextDocument extends PlainDocument{

      public void insertString(int offs,String str, AttributeSet a) throws BadLocationException
      {
            if(str==null) return;
            String oldString=getText(0,getLength());
            String newString=oldString.substring(0,offs)+str+oldString.substring(offs);
            if(newString.equals("-")) newString="-0";
            try
            {
                int newValue=Integer.parseInt(newString);
                if(newValue>=loverLimit &&  newValue<=upperLimit)
                         super.insertString(offs,str,a);

             }
             catch(NumberFormatException e)
             {
             }
      }
  }

  private int loverLimit;
  private int upperLimit;


}
