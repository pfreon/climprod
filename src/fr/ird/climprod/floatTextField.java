
/**
 * Titre : InfoBancs<p>
 * Description : <p>
 * Copyright : Copyright (c) <p>
 * Soci�t� : <p>
 * @author
 * @version 1.0
 * Gestion des textes et de l'interaction avec clavier et souris (commentaire 2020).
 */
package fr.ird.climprod;

import javax.swing.JTextField;
import java.awt.event.*;


import javax.swing.event.*; // Package java de gestion de l'interaction avec clavier et souris (commentaire 2020).
import javax.swing.text.*;  // Package java de gestion des textes (commentaire 2020).

public class floatTextField extends changeTextField {

  public floatTextField(float loverLimit,float upperLimit) {
    super();
    this.loverLimit=loverLimit;
    this.upperLimit=upperLimit;
  /*      this.addInputMethodListener(new java.awt.event.InputMethodListener() {

      public void caretPositionChanged(InputMethodEvent e) {
      }

      public void inputMethodTextChanged(InputMethodEvent e) {
        change=true;
      }
    });*/
  }


  public floatTextField() {
     this(-Float.MAX_VALUE,Float.MAX_VALUE);

  }
  /*  public boolean isChanged(){
        return change;
  }*/
  /* public void setChanged(boolean b){
       change=b;
  }*/


  protected Document createDefaultModel(){
         return new floatTextDocument();
  }

  public float getTextToFloat(){
     try
       {
           return Float.parseFloat(this.getText());
       }
       catch(NumberFormatException e)
       {
          return 0;
       }


  }


  /**
  *
   */
  class floatTextDocument extends PlainDocument{

      public void insertString(int offs,String str, AttributeSet a) throws BadLocationException
      {
            if(str==null) return;
            String oldString=getText(0,getLength());
            String newString=oldString.substring(0,offs)+str+oldString.substring(offs);
            if(newString.equals("-")) newString="-0";
            try
            {
                float newValue=Float.parseFloat(newString);
                if(newValue>=loverLimit &&  newValue<=upperLimit)
                         super.insertString(offs,str,a);
             }
             catch(NumberFormatException e)
             {
             }
      }
  }

  private float loverLimit;
  private float upperLimit;
  private boolean change=false;
}