/**
 */
package fr.ird.climprod;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

class ExporterImage {

    private final BufferedImage img;
    private final String fileName;

    public ExporterImage(Image im, String titre) {
        img = (BufferedImage) im;
        fileName = titre;
    }

    public void write() {
        try {
            ImageIO.write((BufferedImage) img, "jpeg", new File(fileName));
        } catch (IOException ex) {
            Logger.getLogger(ExporterImage.class.getName()).log(Level.SEVERE, "Erreur lors de la sauvegarde de l'image graphique.", ex);
        }
    }
}
