/**
 * Titre : Climprod<p>
 * Classe statique qui regroupe l'ensemble des variables globales avec en
 * commentaire les questions correspondantes lorsque relevant.
 * Donne les titres de certains graphiques et les appelle.
 * Calcul du test de Fisher/**
 * Titre : Climprod<p>
 * Classe statique qui regroupe l'ensemble des variables globales avec en
 * commentaire les questions correspondantes lorsque relevant.
 * Donne les titres de certains graphiques et les appelle.
 * Calcul du test de Fisher.
 * @version 1.0
 */
package fr.ird.climprod;

import javax.swing.JFrame;
import java.util.HashMap;
import org.apache.commons.math3.special.Gamma;
import java.io.InputStream;
import java.util.StringTokenizer;

public class Global {
    static final String[] influenceEnv = {"", "abundance", "catchability", "both"};//probl�me du both
    static JFrame CadreQuestion;                   //Une seule instance est permise pour l'instant     
    static JFrame CadreModeleDirect;
    static JFrame CadreModele;
    static JFrame CadreResultats;
    static JFrame CadreTimePlots;
    static JFrame CadrePlotHistogr;
    static JFrame CadreBiVariatePlots;
    static JFrame CadreFittedPlots;
    static JFrame CadreResidualPlots;
    static JFrame CadreJackniffePlots;
    static JFrame CadreThreeVariatePlots;
    static JFrame CadreMSPlots;
    static JFrame CadreFaits;
    
    static JFrame CadreMain;
    /**
     * ******* Hypothèses de base & données*******
     */

    static double temp;
    static double TimeStepData;				  // Time step of the row data (default Year, decimal intervals mean seasonal data
    static String TimeStepData$;
    static boolean range_data;                            // Does  the  relative  range  of  observed  value<0.4
    static String double_click;	                          // Averstissement usage double click. Rajout 2020
    static int dbl_click; 
    static int changement_exploitation;                   // Have there been changes in the fishing pattern during the period (effort allocation, quota, mesh-size ...)
    static int metapopulation;                            // Does the data-set apply to a sub-stock
    static int stock_unique;                              // Does the data-set apply to a single stock
    static int stock_divise;                              // Is the single stock subdivided into various geographical sub-stocks (all must be exploited by the fleet)
    static int unite_standardisee;                        // Is the fishing effort unit standardized and  is the CPUE proportional to abundance
    static int effet_delais_abundance_negligeable;        // Do time-lags and  deviations from the stable age structure have negligible effects on production rate
    static String pdf$ = "resources/Freon_et_al_FAO_Manuel_Climprod_1993.pdf"; // Name of the pdf file of Help menu
    /**
     * ******* Stabilite de la modélisation *****
     */
    static int under_and_over_exploited;                  // Do you think that the data-set covers periods both of overexploitation and of underexploitation
    static int under_and_optimaly;                        // Do you think that the data-set cover periods both of underexploitation and optimal exploitation
    static int statistiques_anormales;                    // Did you see any abnormal statistics in the previous table
    static int unstability;                               // Is  interannual  variability  too  large
    static int independance;                              // Are  the  two  variables  independent
    static int abnormal_points_scatt;                     // Do  you  see  outlier  points
    static int abnormal_points_dist;                      // Do  you  see  outlier  points
    static int effort_increasing;                         // Constantly  increasing  effort

    static int decreasing_relationship;                   // Does  this  plot  appear  to  be  decreasing
    static int envir_preponderant;                       // Is the influence of fishing effort on CPUE more important than environmental influence

    /*
     * *********Réponses aux questions***************************
     */
    static int climatic_influence;
    static int linear_relationship;                      // Does  this  plot  look  linear
    static int monotonic_relationship;                    // Does  this  plot  look  monotonic

    static int obviously;                                 // Does  this  plot  look  obviously  linear
    static int good_results;                              // Is  this  an  acceptable  model
    static int trend_residuals;                           // Good  fit  and  no  trend  or  autocorrelation  in  resid.

    static String environmental_influence;                // Does the environment influence:
    static int lifespan;                                  // What is the life span of the species
    static int nb_classes_exploitees;                     // Number of significantly exploited year-classes
    static int recruitment_age;                           // Age at recruitment
    static int begin_influence_period;                    // Age at the begining of environmental influence
    static int end_influence_period = 1;                    // Age at the end of environmental influence

    static int cpue_unstable;                             // Is there a strong instability in the cpue time series
    static int reserves_naturelles;                       // Are there natural protected areas for the stock or constantly inacessible adult biomass
    static int fecondite_faible;                          // Is the fecundity of the species very low (sharks, mammals, etc.)
    static int premiere_reproduction_avant_recrutement;   // Are there one or several non negligible spawnings before recruitment
    static int rapport_vie_exploitee_inferieur_deux;      // Is the ratio (lifespan/number of exploited year-classes) lower than 2
    static int sous_stock_isole;                          // Is the sub-stock well isolated, (i.e. with few exchanges) from others
    static int stock_deja_effondre;                       // Did the stock already collapse or exhibit drastic decrease(s) in catches
    static int pessimiste;                                // Do you have any (additional) reason to expect highly unstable behaviour or collapse of the stock
    static int cpue_sous_sur_production;                  // May the stock present large fluctuations in CPUE when overexploited
    /*
     * *********Validation***************************
     */
    static double fF, r_jk;  
    static String fFsignif$;
    static String [] message$ ={"","","","","","","","","","","","","","",""};
    static int Min_95_Noteworthy_MSE;
    static int Min_95_Noteworthy_MSY;
    static int Max_95_Noteworthy_MSE;
    static int Max_95_Noteworthy_MSY;
	static int[] AllPredictedNegative = new int[5];
	static boolean JackknifePlot;    // Flag graphique Jackknife en onglets en même temps que questions. Rajout 2020
    static int Flag_pb_jackknife_Tot;
    static int[] Flag_pb_jackknife=new int[4];						      
    static boolean test_jackknife;
    static String [] param$={"a","b","c","d"};            // Rajout 2020.
    static String[] Test_Jackknife_Param$={" (p<0.01)"," (p<0.05)"," (p>0.05)"," na"}; // Rajout 2020.
    static int[] Res_Test_Jackknife_Par = new int[4];     // Rajout 2020. int[] a = new int[1];
    static int coeff_determination_instable;			  // Reasonable jackknife coefficient R2 (>65% recommended), no extreme yearly coefficient, and acceptable MSY graph
	static int acceptable_graphs;                         // Graphs MSY & MSE acceptable
    /**
     * *******Variables de travail ********
     */
    static boolean bavard = false;                          // Do you want to trace all the procedure of  rule application
	static boolean FlagNewHtmlFolder = false;
    static String nom_fichier = null;                         //Name of the data file
    static int decalmax = 0;
    static int etapeEnCours = 0;                            //num�ro d'�tape dans la fixation du modele
    static final int questionM = 1;                         //Etape1
    static final int fixationM = 2;                         //Etape 2 recherche du mod�le
    static final int validationM = 3;                       //Etape 3 Validation du mod�le

    static String datafilePath = System.getProperty("user.home");

    /**
     * ************ Modèle en cours *************
     */
    static int typeModele;                                // Type Simple-Climat-Mixte
    static int relationCPU_E;                             // Type relation entre CPU et E
    static int relationCPU_V;                             // Type relation entre CPU et V
    static int numero_modele;                             // Current model
    static int nbre_param;                                // Number of parameters
    static double[] val_param;                            // Value of parameters
    static double coeff_determination;                    // R2 
    static double corrected_R2;							  // corrected R2 according to the number of variables
    static double critereAIC;                             // Akaike
    static double critereAIC_Corr;                        // Akaike corrigé
    static double critereBIC;
    static boolean validationOk;                          // true si Validation exécutée
    static boolean modelisationOk;                        // true si mod�le ajusté
    static boolean MSY_MSE_OK;							  // true si pas d'anomalie dans résultats MSY et MSE (boolean ne marche pas avec boolean b4 dans CadreMain.java)
    static int Compteur_erreurs_Jackknife;                // Rajout 2020.
	static String Texte_erreur_jackknife$;        		  // Rajout 2020.
	
    /**
     * *********** Plots ************************
     */
	//static int[]  typePlot = {2, 2, 2, 2, 3, 3, 3, 3, 1, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 3, 2, 2,2,2,2,2};
	//static Hashtable htPlot=new Hashtable();
	//WARNING: NE PAS REMPLACER R2 PAR R² DANS LIGNES SUIVANTES CAR SINON NON DE FICHIER PAR RECONNU PAR HTML.
    static String[] titreG = {"Y Time Plot (unlagged)", "CPUE Time Plot (unlagged)", "E Time Plot (unlagged)", "V Time Plot (unlagged)", "E Distribution (unlagged)", "V Distribution (unlagged)", "Y Distribution (unlagged)", "CPUE Distribution (unlagged)", "Y vs E (unlagged)", "Y vs V (unlagged)", "CPUE vs E (unlagged)", "CPUE vs V (unlagged)", "CPUE versus E (E weighted if relevant)", "CPUE versus V (V lagged if relevant)","E versus V (unlagged)", "Observed and Fitted CPUE (lag effect if relevant)", "Time Plot of residual CPUE (lag effect if relevant)", "Residual CPUE vs CPUE","Residual CPUE versus E (E weighted if relevant)", "Residual CPUE versus V (V lagged and weighted if relevant)", "a (%) Time Plot (lag effect if relevant)", "b (%) Time Plot (lag effect if relevant)", "c (%) Time Plot (lag effect if relevant)", "d (%) Time Plot (lag effect if relevant)", "R2 (%) Time Plot", "Function Y=f(V & E) (lagged if relevant)", "Function Y=f(V & E) (E weighted if relevant)", "Function CPUE=f(V & E)(E weighted if relevant)", "Function CPUE=f(V & E) (E weighted if relevant)", "MSY versus V", "MSE versus V"};
    static String[] titreSx = {"Year (or single annual season)", "Year (or single annual season)", "Year (or single annual season)", "Year (or single annual season)", "Classes", "Classes", "Classes", "Classes", "Effort (E)", "Environment (V)", "Effort (E)", "Environment (V)", "Environment (V)", "Year (or single annual season)", "Year (or single annual season)", "CPUE", "Effort (E)", "Environment (V)", "Year (or single annual season)", "Year (or single annual season)", "  Year (or single annual season)", "Year (or single annual season)", "Year (or single annual season)", "Weighted effort (E)", "Weighted effort (E)", "Weighted effort (E)", " Weighted effort (E)", "Environment (V)", "Environment (V)"};
    static String[] titreSy = {"Catches (Y)", "CPUE ", "Effort (E)", "Environment (V)", "Nb", "Nb", "Nb", "Nb", "Catches (Y)", "Catches (Y)", "CPUE", "CPUE", "CPUE", "CPUE", "    Residual", "Residual", "Residual", "a (%)", "b (%)", "c (%)", "d (%)", "R² (%)", "Catches (Y)", "Catches (Y)", "CPUE", "CPUE", "MSY", "MSE"};

    static Plot[] timePlot = new Plot[4];
    static PlotModal[] distriPlot = new PlotHisto[4];
    static Plot[] scatterPlot = new Plot[4];
    static Plot[] lagPlot = new Plot[2];
    static Plot indePlot = new Plot();
    static Plot[] fittedCpuePlot = new Plot[3];
    static Plot[] residualsPlot = new Plot[2];
    static PlotHisto[] jackknifePlot;
    static Plot[] variatePlot = new Plot[2];
    static Plot[] msyPlot = new Plot[2];
    static HashMap<Integer, String> questionDic = new HashMap<Integer, String>();
    static HashMap<Integer, String> answerDic = new HashMap<Integer, String>();
    static HashMap<Integer, String> warningDic = new HashMap<Integer, String>();

    public static void init() {
        if (CadreModeleDirect != null) {
            CadreModeleDirect.dispose();
        }
        if (CadreModele != null) {
            CadreModele.dispose();
        }
        if (CadreQuestion != null) {
            CadreQuestion.dispose();
        }
        if (CadreResultats != null) {
            CadreResultats.dispose();
        }
        if (CadreBiVariatePlots != null) {
            CadreBiVariatePlots.dispose();
        }
        if (CadreTimePlots != null) {
            CadreTimePlots.dispose();      
        }
        if (CadrePlotHistogr != null) {
            CadrePlotHistogr.dispose();      
        }
        if (CadreFittedPlots != null) {
            CadreFittedPlots.dispose();
        }
        if (CadreResidualPlots != null) {
            CadreResidualPlots.dispose();
        }
        if (CadreJackniffePlots != null) {
            CadreJackniffePlots.dispose();
        }
        if (CadreThreeVariatePlots != null) {
            CadreThreeVariatePlots.dispose();
        }
        if (CadreMSPlots != null) {
            CadreMSPlots.dispose();
        }

        range_data = false;           // Does  the  relative  range  of  observed  value<0.4
        changement_exploitation = -1; // Have there been changes in the fishing pattern during the period (effort allocation, quota, mesh-size ...)
        metapopulation = -1;          // Does the data-set applies to a metapopulation?
        stock_unique = -1;            // Does the data-set apply to a single stock
//      stock_divise = -1;            // Supprimé en 2020 car redondant avec metapopulation. La variable metapopulation est utilisée en remplacement dans les règles concernées. Is the single stock subdivided into various geographical sub-stocks (all must be exploited by the fleet)
        unite_standardisee = -1;      // Is the fishing effort unit standardized and  is the CPUE proportional to abundance
        effet_delais_abundance_negligeable = -1; //	Do time-lags and  deviations from the stable age structure have negligible effects on production rate

        /**
         * ******* Stabilite de la mod�lisation *****
         */
        under_and_over_exploited = -1;// Do you think that the data-set covers periods both of overexploitation and of underexploitation
        under_and_optimaly = -1;      // Do you think that the data-set cover periods both of underexploitation and optimal exploitation
        statistiques_anormales = -1;  // Did you see any abnormal statistics in the previous table
        unstability = -1;             // Is  interannual  variability  too  large
        independance = -1;            // Are  the  two  variables  independent
        abnormal_points_scatt = -1;   // Do  you  see  outlier  points
        abnormal_points_dist = -1;    // Do  you  see  outlier  points
        effort_increasing = -1;       // Constantly  increasing  effort

        decreasing_relationship = -1; // Does  this  plot  appear  to  be  decreasing
        envir_preponderant = -1;     // Is the influence of fishing the environment on CPUE more important than the influence of fishing effort influence

        /**
         * ************************************
         */
        climatic_influence = 1;
        linear_relationship = -1;     // Does  this  plot  look  linear
        monotonic_relationship = -1;  // Does  this  plot  look  monotonic

        obviously = -1;               // Does  this  plot  look  obviously  linear
        good_results = -1;            // Is  this  an  acceptable  model
        trend_residuals = -1;         // Good  fit  and  no  trend  or  autocorrelation  in  resid.

        environmental_influence = "";     // Does the environment influence:
        lifespan = -1;                       // What is the life span of the species
        nb_classes_exploitees = -1;//1;           // Number of significantly exploited year-classes
        recruitment_age = -1;//0;                 // Age at recruitment
        begin_influence_period = 0;          // Age at the begining of environmental influence
        end_influence_period = 0;//0;            // Age at the end of environmental influence

        cpue_unstable = -1;           // Is there a strong instability in the cpue time series
        reserves_naturelles = -1;     // Are there natural protected areas for the stock or constantly inacessible adult biomass
        fecondite_faible = -1;        // Is the fecundity of the species very low (sharks, mammals, etc.)
        premiere_reproduction_avant_recrutement = -1; //	Are there one or several non negligible spawnings before recruitment
        rapport_vie_exploitee_inferieur_deux = -1; //	Is the ratio (lifespan/number of exploited year-classes) lower than 2
        sous_stock_isole = -1; //	Is the sub-stock well isolated, (i.e. with few exchanges) from others
        stock_deja_effondre = -1; //	Did the stock already collapse or exhibit drastic decrease(s) in catches
        pessimiste = -1; //	Do you have any (additional) reason to expect highly unstable behaviour or collapse of the stock

        cpue_sous_sur_production = -1; //	May the stock present large fluctuations in CPUE when overexploited

        r_jk = 0.0d; //	Reasonable jackknife coefficient R2 (>65% recommended), no extreme yearly coefficient, and acceptable MSY graph
        test_jackknife = false;
        coeff_determination_instable = -1;
        /**
         * *******Variables de travail ********
         */

        decalmax = 0;
        etapeEnCours = 0;                    //num�ro d'�tape dans la fixation du modele

        /**
         * ************ Modèle en cours *************
         */
        typeModele = 0;               // Type Simple-Climat-Mixte
        relationCPU_E = 0;           // Type relation entre CPU et E
        relationCPU_V = 0;           // Type relation entre CPU et V
        numero_modele = -2;           // Current model
        nbre_param = 0;               // Number of parameters
        coeff_determination = 0;   //R2
        critereAIC = 0;
        critereAIC_Corr = 0;
        modelisationOk = false;
        validationOk = false;     // true si Validation exécutée
		//bavard=true;
        questionDic.clear();
        answerDic.clear();
        warningDic.clear();
    }
    
    public static double gam(double x){
        return(Gamma.gamma(x));
    }

    // Lecture tables de Fisher et Student
    
   public static double[][] TableFisher_p_01; 
   public static double[][] TableFisher_p_05;
   public static double[][] TableStudent;  
   private static boolean existFile = false;
   public static void initScript(InputStream file, InputStream fileFisher_p_05, InputStream fileStudent) 
   { // Lecture fichier TableFisherSnedecor_p_0.01.csv
     existFile = false; 
     try { 
       String[] dataLine;   
       ReadFileText rft = new ReadFileText(file);
       dataLine = rft.getLines();
       TableFisher_p_01 = new double[100][4];
            if (dataLine != null) //Lecture ok
            {
                StringTokenizer t = new StringTokenizer(dataLine[0], ";");
                if (t.countTokens() != 4) {
                    throw new OnError("Invalid Fisher's table p 0.01 data file.");
                }
                for (int i = 0; i < 100; i++) {
                    StringTokenizer d = new StringTokenizer(dataLine[i], ";");
                    if (d.countTokens() != 4) {
                        throw new OnError("Invalid Fisher's table p 0.01 data file.");
                    }
                    int k = 0;
                    while (d.hasMoreTokens()) {
                        TableFisher_p_01[i][k] = Double.parseDouble(d.nextToken().trim());
                        k = k + 1;
                    }
                }
            }
            existFile = true;

	   ReadFileText rft2 = new ReadFileText(fileFisher_p_05);
       dataLine = rft2.getLines();
       TableFisher_p_05 = new double[100][4];
            if (dataLine != null) //Lecture ok
            {
                StringTokenizer t = new StringTokenizer(dataLine[0], ";");
                if (t.countTokens() != 4) {
                    throw new OnError("Invalid Fisher's table p 0.05 data file.");
                }
                for (int i = 0; i < 100; i++) {
                    StringTokenizer d = new StringTokenizer(dataLine[i], ";");
                    if (d.countTokens() != 4) {
                        throw new OnError("Invalid Fisher's table p 0.05 data file.");
                    }
                    int k = 0;
                    while (d.hasMoreTokens()) {
                        TableFisher_p_05[i][k] = Double.parseDouble(d.nextToken().trim());

                        k = k + 1;
                    }
                }
            }
            existFile = true;
       
	   ReadFileText rft3 = new ReadFileText(fileStudent); // First column = p<0.05; second p<0.01
       dataLine = rft3.getLines();
       TableStudent = new double[50][2];
            if (dataLine != null) //Lecture ok
            {
                StringTokenizer t = new StringTokenizer(dataLine[0], ";");
                if (t.countTokens() != 2) {
                    throw new OnError("Invalid Student's table data file.");
                }
                for (int i = 0; i < 50; i++) {
                    StringTokenizer d = new StringTokenizer(dataLine[i], ";");
                    if (d.countTokens() != 2) {
                        throw new OnError("Invalid Student's table data file.");
                    }
                    int k = 0;
                    while (d.hasMoreTokens()) {
                        TableStudent[i][k] = Double.parseDouble(d.nextToken().trim());
                        k = k + 1;
                    }
                }
            }
            existFile = true;
            
     } 
     	catch (Exception e) {
            MsgDialogBox msg = new MsgDialogBox(0, e.getMessage(), 0, Global.CadreMain);
        }  
   }

    public static double RangeEffort(){ // Computes effort range.   
             double[] effort=Data.getF();
             double[] ext=Stat1.Extremas(effort);
             return (((ext[1]-ext[0])/ext[0])); 
    }

    public static double RangeYear(){ // Computes year range.   
             double[] year=Data.getYears();
             double[] ext=Stat1.Extremas(year);
             return (ext[1]-ext[0]+1); 
    } 
         
    public static double fF(double R2, double p, double n){ // Fisher Fcalc; p = nombre de paramètres autres que constante (a en général)
        return((R2/p)/((1 - R2) / (n-p-1)));
    }
}