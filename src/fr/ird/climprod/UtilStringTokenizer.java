
/**
 * Titre : Climprod<p>
 * La classe StringTokenizer ne consid�re pas comme champs de donn�es
 * les champs vierges (2 s�parateurs cons�cutifs).
 * Cette classe permet de s�parer 2 s�parateurs cons�cutifs dans
 * une cha�ne de caract�res et permet aussi de renseigner le
 * "champ" vierge.
 */
package fr.ird.climprod;


public class UtilStringTokenizer {

  public UtilStringTokenizer(String str,String delim) {
       this(str,delim," ");
  }
  /**
  *Construit une nouvelle cha�ne � partir de str, en ins�rant entre 2
  * d�limiteurs cons�cutifs repr�sent�s par la cha�ne delim,la cha�ne replace.
  * @param String str la cha�ne � analyser.
  * @param String delim le s�parateur.
  * @param String replace la cha�ne � inserer.
  */
  public UtilStringTokenizer(String str,String delim,String replace) {
    if (str.equals(""))
      str=replace;    //chaine vide
    if (str.startsWith(delim))
      str=replace+str;//premier champ vide
    if (str.endsWith(delim))   //dernier champ vide
      str=str+replace;
    String vide$=delim+delim;  //champ vide
    int delimLength=delim.length()-1;
    int vide=str.indexOf(vide$,0);
            while (vide!=-1)   //tant qu'il y a des champs vides...
            {
              str=str.substring(0,vide)+ delim + replace + str.substring(vide+1);
              vide=str.indexOf(vide$,vide+1);
            }
            newToken=str;

 }
 public String getNewString(){
   return newToken;
 }

private String newToken;

}