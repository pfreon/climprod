package fr.ird.climprod;
// Gère fenêtre principale intitulée "Climprod x"

import static fr.ird.climprod.Data.getNbDataRetenue;
import static fr.ird.climprod.Validation.getParamResult;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

import java.io.*;
import java.net.URL;
import java.io.IOException;

import javax.swing.filechooser.FileFilter;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.Hashtable;
import java.util.HashMap;
import javax.swing.table.TableColumn;
import javax.swing.border.TitledBorder;
import java.text.DecimalFormat;

public class CadreMain extends JFrame {

    JPanel contentPane;
    JMenuBar menuBar1 = new JMenuBar();
    JMenu menuFile = new JMenu();
        JMenuItem menuFileExit = new JMenuItem();
    
    JMenu menuHelp = new JMenu();
        JMenuItem menuHelpHowTo = new JMenuItem();
        JMenuItem menuHelpReferences = new JMenuItem();
        JMenuItem menuHelpAbout = new JMenuItem();
    
    JMenu menuHelpDoc = new JMenu();
        JMenuItem submenuTuto = new JMenuItem(); //"Tutorial"
        JMenuItem submenuGuide = new JMenuItem(); // "Reference guide" 
    
    JMenuItem menuFileOpen = new JMenuItem();
    JMenuItem menuFileCreate = new JMenuItem();
    JMenuItem menuFileFolder = new JMenuItem();
    JMenu menuFileOpenFolder = new JMenu();
    JMenuItem[] menuItemOpenFolder;
    JMenuItem[] menuFileFile = new JMenuItem[4];

    JMenu menuPlots = new JMenu();
    JMenuItem menuPlotTime = new JMenuItem();
    JMenuItem menuPlotHistogr = new JMenuItem();
    JMenuItem menuPlotBivariate = new JMenuItem();
    JMenuItem menuPlotFitted = new JMenuItem();
    JMenuItem menuPlotResiduals = new JMenuItem();
    JMenuItem menuPlotJacknife = new JMenuItem();
    JMenuItem menuPlotThreeVariate = new JMenuItem();
    JMenuItem menuPlotMsy = new JMenuItem();
    JMenu menuModels = new JMenu();
    JMenuItem menuModelsSelect = new JMenuItem();
    JMenuItem menuModelsDirectly = new JMenuItem();
    JMenuItem menuModelsPrediction = new JMenuItem();
    JMenuItem menuModelsResults = new JMenuItem();
    JMenu menuOptions = new JMenu();
    JCheckBoxMenuItem menuOptionsBavard = new JCheckBoxMenuItem();

    JPanel jPanData = new JPanel();

  //JPanel jPanPlot = new JPanel();
    //GridLayout gridLayout1 = new GridLayout();
    //PanelPlot[] pPlot=new PanelPlot[4];
    JScrollPane jScrollPane1 = new JScrollPane();
    JScrollPane jScrollPane2 = new JScrollPane();
    JScrollPane jScrollPane3 = new JScrollPane();
    JScrollPane jScrollPane4 = new JScrollPane();

    JTable jTableStat = new JTable();
    JTable jTableCorrelation = new JTable();
    JTable jTableData = new JTable();
    GridBagLayout gridBagLayout1 = new GridBagLayout();
    JTextArea jTextAreaFaits = new JTextArea();

    static public String[] cpu_relation_E = {"Unknown yet", "Linear (Graham-Schaefer model)", "Exponential (Fox model)", "Generalized (Pella & Tomlinson model)", "", "", "Exponential_additive", "Not exponential with high variability of CPUE when overexploitation"};
    static public String[] cpu_relation_V = {"Unknown yet", "Linear", "Exponential", "Power", "Power", "Dome-shaped", "", ""};
    private String workingDirectory = "";
    private String userDir = "";
    private String userHome = "";
    private String userName = "";
    private String[] dataFileList = new String[4];
    private String configFile = "";

    private Vector<String> vLastFile = new Vector<String>();

    private String lastPathSource = "";

    public CadreMain() {
        enableEvents(AWTEvent.WINDOW_EVENT_MASK);
        try {
            initWin();
            Global.init();
            Global.CadreMain = this;
            readConfig();
            
            InputStream inRegle = CadreMain.class.getResourceAsStream("resources/regles.csv");
            if (inRegle == null) {
                MsgDialogBox msg = new MsgDialogBox(0, "FATAL ERROR\nFile regles.csv not found", 0, this);
                System.exit(0);
            }
            
            InputStream inComment = CadreMain.class.getResourceAsStream("resources/Comment.csv");
            if (inComment == null) {
                MsgDialogBox msg = new MsgDialogBox(0, "FATAL ERROR\nFile Comment.csv not found", 0, this);
                System.exit(0);
            }
            TexteRegles.initScript(inRegle, inComment);
                                   
            InputStream inRep = CadreMain.class.getResourceAsStream("resources/Arbre_decisions.csv");
            if (inRep == null) {
                MsgDialogBox msg = new MsgDialogBox(0, "FATAL ERROR\nFile Arbre_decisions.csv not found", 0, this);
                System.exit(0);
            }
            QuestionReponse.initScript(inRep);
            
            InputStream inMod = CadreMain.class.getResourceAsStream("resources/ListeModele.csv");
            if (inMod == null) {
                MsgDialogBox msg = new MsgDialogBox(0, "FATAL ERROR\nFile ListeModele.csv not found", 0, this);
                System.exit(0);
            }
            RechercheModele.initScript(inMod);
            
            InputStream inFisher_p_01 = CadreMain.class.getResourceAsStream("resources/TableFisherSnedecor_p_0.01.csv");
            if (inFisher_p_01 == null) {
                MsgDialogBox msg = new MsgDialogBox(0, "FATAL ERROR\nFile TableFisherSnedecor_p_0.01.csv not found", 0, this);
                System.exit(0);
            }
 
            InputStream inFisher_p_05 = CadreMain.class.getResourceAsStream("resources/TableFisherSnedecor_p_0.05.csv");
			if (inFisher_p_01 == null) {
                MsgDialogBox msg = new MsgDialogBox(0, "FATAL ERROR\nFile TableFisherSnedecor_p_0.05.csv not found", 0, this);
                System.exit(0);
            }
            
            InputStream inStudent = CadreMain.class.getResourceAsStream("resources/TableStudent.csv");
			if (inStudent == null) {
                MsgDialogBox msg = new MsgDialogBox(0, "FATAL ERROR\nFile TableStudent.csv not found", 0, this);
                System.exit(0);
            }
            
            Global.initScript(inFisher_p_01, inFisher_p_05, inStudent);
           
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
 /**
  * ********************** Set up the menu and sub-menus of the main frame *******************************
 */
    private void initWin() throws Exception {
        this.setIconImage(Toolkit.getDefaultToolkit().createImage(CadreMain.class.getResource("resources/images/Climprod.jpg")));
        contentPane = (JPanel) this.getContentPane();
        contentPane.setLayout(new GridLayout(1, 2));
        this.setTitle("Climprod 5.3 beta"); // beta version ici
        this.addWindowListener(new java.awt.event.WindowAdapter() {

            public void windowActivated(WindowEvent e) {
                this_windowActivated(e);
            }
        });
        /**
         * ********************** Composants menus*************************************************
         */
        menuFile.setText("Files");
        menuFile.setMnemonic(KeyEvent.VK_F);
        menuFileExit.setText("Quit");
        menuFileExit.setMnemonic(KeyEvent.VK_E);
        menuFileExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q,InputEvent.CTRL_DOWN_MASK));
        menuFileExit.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                fileExit_actionPerformed(e);
            }
        });
        menuHelp.setText("Help");
        menuHelp.setMnemonic(KeyEvent.VK_H);
  
        menuHelpHowTo.setText("Getting started");
        menuHelpHowTo.setMnemonic(KeyEvent.VK_S);
        menuHelpHowTo.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                helpHowTo_actionPerformed(e);
            }
        }); 
        menuHelpDoc.setText("Documentation");
        menuHelpDoc.setMnemonic(KeyEvent.VK_D);
        menuHelpDoc.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                HelpDoc_actionPerformed(e);
            }
        });
        submenuTuto.setText("Tutorial");
        submenuTuto.setMnemonic(KeyEvent.VK_T);
        submenuTuto.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                DocTuto_actionPerformed(e);
            }
        });        
        submenuGuide.setText("Reference guide");
        submenuGuide.setMnemonic(KeyEvent.VK_G);
        submenuGuide.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                DocGuide_actionPerformed(e);
            }
        });
        
        menuHelpReferences.setText("References");
        menuHelpReferences.setMnemonic(KeyEvent.VK_R);
        menuHelpReferences.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                helpReferences_actionPerformed(e);
            }
        });         
        
        menuHelpAbout.setText("About");
        menuHelpAbout.setMnemonic(KeyEvent.VK_A);
        menuHelpAbout.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                helpAbout_actionPerformed(e);
            }
        });
             
        menuFileOpen.setText("Open (*.CLI, *.csv, *.txt)");
        menuFileOpen.setMnemonic(KeyEvent.VK_O);
        menuFileOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O,InputEvent.CTRL_DOWN_MASK));
        menuFileOpen.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                menuFileOpen_actionPerformed(e);
            }
        });

        menuFileCreate.setText("Create a new CLIMPROD data file");
        menuFileCreate.setMnemonic(KeyEvent.VK_C);
        menuFileCreate.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C,InputEvent.CTRL_DOWN_MASK));
        menuFileCreate.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                menuFileCreate_actionPerformed(e);
            }
        });

        menuFileFolder.setText("Build a history html & graphs directory");
        menuFileFolder.setMnemonic(KeyEvent.VK_M);
        menuFileFolder.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M,InputEvent.CTRL_DOWN_MASK));
        menuFileFolder.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                menuFileFolder_actionPerformed(e);
            }
        });

        for (int i = 0; i < 4; i++) {
            menuFileFile[i] = new JMenuItem();
            //final String name=menuFileFile[i].getText();
            menuFileFile[i].addActionListener(new java.awt.event.ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    menuFileOpenLastFile_actionPerformed(e);
                }
            });
        }

        menuPlots.setText("Plots & graphs");
        menuPlots.setMnemonic(KeyEvent.VK_P);

        menuPlotTime.setEnabled(false);
        menuPlotTime.setMnemonic(KeyEvent.VK_T);
        menuPlotTime.setText("Time graphs");
        menuPlotTime.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                menuPlotTime_actionPerformed(e);
            }
        });
        
		menuPlotHistogr.setEnabled(false);
        menuPlotHistogr.setMnemonic(KeyEvent.VK_H);
        menuPlotHistogr.setText("Histograms graphs");
        menuPlotHistogr.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                menuPlotHistogr_actionPerformed(e);
            }
		});        
        menuPlotBivariate.setEnabled(false);
        menuPlotBivariate.setMnemonic(KeyEvent.VK_B);
        menuPlotBivariate.setText("Bivariate plots");
        menuPlotBivariate.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                menuPlotBivariate_actionPerformed(e);
            }
        });
                
        menuPlotFitted.setEnabled(false);
        menuPlotFitted.setMnemonic(KeyEvent.VK_F);
        menuPlotFitted.setActionCommand("CPUE residual analysis graphs");
        menuPlotFitted.setText("CPUE residual analysis graphs");
        menuPlotFitted.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                menuPlotFitted_actionPerformed(e);
            }
        });
        menuPlotResiduals.setEnabled(false);
        menuPlotResiduals.setMnemonic(KeyEvent.VK_R);
        menuPlotResiduals.setText("Residual CPUE vs E & V Plots"); // Modif 2020.
        menuPlotResiduals.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                menuPlotResiduals_actionPerformed(e);
            }
        });
        menuPlotJacknife.setEnabled(false);
        menuPlotJacknife.setMnemonic(KeyEvent.VK_J);
        menuPlotJacknife.setActionCommand("Jacknife bar graphs");
        menuPlotJacknife.setText("Jackknife bar graphs");
        menuPlotJacknife.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                menuPlotJacknife_actionPerformed(e);
            }
        });
        menuPlotThreeVariate.setEnabled(false);
        menuPlotThreeVariate.setMnemonic(KeyEvent.VK_T);
        menuPlotThreeVariate.setText("CPUE=f() & Y=() tri-variate graphs");
        menuPlotThreeVariate.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                menuPlotThreeVariate_actionPerformed(e);
            }
        });
        menuPlotMsy.setEnabled(false);
        menuPlotMsy.setMnemonic(KeyEvent.VK_M);
        menuPlotMsy.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                menuPlotMsy_actionPerformed(e);
            }
        });
        menuPlotMsy.setText("MSY & MSE vs V graphs");
        menuModels.setEnabled(false);
        menuModels.setText("Modelization");
        menuModels.setMnemonic(KeyEvent.VK_M);
        menuModelsSelect.setText("Select the appropriate model and fit it");
        menuModelsSelect.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,InputEvent.CTRL_DOWN_MASK));
        menuModelsSelect.setMnemonic(KeyEvent.VK_S);
        menuModelsSelect.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                menuModelsSelect_actionPerformed(e);
            }
        });
        menuModelsDirectly.setText("Fit a model directly");
        menuModelsDirectly.setMnemonic(KeyEvent.VK_F);
        menuModelsDirectly.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F,InputEvent.CTRL_DOWN_MASK));
        menuModelsDirectly.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                menuModelsDirectly_actionPerformed(e);
            }
        });
        menuOptions.setText("Option");
        menuOptions.setMnemonic(KeyEvent.VK_O);
        menuOptionsBavard.setText("Trace all the procedure");
        menuOptionsBavard.setMnemonic(KeyEvent.VK_T);
        menuOptionsBavard.setSelected(false);
        menuOptionsBavard.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                Global.bavard = menuOptionsBavard.isSelected();

            }
        });

        menuModelsPrediction.setEnabled(false);
        menuModelsPrediction.setText("Intent prediction (allowed if good jackknife test) ");
        menuModelsPrediction.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P,InputEvent.CTRL_DOWN_MASK));
        menuModelsPrediction.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                menuModelsPrediction_actionPerformed(e);
            }
        });
        menuModelsResults.setEnabled(false);
        menuModelsResults.setText("Display result tables");
        menuModelsResults.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R,InputEvent.CTRL_DOWN_MASK));
        menuModelsResults.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                menuModelsResults_actionPerformed(e);
            }
        });
        menuFileOpenFolder.setText("Open an existing html & graphs directory");
        menuPlots.add(menuPlotTime);
        menuPlots.add(menuPlotHistogr);
        menuPlots.add(menuPlotBivariate);
        menuPlots.add(menuPlotFitted);
        menuPlots.add(menuPlotResiduals);
        menuPlots.add(menuPlotJacknife);
        menuPlots.add(menuPlotThreeVariate);
        menuPlots.add(menuPlotMsy);

        menuModels.add(menuModelsSelect);
        menuModels.add(menuModelsDirectly);
        menuModels.addSeparator();
        menuModels.add(menuModelsPrediction);
        menuModels.addSeparator();
        menuModels.add(menuModelsResults);

        menuOptions.add(menuOptionsBavard);

        menuFile.add(menuFileOpen);
        menuFile.add(menuFileCreate);
        menuFile.add(menuFileExit);
        menuFile.addSeparator();
        menuFile.add(menuFileFolder);
        menuFile.add(menuFileOpenFolder);

        menuHelp.add(menuHelpHowTo);
        menuHelp.add(menuHelpDoc);        
            menuHelpDoc.add(submenuTuto); //Adding menu items to the sub-menu
            menuHelpDoc.add(submenuGuide);
        menuHelp.add(menuHelpReferences);
        menuHelp.add(menuHelpAbout);

        menuBar1.add(menuFile);
        menuBar1.add(menuModels);
        menuBar1.add(menuPlots);
        menuBar1.add(menuOptions);
        menuBar1.add(menuHelp);
        /**
         * ********************** Composants panels*************************************************
         */
        jPanData.setLayout(gridBagLayout1);
        jPanData.setVisible(false);
        jPanData.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)), "Data and statistical tables"));

        jTableStat.setFont(new java.awt.Font("Dialog", 0, 12)); // Avant 0, 10. Modif. 2020.
        														// Premier argument type de police de charactere du tableau stat menu principal.
        														// Avant-dernier chiffre controle type de charactere (0= normal, 1 = gras, 2 = italique)
        														// Dernier chiffre controle taille caractères 
        jTableStat.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        jTableStat.setCellSelectionEnabled(true);
        jTableStat.setRowSelectionAllowed(false);
        jTableCorrelation.setFont(new java.awt.Font("Dialog", 0, 12)); // Avant 0, 10. Modif. 2020.
        jTableCorrelation.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        jTableCorrelation.setCellSelectionEnabled(true);
        jTableCorrelation.setRowSelectionAllowed(false);
        jTableData.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        jTableData.setCellSelectionEnabled(true);
        jTableData.setRowSelectionAllowed(false);

        jTableCopyPaste DataToClipboard = new jTableCopyPaste(jTableData, true, false);
        jTableCopyPaste StatToClipboard = new jTableCopyPaste(jTableStat, true, false);
        jTableCopyPaste CorrToClipboard = new jTableCopyPaste(jTableCorrelation, true, false);

        jScrollPane4.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)), "Current  known facts"));
        jTextAreaFaits.setLineWrap(true);
        jTextAreaFaits.setWrapStyleWord(true);
        jTextAreaFaits.setEditable(false);
        jScrollPane1.getViewport().add(jTableData, null);
        jScrollPane2.getViewport().add(jTableStat, null);
        jScrollPane3.getViewport().add(jTableCorrelation, null);
        this.setJMenuBar(menuBar1);

        contentPane.add(jPanData, null);
        jPanData.add(jScrollPane1, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 2, 5, 2), 0, 200));
        jPanData.add(jScrollPane2, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 2, 5, 2), 0, 50));
        jPanData.add(jScrollPane3, new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 2, 5, 2), 0, -25));

        contentPane.add(jScrollPane4, null);
        jScrollPane4.getViewport().add(jTextAreaFaits, null);

   // jPanPlot.setLayout(gridLayout1);
        // gridLayout1.setColumns(2);
        // gridLayout1.setHgap(2);
        // gridLayout1.setRows(4);
        // gridLayout1.setVgap(2);
        //contentPane.add(jPanPlot, null);
    }

    //Operation Fichier | Quitter effectuee
    public void fileExit_actionPerformed(ActionEvent e) {
        try {
            saveConfig();
        } catch (Exception ec) {
        }
        System.exit(0);
    }

    public void helpAbout_actionPerformed(ActionEvent e) {
		//Cadre_SplitPlot dc=new Cadre_SplitPlot(Global.jackknifePlot);
		//dc.setVisible(true);

        CadreMain_AboutBox dlg = new CadreMain_AboutBox(this);
        Dimension dlgSize = dlg.getPreferredSize();
        Dimension frmSize = getSize();
        Point loc = getLocation();
        dlg.setLocation((frmSize.width - dlgSize.width) / 2 + loc.x, (frmSize.height - dlgSize.height) / 2 + loc.y);
        //System.out.println("Dans CadreMain.java ligne 441 fenêtre Help de Menu principal frmSize.width = " + frmSize.width + "  dlgSize.width = " + dlgSize.width + "  frmSize.height = " + frmSize.height + "  dlgSize.height = " + dlgSize.height);
	    dlg.setModal(true);
        dlg.setVisible(true);
    }
    
    public void helpHowTo_actionPerformed(ActionEvent e) {

        CadreMain_HowToBox dlg = new CadreMain_HowToBox(this);
        Dimension dlgSize = dlg.getPreferredSize();
        Dimension frmSize = getSize();
        Point loc = getLocation();
        dlg.setLocation((frmSize.width - dlgSize.width) / 2 + loc.x, (frmSize.height - dlgSize.height) / 2 + loc.y);
        //System.out.println("Dans CadreMain.java ligne 441 fenêtre Help de Menu principal frmSize.width = " + frmSize.width + "  dlgSize.width = " + dlgSize.width + "  frmSize.height = " + frmSize.height + "  dlgSize.height = " + dlgSize.height);
	    dlg.setModal(true);
        dlg.setVisible(true);
    }
 
    public void HelpDoc_actionPerformed(ActionEvent e) {
    }
    
    public void DocTuto_actionPerformed(ActionEvent e) {
    //File pdfFile = new File(getClass().getClassLoader().getResource("Freon_et_al_FAO_Manuel_Climprod_1993.pdf").getFile()); // Ne marche pas, à réparer pour lire fichier pdf dans climprod\resources?
    //AnyPlatformAppPDF.mainPDF(getClass().getClassLoader().getResource("Freon_et_al_FAO_Manuel_Climprod_1993.pdf").getFile()); // Ne marche pas, à réparer pour lire fichier pdf dans climprod\resources?
    AnyPlatformAppPDF.mainPDF("C:/Program Files/CLIMPROD_5.0/Documents/Tutorial_CLIMPROD_5.0.pdf");
    //jarPDF(); // Ne marche pas, à réparer pour lire fichier pdf dans climprod\resources?
    }
    
    public void DocGuide_actionPerformed(ActionEvent e) {
        AnyPlatformAppPDF.mainPDF("C:/Program Files/CLIMPROD_5.0/Documents/Reference_Guide_CLIMPROD_5.0.pdf");
    }
/*        public void jarPDF(){
            InputStream jarPdf = getClass().getClassLoader().getResourceAsStream(Global.pdf$);
//            System.out.println("Global.pdf$: " + Global.pdf$ + jarPdf.getClass()); // 
                        try {
                File pdfTemp = new File("Freon_et_al_FAO_Manuel_Climprod_1993.pdf");
                // Extraction du PDF qui se situe dans l'archive
                FileOutputStream fos = new FileOutputStream(pdfTemp);
                while (jarPdf.available() > 0) {
                      fos.write(jarPdf.read());
                }   // while (pdfInJar.available() > 0)
                fos.close();
                // Ouverture du PDF
                Desktop.getDesktop().open(pdfTemp);
            }   // try
                        catch (Exception ex) {
        ex.printStackTrace();
            }
            }*/
    
    // get file from classpath, resources folder
    private File getFileFromResources() {
        ClassLoader classLoader = getClass().getClassLoader();
        URL resource = classLoader.getResource(Global.pdf$);
        System.out.println("resource = " + resource);
        if (resource == null) {
            throw new IllegalArgumentException("PDF file is not found!");
        } else {
            return new File(resource.getFile());
        }
    }  
    public void helpReferences_actionPerformed(ActionEvent e) {

        CadreMain_ReferencesBox dlg = new CadreMain_ReferencesBox(this);
        Dimension dlgSize = dlg.getPreferredSize();
        Dimension frmSize = getSize();
        Point loc = getLocation();
        dlg.setLocation((frmSize.width - dlgSize.width) / 2 + loc.x, (frmSize.height - dlgSize.height) / 2 + loc.y);
        //System.out.println("Dans CadreMain.java ligne 441 fenêtre Help de Menu principal frmSize.width = " + frmSize.width + "  dlgSize.width = " + dlgSize.width + "  frmSize.height = " + frmSize.height + "  dlgSize.height = " + dlgSize.height);
	    dlg.setModal(true);
        dlg.setVisible(true);
    }

    @Override
    protected void processWindowEvent(WindowEvent e) {
        super.processWindowEvent(e);
        if (e.getID() == WindowEvent.WINDOW_CLOSING) {
            fileExit_actionPerformed(null);
        }
    }

    void menuFileOpen_actionPerformed(ActionEvent e) {
        jPanData.setVisible(false);
        JFileChooser d = new JFileChooser();
        d.setCurrentDirectory(new File(Global.datafilePath));
        d.setDialogTitle("Open a data file");
        Global.init();
        Global.nom_fichier = null;
        //*********************************
        String nomFichier = "";
        //Data.readFile(nomFichier);
        //Global.nom_fichier=nomFichier;
        //this.setData();
        //*************************************************************************
         d.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                return f.getName().toLowerCase().endsWith(".csv") || f.isDirectory();
            }

            @Override
            public String getDescription() {
                return "Data file (*.csv)";
            }
        });
        
         d.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                return f.getName().toLowerCase().endsWith(".txt") || f.isDirectory();
            }

            @Override
            public String getDescription() {
                return "Data file (*.txt)";
            }
        });
        d.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                return f.getName().toLowerCase().endsWith(".cli") || f.isDirectory();
            }

            @Override
            public String getDescription() {
                return "Data file (*.CLI)";
            }
        });
        d.setSelectedFile(new File(nomFichier));
        int res = d.showOpenDialog(this);
		//System.out.println("Flag jTextAreaFaits.setText(makeFaits()) ligne 510 CadreMain.java res = " + res + " nomFichier = " + nomFichier);
        if (res == JFileChooser.APPROVE_OPTION) {
            nomFichier = d.getSelectedFile().getAbsolutePath();
            if (nomFichier != null) {
                Data data = new Data();
                data.readFile(nomFichier);
                Global.nom_fichier = data.getFileName();
                if (Global.nom_fichier != null) {
                    this.setData();
                }
                Global.datafilePath = d.getSelectedFile().getParent();
                updateMenu();
                jTextAreaFaits.setText(makeFaits());
				//System.out.println("Flag jTextAreaFaits.setText(makeFaits()) ligne 523 CadreMain.java");
            }
        }

    }
    /*
     * Création d'un nouveau fichier de données au format Climprod
     */

    void menuFileCreate_actionPerformed(ActionEvent e) {

        CreateNewFile dlg = new CreateNewFile(this);
        dlg.setTitle("Climprod: Climprod file editor ");
        dlg.setModal(true);
        dlg.setVisible(true);
   		//int d=dlg.getResult();

        /*  if(d==dlg.Ok_Option)
         {
         String[] dd=dlg.getFolderName();
         existingFolder(dd);

         }*/
    }


    /*
     * Création d'un folder html
     */
    void menuFileFolder_actionPerformed(ActionEvent e) {

        RapportHtml dlg = new RapportHtml(this, this.makeFaits());
        dlg.setTitle("Climprod: Build an html & graphs directory ");
        dlg.setModal(true);
        dlg.setVisible(true);
		int d;
		if (Global.FlagNewHtmlFolder == true) {
			d = dlg.getResult();
			Global.FlagNewHtmlFolder = false; // (réinitialization)
			System.out.println("In CadreMain ligne 622 d = " + d);

	        if (d == dlg.Ok_Option) {
				System.out.println("In CadreMain ligne 625 Flag");
	            String[] dd = dlg.getFolderName();
				System.out.println("In CadreMain ligne 627 dd = " + dd);
            	existingFolder(dd);
        	}
		}
    }

    /*
     *  Ouvre le folder sélectionné dans le navigateur par défault
     */
    void menuItemOpenFolder_actionPerformed(ActionEvent e) {
   //JMenuItem jmi= (JMenuItem) e.getSource();
        //System.out.println(e.getActionCommand());
        AppelNavigateur.displayURL(e.getActionCommand());
    }

    void menuFileOpenLastFile_actionPerformed(ActionEvent e) {
        JMenuItem jmi = (JMenuItem) e.getSource();
        String nomFichier = jmi.getText();
        //System.out.println("fil " +nomFichier);
        Global.init();
        Global.nom_fichier = null;
        Data data = new Data();
        data.readFile(nomFichier);
        Global.nom_fichier = data.getFileName();
        if (Global.nom_fichier != null) {
            this.setData();
            updateMenu();
            this.this_windowActivated(new WindowEvent(this, WindowEvent.WINDOW_ACTIVATED));
            return;
        }
        MsgDialogBox msg = new MsgDialogBox(0, "File " + nomFichier + " not found.", 0, this);

    }

    void menuPlotTime_actionPerformed(ActionEvent e) {
        if (Global.CadreTimePlots == null) {
            Cadre_Plot dlg;
            dlg = new Cadre_Plot(Global.timePlot);
            dlg.setTitle("Climprod: Time plots");
            dlg.setVisible(true);
            Global.CadreTimePlots = dlg;
        } else {
            Global.CadreTimePlots.setVisible(true);
        }

    }
    
	void menuPlotHistogr_actionPerformed(ActionEvent e) {
        if (Global.CadrePlotHistogr == null) {
            Cadre_Plot dlg;
            dlg = new Cadre_Plot(Global.distriPlot);
            dlg.setTitle("Climprod: Histograms plots");
            dlg.setVisible(true);
            Global.CadrePlotHistogr = dlg;
        } else {
            Global.CadrePlotHistogr.setVisible(true);
        }
	}

    void menuPlotBivariate_actionPerformed(ActionEvent e) {
        if (Global.CadreBiVariatePlots == null) {
            Cadre_Plot dlg;
            dlg = new Cadre_Plot(Global.scatterPlot);
            dlg.setTitle("Climprod: Bivariate plots");
            dlg.setVisible(true);
            Global.CadreBiVariatePlots = dlg;
        } else {
            Global.CadreBiVariatePlots.setVisible(true);
        }

    }

    void this_windowActivated(WindowEvent e) {  // Gestion de l'acces (pas grisé) aux sous-menus de "Plots" du menu principal.
        /* Frame[] liste=this.getFrames();
         for (int i=0;i<liste.length;i++)
         System.out.println(liste[i].getName());*/
        boolean b1 = false; 
        boolean b2 = false;
        boolean b3 = false;
        boolean b4 = false;
        boolean b5 = false;
        boolean b6 = false;
       
        if (Global.nom_fichier != null) {
            b1 = true;                  // Autorise Menus Files et Modelization + Time plots + Histogram plots + Bivariate plots
            b3 = Global.validationOk;   // Autorise Jackknife plots
            b2 = Global.modelisationOk; // Autorise Observed-fitted & residual CPUE + Resid f(E) vs V et f(V) vs E + CPUE = f() & Y = f()
			b6 = Global.MSY_MSE_OK;		// Co-autorise graphiques MSY et MSE					// 			
										// b4 autorise graphiques MSY et MSE (Global.MSY_MSE_OK ne marche pas si boolean)
			b4 = b3 && b6 && (((Global.numero_modele > 5) || (Global.numero_modele < 2)) && (Global.numero_modele != 20) && (Global.numero_modele != 33));
            b5 = Global.test_jackknife; // Autorise sous-menu Use the model for prediction
        }
        this.jPanData.setVisible(b1);
        this.menuFileOpenFolder.setEnabled(b1);
        this.menuFileFolder.setEnabled(b1);
        this.menuModels.setEnabled(b1);
        this.menuModelsResults.setEnabled(b2);
        // this.menuEdition.setEnabled(b1);
        //  this.menuEditionResults.setEnabled(b2);
        this.menuPlots.setEnabled(b1);
        this.menuPlotTime.setEnabled(b1);
        this.menuPlotHistogr.setEnabled(b1);
        this.menuPlotBivariate.setEnabled(b1);
        this.menuPlotFitted.setEnabled(b2);
        this.menuPlotResiduals.setEnabled(b2);
        this.menuPlotThreeVariate.setEnabled(b2);
        this.menuPlotJacknife.setEnabled(b3);
        this.menuPlotMsy.setEnabled(b4);
        this.menuModelsPrediction.setEnabled(b5);
        jTextAreaFaits.setText(makeFaits());
		//System.out.println("Flag jTextAreaFaits.setText(makeFaits()) ligne 658 CadreMain.java");
    }

    void menuPlotFitted_actionPerformed(ActionEvent e) {
        if (Global.CadreFittedPlots == null) {
            Cadre_Plot dlg;
            dlg = new Cadre_Plot(Global.fittedCpuePlot);
            dlg.setTitle("Climprod: Fitted plots");
            dlg.setVisible(true);
            Global.CadreFittedPlots = dlg;
        } else {
            Global.CadreFittedPlots.setVisible(true);
        }
    }

    void menuPlotResiduals_actionPerformed(ActionEvent e) {
        if (Global.CadreResidualPlots == null) {
            Cadre_Plot dlg;
            dlg = new Cadre_Plot(Global.residualsPlot);
            dlg.setTitle("Climprod: Residuals plots");
            dlg.setVisible(true);
            Global.CadreResidualPlots = dlg;
        } else {
            Global.CadreResidualPlots.setVisible(true);
        }
    }

    void menuPlotJacknife_actionPerformed(ActionEvent e) {
        if (Global.CadreJackniffePlots == null) {
            Cadre_Plot dlg;
            dlg = new Cadre_Plot(Global.jackknifePlot);
            dlg.setTitle("Climprod: Jackknife plots");
            dlg.setVisible(true);
            Global.CadreJackniffePlots = dlg;
        } else {
            Global.CadreJackniffePlots.setVisible(true);
        }
    }

    void menuPlotThreeVariate_actionPerformed(ActionEvent e) {
        if (Global.CadreThreeVariatePlots == null) {
            Cadre_Plot dlg;
            dlg = new Cadre_Plot(Global.variatePlot);
            dlg.setTitle("Climprod: CPUE=f() & Y=() plots");
            dlg.setVisible(true);
            Global.CadreThreeVariatePlots = dlg;
        } else {
            Global.CadreThreeVariatePlots.setVisible(true);
        }
    }

    void menuPlotMsy_actionPerformed(ActionEvent e) {
        if (Global.CadreMSPlots == null) {
            Cadre_Plot dlg;
            dlg = new Cadre_Plot(Global.msyPlot);
            dlg.setTitle("Climprod: MSY & MSE plots");
            dlg.setVisible(true);
            Global.CadreMSPlots = dlg;
        } else {
            Global.CadreMSPlots.setVisible(true);
        }
    }

    void menuModelsSelect_actionPerformed(ActionEvent e) {
        Global.init();
        if (Global.CadreQuestion == null) {
            Cadre_Question dlg = new Cadre_Question(this);
            dlg.setVisible(true);
            Global.CadreQuestion = dlg;
        } else {
            Global.CadreQuestion.setVisible(true);
        }

    }

    void menuModelsDirectly_actionPerformed(ActionEvent e) {
        Global.init();
        if (Global.CadreModeleDirect == null) {
            Cadre_ModeleDirect dlg2 = new Cadre_ModeleDirect();
            dlg2.setVisible(true);
            Global.CadreModeleDirect = dlg2;
        } else {
            Global.CadreModeleDirect.setVisible(true);
        }
    }

    void menuEditionResults_actionPerformed(ActionEvent e) {
        if (Global.CadreResultats == null) {
            Cadre_Resultats dlg2 = new Cadre_Resultats();
            dlg2.setVisible(true);
            Global.CadreResultats = dlg2;
        } else {
            Global.CadreResultats.setVisible(true);
        }

    }

    private void setData() {
       // while(jPanPlot.getComponentCount()!=0)
        //             jPanPlot.remove(0);
        // Plot[] tp=new Plot[4];
        renseignerTable(jTableData, Data.getDataTable());
        renseignerTable(jTableStat, Data.getStatistics());
        renseignerTable(jTableCorrelation, Data.getCorrelationTable());
        /*for(int i=0;i<4;i++)
         {
         //pPlot[i*2]=new PanelPlot();
         //pPlot[i*2].setPlot(Global.timePlot[i]);
         pPlot[i]=new PanelPlot();
         tp[i]=(Plot)Global.timePlot[i].clone();
         tp[i].setTitreX("");
         pPlot[i].setPlot(tp[i]);
         }*/
        //for(int i=0;i<4;i++)
        //{
        //    pPlot[2*i+1]=new PanelPlot();
        //     pPlot[2*i+1].setPlot(Global.scatterPlot[i]);
        // }
        // for(int i=0;i<4;i++)
        //      jPanPlot.add(pPlot[i],null);

    }

    private void renseignerTable(JTable t, Object[][] res) {
        if (res == null) {
            return;
        }
        int c = res[0].length;
        int l = res.length - 1;
        String[] column = new String[c];
        for (int j = 0; j < c; j++) {
            column[j] = (String) res[0][j];
        }

        Object[][] dat = new Object[l][c];
        for (int i = 0; i < l; i++) {
            for (int j = 0; j < c; j++) {
                dat[i][j] = res[i + 1][j];
            }

        }
        ModeleDeTableStat mdt = new ModeleDeTableStat(dat, column);
        t.setModel(mdt);
        TableColumn tmcolumn;
        int nbc = t.getColumnModel().getColumnCount();
        for (int i = 0; i < nbc; i++) {
            tmcolumn = t.getColumnModel().getColumn(i);
            tmcolumn.setMinWidth(80); // Width of the tables on the left panel.
        }

    }

    private String makeFaits() {
        String s = "\n";
        String faits = "";
        DecimalFormat nf = new DecimalFormat(" 0.00");
        DecimalFormat nf2= new DecimalFormat(" 0.000000;-0.000000");
        Global.Res_Test_Jackknife_Par = new int[4];  // Rajout 2020.
		//System.out.println("Flag jTextAreaFaits.setText(makeFaits()) ligne 816 CadreMain.java Global.nom_fichier = " + Global.nom_fichier);
        if (Global.nom_fichier != null) {

            faits = "Data file path:" + Global.nom_fichier + s + s;          
            faits = faits + "Time interval: " + Global.TimeStepData + " year" + s;
            faits = faits + "Number of observed data (" + Global.TimeStepData$ + "s): " + Data.getNbYears() + s;
			Global.temp=Global.RangeEffort();
			faits = faits + "Relative range of fishing effort variation: " + nf.format(Global.temp*100) + "%" + s + s;
			//System.out.println("Flag (makeFaits()) ligne 823 CadreMain.java Global.envir_preponderant = " + Global.envir_preponderant + " Global.environmental_influence = " + Global.environmental_influence);
			//WARNING: THE UPDATE OF THE COMMENTS RELATIVE TO THE PREDOMINENCE OF V OR E ON CPUE 
			//AS WELL AS THE COMMENTS RELATIVE TO TYPE OF ENVIRONMENTAL INFLUENCE AND TO THE
			//KIND OF RELATIONSHIP BETWEEN CPUE & E OR CPUE & V ARE ONLY PERFORMED WHEN CLICKING
			//ON THE TYPE OF MODEL AND ON THE SELECTED MODEL ITSELF WHEN USING THE "FIT A MODEL DIRECTLY" OPTION.
            if (Global.envir_preponderant != -1 || Global.environmental_influence != "") {    // Simple models
                if (Global.envir_preponderant == 1) {
                    faits = faits + "Influence of fishing effort on CPUE is preponderant" + s; // Models CPUE = f(E)
                } else if (Global.envir_preponderant == 2) {
                    faits = faits + "Influence of Environment on CPUE is preponderant" + s;    // Models CPUE = f(V)
                }
			}
            if (Global.nb_classes_exploitees != -1) {
                faits = faits + "Number of significantly exploited year-classes: " + Global.nb_classes_exploitees + s;
                if (Global.environmental_influence != "") {
                    faits = faits + "Environmental influence: " + Global.environmental_influence + s;
                }
                if (Global.recruitment_age != -1) { 
                faits = faits + "Age at recruitment: " + Global.recruitment_age + s;
                faits = faits + "Age at the begining of environmental influence: " + Global.begin_influence_period + s;
                faits = faits + "Age at the end of environmental influence: " + Global.end_influence_period + s;
				}

				//System.out.println("In CadreMain.java Global.relationCPU_E = " + Global.relationCPU_E);
                if (Global.relationCPU_E != 0) {
                    faits = faits + s + "Relationship between CPUE and E: " + cpu_relation_E[Global.relationCPU_E] + s;
                }
                //String[] cpu_v  ={"Linear [CPUE=a+b.V]","General [CPUE=a+b.V^c]","Power [CPUE=a.V^b]","Quadratic: [CPUE=a.V+b.V^2+c]"};
				//System.out.println("In CadreMain.java Global.relationCPU_V = " + Global.relationCPU_V);                   
                if (Global.relationCPU_V != 0 && Global.relationCPU_E != 0) {
                    faits = faits + "Relationship between CPUE and V: " + cpu_relation_V[Global.relationCPU_V] + s;
                }
				else if (Global.relationCPU_V != 0) {
                    faits = faits + s + "Relation between CPUE and V: " + cpu_relation_V[Global.relationCPU_V] + s;
				}
            }
			//System.out.println("In CadreMain.java Global.modelisationOk = " + Global.modelisationOk);
            if (Global.modelisationOk) 
		   		{
                faits = faits + s + "Selected model: " + RechercheModele.getEquation() + s;
                faits = faits + "Number of years used to fit the model: " + Data.getNbDataRetenue() + s;
                faits = faits + "Number of degrees of freedom: " + Integer.toString(Data.getNbDataRetenue() - Global.nbre_param) + s + s;
                String[][] res$ = (String[][]) Modele.getResult();

                for (int i = 0; i < Global.nbre_param + 1; i++) 
					{
                    for (int j = 0; j < res$[i].length; j++) //faits=faits+"    "+res$[i][j]+"    "+"\t";
                        {faits = faits + res$[i][j] + "\t";}
                    faits = faits + s;
                    }
                faits = faits + s;
                for (int i = Global.nbre_param + 3; i < res$.length; i++) 
					{
                    for (int j = 0; j < res$[i].length; j++) 
			    		{
                        if (res$[i][j] != null) 
							{faits = faits + res$[i][j] + "\t";}
                        }
                    faits = faits + s;
                   }
				//System.out.println("In CadreMain.java Global.validationOk = " + Global.validationOk);
                if (Global.validationOk) 
					{       
					//Validation.valide_modele(); Rajout du 05 08 2020 mais perturbe mise à jour des graphiques jackknife dans fit a model directly si deux ajustements consécutifs avec entre les 2 on repasse par menu principal
					Validation.getParamResult();
					if (Global.coeff_determination == 0.0)
						faits=faits + s + "*R² estimate was negative and consequently set to zero.\nThis result is due to a poor fitting by the Marquart algorithm\nand it is commonly observed when the model does not include\nan intercept parameter or when the model includes a non-linear function.\nConsequently the display of the jackknife R² graph is not available." + s;
					faits=faits + s + "Parameters      Jackknife t-ratio test result";   
					for(int i=0;i<Validation.nbre_par;i++) {
						if (Global.Flag_pb_jackknife[i] != 1)
						   	faits=faits + s + "  " + Global.param$[i]+ "                          " + Global.Test_Jackknife_Param$[Global.Res_Test_Jackknife_Par[i]];
           				else 
						   	faits=faits + s + "  " + Global.param$[i]+ "**                       " + Global.Test_Jackknife_Param$[Global.Res_Test_Jackknife_Par[i]];
					} 
            		if (Global.Flag_pb_jackknife_Tot != 0) faits=faits + s + "**" + Global.message$[0];
            			if (Global.test_jackknife) { 
			     			faits = faits + s + "Overall T_Jackknife on parameters: good" + s;} 
						else {faits = faits + s + "Overall T_Jackknife on parameters: bad" + s;}
					if(Global.r_jk != 0.00) 
		        		faits = faits + s + "Jackknife coefficient of determination R²: " + nf.format(Global.r_jk) + s;
		    		else {
		        		faits = faits + s + "Jackknife coefficient of determination R²***: " + nf.format(Global.r_jk) + s;
        				faits=faits+s+"***Jackknife R² estimate was negative and consequently set to zero.\nThis result is due to a poor fitting by the Marquart algorithm\nand it is commonly observed when the model does not include\nan intercept parameter or when the model includes a non-linear function.\nConsequently the display of the jackknife R² graph is not available." + s;
				 		}
					}
				if (Global.message$[13] != "") // The estimate of the initial value of at least one of the parameter is equal to infinity.
					faits=faits + s + Global.message$[13]; 
				if (Global.message$[6] != "")  // At least one of the noteworthy values of MSE is negative or \nlarger than 1.0E10 
					faits=faits + s + Global.message$[6]; 
				else if (Global.message$[10] != "") // At least one of the central values of MSE is negative for noteworthy V values 
					faits=faits + s + Global.message$[10]; 
				else if (Global.message$[3] != "") // All noteworthy values of MSY and/or MSE upper limits at 95% \nare null or negative 
					faits=faits + s + Global.message$[3];  
				else {
					if (Global.message$[5] != "") // At least one set of predicted values of CPUE=f(E,V) and Y=f(E,V) corresponding to Vmin or Vmax presents only negative values.
						faits=faits + s + Global.message$[5];
					else {
					if (Global.message$[4] != "") // All noteworthy values of MSY and MSE lower limits at 95% are null or negative. 
						faits=faits + s + Global.message$[4];
						else {
							if (Global.message$[1] != "") // All noteworthy values of MSE lower limit at 95% are null or negative. 					
								faits=faits + s + Global.message$[1];
							if (Global.message$[7] != "") // Please note that some MSY central values for at least one of the noteworthy V values are unexpectidly negative.           
								faits=faits + s + Global.message$[7];
							if (Global.message$[2] != "") // All noteworthy values of MSY lower limit at 95% are null or negative.
								faits=faits + s + Global.message$[2];
							if (Global.message$[8] != "")      // Please note that the width of the 95% confidence interval of MSY central values for at least one of the noteworthy V values is larger than the corresponding MSY value.
								faits=faits + s + Global.message$[8];
							if (Global.message$[9] != "")      // Please note that the width of the 95% confidence interval of MSE central values for at least one of the noteworthy V values is larger than the corresponding MSE value.;	
								faits=faits + s + Global.message$[9];
							if (Global.message$[11] != "")     // Please note that the MSY central value for the mean V value is 10 times larger than the maximum observed catch value.
								faits=faits + s + Global.message$[11];
							if (Global.message$[12] != "")     // Please note that the MSE central value for the mean V value is 5 times larger than the maximum observed fishing effort value.
								faits=faits + s + Global.message$[12];
						}            
    				}
				}
			}
        }
    else {
         faits = "Data file: No selected file.";
     	 }
    return faits;
}

    void menuModelsPrediction_actionPerformed(ActionEvent e) {
        Cadre_Prediction dlg = new Cadre_Prediction(this);
        dlg.setTitle("Climprod: Prediction for the fitted model " + RechercheModele.getEquation());
        dlg.setModal(true);
        dlg.setVisible(true);
    }

    void menuModelsResults_actionPerformed(ActionEvent e) {
        if (Global.CadreResultats == null) {
            Cadre_Resultats dlg2 = new Cadre_Resultats();
            dlg2.setVisible(true);
            Global.CadreResultats = dlg2;
        } else {
            Global.CadreResultats.setVisible(true);
        }

    }

    /*
     Lecture dernière configuration dans fichier .climprod\Climprod*.txt où * = username du système d'opération.
     */
    private void readConfig() {

        String fileSep = "";
        String fileName = "";
		Global.double_click = "ShowWarningDoubleClick"; // Rajout 2020.
        vLastFile.clear();
        fileSep = System.getProperty("file.separator");
        if (fileSep.equals("/")) {
            fileSep = "//";
        }

        userName = System.getProperty("user.name");
        userHome = System.getProperty("user.home");
        configFile = userHome + fileSep + ".climprod" + fileSep + "Climprod" + userName.replace(' ', '_') + ".txt";
        File pref = new File(configFile);
        if (pref.exists()) {
            try {
                ReadFileText fl = new ReadFileText(configFile);
                String[] fldat = fl.getLines();
                //Hashtable ht = new Hashtable();
                HashMap<String, String> ht = new HashMap<String, String>();
                for (int i = 0; i < fldat.length; i++) {
                    String key = "";
                    String value = "";
                    StringTokenizer d = new StringTokenizer(fldat[i], "|");
                    //System.out.println(d.countTokens());
                    if (d.countTokens() == 2) {
                        ht.put(d.nextToken(), d.nextToken());
                    } else {
                        ht.put(d.nextToken(), " ");
                    }
                }
                userDir = (String) ht.get("userDir");					// Adresse climprod.jar ?
                userHome = (String) ht.get("userHome");					// Adresse user
                workingDirectory = (String) ht.get("workingDirectory"); // Adresse du répertoire .climprod dans C:
                lastPathSource = (String) ht.get("lastPathSource");	    // Adresse du répertoire exaple_data
				Global.double_click = (String) ht.get("double_click");  // Statut du double-click Rajout 2020.
                String $key = (String) ht.get("file1");					// Entête adresse et nom derniers fichiers .CLI utilisés
                //System.out.println("key " +$key);
                int i = 1;
                while ($key != null) {
                    vLastFile.add($key);
                    updateMenu();
                    i = i + 1;
                    $key = (String) ht.get("file" + (i));
                }
                $key = (String) ht.get("folder1");				// Entête adresse et nom derniers répertoires Html&GraphsFolder utilisés				
                //System.out.println("key " +$key);
                i = 1;
                Vector<String> vFolder = new Vector<String>();
                while ($key != null) {
                    vFolder.add($key);
                    i = i + 1;
                    $key = (String) ht.get("folder" + (i));
                }
                int c = vFolder.size();
                if (c > 0) {
                    String[] sFolder = new String[vFolder.size()];
                    vFolder.copyInto(sFolder);
                    existingFolder(sFolder);
                }

            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        } else //n'existe pas on le renseigne et on le crée
        {
            userDir = System.getProperty("user.dir");
            workingDirectory = userHome + fileSep + ".climprod";
            File wd = new File(workingDirectory);
            wd.mkdir();
            lastPathSource = workingDirectory;
            try {
                saveConfig();
            } catch (Exception ew) {
            }

        }
        Global.datafilePath = lastPathSource;

    }

    /*
     Sauvegarde dernière configuration dans .climprod\Climprod*.txt où * = username du système d'opération.
     */
    private void saveConfig() throws IOException {
        //System.out.println("configFile  " +configFile);
        PrintWriter out = new PrintWriter(new FileWriter(configFile));
        out.println("lastPathSource|" + Global.datafilePath); // Cf supra pour commentaires sur ces noms.
		out.println("double_click|" + Global.double_click);  // Rajout 2020.
        out.println("workingDirectory|" + workingDirectory);
        out.println("userDir|" + userDir);
        out.println("userHome|" + userHome);
        for (int i = 0; i < vLastFile.size(); i++) {
            out.println("file" + (i + 1) + "|" + (String) vLastFile.get(i));
        }
        if (menuItemOpenFolder != null) {
            for (int i = 0; i < menuItemOpenFolder.length; i++) {
                out.println("folder" + (i + 1) + "|" + (String) menuItemOpenFolder[i].getText());
            }
        }
        out.close();
    }

    /* Gestion des menus.Permet de mémoriser les
     4 derniers fichiers ouverts
     */
    private void updateMenu() {
        int size = 0;
        if (vLastFile.isEmpty()) {
            vLastFile.add(Global.nom_fichier);
        } else {
            size = vLastFile.size();
            if (Global.nom_fichier != null) {
                for (int i = 0; i < size; i++) {
                    if (Global.nom_fichier.equals((String) vLastFile.get(i))) {
                        vLastFile.remove(i);
                        size--;
                        break;
                    }
                }
                if (size == 4) {
                    vLastFile.remove(3);
                }
                vLastFile.add(0, Global.nom_fichier);
            } else {
                size--;
            }

        }
        if (size == 0) {
            menuFile.addSeparator();
        }
        if (size != vLastFile.size()) {
            menuFile.add(menuFileFile[size]);  //menu pour taille-1
        }
        for (int i = 0; i < vLastFile.size(); i++) {
            menuFileFile[i].setText((String) vLastFile.get(i));
        }
    }
    /*
     * Gestion des éléments de menus Folder
     */

    private void existingFolder(String[] dd) {
        menuFileOpenFolder.removeAll();
        menuItemOpenFolder = new JMenuItem[dd.length];
        for (int i = 0; i < dd.length; i++) {

            menuItemOpenFolder[i] = new JMenuItem(dd[i]);
            //menuItemOpenFolder[i].setText(dd[i]);
            menuItemOpenFolder[i].addActionListener(new java.awt.event.ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    menuItemOpenFolder_actionPerformed(e);
                }
            });
            menuFileOpenFolder.add(menuItemOpenFolder[i]);
        }
    }
}
