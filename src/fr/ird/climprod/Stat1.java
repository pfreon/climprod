
/**
 * Titre : Climprod<p>
*/
package fr.ird.climprod;
import java.util.Arrays;
public class Stat1 {
 /**
 * Calcul de la moyenne sur un tableau d'entiers i.
 * @param int[]i.
 * @return un double, la moyenne.
 */
 static public double moy( int i[]) {
      double[] d=new double[i.length];
      double dum=1.0d;
      for (int j=0;j<d.length;j++)
          d[j]=i[j]*dum;
      return Stat1.moy(d);
  }
   /**
 * Calcul de la moyenne sur un tableau de float f.
 * @param float[]f.
 * @return un double, la moyenne.
 */
 static public double moy( float f[]) {
     double[] d=new double[f.length];
     double dum=1.0d;
      for (int j=0;j<d.length;j++)
          d[j]=f[j]*dum;
      return Stat1.moy(d);
  }
  /**
 * Calcul de la moyenne sur un tableau de doubles d.
 * @param double[]d.
 * @return un double, la moyenne.
 */
 static public double moy( double d[]) {
      double dm=0;
      int nb=d.length;
      for (int i=0;i<nb;i++)
            dm=dm+d[i];
       return (dm/nb);
 }
 /**
 * Calcul de la variance sur un tableau d'entiers i .
 * @param int[]i.
 * @param boolean echantilon, si vrai les donn�es sont issues d'un �chantillon,si faux elles repr�sentent la population enti�re.
 * @return un double, la variance.
 */
static public double var(int i[],boolean echantillon){
 double[] d=new double[i.length];
      double dum=1.0d;
      for (int j=0;j<d.length;j++)
          d[j]=i[j]*dum;
      return Stat1.var(d,echantillon);
 }
 /**
 * Calcul de la variance sur un tableau de floats f.
 * @param float[]f.
 * @param boolean echantilon, si vrai les donn�es sont issues d'un �chantillon,si faux elles repr�sentent la population enti�re.
 * @return un double, la variance.
 */
static public double var(float f[],boolean echantillon){
 double[] d=new double[f.length];
     double dum=1.0d;
      for (int j=0;j<d.length;j++)
          d[j]=f[j]*dum;
      return Stat1.var(d,echantillon);
 }
 /**
 * Calcul de la variance sur un tableau de doubles d.
 * @param double[]d.
 * @param boolean echantilon, si vrai les donn�es sont issues d'un �chantillon,si faux elles repr�sentent la population enti�re.
 * @return un double, la variance.
 */
static public double var(double d[],boolean echantillon){
  int nb=d.length;
  if (nb==1)
    return 0.0d;
  double sm2=0;
  for (int i=0;i<nb;i++)
   sm2=sm2+(d[i]*d[i]);
  double m=Stat1.moy(d);
  if (echantillon)
    return  ( (sm2-(nb*(m*m)))/(nb-1));
  else
  return( (sm2-(nb*(m*m)))/(nb));
 }
 /**
 * Calcul de la covariace sur 2 tableaux d'entiers.
 * Attention les tableaux doivent �tre de m�me taille.
 * @param int[] x,int[] y
 * @return un double, la covariance.
 */
static public double cov(int[] x,int[] y){
 double[] da=new double[x.length];
     double[] db=new double[y.length];
     int nb=x.length;
     if(nb<y.length) //déclenche une errreur si lg différentes.
        nb=y.length;
     double dum=1.0d;
      for (int j=0;j<nb;j++)
      {
          da[j]=x[j]*dum;
          db[j]=y[j]*dum;
      }
      return cov(da,db);
}
/**
 * Calcul de la covariace sur 2 tableaux de floats.
 * Attention les tableaux doivent �tre de m�me taille.
 * @param float[] x,float[] y
 * @return un double, la covariance.
 */
  static public double cov(float[] x,float[] y){
     double[] da=new double[x.length];
     double[] db=new double[y.length];
     int nb=x.length;
     if(nb<y.length) //d�clanche une errreur
        nb=y.length;
     double dum=1.0d;
      for (int j=0;j<nb;j++)
      {
          da[j]=x[j]*dum;
          db[j]=y[j]*dum;
      }
      return cov(da,db);
}
/**
 * Calcul de la covariace sur 2 tableaux de double.
 * Attention les tableaux doivent �tre de m�me taille.
 * @param float[] x,float[] y
 * @return un double, la covariance.
 */
  static public double cov(double[] x,double[] y){

  double c=0;
  int nb=x.length;
  if(nb<y.length) //d�clenche une erreur si lg diff�rentes.
        nb=y.length;
  double ma=Stat1.moy(x);
  double mb=Stat1.moy(y);
  for (int i=0;i<nb;i++)
     c=c+(x[i]-ma)*(y[i]-mb);
  return c/nb;

 }
/**
 * Calcul du coefficient de correlation conventionel de Pearson sur 2 tableaux d'entiers.
 * Attention les tableaux doivent �tre de m�me taille.
 * @param int[] x,int[] y
 * @return un double, le coefficient de Pearson.
 */
static public double pearson(int[] x,int[] y){
 double[] da=new double[x.length];
     double[] db=new double[y.length];
     int nb=x.length;
     if(nb<y.length) //d�clanche une errreur si lg diff�rentes.
        nb=y.length;
     double dum=1.0d;
      for (int j=0;j<nb;j++)
      {
          da[j]=x[j]*dum;
          db[j]=y[j]*dum;
      }
      return pearson(da,db);
}
/**
 * Calcul du coefficient de correlation conventionel de Pearson sur 2 tableaux de float.
 * Attention les tableaux doivent �tre de m�me taille.
 * @param float[] x,float[] y
 * @return un double, le coefficient de Pearson.
 */
static public double pearson(float[] x,float[] y){
     double[] da=new double[x.length];
     double[] db=new double[y.length];
     int nb=x.length;
     if(nb<y.length) //d�clanche une errreur
        nb=y.length;
     double dum=1.0d;
      for (int j=0;j<nb;j++)
      {
          da[j]=x[j]*dum;
          db[j]=y[j]*dum;
      }
      return pearson(da,db);
}
/**
 * Calcul du coefficient de correlation conventionel de Pearson sur 2 tableaux de double.
 * Attention les tableaux doivent �tre de m�me taille.
 * @param double[] x,double[] y.
 * @return un double, le coefficient de Pearson.
 */
 static public double pearson(double[] x,double[] y){
 double xs2=0.0000000001;
 double ys2=0.0000000001;
  int nb=x.length;
  double ma=Stat1.moy(x);
  double mb=Stat1.moy(y);
  double c=Stat1.cov(x,y)*nb;

  for(int i=0;i<nb;i++)
  {
      xs2=xs2+((x[i]-ma)*(x[i]-ma));
      ys2=ys2+((y[i]-mb)*(y[i]-mb));
  }
  return (c/Math.sqrt(xs2*ys2) ) ;
 }

/**
 * Calcul du coefficient de corr�lation sur les rangs de Spearman (rho) sur 2 tableaux de double.
 * Attention les tableaux doivent �tre de m�me taille.
 * @param double[] x,double[] y.
 * @return un double, le coefficient de corr�lation rho.
 */
 static public double rho(double[] x,double[] y){

  int nb=x.length;

  double num=Stat1.cov(x,y);
  double dem=Math.pow(Stat1.var(x,false),0.5)*Math.pow(Stat1.var(y,false),0.5);
  //System.out.println(num+"   "+dem);
  if(dem>1.e-20)
      return num/dem;
  else
      return 0;

 }
/**
 * Calcul des 1/nbQuantiles de d.
 * Ex. Pour nbQuantiles=3 calcule pour .25,.5,.75.
 * Si nbQuantiles<1 retourne null.
 * @param double[] d le tableau dont on veut les quantiles.
 * @param int nbQuantiles, les nombre de quantiles � calculer.
 * @return double[] le tableau de valeur prises par chaque quantile.
 */
static public double[] quant(double[] d,int nbQuantiles){
if (nbQuantiles<1) return null;
int nb=d.length;
double[] qt=new double[nbQuantiles];
double[] dt=new double[nb];
System.arraycopy(d,0,dt,0,nb);
Arrays.sort(dt);
float pas=(float)nb/(float)(nbQuantiles+1);

for (int i=1;i<nbQuantiles+1;i++)
{
    int i1=(int)(0.99999+(pas*(float)(i)));
    int i2=(int)(1.00+(pas*(float)(i)));
    qt[i-1]=(dt[i1-1]+dt[i2-1])/2;
    //System.out.println((i*pas)+ "  " +(i1-1)+"  " +(i2-1)+ " " +qt[i-1]);

}

return qt;
}
/**
 * Centre et r�duit une variable.
 * @param double[] x la variable dont on veut les valeurs centr�es-r�duites
 * @return double[] les valeurs centr�es-r�duites.
 *  */
static public double[] normaliser(double [] x)
{
    double m=Stat1.moy(x);
    double ect=Math.pow(Stat1.var(x,true),0.5);
    if (ect==0)
      return x;
    double[] norme=new double[x.length];
    for (int i=0;i<x.length;i++)
        norme[i]=(x[i]-m)/ect;
    return norme;
}

/**
 * Centre et r�duit une variable.
 * @param double[] x la variable dont on veut les valeurs centr�es-r�duites
 * @return double[] les valeurs centr�es-r�duites.
 *  */
static public float[] histoCentreReduit(float [] x,boolean freqRelative)
{
     double dum=1.0d;
     int[] classe={-2,-1,0,1,2};
     float[] compte={0,0,0,0,0,0};
     boolean b=false;
     double [] dx=new double[x.length];
     for (int i=0;i<x.length;i++)
          dx[i]=x[i]*dum;
     dx=normaliser(dx);
     int partieEntiere=0;
     for (int i=0;i<dx.length;i++)
     {
           if(dx[i]==0)
           {
              if(b)
                  compte[2]=compte[2]+1;
              else
                  compte[3]=compte[3]+1;
              b=!b;
           }
           else if (dx[i]<0)
           {
              for (int j=0;j<3;j++)
              {
                  if(dx[i]<=classe[j])
                  {
                      compte[j]=compte[j]+1;
                      break;
                  }
              }
            }
            else if (dx[i]<2)
           {
              for (int j=3;j<5;j++)
              {
                  if(dx[i]<classe[j])
                  {
                      compte[j]=compte[j]+1;
                      break;
                  }
              }

           }
           else
           {
                compte[5]=compte[5]+1;
           }
      //System.out.println(dx[i]+  "****"+ compte[0]+  " * "+ compte[1]+  " * "+ compte[2]+  " * "+ compte[3]+  " * "+ compte[4]+  " * "+ compte[5]);
     }
     if (freqRelative)
     {
          for (int i=0;i<compte.length;i++)
                  compte[i]=compte[i]/dx.length;
     }
     return compte;
}

static public double min(double [] s){ // Utilisé pour analyser autre chose que données brutes (axes graphiques en particulier).
double min=s[0];
for (int i=0;i<s.length;i++)
{
      if(s[i]<min)
              min=s[i];
}
return min;
}

static public double max(double [] s){
double max=s[0];
for (int i=0;i<s.length;i++)
{
      if(s[i]>max)
              max=s[i];

}
return max;

}
static public double[] Extremas(double [] s){ // Utilisé pour analyser données brutes et dans Validation.java.
double[] ext=new double[2];
ext[0]=s[0];
ext[1]=s[0];
for (int i=0;i<s.length;i++)
{
      if(s[i]<ext[0])
              ext[0]=s[i];
      else if (s[i]>ext[1])
              ext[1]=s[i];
}
// System.out.println("Flag 2 Stat1, ext[0] = " + ext[0] + " ext[1] = " + ext[1]); 
return ext;
}

static public double[][] distribuer(int nb ,double[] d){
if (nb <=0) return null;
double[][] z=new double[nb][2];
double[] dt=new double[d.length];
System.arraycopy(d,0,dt,0,d.length);
Arrays.sort(dt);
double ecart=(dt[dt.length-1]-dt[0])/nb;
double ecart2=ecart/2;
double pas=dt[0]+ecart/2;

for (int i=0;i<nb;i++){
    z[i][1]=0;
    z[i][0]=pas+(i*ecart);
}
for (int i=0;i<d.length;i++){
      int k=(int)(Math.abs((dt[0]-dt[i]+1.e-5)/ecart));
      //System.out.println("k--- " +k);
      z[k][1]=z[k][1]+1;
}
return z;
}



static private double[] transtype(Object [] t)
{
      double[] d=new double[t.length];
       for (int j=0;j<d.length;j++)
          d[j]=Double.parseDouble(t[j].toString());

      return d;
}

static public double Kurtosis(double[] s){
        int nb=s.length;
        if (nb<4) return 0;
        double 	t4=1.E-20;
        double[] sn=normaliser(s);
        for(int i=0;i<nb; i++)
        { t4 +=+sn[i]*sn[i]*sn[i]*sn[i];}
          return    (double)(nb*(nb+1))/((nb-1)*(nb-2)*(nb-3))*t4 - (double)3*(nb-1)*(nb-1)/((nb-2)*(nb-3));
        /*
	double 	t1,t2,t3,t4;  //marche aussi
	double 	va,denom,numer;
        double m=Stat1.moy(s);
	t1=t2=t3=t4=1.E-20;
	for(int i=0;i<nb; i++){
			t1 += s[i];
			t2 += s[i]*s[i];
			t3 += s[i]*s[i]*s[i];
			t4 += s[i]*s[i]*s[i]*s[i];
	}


        va=var(s,true);
        if(va>1.e-10)
        {
            numer =	(nb*t4) - (4*t1*t3) + 6*t1*t1*t2/nb - 3*t1*t1*t1*t1/(nb*nb);
	    denom =	((nb-1)*(nb-2)*(nb-3)*(va*va));
            return (nb+1)*(numer/denom) -
				(double)3*(nb-1)*(nb-1)/((nb-2)*(nb-3));

        }
       else
          return 0;
        */

}

static public double Skewness(double[] s){
        int nb=s.length;
        if (nb==1) return 0;
	double 	t1,t2,t3;
	double 	va,denom,numer;
	t1=t2=t3=1.E-20;
	for(int i=0;i<nb; i++){
			t1 += s[i];
			t2 += s[i]*s[i];
			t3 += s[i]*s[i]*s[i];
	}
        va=var(s,true);
        if(va>1.e-10)
        {
	  numer =(nb*t3-3*t1*t2+2*t1*t1*t1/nb);
          denom =((nb-1)*(nb-2)*(Math.sqrt(va))*va);
	  return numer/denom;
        }
        else
          return 0;


}

}