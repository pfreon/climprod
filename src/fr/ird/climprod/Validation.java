
/*
 * Titre : Climprod<p>
 * Effectue validation du modele par calcul R2 jackknife, test t Jackknife.
 * Calcul des MSY MSE pour les valeurs remarquables presentees dans dossier html, onglet validation
 * et dans menu 'Modelisation', 'Display results tables'. 
 * Calcule les valeurs et affiche les graphiques MSE & MSY versus V Plots disponibles dans * menu principal, onglet "Plots".
*/
package fr.ird.climprod;
import static fr.ird.climprod.Data.getNbDataRetenue;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.awt.Color;
import java.io.*;
import java.lang.*;

public class Validation {
      static private int nparmax=4,nv=100; // nv = longueur arbitraire boucle Jackknife (avant = 50). Modif 2020
	  static private int nim;
	  static private int nimTabFish;
	  static private int nimTabStudent;
	  static public int jk,nbre_par;
      static private int riter,iter,ritertot,maxrit=150;
      static private double val_init,val_fin,   alpha=4,ecart;
      static private double sompar,variance_pue,eps=1.E-8,mul=1.E-3,yy,r_tot;

      static private double[][] covar=new double[nparmax+1][nparmax+1];

      static private double[][] hessien=new double[nparmax+1][nparmax+1];
      static private double[] covpy=new double[nparmax+2];
      static private double[] par_alors=new double[nparmax+2];
      static private double[] racineh=new double[nparmax+2];
      static private double[] derivee=new double[nparmax+2];
      static private double[] delta=new double[nparmax+2];
      static private double[] par_init=new double[nparmax+2];
      static private double[] par_inter=new double[nparmax+2];
      static private double[] spar=new double[nparmax+2];
      static private double[] par=new double[nparmax+2];
      static private double[] dpar=new double[nparmax+2];
      static private double[] sompari=new double[nparmax+2];
      static private double[] f,v,vbar,pue,res;
      static private double[]  resiter=new double [maxrit];
      static private double[][] derfonc=new double [61][nparmax+1];
      static private double[][] jkn=new double [61][nparmax+1];
	  static private double [] Parjk=new double [5]; // Modif 2020. Valeurs jackknife des paramètres de la regression (= moyennne des n années) d'après SAHINLER et TOPUZ (2007).
      static private double[][] pseudo=new double [61][nparmax+1];
      static private double[] val_v=new double[4];
      static private double[] moyenne_pseudo=new double[nparmax+2];
      static private double[][] covar_pseudo=new double[nparmax+1][nparmax+1]; // Matrice de covariance des paramètres
      static private double[] ecart_pseudo=new double[nparmax+2];

      static private         double []f_ms_m=new double[nv];
      static private         double []f_ms_s=new double[nv];
      static private         double []y_ms_m=new double[nv];
      static private         double []y_ms_s=new double[nv];
      static private 	     double []vec_y_v=new double[nv];
      static private         double []vec_y_min=new double[nv];
      static private         double []vec_y_max=new double[nv];
      static private         double []vec_f_v=new double[nv];
      static private         double []vec_f_min=new double[nv];
      static private         double []vec_f_max=new double[nv];

static public double[]f_ms_m2=new double[4];      // Central values MSE according to V  noteworthy values (Modif 2020).
static private double []vec_f_min2=new double[4]; // Min values MSE 95% interval according to V noteworthy values (Modif 2020).
static public double []vec_f_max2=new double[4];  // Max values MSE 95% interval according to V noteworthy values (Modif 2020). 
static public double[]y_ms_m2=new double[4];      // Central values MSY according to V noteworthy values (Modif 2020).
static private double []vec_y_min2=new double[4]; // Min values MSY 95% interval according to V noteworthy values (Modif 2020).
static public double []vec_y_max2=new double[4];  // Max values MSY 95% interval according to V noteworthy values (Modif 2020).


/***************************************
 * Algorithme de Marquardt
****************************************/

private static void marquardt()
{
	
    int 	iter=0,riter=0,i,j,k;
	double 	c,s,val_fin,val_init,ecart=100.0,alpha=4,lambda=16,rl =1.5;
    double[] val_iter=new double[maxrit];
    
	for(i=0;i<nbre_par;i++) {
		par_alors[i] = par_init[i];
	//System.out.println("i = " + i + " par_alors[i] = " + par_alors[i]);
	}
	//ritertot=0;
	val_fin=somme_residus_jk(par_alors);
    //System.out.println("valfin  "+val_fin);
	//resiter[0]=val_fin;
	//ecart= 100;
	//riter=0;
	while	(
			(iter<maxrit-1) &&
			((ecart > 0.05) || (iter<40)) &&
			(ecart  > 0.0001) &&
			(riter  > (-17))
		) {
		iter++;
                val_init = val_fin;
		calcul_derivee_hess();
                for(i=0;i<nbre_par;i++) hessien[i][i] += eps;
		for(i=0;i<nbre_par;i++) racineh[i] = Math.sqrt(hessien[i][i]);
		for(i=0;i<nbre_par;i++) derivee[i] /= racineh[i];
		for(i=0;i<nbre_par;i++) for(j=0;j<nbre_par;j++)
			hessien[i][j] /= (racineh[i]*racineh[j]);
		for(i=0;i<nbre_par;i++) hessien[i][i] += lambda;
                //for(i=0;i<nbre_par;i++)
                   // System.out.println("rac "+racineh[i]+" deri " +derivee[i] );
                //*******************************************
				//inverse_gauss( nbre_par, hessien, delta, derivee );
                  for(i=0;i<nbre_par-1;i++) {
	              for(j=i+1;j<nbre_par;j++){
		        c=hessien[j][i]/hessien[i][i];
		      for(k=i+1;k<nbre_par;k++) hessien[j][k]=hessien[j][k]-c*hessien[i][k];
    		      derivee[j]=derivee[j]-c*derivee[i];
	              }
                  }
                  delta[nbre_par-1] = -derivee[nbre_par-1]/hessien[nbre_par-1][nbre_par-1];
                  for(i=nbre_par-2;i>=0;i--){
	                s=derivee[i];
	                for(k=i+1;k<nbre_par;k++) s += hessien[i][k]*delta[k];//bizare
	                delta[i]= -s/hessien[i][i];
                  }
                  for(i=0;i<nbre_par;i++) delta[i] = - delta[i];
               //
		for(i=0;i<nbre_par;i++)
                {
                // System.out.println("delta "+ delta[i]);
                 delta[i] /= racineh[i];
                }
		for(i=0;i<nbre_par;i++) par_inter[i] = par_alors[i]-delta[i];
		val_fin = somme_residus_jk(par_inter);
                //System.out.println("valfin "+ iter+" " +val_fin);
		riter=0;
		if (val_fin < val_init) {
			while (val_fin < val_init) {
				val_init = val_fin;
				for(i=0;i<nbre_par;i++){
					delta[i] *= alpha;
					par_inter[i] = par_alors[i] - delta[i];
					}
				val_fin = somme_residus_jk(par_inter);
				riter++;
				//ritertot++;
				}
			for(i=0;i<nbre_par;i++) {
				delta[i] /= alpha;
				par_inter[i] = par_alors[i] - delta[i];
				}
                        riter--;
			val_fin = somme_residus_jk(par_inter);
			}
		else {
			while (val_fin > val_init)  {
				for(i=0;i<nbre_par;i++){
					delta[i] /= alpha;
					par_inter[i] = par_alors[i]-delta[i];
					}
				val_fin = somme_residus_jk(par_inter);
				riter--;
				if (riter < (-10)) for(i=0;i<nbre_par;i++) delta[i]=0;
				//ritertot++;
				}
			}
		val_iter[iter]=val_fin;
		if ( riter<0) 	lambda *=rl;
			else 	lambda /=rl;
		if(iter>5) ecart= 100*(val_iter[iter-5] - val_fin)/val_iter[iter-5];

		for(i=0; i<nbre_par; i++) par_alors[i] = par_inter[i];
		}
	}

/****************************************************
* calcul_derivee_hess()
****************************************************/

private static void calcul_derivee_hess()
{
	int 	i,j,n;
	double 	v1,v2;
	for(i=0;i<nbre_par;i++) {
		par[i] 	= par_alors[i];
		dpar[i] = mul*par[i];
		}
	for(n=0;n<nim;n++) {
                if(n !=jk)
                {
		v1 = EquationModele.fonction_modele(f[n],v[n],vbar[n],par);
                //System.out.println("v1 " +n+ "  "+ v1);
		res[n] = v1 - pue[n];
		for(i=0;i<nbre_par;i++) {
			par[i] += dpar[i];
			v2 = EquationModele.fonction_modele(f[n],v[n],vbar[n],par);
                        //System.out.println("v2 " +i+ "  "+ v2);
			derfonc[n][i] = (v2 - v1)/dpar[i];
			par[i] -= dpar[i];
			}
		}
        }
	for(i=0;i<nbre_par;i++) {
			derivee[i] = 0;
			for(n=0;n<nim;n++) if(n !=jk)
				derivee[i] += (res[n]*derfonc[n][i]);
                        //System.out.println("derive " +i+ "  "+ derivee[i]);

			}
	for(i=0;i<nbre_par;i++)
		for(j=0;j<=i;j++){
			hessien[i][j]= eps;
			for(n=0;n<nim;n++) if(n !=jk)
				hessien[i][j] += (derfonc[n][i]*derfonc[n][j]);
			hessien[j][i] = hessien[i][j];
                         //System.out.println("hessien " +i+ "  "+ hessien[i][j]);
			}

	}

private static double somme_residus_jk(double[] par)
{
	double cc=0;
	int i;
	for(i=0;i<nim;i++)if(i !=jk)
        {
          double c= EquationModele.fonction_modele(f[i],v[i],vbar[i],par) - pue[i];
          cc += (c*c);
        }
	return(cc);
}

private static void calc_var_jk()
{

 int 	i;
	double 	s1=0,s2=0;
	for(i=0;i<nim;i++) if(i != jk) {
		s1 += pue[i];
		s2 += (pue[i]*pue[i]);
		}
	if (jk != -1) variance_pue = s2 - (s1*s1)/(double)(nim-1);
		else variance_pue = s2 - (s1*s1)/(double)nim;
}

public static void valide_modele()
{
	int 	i,j,k,l,iv;
	double  var_tot, sdc_tot, sdc_jk;
	double	f_ms,y_ms,vmin,vmax,vv,ecartv;
	String message = "";

	Global.Res_Test_Jackknife_Par = new int[4];    // Rajout 2020.
        double[] pue_c,pue_j;

       	double f_ms2;

        double[]f_ms_s2=new double[4];
        double y_ms2;

        double[]y_ms_s2=new double[4];
	double []vec_y_v2=new double[nv];

	double []vec_f_v2=new double[nv];

	double[]trjk=new double[nv]; // Valeur relative des coefficients de regression jackknife par année par rapport au coefficient conventionel.
	//char 	cc[2];
	//char	*nom_jk[] = {"R2(%) time plot","a(%) time plot","b(%) time plot","c(%) time plot","d(%) time plot"};
	//char val[25];
	/**************** initialisations ********************/
          Global.MSY_MSE_OK=false;
          nbre_par=Global.nbre_param;
          for(i=0;i<nbre_par;i++) par_init[i] = Global.val_param[i];
          nim=Data.getNbDataRetenue();
          res= new double[nim];
          pue_c=new double[nim];
          pue_j=new double[nim];
          f=Data.getF();
          v=Data.getV();
          vbar=Data.getVbar();
          pue=Data.getPue();
	if((Global.numero_modele > 5) || (Global.numero_modele< 2) && (Global.numero_modele != 33)) { // Exclus les modèles CPUE = f(V). Commentaire 2020

		for(iv=0;iv<nv;iv++){
			f_ms_m[iv] = 0.0;
			f_ms_s[iv] = 0.0;
			y_ms_m[iv] = 0.0;
			y_ms_s[iv] = 0.0;
		}
		for(iv=0;iv<4;iv++){
			f_ms_m2[iv] = 0.0;
			f_ms_s2[iv] = 0.0;
			y_ms_m2[iv] = 0.0;
			y_ms_s2[iv] = 0.0;

			}
	}
	for(i=0; i<5;i++) Parjk[i] = 0;	
    double[] ext=Stat1.Extremas(v);
	val_v[2] = ext[0];		   // V mini
	val_v[3] = ext[1];         // V maxi
        ext=Stat1.quant(v,3);
        val_v[0]=Stat1.moy(v); // V moyen
        val_v[1]=ext[1];       // V median
	ecartv = (val_v[3] - val_v[2])/nv; // Ceci est le pas d'incrémentation de V (vv) dans la boucle Jakknife qui sert ensuite au tracé des courbes.


	jk = -1;
	calc_var_jk();
	var_tot = variance_pue;
	sdc_tot = somme_residus_jk(par_init);
	r_tot = 100*(1 - (sdc_tot/var_tot)); // Coefficient de détermination conventionnel R2. Commentaire 2020
	sdc_jk  = 0;
        //for(i=0;i<nbre_par;i++) System.out.print(par_init[i]+"  ");
		//System.out.println("R²  "+r_tot);
	
	/**************** boucle jackknife ******************/
	for(jk=0;jk<nim;jk++){   // Boucle années

		calc_var_jk();
		marquardt();
		pue_c[jk] = EquationModele.fonction_modele(f[jk],v[jk],vbar[jk],par_init); // par_init = valeur initiale paramètre dans Marquart
		pue_j[jk] = EquationModele.fonction_modele(f[jk],v[jk],vbar[jk],par_alors); // par_alors = valeur finale paramètre dans Marquart
		sdc_jk += (pue[jk]-pue_j[jk])*(pue[jk]-pue_j[jk]); // Somme des carrés des écarts
		for(i=0;i<nbre_par;i++) {
			jkn[jk][i+1]=par_alors[i];// jkn[nim][nbre_par + 1] = valeur du paramètre après retrait de l'année nim.
			//System.out.println("jk = " + jk + " i = " + i + " par_alors[i] = " + par_alors[i]);
			Parjk[i+1]+=par_alors[i];
		}	
		val_fin = somme_residus_jk(par_alors);
		jkn[jk][0] = 1 - (val_fin/variance_pue); // pour r2
		for(i=0;i<nbre_par;i++)
		  pseudo[jk][i]= nim*par_init[i]-(nim-1)*par_alors[i]; // ici. PF pseudo-valeurs = différence entre valeurs paramètres avec ou sans retrait d'une année, pondérée par le nb total d'années dans le modèle.
		vv = val_v[2]; // V mini au début de la boucle Jackknife
		if((Global.numero_modele > 5) || (Global.numero_modele < 2) && (Global.numero_modele != 33)) // Exclus les modèles CPUE = f(V) Commentaire 2020
			for(iv=0;iv<nv;iv++){   // Sous boucle nv = longueur arbitraire boucle Jackknife (iv). Modif 2020
				f_ms = EquationModele.minimum_fonction(vv,par_alors);               // Calcul des MSE pour nv valeurs consécutives de V et modèle ajusté avec nim-1 années.
				y_ms = f_ms * EquationModele.fonction_modele(f_ms,vv,vv,par_alors); // Calcul des MSY pour nv valeurs consécutives  V (Y = CPUE * E) et modèle ajusté avec nim-1 années.
				//System.out.println("jk = " + jk + "vv = " + vv + "; f_ms = " + f_ms + "; y_ms2= " + y_ms);
				f_ms_m[iv] += f_ms;
				f_ms_s[iv] += (f_ms*f_ms);
				y_ms_m[iv] += y_ms;
				y_ms_s[iv] += (y_ms*y_ms);
				vv += ecartv; // Augmentation du pas de V.
				}
	    //******************************************************************
	    //*** Calcul des MSY MSE pour les valeurs remarquables**************
	    //*** presentees dans dossier html, onglet validation et************
	    //*** dans tableau validation de "Modelisation" -> "Display ********
	    //*** result tables" ***********************************************
	    //******************Modifications du 20/10/96***********************
	    if((Global.numero_modele > 5) || (Global.numero_modele < 2) && (Global.numero_modele != 33)) // Exclus les modèles CPUE = f(V) Commentaire 2020	
        Global.message$[6] = "";
		for(iv=0;iv<4;iv++){  // Sous boucle iv. Attention, ici iv ne varie plus jusqu'à nv mais de 0 à 3 pour les valeurs remarquables.
		        	vv = val_v[iv];
				f_ms2 = EquationModele.minimum_fonction(vv,par_alors); 				   // Calcul des MSE pour valeurs remarquables de V et modèle ajusté avec nim-1 années..
				y_ms2 = f_ms2 * EquationModele.fonction_modele(f_ms2,vv,vv,par_alors); // Calcul des MSY pour valeurs remarquables de V (Y = CPUE * E)et modèle ajusté avec nim-1 années..
				//System.out.println("jk = " + jk + " iv = " + iv + "; vv = " + vv + "; f_ms2== EquationModele.minimum_fonction(vv,par_alors) = " + f_ms2);
				f_ms_m2[iv] += f_ms2;
				f_ms_s2[iv] += (f_ms2*f_ms2);
				y_ms_m2[iv] += y_ms2;
				y_ms_s2[iv] += (y_ms2*y_ms2);
		}
		
	    //*******************************************************************

		if(jkn[jk][0]<0) jkn[jk][0] = 0.0;
               // for(i=0;i<nbre_par;i++) System.out.print(par_alors[i]+"   ");
		       //System.out.println("R�  " +100*jkn[jk][0]);
	}

	/**************** calculs des pseudo-valeurs *********************/
       	for(i=0;i<nbre_par;i++)  {
			moyenne_pseudo[i] = 0.0;//ici
			for(jk=0;jk<nim;jk++)
				moyenne_pseudo[i] += pseudo[jk][i]; //ici 2
			moyenne_pseudo[i] /= (double)nim;//ici
		}
	for(i=0;i<nbre_par;i++) 
	for(j=0;j<nbre_par;j++) { 
		covar_pseudo[i][j] = 0.0;//ici // Matrice de covariance des paramètres
		for(jk=0;jk<nim;jk++)
				covar_pseudo[i][j] += pseudo[jk][i] * pseudo[jk][j];//ici 2 (i+1 2fois)
		covar_pseudo[i][j] /= (double)nim;//ici; moyenne des années 
		covar_pseudo[i][j] -= (moyenne_pseudo[i]*moyenne_pseudo[j]);//ici
		covar_pseudo[i][j] /= (double)nim;//ici; moyenne des années
		//System.out.println("i = " + i + " j= " + j + " covar "+covar_pseudo[i][j]);
	}
	for(i=0;i<nbre_par;i++)  ecart_pseudo[i] = Math.sqrt(covar_pseudo[i][i]); // Ecart-type pseudo-valeurs = diagonale matrice covariance. Commentaire 2020
	
        /****************************** Test du Jackknife *******************************/
    nimTabStudent = Data.getNbDataRetenue();
    if (nimTabStudent > 50) nimTabStudent = 50;
    Global.test_jackknife=true; // Initialisation test (vrai par défaut)
        // TODO vérifier et décommenter
    for(i=0;i<nbre_par;i++) 
    	{
    		//System.out.println(" i = " + i + " ecart_pseudo[i] = " + ecart_pseudo[i]);
    		if(ecart_pseudo[i]!=0)
          	{  	
				if (Math.abs(par_init[i]/ecart_pseudo[i]) < Global.TableStudent[nimTabStudent- Global.nbre_param - 1][0]) { // Modifié en utilisant tableau valeurs seuils de t
					Global.test_jackknife=false;
					Global.Res_Test_Jackknife_Par[i] = 2;   // Rajout 2020
					//System.out.println("Ligne 380 Validation.java i: " + i + " Global.Res_Test_Jackknife_Par[i] = " + Global.Res_Test_Jackknife_Par[i]);
				}
				else
				if (Math.abs(par_init[i]/ecart_pseudo[i]) >= Global.TableStudent[nimTabStudent- Global.nbre_param - 1][0] && Math.abs(par_init[i]/ecart_pseudo[i]) < Global.TableStudent[nimTabStudent- Global.nbre_param - 1][1]) {			
					Global.Res_Test_Jackknife_Par[i] = 1;
				}
				else { Global.Res_Test_Jackknife_Par[i] = 0; 
				}	
     		}
     		 else
            Global.Res_Test_Jackknife_Par[i]= 3;
		}
 
	Global.r_jk  = (1 - (sdc_jk/var_tot)); // Valeur du R2 jackknife. Commentaire 2020
	if(r_tot<0.0) r_tot = 0.0;
	if(Global.r_jk<0.0) Global.r_jk = 0.0;
        //System.out.println(" Conventional coefficient of determination R² = " + r_tot);
	    //System.out.println("    Jackknife coefficient of determination R² = "+Global.r_jk);
        Global.coeff_determination=r_tot/100;
    for(i=0;i<nbre_par;i++)
        {
		Parjk[i+1] /= nim; // Modif 2020. Valeurs jackknife des paramètres de la regression (= moyennne des n années).
		//System.out.println("i = " + i + "; Parjk[i+1] = " + Parjk[i+1] + " nim = " + nim);
	    for(jk=0;jk<nim;jk++) trjk[jk] = 100*jkn[jk][i+1]/par_init[i];//ici. Rajout commentaire 2020: Valeur relative des coefficients de regression jackknife par année par rapport au coefficient conventionel.
               // System.out.print(trjk[jk]+"  ");
               // System.out.println();
        }

		/*******Calcul des valeurs théoriques en fonction**** 
		********de valeurs remarquables de V observées*******
		********   et warnings de valeurs anormales**********
		********* Modification du 20/10/96*******************/

		Global.Min_95_Noteworthy_MSE = 0;
		Global.Min_95_Noteworthy_MSY = 0;
		Global.Max_95_Noteworthy_MSE = 0;
		Global.Max_95_Noteworthy_MSY = 0;
		for (int n=1; n<5;n++) Global.message$[n] = ""; 
		for (int n=6; n<14;n++) Global.message$[n] = "";
	for(iv=0;iv<4;iv++){
			vv = val_v[iv];
			f_ms_m2[iv] /= nim; // Moyenne des valeurs MSE résultant du jackknife
			f_ms_s2[iv] /= nim;
			f_ms_s2[iv] -= (f_ms_m2[iv]*f_ms_m2[iv]);//sqr
			//System.out.println("iv = " + iv + "; vv = " + vv + "; f_ms_m2[iv] = " + f_ms_m2[iv]); 
			if(f_ms_s2[iv]>0.0)f_ms_s2[iv] = Math.sqrt(f_ms_s2[iv]);
			else f_ms_s2[iv] = 0.0;
			if (f_ms_m2[iv] > 1.0E10  && Global.numero_modele != 20 && (Global.numero_modele > 5 || Global.numero_modele < 2) && Global.numero_modele != 33)
				{
				Global.message$[6] = "Warning: \nAt least one of the central values of MSE larger than 1.0E10\nfor all noteworthy V values which suggest an unrealistic shape \nof MSY vs V and/or MSE vs V. For more details please open \nthe menu 'Modelization' and click on 'Display result tables'\nand on the tab 'Validation'.\nThe graphs MSY vs V and MSY vs V are not shown.\n\n" ;
				Global.MSY_MSE_OK=false;
				}
			if (f_ms_m2[iv] < 0 && Global.numero_modele != 20 && (Global.numero_modele > 5 || Global.numero_modele < 2) && Global.numero_modele != 33)
				{				
				Global.message$[10] = "Warning: \nAt least one of the central values of MSE is negative for\nall noteworthy V values which suggest an unrealistic shape \nof MSY vs V and/or MSE vs V. For more details please open \nthe menu 'Modelization' and click on 'Display result tables'\nand on the tab 'Validation'.\nThe graphs MSY vs V and MSY vs V are not shown.\n\n" ;
				Global.MSY_MSE_OK=false;
				}
				//System.out.println("Ligne 459 Validation Global.MSY_MSE_OK = " + Global.MSY_MSE_OK);
			//System.out.println(" iv = " + iv + " f_ms_m2[iv] = " + f_ms_m2[iv] + "f_ms_m2[iv] > 1.0E10 || f_ms_m2[iv] < 0");                          ";

			vec_f_min2[iv] = f_ms_m2[iv] - 1.96 * f_ms_s2[iv];
			vec_f_max2[iv] = f_ms_m2[iv] + 1.96 * f_ms_s2[iv];
			//System.out.println("iv = " + iv + " vec_f_min2[iv] = " + vec_f_min2[iv]);
			y_ms_m2[iv] /= nim; // Moyenne des valeurs MSY résultant du jackknife
			y_ms_s2[iv] /= nim;
			y_ms_s2[iv] -= (y_ms_m2[iv]*y_ms_m2[iv]);//sqr
			//System.out.println("iv = " + iv + "; y_ms_m2[iv] = " + y_ms_m2[iv]); 
			if(y_ms_s2[iv]>0.0)y_ms_s2[iv] = Math.sqrt(y_ms_s2[iv]);
			else y_ms_s2[iv] = 0.0;
			vec_y_min2[iv] = y_ms_m2[iv] - 1.96 * y_ms_s2[iv];
			vec_y_max2[iv] = y_ms_m2[iv] + 1.96 * y_ms_s2[iv];
			if(vec_f_min2[iv]<0.01) Global.Min_95_Noteworthy_MSE = Global.Min_95_Noteworthy_MSE +1;
			if(vec_y_min2[iv] <0.01) Global.Min_95_Noteworthy_MSY = Global.Min_95_Noteworthy_MSY +1;
			if(vec_f_max2[iv]<0.01) Global.Max_95_Noteworthy_MSE = Global.Max_95_Noteworthy_MSE +1;
			if(vec_y_max2[iv] <0.01) Global.Max_95_Noteworthy_MSY = Global.Max_95_Noteworthy_MSY +1;
			if(y_ms_m2[iv] < 0.0)
				Global.message$[7] = "Please note that some MSY central values for at least one \nof the noteworthy V values are unexpectidly negative.\n\n";
            if((((vec_y_max2[iv] - vec_y_min2[iv])) / y_ms_m2[iv]) > 1.0)
                Global.message$[8] = "Please note that the width of the 95% confidence interval of MSY central \nvalues for at least one of the noteworthy V values is larger than\nthe corresponding MSY value. \nDetails available in the result table (tab 'Validation').\n\n";
            if((((vec_f_max2[iv] - vec_f_min2[iv])) / f_ms_m2[iv]) > 1.0) 
                Global.message$[9] = "Please note that the width of the 95% confidence interval of MSE \ncentral values for at least one of the noteworthy V values is larger \nthan the corresponding MSE value. \nDetails available in the result table (tab 'Validation').\n\n";					  
			//System.out.println("iv = " + iv + " Val MSY interv. 95% = " + (vec_y_max2[iv] - y_ms_m2[iv]) + " Val central MSY = " + y_ms_m2[iv]);
			if(y_ms_m2[iv] > (Data.stat[7][0] * 10))
				Global.message$[11] = "Please note that some MSY central values for at least\none of the noteworthy V values are 10 times larger\nthan the maximum observed catch value, which is unexpected\nif the stock underwent optimal and/or overexploitation.\n\n";
			if(f_ms_m2[iv] > (Data.stat[7][2] * 5))
				Global.message$[12] = "Please note that some MSE central values for at least\none of the noteworthy V values are 5 times larger \nthan the maximum observed fishing effort value, which is \nunexpected if the stock underwent optimal and/or \noverexploitation.\n\n";
	}
	//System.out.println("y_ms_m2[0] = " + y_ms_m2[0] + " f_ms_m2[1] = " + f_ms_m2[1] + " f_ms_m2[2] = " + f_ms_m2[2] + " f_ms_m2[3] = " + f_ms_m2[3]);
    //saveResult();
	//System.out.println("Global.Max_95_Noteworthy_MSE = " + Global.Max_95_Noteworthy_MSE + " Global.Min_95_Noteworthy_MSE = " + Global.Min_95_Noteworthy_MSE);  
	//System.out.println("Global.Max_95_Noteworthy_MSY = " + Global.Max_95_Noteworthy_MSY + " Global.Min_95_Noteworthy_MSY = " + Global.Min_95_Noteworthy_MSY);  
	//System.out.println("Flag makePlotJack() Validation.java ligne 484");
	makePlotJack();  // Make jackknife parameters (and not anymore MSY = f(V) + MSE = f(V) plots)
    Global.validationOk=true; 
      
	if ((Global.Max_95_Noteworthy_MSE == 4 || Global.Max_95_Noteworthy_MSY == 4) && Global.numero_modele != 20 && (Global.numero_modele > 5 || Global.numero_modele < 2) && Global.numero_modele != 33) {
		Global.message$[3] = "WARNING: \nAll noteworthy values of MSY and/or MSE upper limits at 95% \nare null or negative which suggest a poor fit or an unrealistic shape \nof MSY vs V and/or MSE vs V. \nFor more details please open the menu 'Modelization' and click \non 'Display result tables' and on the tab 'Validation'.\nThe graphs MSY vs V and MSY vs V are not shown.\n\n" ;
		Global.MSY_MSE_OK=false;
    }
	if ((Global.Min_95_Noteworthy_MSE>=4 && Global.Min_95_Noteworthy_MSY>=4) && Global.numero_modele != 20 && (Global.numero_modele > 5 || Global.numero_modele < 2) && Global.numero_modele != 33) {
		Global.message$[4] = "WARNING: \nAll noteworthy values of MSY and MSE lower limits at 95% are \nnull or negative which suggest a poor fit or an unrealistic shape \nMSY vs V and MSE vs V. \nFor more details please open the menu 'Modelization' and click \non 'Display result tables' and on the tab 'Validation'.\n\n" ;
	}
	if (Global.Min_95_Noteworthy_MSE>=4 && Global.numero_modele != 20 && (Global.numero_modele > 5 || Global.numero_modele < 2) && Global.numero_modele != 33) { 
		Global.message$[1] = "WARNING: \nAll noteworthy values of MSE lower limit at 95% are \nnull or negative which suggest a poor fit or an unrealistic shape \nMSE vs V. \nFor more details please open the menu 'Modelization' and click \non 'Display result tables' and on the tab 'Validation'.\n\n" ;
    }
	if (Global.Min_95_Noteworthy_MSY>=4 && Global.numero_modele != 20 && (Global.numero_modele > 5 || Global.numero_modele < 2) && Global.numero_modele != 33) {
		Global.message$[2] = "WARNING: \nAll noteworthy values of MSY lower limit at 95% are \nnull or negative which suggest a poor fit or an unrealistic shape \nMSY vs V. \nFor more details please open the menu 'Modelization' and click \non 'Display result tables' and on the tab 'Validation'.\n\n" ;
	}
	if (Global.message$[6] == "" && Global.message$[10] == "" && Global.message$[3] == "") {
		Global.MSY_MSE_OK=true;
    	makePlotMSE_MSY();
    }
	else {
	Global.msyPlot[0]=new Plot(); // [0] MS-E  versus V; & [1] MSY  versus V. 2020.
	Global.msyPlot[1]=new Plot();
	}
	//System.out.println("Ligne 514 Validation Global.MSY_MSE_OK = " + Global.MSY_MSE_OK);
}

public static Object[][]  getParamResult(){
	if(!Global.validationOk) return null;
	nimTabFish = Data.getNbDataRetenue();
	if (nimTabFish > 100) nimTabFish = 100;

 	// Sous-cadre de droite de fenetre "Climprod: Fit a model directly", titre "Validation:Main results"
 
	String [] title$={"Parameters","Value","Jackknife Standard deviation","Jackknife t-ratio & (test)"};
	Object[][] data$=new String[nbre_par+6][4];
	DecimalFormat nf= new DecimalFormat(" 0.00000;-0.00000");
	DecimalFormat nf2= new DecimalFormat(" 0.00");
	NumberFormat numFormat = new DecimalFormat();
	numFormat = new DecimalFormat("0.####E0");
	Global.test_jackknife=true; // Initialisation overall test
    for(int j=0;j<4;j++)
          data$[0][j]=title$[j];
    for(int i=0;i<nbre_par;i++)
    {
          if (Global.Flag_pb_jackknife[i] != 1)
             data$[i+1][0]=Global.param$[i];  		    // nom du paramètre de régression (a, b, ...)
          else 
          	 data$[i+1][0]=Global.param$[i] + "*";
          if ((Parjk[i+1] < 0.001 && Parjk[i+1] > 0) || Parjk[i+1] > 999.999 || (Parjk[i+1] > -1.0E-3 && Parjk[i+1] < 0)|| Parjk[i+1] < -999.999) data$[i + 1][1] = numFormat.format(Parjk[i+1]);
          else data$[i + 1][1] = nf.format(Parjk[i+1]); // Valeur paramètre de régression estimé fin Marquart
            if ((ecart_pseudo[i] < 0.001 && ecart_pseudo[i] > 0) || ecart_pseudo[i] > 999.999 || (ecart_pseudo[i]> -1.0E-3 && ecart_pseudo[i] < 0)|| ecart_pseudo[i] < -999.999) data$[i + 1][2] = numFormat.format(ecart_pseudo[i]);
            else data$[i + 1][2] = nf.format(ecart_pseudo[i]); // Jackknife Standard deviation
          if(ecart_pseudo[i]!=0)  // Répétition partielle des instructions lignes 384-403
          {
		    if (Math.abs(Parjk[i+1]/ecart_pseudo[i]) < Global.TableStudent[nimTabStudent- Global.nbre_param - 1][0]) { // Teste si valeur ABSOLUE < table Student.
				Global.Res_Test_Jackknife_Par[i] = 2;   // Rajout 2020
				Global.test_jackknife=false;
    		}
			else
			if (Math.abs(Parjk[i+1]/ecart_pseudo[i]) >= Global.TableStudent[nimTabStudent- Global.nbre_param - 1][0] && Math.abs(Parjk[i+1]/ecart_pseudo[i]) < Global.TableStudent[nimTabStudent- Global.nbre_param - 1][1]) {			
				Global.Res_Test_Jackknife_Par[i] = 1;
			}
			else { Global.Res_Test_Jackknife_Par[i] = 0; 
			}
            if (((Parjk[i+1]/ecart_pseudo[i]) < 0.001 && (Parjk[i+1]/ecart_pseudo[i]) > 0) || (Parjk[i+1]/ecart_pseudo[i]) > 999.999 || ((Parjk[i+1]/ecart_pseudo[i])> -1.0E-3 && (Parjk[i+1]/ecart_pseudo[i]) < 0) || (Parjk[i+1]/ecart_pseudo[i]) < -999.999) data$[i + 1][3] = numFormat.format(Parjk[i+1]/ecart_pseudo[i]) + Global.Test_Jackknife_Param$[Global.Res_Test_Jackknife_Par[i]];
            else data$[i + 1][3] = nf.format(Parjk[i+1]/ecart_pseudo[i]) + Global.Test_Jackknife_Param$[Global.Res_Test_Jackknife_Par[i]]; // Jackknife t-ratio & test.
			//System.out.println("Global.Test_Jackknife_Param$[2] = " + Global.Test_Jackknife_Param$[2]);
            // System.out.println("ecart_pseudo[i] = " + ecart_pseudo[i] + " i = " + i);
            //System.out.println("Global.TableStudent[nimTabStudent- Global.nbre_param - 1][1] = " + Global.TableStudent[nimTabStudent- Global.nbre_param - 1][1] + "  valeur à comparer à table Student;  Test Jakknife = " + Global.test_jackknife); // Test 2020    	
          }
          else
            data$[i+1][3]="na";
     }
     data$[nbre_par+1][0]= "Overall t Jackknife";
     if(Global.test_jackknife && Global.r_jk > 0.0) {
          data$[nbre_par+1][1]="good";
     }
     else
          data$[nbre_par+1][1]="bad";
     if (Global.coeff_determination != 0.0)          
         data$[nbre_par+2][0]= "Jackknife  R²";
     else
     	 data$[nbre_par+2][0]= "Jackknife  R²**";
         data$[nbre_par+2][1]= nf2.format(Global.r_jk);
	 if (Global.Flag_pb_jackknife_Tot != 0) {
	     data$[nbre_par+4][0] = "*See jackknife graph and";
	     data$[nbre_par+4][1] = "comment in the current";
	     data$[nbre_par+4][2] = "known facts window.";
	     }
	 if (Global.coeff_determination == 0.0) { 
	     data$[nbre_par+5][0] = "**See comment in the "; 
	     data$[nbre_par+5][1] = "current known facts ";
	     data$[nbre_par+5][2] = "window.";
	     } 
     return data$;
  }

/****************************************************
*              Jacknife plot						*
*			and warning messages					*
*****************************************************/

static private void makePlotJack(){
	
   /*********** Jacknife plots by parameter ********************
   ************ Makes use of PlotModal.java *******************/ 

	String[] ty={"a","b","c","d"};
	double[] years=Data.getYears();
	String[] etiq$=new String[nim];
	double[] etiq=new double[nim];
	double[] serie100=new double[nim];
	String message = "";
	DecimalFormat df=new DecimalFormat("0");
	for(int i=0;i<nim;i++)
	{
        etiq$[i]=df.format(years[i]).toString();
        etiq[i]=i+1;
	}
	for(int i=0;i<nim;i++)	serie100[i] = 100;
		PlotSerie ps100=new  PlotSerie("100%",etiq,"100%",serie100);
	ps100.setFigure(2);
	ps100.setCouleur(Color.black);
	//PlotSerie[] ps=new PlotSerie[nbre_par+1];
	Global.jackknifePlot= new PlotHisto[nbre_par+1];
	//System.out.println("Validation makePlotJack ligne 603 Validation nbre_par+1 = " + nbre_par + " Global.nbre_param = " + Global.nbre_param); 
	double[] trjk;
	int[] Nb_years_greater100=new int[nim]; // Number of years with coefficient > 100% Test 2020
	int[] Nb_years_lower100=new int[nbre_par];
    Global.Flag_pb_jackknife_Tot = 0;
    Global.message$[0] = "";
    
    for(int i=0;i<nbre_par;i++) Global.Flag_pb_jackknife[i] = 0; // Initialisation. Rajout 2020	
	for(int i=0;i<nbre_par;i++){
        trjk=new double[nim];
        Nb_years_lower100[i]= 0;
        Nb_years_greater100[i] = 0;
		for(jk=0;jk<nim;jk++)
		{
 	    	trjk[jk] = 100*jkn[jk][i+1]/par_init[i];  // Valeurs des barres des histogrammes des paramètres (par_init n'est plus la valeur initiale mais finale)
			//System.out.println("i = " + i +" jk = " + jk + " trjk[jk] = " + trjk[jk] + " jkn[jk][i+1] = " + jkn[jk][i+1] + " par_init[i] = " + par_init[i]); 	    	
			if (trjk[jk] <= 100) Nb_years_lower100[i] = Nb_years_lower100[i] +1; // Compte le nb d'occurence <100%
			if (trjk[jk] >= 100) Nb_years_greater100[i] = Nb_years_greater100[i] +1; // Compte le nb d'occurence >100%
			if (trjk[jk] > 200 || trjk[jk] < 1) ++Global.Flag_pb_jackknife_Tot;
		}
		if (Nb_years_greater100[i] == nim || Nb_years_lower100[i] == nim || Global.Flag_pb_jackknife_Tot > 0)
		{
		 	++Global.Flag_pb_jackknife_Tot;
			//System.out.println("Problem in Validation.java Nb years = " + nim + " i = " + i + " Flag_pb_jackknife_Tot = " + Global.Flag_pb_jackknife_Tot + " Nb_years_greater100[i] = " + Nb_years_greater100[i] + " Nb_years_lower100[i] " + Nb_years_lower100[i]);
		 	Global.Flag_pb_jackknife[i] = 1;
		 	Global.message$[0] = "Likely problem in Jackknife computation. \nAll years with parameter jackknife values < or > to 100%\nor at least one year with value >200% or <1%. \nThis is unexpected except if the length of the \ndataset is limited.\nHence jackknife results are questionable.\n" ;
		}
		//else 
		//System.out.println("No problem in Jackknife computation Nb years = " + nim  + " i = " + i + "  Nb_years_lower100[i] = " + Nb_years_lower100[i] + "  Nb_years_greater100[i] = " + Nb_years_greater100[i]); // Test 2020);
        PlotSerie ps= new  PlotSerie("Year/season",etiq,ty[i],trjk);
        ps.setFigure(3);
        Global.jackknifePlot[i]=new PlotHisto();
        Global.jackknifePlot[i].setValeurs(ps100);
        Global.jackknifePlot[i].setValeurs(ps);
		//System.out.println("Validation makePlotJack ligne 636 i: " + i + " Global.Res_Test_Jackknife_Par[i]: " + Global.Res_Test_Jackknife_Par[i]); 
        Global.jackknifePlot[i].setTitreGraphique1(Global.titreG[i+20] + ".");
        Global.jackknifePlot[i].setTitreGraphique2("Jackknife t-ratio " 
        	+ Global.Test_Jackknife_Param$[Global.Res_Test_Jackknife_Par[i]]);       
        Global.jackknifePlot[i].setTitreX(Global.titreSx[i+18]);
        Global.jackknifePlot[i].setTitreY(Global.titreSy[i+17]);
        
        double minx = Stat1.min(Data.getYears());
        double maxx = Stat1.max(Data.getYears());
        double maxY=Global.jackknifePlot[i].getMaxAxeY();
        if(maxY<150)
        {
           Global.jackknifePlot[i].setMinAxeY(0.0);
           Global.jackknifePlot[i].setMaxAxeY(maxY+10-(maxY%10));
           Global.jackknifePlot[i].setpasY(10.d);
        }
        else
         Global.jackknifePlot[i].ajusteExtremas(false,true,false,true);

        Global.jackknifePlot[i].setEtiquettes(etiq$);
        Global.jackknifePlot[i].setpasX(StrictMath.round((maxx-minx)/20));
        Global.jackknifePlot[i].setXcutYat(0.d);
        Global.jackknifePlot[i].setDecimalsurY(0);

	}
	/*******************************************************
	******************* R² jacknife plot *******************
	************ Makes use of PlotModal.java ***************
	*******************************************************/
	
	trjk=new double[nim];   	
	for(int ii=0;ii<nim;ii++)
    	if (r_tot > 0.0) trjk[ii] = 10000*jkn[ii][0]/r_tot; // Rajout 2020
    	else trjk[ii] = 0.0;
    DecimalFormat nf = new DecimalFormat("0.00");
    PlotSerie ps= new  PlotSerie("Years",etiq,"",trjk);
    ps.setFigure(3);
    ps.setCouleur(Color.blue);
    Global.jackknifePlot[nbre_par]=new PlotHisto();
    Global.jackknifePlot[nbre_par].setValeurs(ps);
    Global.jackknifePlot[nbre_par].setValeurs(ps100);
	Global.jackknifePlot[nbre_par].setTitreGraphique1(Global.titreG[24] 
		+ " (R2 = " + nf.format(Global.coeff_determination)
	    + "; Corrected R2 = " + nf.format(Global.corrected_R2));
	Global.jackknifePlot[nbre_par].setTitreGraphique2(" F test: " 
		+ Global.fFsignif$ + "; Jackknife R2 = " 
	    + nf.format(Global.r_jk) + ")");
    Global.jackknifePlot[nbre_par].setTitreX(Global.titreSx[22]);
    Global.jackknifePlot[nbre_par].setTitreY(Global.titreSy[21]);

    double maxY=Global.jackknifePlot[nbre_par].getMaxAxeY();
    if(maxY<150)
        {
           Global.jackknifePlot[nbre_par].setMinAxeY(0);
           Global.jackknifePlot[nbre_par].setMaxAxeY(maxY+10-(maxY%10));
           Global.jackknifePlot[nbre_par].setpasY(10.d);
        }
    else
        Global.jackknifePlot[nbre_par].ajusteExtremas(false,true,false,true);
	Global.jackknifePlot[nbre_par].setDecimalsurY(0); 
    Global.jackknifePlot[nbre_par].setEtiquettes(etiq$);
    Global.jackknifePlot[nbre_par].setpasX(3.d);
    Global.jackknifePlot[nbre_par].setXcutYat(0.d);
}

	/**************************************************************************
	****************************MSE & MS-Y versus V Plots**********************
	**************************************************************************/

static private void makePlotMSE_MSY(){  // Rajout 2020

	Global.msyPlot[0]=new Plot(); // [0] MS-E  versus V; & [1] MSY  versus V. 2020.
	Global.msyPlot[1]=new Plot();

	// if suivant exclus les modèles CPUE = f(V). Commentaire 2020

	if(((Global.numero_modele > 5) || (Global.numero_modele < 2)) && (Global.numero_modele != 20) && (Global.numero_modele != 33)) // Exclusion modèles CPUE=f(V) et modèle exponentiel additif
  	{
  		double vv,ecartv;

  		//System.out.println("num mod  " +Global.numero_modele);
  		ecartv = (val_v[3] - val_v[2])/nv;
  		vv = val_v[2];
  		for(int iv=0;iv<nv;iv++){
			f_ms_m[iv] /= nim;
        	//System.out.println("MSE : "+f_ms_m[iv]);
			f_ms_s[iv] /= nim;
			f_ms_s[iv] -= (f_ms_m[iv]*f_ms_m[iv]);
			if(f_ms_s[iv]>0.0)f_ms_s[iv] = Math.sqrt(f_ms_s[iv]);
			else f_ms_s[iv] = 0.0;
			vec_f_v[iv] = vv;
        	//System.out.println("X : "+vv);
			vec_f_min[iv] = f_ms_m[iv] - 1.96 * f_ms_s[iv];
        	//System.out.println("MSEmin : "+vec_f_min[iv]);
			vec_f_max[iv] = f_ms_m[iv] + 1.96 * f_ms_s[iv];
        	//System.out.println("MSEmax : "+vec_f_max[iv]);
			y_ms_m[iv] /= nim;
			y_ms_s[iv] /= nim;
			y_ms_s[iv] -= (y_ms_m[iv]*y_ms_m[iv]);
			if(y_ms_s[iv]>0.0)y_ms_s[iv] = Math.sqrt(y_ms_s[iv]);
			else y_ms_s[iv] = 0.0;
			vec_y_v[iv] = vv;
			vec_y_min[iv] = y_ms_m[iv] - 1.96 * y_ms_s[iv];
			vec_y_max[iv] = y_ms_m[iv] + 1.96 * y_ms_s[iv];
			vv += ecartv;
 		}

  		PlotSerie[] pms=new PlotSerie[6];
  		pms[0]=new PlotSerie("Environment (V)",vec_f_v,"MSE jackknife",f_ms_m);
  		pms[1]=new PlotSerie("Environment (V)",vec_f_v,"Lower CI limit (95%)",vec_f_min);
  		pms[2]=new PlotSerie("Environment (V)",vec_f_v,"Upper CI limit (95%)",vec_f_max);
  		pms[3]=new PlotSerie("Environment (V)",vec_y_v,"MSY jacknife",y_ms_m);
  		pms[4]=new PlotSerie("Environment (V)",vec_y_v,"Lower conf. limit (95%)",vec_y_min);
  		pms[5]=new PlotSerie("Environment (V)",vec_y_v,"Upper conf. limit (95%)",vec_y_max);

  		for(int i=0;i<6;i++){
     	 	pms[i].setFigure(2);
      		if(i==2 || i==5)
          	pms[i].setCouleur(Color.yellow);
      		else if(i==1 || i==4)
          	pms[i].setCouleur(Color.green);
  		}

  		Global.msyPlot[0].setValeurs(pms[0]);
  		Global.msyPlot[0].setValeurs(pms[1]);
  		Global.msyPlot[0].setValeurs(pms[2]);
  		Global.msyPlot[1].setValeurs(pms[3]);
 		Global.msyPlot[1].setValeurs(pms[4]);
  		Global.msyPlot[1].setValeurs(pms[5]);

  		Global.msyPlot[0].setTitreGraphique("Jackknife MSE versus V");
  		Global.msyPlot[1].setTitreGraphique("Jackknife MSY versus V");
  		for(int i=0;i<2;i++){
    		Global.msyPlot[i].setMinAxeY(0.0);
    		Global.msyPlot[i].setMinAxeX(Data.stat[6][3]-(Data.stat[8][3]/20));
			//System.out.println("i = " + i + " Data.stat[6][3] = " + Data.stat[6][3] + " Data.stat[6][3]-Data.stat[6][3]/10 = " + (Data.stat[6][3] - Data.stat[6][3]*0.10));
			Global.msyPlot[i].ajusteExtremas(false,true,false,true);// Valeurs booléenes pour ajustement extrémités axes. Gère aussi décimale mais dans Plot.java.
    		Global.msyPlot[i].ShowLegend=true;
    		if (Data.stat[8][3] < 100.0) Global.msyPlot[i].setDecimalsurX(2); // stat[7][3] <10 = maximum V. Avant setDecimalsurX(0) -> pas de décilmale sur axe des X. Avec setDecimalsurX(1) -> décimales SI NECESSAIRE. Modif 2020.
		    else Global.msyPlot[0].setDecimalsurX(0);
		    //System.out.println("Data.stat[7][2] dans Validation.java= " + Data.stat[7][2]);
  		}
		if (f_ms_m2[0] < 100)			   
    		Global.msyPlot[0].setDecimalsurY(2);  // Avant pas de if et setDecimalsurX(0). MSE & MS-Y versus V Plots Modif 2020.
		else
			Global.msyPlot[0].setDecimalsurY(0);
		if (y_ms_m2[0] < 100.0)			   
    		Global.msyPlot[1].setDecimalsurY(2);  // Avant pas de if et setDecimalsurX(0). MSE & MS-Y versus V Plots Modif 2020.
		else
			Global.msyPlot[1].setDecimalsurY(0);
	}
 }
 
/*
  Donne les valeurs de MSE pour les valeurs remarquables(moyenne-médiane-min-max)
  @return un Object ou null si validation non Ok
*/

public static Object[][]  getMS_EResult(){
	if(!Global.validationOk) return null;
	Object[][] data$=new String[5][4];
	String[] title$={"Noteworthy V values","MSE Lower limit 95%","MSE  Central Value ","MSE Upper limit 95%"};
	//String[] limitMS_V={"MS_V Lower limit 95%","MS_V  Central Value ","MS_V Upper limit 95%"};
	String[] stat$={"   Mean: "," Median: ","Minimum: ","Maximum: "};
	DecimalFormat nf= new DecimalFormat(" 0.00;-0.00");

	for(int i=0;i<4;i++)
	  stat$[i]=stat$[i]+ nf.format(val_v[i]);
	for(int j=0;j<4;j++)
    data$[0][j]=title$[j];

	for(int i=0;i<4;i++)
	{
    	data$[i+1][0]=stat$[i];
    	data$[i+1][1]=nf.format(vec_f_min2[i]);
    	data$[i+1][2]=nf.format(f_ms_m2[i]);
    	data$[i+1][3]=nf.format(vec_f_max2[i]);
	}

	return data$;
}

/*
  Donne les valeurs de MS_Y pour les valeurs remarquables(moyenne-m�diane-min-max)
  @return un Object ou null si validation non Ok
*/
public static Object[][]  getMS_YResult(){
	if(!Global.validationOk) return null;
	Object[][] data$=new String[5][4];
	String[] title$={"Noteworthy V values","MSY Lower limit 95%","MSY  Central Value ","MSY Upper limit 95%"};
	String[] stat$={"   Mean: "," Median: ","Minimum: ","Maximum: "};
	DecimalFormat nf= new DecimalFormat(" 0.00;-0.00");

	for(int i=0;i<4;i++)
	  stat$[i]=stat$[i]+ nf.format(val_v[i]);

	for(int j=0;j<4;j++)
      data$[0][j]=title$[j];

	for(int i=0;i<4;i++)
	{
    	data$[i+1][0]=stat$[i];
    	data$[i+1][1]=nf.format(vec_y_min2[i]);
    	data$[i+1][2]=nf.format(y_ms_m2[i]);
    	data$[i+1][3]=nf.format(vec_y_max2[i]);
	}

	return data$;
}

/*
  Donne le détails des résultats sur les paramètres du modèle suite à la validation Jackknife
  @return un tableau Object ou null si validation non Ok
*/

/*
  Donne la valeur des paramètres pour chaque annèe suite à la validation
  @return un tableau Object ou null si validation non Ok
*/
public static Object[][]  getYearResult(){

	if(!Global.validationOk) return null;

	String[] title$={"Year/season","a (%)","b (%)","c (%)","d (%)"};
	double[] years=Data.getYears();
	Object[][] data$=new String[nim+1][nbre_par+2];
	DecimalFormat nf0= new DecimalFormat("0");
	DecimalFormat nf= new DecimalFormat(" 0.00;-0.00");
	for(int j=0;j<nbre_par+1;j++)
          data$[0][j]=title$[j];
	data$[0][nbre_par+1]="R²";

	for(jk=0;jk<nim;jk++)
 	    data$[jk+1][0] = nf0.format(years[jk]);

	for(int i=0;i<nbre_par;i++){
        for(jk=0;jk<nim;jk++)
 	    data$[jk+1][i+1] = nf.format(100*jkn[jk][i+1]/par_init[i]);
	}
	for(int i=0;i<nim;i++)
    data$[i+1][nbre_par+1] = nf.format(10000*jkn[i][0]/r_tot);

	return data$;
  }

}
