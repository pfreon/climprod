 /* Titre : Climprod<p>
 * Appelle et lance les graphiques serie, bivariees et histogrammes 
 * (sauf ceux lancés dans Data.java) pour réponse à questions graphiques 
 * au debut des questions du menu "Select the appropriate model and fit it".
 * Ajuste un modèle par l'algorithme de Marquardt par la méthode marquart() qui 
 * se trouve dans Validation.java .
 * Réalise le test F de Fisher*/
 
package fr.ird.climprod;

import static fr.ird.climprod.Data.getNbDataRetenue;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;
import java.awt.Color;

public class Modele {
    static final private int nparmax = 4;
	static final private int maxrit = 150;

    static final private double rl = 1.5;
    static final private double alpha = 4;
    static final private double eps = 1.E-8;
    static final private double mul = 1.E-3;
	static public int nim;
	static public boolean Flag_additive_model_fitted; 
    static private int nimTabFish;
    static private int nbYears; 				 // Addition 2020.
    static private double lambda;
    static private int nbre_par;
    static private int iter, riter, ritertot;
    static private double val_init, val_fin, ecart;
    static private double r,sompar, variance_pue, yy, aic, aic_corr, bic;

    static private double[] ptmp;
    static private double[][] covar;

    static private double[][] hessien;
    static private double[] covpy;
    static private double[] par_alors;
    static private double[] racineh;
    static private double[] derivee;
    static private double[] delta;
    static private double[] par_init;
    static private double[] par_inter;
    static private double[] spar;
    static private double[] par;
    static private double[] dpar;
    static private double[] sompari;
    static private double[] f, v, vbar, pue, res, fittedpue;
    static private double[] resiter;
    static private double[][] derfonc;

    public static void Estimer() {
        String message = "";
        Global.validationOk = false;
        initVariable();
        Data.init_val();
        nim = Data.getNbDataRetenue();

        f = Data.getF(); // f = effort (moyenne pondérée si nécessaire)
        v = Data.getV(); // v = environnement
        vbar = Data.getVbar();
        pue = Data.getPue();
        res = new double[nim];
        fittedpue = new double[nim];
        variance_pue = Stat1.var(pue, true) * nim;
        //calc_var();
        calcul_val_init();
		double PosInfinity = Double.POSITIVE_INFINITY;
		double NegInfinity = Double.NEGATIVE_INFINITY;
        Global.message$[13] = "";
        for (int i = 0; i < nbre_par; i++) {
            par_init[i] = par_alors[i];      // Valeur initiale paramètre pour une estimation des valeurs initiales du modèle simplifié initial (sans constante) avant ajustement du modèle final incluant une constante. Comment. 2020
			if (par_alors[i] == PosInfinity || par_alors[i] == NegInfinity)
				Global.message$[13] = "The estimate of the initial value of at least one of the parameter is equal to infinity.\nThis is likely due to a poor fitting of the model. As a result, the plots related to\nmodelling are not shown.\n\n";
        }         
		//System.out.println( "Avant marquardt");
		//for(int i=0;i<nbre_par;i++)System.out.print( par_alors[i]+" ");
        //  System.out.println();
        
     	marquardt();
        String message2 = "";
        sompar = somme_residus(par_alors);
		// System.out.println("sompar = " + sompar);
		System.out.println("iter = " + iter + " ritertot = " + ritertot);
        if (iter >= maxrit-1) {
            message2 = "No convergence after" + maxrit + " iterations. I stop volontary the algorithm.";
            message2 = message2
                    + " The fit will probably be poor and the validation might be impossible to run.";
            MsgDialogBox msg2 = new MsgDialogBox(0, message2, 1, Global.CadreMain);
        }

        variance_pue = variance_pue / (double) nim;  // variance_pue * nim en ligne 64...
	// System.out.println("variance_pue = " + variance_pue + "; nim = " + (double) nim);
        sompar = sompar / (double) nim;   //   Résidus moyens
        if (sompar > variance_pue) {    // Si résidus moyens > variance des CPUE on met r à zéro + message.
            sompar = variance_pue;
        }
       //System.out.println("sompar = " + sompar);
        r = (1. - sompar / variance_pue);
        //System.out.println("r = " + r + " sompar = " + sompar + " variance_pue = " + variance_pue);
        Global.corrected_R2 = 1. - (((double)nim - 1.) / ((double)nim - (double)nbre_par)) * (1. - r); // Addition 2020  
        //System.out.println("Global.corrected_R2 = " + Global.corrected_R2 + " r = " + r + " nim = " + nim + " nbre_par = " + nbre_par);
        //System.out.println("sompar 3 = " + sompar+" r = " + r);
        if (r < 0) {
            r = 0;
        }
        if (r == 0){
            message = "R² estimate was negative and consequently set to zero.\nThis result is due to poor fitting by the Marquart algorithm\nand it is commonly observed when the model does not include\nan intercept parameter or when the model includes a non-linear function.";
            MsgDialogBox msg = new MsgDialogBox(0, message, 1, Global.CadreMain);
        }
        if (Global.corrected_R2 < 0) {
            Global.corrected_R2 = 0;
        }
        //System.out.println("Variance " + variance_pue);
        // System.out.println("Variance r�siduelle " + sompar);
        // System.out.println("R� " +r);
        aic = AICritere();
        aic_corr = AICritereCorr();
        bic = BICCritere();
        Global.nbre_param = nbre_par;
        Global.val_param = new double[nbre_par];
        //  System.out.println( "Apr�s marquardt");
        for (int i = 0; i < nbre_par; i++) {
            Global.val_param[i] = par_alors[i];
        }
        Global.coeff_determination = r;
        Global.critereAIC = aic;
        Global.critereAIC_Corr = aic_corr;
        Global.critereBIC = bic;
           // for(int i=0;i<nim;i++)
        //     System.out.println("********"+res[i]);
        calc_residus();
        makePlot();
        // saveResult();
        Global.modelisationOk = true;
        nbYears = Data.getNbYears(); // This line and the 22 next one were added/modified to provide additional warning on df limitations. Modif, 2020.
        if ((nim - nbre_par) < 10) {  // nim = nbData - decalmax, as define in Data.java getNbDataRetenue()
           if ((nim - nbre_par) < 8) {
            message
                    = "WARNING! \nToo few d.f. left for fitting the retained model. \n\nOwing to " + (nbYears - nim) + " annual lags related to delay in environmental influence or \nto fishing effort averaging, and to " + nbre_par + " parameters included in the model,";
            message = message
                    + "\nonly " + (nim - nbre_par) + " degrees of freedom are left for fitting.\nResults will be extremely poor.\n\n";
            message = message
                    + " Please make sure that your estimates of: \n    -the number of significantly exploited year-classes;\n    -age at recruitment;";
            message = message
                    + "\n    -age at the begining of the environmental influence; \n    -age at the end of the environmental influence; \n are correct.\n\n";
            message = message
                    + "If so, it is strongly recommanded that you stop here the procedure.";
            MsgDialogBox msg = new MsgDialogBox(0, message, 1, Global.CadreMain);
            }
            else
            {message = "";
            message = "WARNING! \nFew d.f. left for fitting the retained model. \n\nOwing to " + (nbYears - nim) + " annual lags related to delay in environmental influence or \nto fishing effort averaging, and to " + nbre_par + " parameters included in the model,";            message = message
                    + "\nonly " + (nim - nbre_par) + " degrees of freedom are left for fitting.\n\nResults will be poor.";
            message = message
            		+ "\nDespite this warning I shall continue fitting.";
            MsgDialogBox msg = new MsgDialogBox(0, message, 1, Global.CadreMain);
            }
        }
    }

    private static void calcul_val_init() { // Fixation ou Calcul des valeurs initiales pour algo Marquardt
        int l, k;
        double tmp;

        initcov();

        switch (Global.numero_modele) {
            case 0:   // Modèle Schaefer CPUE=a+b.E. Commentaire 2020
                nbre_par = 2; // ATTENTION, pour modèle complexe deux définitions de nbre_par (nombre de paramètres pour modèle simplifié puis plus bas nb pour modèle final).
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;     // Initialisation à 1 du paramètre a. 
                    ptmp[1] = f[k];  // f = effort
                    yy = pue[k];
                    itercov();
                }
                //inverse_gauss(nbre_par,covar,par_alors,covpy);
                inverse_gauss();
                break;
            case 1:  // Modèle de Fox (exponentiel). CPUE=a.exp(b.E)CPUE=a.exp(b.E). Commentaire 2020
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = f[k];
                    yy = Math.log(pue[k]);  // Transformation log de CPUE. Commentaire 2020
                    itercov();
                }
                inverse_gauss();
                //inverse_gauss(nbre_par,covar,par_alors,covpy);
                par_alors[0] = Math.exp(par_alors[0]);  // Transformation exp. de paramètre a Commentaire 2020
                break;
            case 2:    // CPUE=a+b.V. Commentaire 2020
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = vbar[k];
                    yy = pue[k];
                    itercov();
                }
                inverse_gauss();
                break;
            case 3:		// CPUE=a.V^b. Commentaire 2020
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = Math.log(vbar[k]);    // Transformation log de V. Commentaire 2020
                    yy = Math.log(pue[k]);			// Transformation log de CPUE. Commentaire 2020
                    itercov();
                }
                inverse_gauss();
                par_alors[0] = Math.exp(par_alors[0]); // Transformation exp. de paramètre a Commentaire 2020.
                break;
            case 4:		// CPUE=a+b.V^c. Commentaire 2020
                nbre_par = 2;  // ATTENTION, nombre de paramètres pour modèle simplifié.
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = vbar[k];
                    yy = pue[k];
                    itercov();
                }
                inverse_gauss();
                nbre_par = 3; // Nombre de paramètres pour modèle final.
                par_alors[2] = 1.; //Parametre c initialisé à 1
                break;
            case 5:	// CPUE=a+b.V+c.V^2. avant CPUE=a.V+b.V^2+c. Commentaire 2020
                nbre_par = 3;
                for (k = 0; k < nim; k++) {
                    ptmp[1] = vbar[k];
                    ptmp[2] = (vbar[k] * vbar[k]);
                    ptmp[0] = 1.;
                    yy = pue[k];
                    itercov();
                }
                inverse_gauss();
                break;
            case 6:  // Modèle généralisé CPUE=(a+b.E)^(1/(c-1). Commentaire 2020
                nbre_par = 2;  // Nombre de paramètres pour modèle final.
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = f[k];
                    yy = pue[k];
                    itercov();
                }
                inverse_gauss();
                nbre_par = 3;  // Nombre de paramètres pour modèle final)
                par_alors[2] = 2.; // Paramètre c initialisé à 2.0. Commentaire 2020
                break;
            case 7:                // CPUE=a.V^b+c.E. Commentaire 2020
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = vbar[k];
                    ptmp[1] = f[k];    // Ajuste CPUE = aV+bE en première approximation
                    yy = pue[k];
                    itercov();
                }
                inverse_gauss();
                par_alors[2] = par_alors[1]; // Substitution de b à c pour ajustement modèle final.
                par_alors[1] = 1;  // Initialisation à 1 du parametre b. Commentaire 2020
                nbre_par = 3;
                break;
			case 34:   // CPUE=a.V.exp(b.V)+c.E
                nbre_par = 2;
                for (k = 0; k < nim; k++) { // Ajuste CPUE = a.exp(b.V) en première approximation
					ptmp[0] = 1;
                    ptmp[1] = vbar[k];
                    yy = Math.log(pue[k] / vbar[k]); // Division par vbar afin de passer de CPUE = aV.exp(b.V) à CPUE = a.exp(b.V)
                    itercov();
                }
                inverse_gauss();
				//System.out.println("par_alors[0] = " + par_alors[0] + " par_alors[1] = " + par_alors[1] + " par_alors[2] = " + par_alors[2]);
                par_alors[0] = Math.exp(par_alors[0]);
                par_alors[2] = eps;
				//System.out.println("Ensuite par_alors[0] = " + par_alors[0] + " par_alors[1] = " + par_alors[1] + " par_alors[2] = " + par_alors[2]);
                nbre_par = 3;
                break;
				/*case 35:   // CPUE=a+b.V.exp(c.V)+d.E (Idem modèle 34 CPUE=a.V.exp(b.V)+c.E mais avec constante. Ajustement pas toujours réaliste, bénéfice minime, paramètre 'a' souvent non significatif. Parfois pb d'ajustement (ne converge pas ou MSY et MSE remarquables = 0 ou < 0.
				     nbre_par = 3;
				     for (k = 0; k < nim; k++) { // Ajuste CPUE = a+b.exp(c.V) en première approximation
					 	ptmp[0] = 1 / vbar[k];
					 	ptmp[1] = 1;
					 	ptmp[2] = vbar[k];
					 	yy = Math.log(pue[k] / vbar[k]); // Division par vbar afin de passer de CPUE = a+b.V.exp(c.V) à CPUE = a+b.exp(c.V)
				     	//System.out.println("k = " + k + " yy = " + yy);
					 	itercov();
			   	      }
					  inverse_gauss();
					  System.out.println("par_alors[0] = " + par_alors[0] + " par_alors[1] = " + par_alors[1] + " par_alors[2] = " + par_alors[2] + " par_alors[3] = " + par_alors[3]);
				      par_alors[1] = Math.exp(par_alors[1]);
				      par_alors[3] = eps;
				      System.out.println("Ensuite par_alors[0] = " + par_alors[0] + " par_alors[1] = " + par_alors[1] + " par_alors[2] = " + par_alors[2] + " par_alors[3] = " + par_alors[3]);
				      nbre_par = 4;
				break; */
			case 35:  //CPUE=a.V.exp(b.V).exp(c.E)
			    nbre_par = 2;
			    for (k = 0; k < nim; k++) { // Ajuste CPUE = a.exp(b.V) en première approximation
					ptmp[0] = 1;
			        ptmp[1] = vbar[k];
			        yy = Math.log(pue[k] / vbar[k]); // Division par vbar afin de passer de CPUE = aV.exp(b.V) à CPUE = a.exp(b.V)
			        itercov();
			    }
			    inverse_gauss();
				//System.out.println("par_alors[0] = " + par_alors[0] + " par_alors[1] = " + par_alors[1] + " par_alors[2] = " + par_alors[2]);
			    par_alors[0] = Math.exp(par_alors[0]);
			    par_alors[2] = eps;
				//System.out.println("Ensuite par_alors[0] = " + par_alors[0] + " par_alors[1] = " + par_alors[1] + " par_alors[2] = " + par_alors[2]);
			    nbre_par = 3;
    			break; 
			case 37:   // CPUE=a.V.exp(b.V)+c.(aV.exp(b.V))^2).E  // Capturabilité
				nbre_par = 2;
				for (k = 0; k < nim; k++) { // Ajuste CPUE = a.exp(b.V) en première approximation
					ptmp[0] = 1;
				    ptmp[1] = vbar[k];
				    yy = Math.log(pue[k] / vbar[k]); // Division par vbar afin de passer de CPUE = aV.exp(b.V) à CPUE = a.exp(b.V)
				    itercov();
				}
				inverse_gauss();
				//System.out.println("par_alors[0] = " + par_alors[0] + " par_alors[1] = " + par_alors[1] + " par_alors[2] = " + par_alors[2]);
			    par_alors[0] = Math.exp(par_alors[0]);
			    par_alors[2] = eps;
				//System.out.println("Ensuite par_alors[0] = " + par_alors[0] + " par_alors[1] = " + par_alors[1] + " par_alors[2] = " + par_alors[2]);
			    nbre_par = 3;    
				break; 
			case 36:   // CPUE=a.V.exp(b.V).exp(c.V.exp(b.V).E)
				nbre_par = 2;
				for (k = 0; k < nim; k++) {
				    ptmp[0] = 1;
				    ptmp[1] = vbar[k];
				    yy = Math.log(pue[k] / vbar[k]); // Division par vbar afin de passer de CPUE = aV.exp(b.V) à CPUE = a.exp(b.V)
				    itercov();
				}
			    inverse_gauss();
			    par_alors[0] = Math.exp(par_alors[1]);
			    par_alors[2] = eps;
				// System.out.println("Ensuite par_alors[0] = " + par_alors[0] + " par_alors[1] = " + par_alors[1] + " par_alors[2] = " + par_alors[2]);
			    nbre_par = 3;
				break;
			/*case 38:   // CPUE=a.V.exp(b.V).exp(c.V.exp(d.V).E) // ABONDANCE et/ou MIXTE. Pb: ajustement difficile et souvent irréaliste.
				nbre_par = 2;
				for (k = 0; k < nim; k++) {
				    ptmp[0] = 1;
				    ptmp[1] = vbar[k];
				    yy = Math.log(pue[k] / vbar[k]); // Division par vbar afin de passer de CPUE = aV.exp(b.V) à CPUE = a.exp(b.V)
				    itercov();
				}
			    inverse_gauss();
				par_alors[0] = Math.exp(par_alors[1]);
				par_alors[2] = eps;
				par_alors[3] = par_alors[1];
				nbre_par = 4;
				break; */
            case 8:                //CPUE=a+b.V+c.E. Commentaire 2020
                nbre_par = 3;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = vbar[k];
                    ptmp[2] = f[k];
                    yy = pue[k];
                    itercov();
                }
                inverse_gauss();
                break;
            case 9:					//CPUE=a.V+b.E. Commentaire 2020
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = vbar[k];
                    ptmp[1] = f[k];
                    yy = pue[k];
                    itercov();
                }
                inverse_gauss();
                break;
            case 10:				//CPUE=a+b.V+c.V^2+d.E
                nbre_par = 4;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = vbar[k];
                    ptmp[2] = (vbar[k] * vbar[k]);
                    ptmp[3] = f[k];
                    yy = pue[k];
                    itercov();
                }
                inverse_gauss();
                break;
            case 11: // CPUE=a.V^b.exp(c.E)
                nbre_par = 3;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = Math.log(vbar[k]);
                    ptmp[2] = f[k];
                    yy = Math.log(pue[k]);
                    itercov();
                }
                inverse_gauss();
                par_alors[0] = Math.exp(par_alors[0]);
                break;
            case 12: // CPUE=(a+b.V).exp(c.E)
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = f[k];
                    yy = Math.log(pue[k]);
                    itercov();
                }
                inverse_gauss();
                par_alors[0] = Math.exp(par_alors[0]);
                par_alors[2] = par_alors[1];
                par_alors[1] = eps;
                nbre_par = 3;
                break;
            case 13: // CPUE=a.V.exp(b.E)
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = f[k];
                    yy = Math.log(pue[k] / vbar[k]);
                    itercov();
                }
                inverse_gauss();
                par_alors[0] = Math.exp(par_alors[0]);
                break;
			case 33:   // CPUE = a V exp(bV)
				nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = vbar[k];
                    yy = Math.log(pue[k] / vbar[k]);
                    itercov();
                }
                inverse_gauss();
                par_alors[0] = Math.exp(par_alors[0]);
                break;
            case 14: // CPUE=a+b.V+c.(a+b.V)^2.E  // Modèle #22 CAPTURABILITE
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = f[k];
                    yy = pue[k];
                    itercov();
                }
                inverse_gauss();
                par_alors[2] = -par_alors[1] / (par_alors[0] * par_alors[0]);
                par_alors[1] = eps;
                nbre_par = 3;
                break;
            case 15: // CPUE=a.V^b+c.V^(2.b).E
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = f[k];
                    yy = pue[k];
                    itercov();
                }
                inverse_gauss();
                par_alors[2] = par_alors[1];
                par_alors[1] = eps;
                nbre_par = 3;
                break;
            case 16: // CPUE=a.V+b.V^2.E
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = v[k];
                    ptmp[1] = v[k] * vbar[k] * f[k];
                    yy = pue[k];
                    itercov();
                }
                inverse_gauss();
                break;
            case 17: // CPUE=a.V^b.exp(c.V^b.E)
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = f[k];
                    yy = Math.log(pue[k]);
                    itercov();
                }
                inverse_gauss();
                par_alors[0] = Math.exp(par_alors[1]);
                par_alors[2] = par_alors[1];
                par_alors[1] = eps;
                nbre_par = 3;
                break;
            case 18: // CPUE=(a+b.V).exp(c.(a+b.V).E)
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = f[k];
                    yy = Math.log(pue[k]);
                    itercov();
                }
                inverse_gauss();
                par_alors[0] = Math.exp(par_alors[0]);
                par_alors[2] = par_alors[1] / par_alors[0];
                par_alors[1] = eps;
                nbre_par = 3;
                break;
            case 19: // CPUE=a.V.exp(b.V.E)
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = f[k] * vbar[k];
                    yy = Math.log(pue[k] / v[k]);
                    itercov();
                }
                inverse_gauss();
                par_alors[0] = Math.exp(par_alors[0]);
                break;
            case 20:  // CPUE=a.exp(b.E)+c.V+d MODELE ABONDANCE ADDITIF (impossible reformuler en a+bV+c exp(d E) du fait que les paramètres de l'initialisation doivent être les premiers).
				Flag_additive_model_fitted = true;
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = f[k];
                    yy = Math.log(pue[k]);
                    itercov();
                }
                inverse_gauss();
                nbre_par = 4;
                par_alors[0] = Math.exp(par_alors[0]);
                par_alors[2] = eps;
                par_alors[3] = eps;
                break;
            case 21: // CPUE=(a.V+b.V^2).exp(c.E)
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = f[k];
                    yy = Math.log(pue[k] / vbar[k]);
                    itercov();
                }
                inverse_gauss();
                par_alors[0] = Math.exp(par_alors[0]);
                par_alors[2] = par_alors[1];
                par_alors[1] = eps;
                nbre_par = 3;
                break;
            case 22:  // CPUE=((a.V^b)+d.E)^(1/(c-1))
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = vbar[k];
                    ptmp[1] = f[k];
                    yy = pue[k];
                    itercov();
                }
                inverse_gauss();
                par_alors[3] = par_alors[1];
                par_alors[1] = 1;
                par_alors[2] = 2;
                nbre_par = 4;
                break;
            case 23: // CPUE= ((a.V+b.V^2)^(c-1)+d.E)^(1/(c-1)) avant ((a.V+b.V^2)^(d-1)+c.E)^(1/(d-1))
                nbre_par = 3;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = vbar[k];
                    ptmp[1] = (vbar[k] * vbar[k]);
                    ptmp[2] = f[k]; // 3
                    yy = pue[k];
                    itercov();
                }
                inverse_gauss();
				par_alors[2] = 2; // 2...
				par_alors[3] = par_alors[1];  // Résultat très proche de formulation initiale après rajout de cette ligne inspirée de modèle case 22.
                nbre_par = 4;
                break;
            case 24: // CPUE=a.V.(b-c.V)+d.V^2.(b-c.V)^2.E
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = v[k];
                    ptmp[1] = v[k] * vbar[k] * f[k];
                    yy = pue[k];
                    itercov();
                }
                inverse_gauss();
                par_alors[3] = par_alors[1];
                par_alors[1] = 1.0;
                par_alors[2] = eps;
                nbre_par = 4;
                break;
            case 25: // CPUE=a.V.(1+b.V).exp(c.V.(1+b.V).E)
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = f[k] * vbar[k];
                    yy = Math.log(pue[k] / v[k]);
                    itercov();
                }
                inverse_gauss();
                par_alors[0] = Math.exp(par_alors[0]);
                par_alors[2] = par_alors[1];
                par_alors[1] = eps;
                nbre_par = 3;
                break;
            case 26: // CPUE=a.V^(b+c)+d.V^(2.b).E
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = v[k];
                    ptmp[1] = f[k] * (v[k] * v[k]);
                    yy = pue[k];
                    itercov();
                }
                inverse_gauss();
                par_alors[0] = par_alors[0];
                par_alors[3] = par_alors[1];
                par_alors[1] = 1.0;
                par_alors[2] = eps;
                nbre_par = 4;
                break;
            case 27: // CPUE=a.V^(1+b)+c.V^(2+b)+d.V^(2.b).E
                nbre_par = 3;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = v[k];
                    ptmp[1] = v[k] * vbar[k];
                    ptmp[2] = f[k];
                    yy = pue[k];
                    itercov();
                }
                inverse_gauss();
                par_alors[0] = par_alors[0];
                par_alors[3] = par_alors[2];
                par_alors[2] = par_alors[1];
                par_alors[1] = eps;
                nbre_par = 4;
                break;
            case 28: // CPUE=a.V^b.exp(c.V^d.E)
                nbre_par = 3;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = Math.log(v[k]);
                    ptmp[2] = f[k];
                    yy = Math.log(pue[k]);
                    itercov();
                }
                inverse_gauss();
                par_alors[0] = Math.exp(par_alors[1]);
                par_alors[3] = eps;
                nbre_par = 4;
                break;
            case 29: // CPUE=a.V^b.exp(c.V^d.E)
                nbre_par = 3;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = 1;
                    ptmp[1] = Math.log(vbar[k]);
                    ptmp[2] = f[k];
                    yy = Math.log(pue[k]);
                    itercov();
                }
                inverse_gauss();
                par_alors[0] = Math.exp(par_alors[0]);
                par_alors[3] = eps;
                nbre_par = 4;
                break;
            case 30: // CPUE=(a.V^(1+b)+c.V^(2+b)).exp(d.V^b.E)
                nbre_par = 2;
                for (k = 0; k < nim; k++) {
                    ptmp[0] = v[k];
                    ptmp[1] = v[k] * vbar[k];
                    yy = pue[k];
                    itercov();
                }
                inverse_gauss();
                par_alors[0] = par_alors[0];
                par_alors[2] = par_alors[1];
                par_alors[1] = eps;
                par_alors[3] = eps;
                nbre_par = 4;
                break;
        }

    }

    private static void initcov() {
        int i, j;
        for (i = 0; i < nparmax; i++) {
            for (j = 0; j < nparmax; j++) {
                covar[i][j] = 0;
            }
        }
        for (j = 0; j < nparmax; j++) {
            covpy[j] = 0;
        }
        for (j = 0; j < nparmax; j++) {
            ptmp[j] = 0;
        }
    }

    private static void itercov() {
        int i, j;
        for (i = 0; i < nbre_par; i++) {
            for (j = 0; j < nbre_par; j++) {
                covar[i][j] += ptmp[i] * ptmp[j];
				//System.out.println("Dans itercov(), i = " + i + "; j = " + j +  "; ptmp[i] = " + ptmp[i] + " ptmp[j] = " + ptmp[j] + "; covar[i][j] = " + covar[i][j]);                
            }
        }
        for (j = 0; j < nbre_par; j++) {
            covpy[j] += ptmp[j] * yy;
        }
    }

    private static void inverse_gauss() {
		System.out.println("Global.numero_modele = " + Global.numero_modele);
        int i, j, k;
        double c, s;
        for (i = 0; i < nbre_par - 1; i++) {
            for (j = i + 1; j < nbre_par; j++) {
                c = covar[j][i] / covar[i][i];
				//System.out.println("Dans inverse_gauss 1ere boucle, j=" + j + "; i=" + i + "; c= " + c + "; covar[j][i]=" + covar[j][i] + "; covar[i][i]=" + covar[i][i]);
                for (k = i + 1; k < nbre_par; k++) {
					//System.out.println("Dans inverse_gauss 2eme boucle A, i =" + i + "; j=" + j + "; k=" + k + "; covar[j][k]=" + covar[j][k]);
                    covar[j][k] = covar[j][k] - c * covar[i][k];
					//System.out.println("Dans inverse_gauss 2eme boucle B, j =" + j + "; k=" + k + "; covar[j][k]=" + covar[j][k] + "; c * covar[i][k]=" + c * covar[i][k]);
                }
                covpy[j] = covpy[j] - c * covpy[i];
            }
        }
		//System.out.println("Dans inverse_gauss, [nbre_par - 1] = " + (nbre_par - 1) +"; covpy[nbre_par - 1] = " + covpy[nbre_par - 1] + "; covar[nbre_par - 1][nbre_par - 1] = " + covar[nbre_par - 1][nbre_par - 1]);
        par_alors[nbre_par - 1] = covpy[nbre_par - 1] / covar[nbre_par - 1][nbre_par - 1];
        for (i = nbre_par - 2; i >= 0; i--) {
            s = covpy[i];
            for (k = i + 1; k < nbre_par; k++) {
                s -= covar[i][k] * par_alors[k];
				//System.out.println("Dans inverse_gauss, k = " + k +"; s = " + s +"; par_alors[k] = " + par_alors[k]);
            }
            par_alors[i] = s / covar[i][i];
			//System.out.println("Dans inverse_gauss, i = " + i + "; par_alors[i] = " + par_alors[i]);
        }
    }
    /*
     private static void inverse_gauss(int nn,double[][]aa,double[]xx,double[]yy)
     {
     int 	i,j,k;
     double 	c,s;
     for(i=0;i<nn-1;i++) {
     for(j=i+1;j<nn;j++){
     c=aa[j][i]/aa[i][i];
     for(k=i+1;k<nn;k++) aa[j][k]=aa[j][k]-c*aa[i][k];
     yy[j]=yy[j]-c*yy[i];
     }
     }
     xx[nn-1] = yy[nn-1]/aa[nn-1][nn]-1;
     for(i=nn-2;i>=0;i--){
     s=yy[i];
     for(k=i+1;k<nn;k++) s -= aa[i][k]*xx[k];
     xx[i]= s/aa[i][i];
     }
     }
     */

    private static void marquardt() {
        int i, j, k;
        double c, s;
        for (i = 0; i < nbre_par; i++) {
            par_alors[i] = par_init[i]; // Initialisation algorithme avec paramètres initiaux
			//System.out.println("Pendant début marquardt i = " + i + "; par_alors[i]  = " + par_alors[i]);
        }
        iter = 0;
        ritertot = 0;
        val_fin = somme_residus(par_alors);
		//System.out.println("valfin  "+val_fin);
        resiter[0] = val_fin;
        ecart = 0.1;
        riter = 0;
        while ((iter < maxrit - 1)
                && ((ecart > 0.05) || (iter < 40))
                && (ecart > 0.0001)
                && (riter > (-17))) {
            iter++;
            /* System.out.println( "Pendant marquardt");
             for(int b=0;b<nbre_par;b++) {
             System.out.println( "Iteration " +iter);
             System.out.println(b + " "+ par_alors[b]);
             }*/
			//System.out.println( "Iteration " +iter);
            val_init = val_fin;
            calcul_derivee_hess();
            for (i = 0; i < nbre_par; i++) {
                racineh[i] = Math.sqrt(hessien[i][i]) + 1.E-15;
				//System.out.println("Pendant milieu 0 marquardt i = " + i + ", Math.sqrt(hessien[i][i]) = " + Math.sqrt(hessien[i][i]));
            }
            for (i = 0; i < nbre_par; i++) {
                derivee[i] /= racineh[i];
            }
            for (i = 0; i < nbre_par; i++) {
                for (j = 0; j < nbre_par; j++) {
                    hessien[i][j] /= (racineh[i] * racineh[j]);
                }
            }
            for (i = 0; i < nbre_par; i++) {
                hessien[i][i] += lambda;
            }
            /*for(i=0;i<nbre_par;i++)
             System.out.println("racnan "+racineh[i]+" deri " +derivee[i] );*/
                //*******************************************
            //inverse_gauss( nbre_par, hessien, delta, derivee );
            for (i = 0; i < nbre_par - 1; i++) {
                for (j = i + 1; j < nbre_par; j++) {
                    c = hessien[j][i] / hessien[i][i];
                    for (k = i + 1; k < nbre_par; k++) {
                        hessien[j][k] = hessien[j][k] - c * hessien[i][k];
                    }
					//System.out.println("Pendant milieu 1 marquardt i = " + i + ", j = " + j + "; c = " + c + "; derivee[i] = " + derivee[i] + "; derivee[j] = " + derivee[j]);
                    derivee[j] = derivee[j] - c * derivee[i];
                }
            }
            delta[nbre_par - 1] = derivee[nbre_par - 1] / hessien[nbre_par - 1][nbre_par - 1];//bug ici
			//System.out.println("Pendant milieu 1bis marquardt [nbre_par - 1] = " + (nbre_par - 1) + "; delta[nbre_par - 1] = " + delta[nbre_par - 1]);
			//System.out.println("Pendant milieu 1 marquardt [nbre_par - 1] = " + (nbre_par - 1) + "; derivee[nbre_par - 1] = " + derivee[nbre_par - 1] + ";  hessien[nbre_par - 1][nbre_par - 1] = " +  hessien[nbre_par - 1][nbre_par - 1]);
            for (i = nbre_par - 2; i >= 0; i--) {
                s = derivee[i];
                for (k = i + 1; k < nbre_par; k++) {
                    s -= hessien[i][k] * delta[k];
                }
                delta[i] = s / hessien[i][i];
            }
            //
            for (i = 0; i < nbre_par; i++) {
                //System.out.println("delta "+ delta[i]);
                delta[i] /= racineh[i];
            }
            for (i = 0; i < nbre_par; i++) {
                par_inter[i] = par_alors[i] - delta[i];
				//System.out.println("Pendant milieu 2 marquardt i = " + i + "; par_alors[i]  = " + par_alors[i] + "; delta[i] = " + delta[i] + "; par_inter[i] = " + par_inter[i]);
            }
            val_fin = somme_residus(par_inter);
            //System.out.println("valfin "+ iter+" " +val_fin);
            riter = 0;
            if (val_fin < val_init) {
                while (val_fin < val_init) {
                    val_init = val_fin;
                    for (i = 0; i < nbre_par; i++) {
                        delta[i] *= alpha;
                        par_inter[i] = par_alors[i] - delta[i];
                    }
                    val_fin = somme_residus(par_inter);
                    riter++;
                    ritertot++;
                }
                for (i = 0; i < nbre_par; i++) {
                    delta[i] /= alpha;
                    par_inter[i] = par_alors[i] - delta[i];
                }
                val_fin = somme_residus(par_inter);
            } else {
                while (val_fin > val_init) {
                    for (i = 0; i < nbre_par; i++) {
                        delta[i] /= alpha;
                        par_inter[i] = par_alors[i] - delta[i];
                    }
                    val_fin = somme_residus(par_inter);
                    riter--;
                    if (riter < (-15)) {
                        for (i = 0; i < nbre_par; i++) {
                            delta[i] = 0;
                        }
                    }
                    ritertot++;
                }
            }
            val_fin = somme_residus(par_inter);
            if (riter < 0) {
                lambda *= rl;
            } else {
                lambda /= rl;
            }
            resiter[iter] = val_fin;
            if (iter > 5) {
                ecart = 100 * (resiter[iter - 5] - resiter[iter]) / variance_pue;
            }
            //impression_iter();
            boolean bNan = false;
            while (bNan == false && i < nbre_par) {
                for (i = 0; i < nbre_par; i++) {
                    bNan = Double.isNaN(par_inter[i]);
                }
            }

            if (!bNan) {
                for (i = 0; i < nbre_par; i++) {
                    par_alors[i] = par_inter[i];
					// System.out.println("Pendant fin marquardt i = " + i + "; par_alors[i]  = " + par_alors[i] + "; par_inter[i] = " + par_inter[i]);
                }
            }
        }
    }

    private static double somme_residus(double[] par) { // Somme des résidus au carré
        double cc = 0;
        int i;
        for (i = 0; i < nim; i++) {
            double c = EquationModele.fonction_modele(f[i], v[i], vbar[i], par) - pue[i];
            cc += (c * c);
        }
        return (cc);
    }

    private static void calcul_derivee_hess() {  // matrice de Hesse ou Hessian est une matrice carrée de dérivées partielles du second ordre d'une fonction scalaire Matrice de Hesse - https://fr.qaz.wiki/wiki/Hessian_matrix
        int i, j, n;
        double v1, v2;
        for (i = 0; i < nbre_par; i++) {
            par[i] = par_alors[i];
            dpar[i] = mul * par[i];
			//System.out.println("calcul_derivee_hess 1, i = " + i + "; par[i] = " +  par[i] + "; mul = " + mul + "; dpar[i] = " + dpar[i]); // par[i] et donc dpar[i] = 0 pour i=3 première itération, ensuite = NaN pour toutes valeurs de i.
        }
        for (n = 0; n < nim; n++) {
            fittedpue[n] = EquationModele.fonction_modele(f[n], v[n], vbar[n], par);
            // v1 = EquationModele.fonction_modele(f[n],v[n],vbar[n],par);
            //System.out.println("calcul_derivee_hess 2, v1 " +n+ "  "+ v1);
            res[n] = fittedpue[n] - pue[n];
            for (i = 0; i < nbre_par; i++) {
                par[i] += dpar[i];
				//System.out.println("calcul_derivee_hess 3, Year = " + n + " ; i = " + i + "; par[i] = " + par[i] + "; dpar[i] = " + dpar[i]);
                v2 = EquationModele.fonction_modele(f[n], v[n], vbar[n], par);
				//System.out.println("calcul_derivee_hess 4, v2 " +i+ "  "+ v2 + ", fittedpue[n] = " + fittedpue[n] + "; dpar[i] = " + dpar[i]); // OK seulement 1er itération (MAIS AVEC  EGAL A 0, CE QUI PROVOQUE NaN SUR LIGNE SUIVANTE), ensuite les 3 variables = NaN
                derfonc[n][i] = (v2 - fittedpue[n]) / dpar[i];
                //derfonc[n][i] = (v2 - v1)/dpar[i];
				//System.out.println("calcul_derivee_hess 5, Year = " + n + "; i = " + i + "; derfonc[n][i] = " + derfonc[n][i]); // NaN dès première itération due à division par 0.
                par[i] -= dpar[i];
            }
        }
        for (i = 0; i < nbre_par; i++) {
            derivee[i] = 0;
            for (n = 0; n < nim; n++) {
                derivee[i] += (res[n] * derfonc[n][i]);
				// System.out.println("calcul_derivee_hess 6, Year = " + n + " derivee " +i+ " = " + derivee[i] + "; res[n] = " + res[n] + "; derfonc[n][i] = " + derfonc[n][i]);
            }
        }
        for (i = 0; i < nbre_par; i++) {
            for (j = 0; j <= i; j++) {
                hessien[i][j] = 0;
                for (n = 0; n < nim; n++) {
                    hessien[i][j] += (derfonc[n][i] * derfonc[n][j]);
                }
                hessien[j][i] = hessien[i][j];
				//System.out.println("calcul_derivee_hess 7, hessien " +i+ ", " + j + " = "+ hessien[i][j]);
            }
        }

    }

    /*private static void calc_var() {
        int i;
        double s1 = 0, s2 = 0;
        for (i = 0; i < nim; i++) {
            s1 += pue[i];
            s2 += (pue[i] * pue[i]);
        }
        variance_pue = s2 - ((s1 * s1) / (double) nim);

 		//System.out.println("variance_pue = " + variance_pue + " Stat1.var(pue,false) = " + Stat1.var(pue,false) );
    }*/

    private static void calc_residus() {
        for (int i = 0; i < nim; i++) {
            fittedpue[i] = EquationModele.fonction_modele(f[i], v[i], vbar[i], par);
            res[i] = pue[i] - fittedpue[i];
            // System.out.println("------"+res[i]);
        }
    }

    private static double AICritere() {

        double sse = somme_residus(par_alors); // Residual deviance ou somme des carrés résiduels
//        return nim * Math.log((sse / (nim + 1.E-15)) + 1.E-15) + 2 * nbre_par + nim; 
//        return nim * Math.log((sse / (double)nim )) + 2 * nbre_par + nim; 
//		  return Math.log(Math.pow(sse, 2.)) + (2. * nbre_par / (double)nim);
//		  System.out.println("variance_pue = " + variance_pue + " sse = " + somme_residus(par_alors) + " nim = " + nim);
		  return (double)nim * Math.log(sse/(double)nim) + (2. * (double)nbre_par); //valable si les erreurs sont distribuées normalement. Sinon −2 * log(vraissemblance) + 2 k où k est le nb de paramètres
    }
    private static double AICritereCorr() {

        double sse = somme_residus(par_alors); // Residual deviance
        return (double)nim * Math.log(sse) + (2. * (double)nbre_par) + ((2.* (double)nbre_par * ((double)nbre_par + 1.)) / ((double)nim - (double)nbre_par - 1.));
        // Alternative equation (same result) : return (double)nim * Math.log(sse) + (2. * (double)nbre_par) + ((((2.* (double)nbre_par) * (double)nbre_par) + 2 * (double)nbre_par) / ((double)nim - (double)nbre_par - 1.));
    }
    private static double BICCritere() {    // Bayesian Information Criterion
        double sse = somme_residus(par_alors); // Residual deviance ou somme des carrés résiduels
		  return (double)nim * Math.log(sse) + (Math.log(nim) * (double)nbre_par); //valable si les erreurs sont distribuées normalement.     )
    }    
    private static void makePlot() {
        /**
         * ***************** cpue plot *****************************
         * ********** observed, fitted & residual time series *************** 
         */
        double minx = Stat1.min(Data.getYears());
        double maxx = Stat1.max(Data.getYears());
        PlotSerie[] ps = new PlotSerie[5];
        ps[0] = new PlotSerie("x", Data.getYears(), "Observed CPUE", Data.getPue());
        ps[1] = new PlotSerie("x", Data.getYears(), "Fitted CPUE", fittedpue);
        ps[2] = new PlotSerie("x", Data.getYears(), "Residuals", res);
        ps[3] = new PlotSerie("x", Data.getPue(), "Residuals", res);

        Global.fittedCpuePlot[0] = new Plot();
        ps[0].setFigure(2);
        ps[1].setFigure(2);
        ps[0].setCouleur(Color.blue);
        Global.fittedCpuePlot[0].setValeurs(ps[0]);
        Global.fittedCpuePlot[0].setValeurs(ps[1]);
        Global.fittedCpuePlot[0].ShowLegend = true;

        Global.fittedCpuePlot[1] = new Plot();
        ps[2].setFigure(2);
        Global.fittedCpuePlot[1].setValeurs(ps[2]);
        Global.fittedCpuePlot[1].ajusteExtremas(false, false, false, false);
        Global.fittedCpuePlot[1].setXcutYat(0);

        Global.fittedCpuePlot[2] = new Plot();
        ps[2].setFigure(2);
        Global.fittedCpuePlot[2].setValeurs(ps[3]);
        Global.fittedCpuePlot[2].ajusteExtremas(false, false, false, false);
        Global.fittedCpuePlot[2].setXcutYat(0);
        
        for (int i = 0; i < 2; i++) {
            Global.fittedCpuePlot[i].setTitreGraphique(Global.titreG[15 + i]);
            Global.fittedCpuePlot[i].setTitreX(Global.titreSx[13 + i]);
            Global.fittedCpuePlot[i].setTitreY(Global.titreSy[13 + i]);
            Global.fittedCpuePlot[i].setDecimalsurX(0); // Pas de décimale car années en X. Comment 2020.
			if (Data.stat[8][1] > 100) Global.fittedCpuePlot[i].setDecimalsurY(0); // stat[8][1] = Etendue CPUE. Avant setDecimalsurX(0) -> pas de décilmale sur axe des X. Avec setDecimalsurX(1) -> décimales SI NECESSAIRE. Modif 2020
			else Global.fittedCpuePlot[i].setDecimalsurY(2);  // Modif 2021.
            Global.fittedCpuePlot[i].setMinAxeX(minx - 1);
	    Global.fittedCpuePlot[i].setpasX(StrictMath.round((maxx-minx)/20)); // setpasX can be found in Plot.java.  
        }
            Global.fittedCpuePlot[2].setTitreGraphique(Global.titreG[17]);
            Global.fittedCpuePlot[2].setTitreX(Global.titreSx[15]);
            Global.fittedCpuePlot[2].setTitreY(Global.titreSy[14]);
            Global.fittedCpuePlot[2].setDecimalsurX(1);
			if (Data.stat[8][1] > 100) Global.fittedCpuePlot[2].setDecimalsurX(0); // stat[8][1] = Etendue CPUE. Avant setDecimalsurX(0) -> pas de décilmale sur axe des X. Avec setDecimalsurX(1) -> décimales SI NECESSAIRE. Modif 2020
			else Global.fittedCpuePlot[2].setDecimalsurX(2);  // Modif 2021.
            Global.fittedCpuePlot[2].setMinAxeX(Data.stat[6][1]-(Data.stat[8][1] / 20));
            Global.fittedCpuePlot[2].setpasX((Data.stat[8][1] / 10)); 
        /**
         * ****************** residuals plot **************************
         * i = 0 pour Résidus CPUE = f(E)
         * i = 1 pour Résidus CPUE = f(V)
         */

        for (int i = 0; i < 2; i++) {
            Global.residualsPlot[i] = new Plot();
        }

        Global.residualsPlot[0].setValeurs(new PlotSerie("", Data.getF(), "", res));
        Global.residualsPlot[1].setValeurs(new PlotSerie("", Data.getV(), "", res));
        for (int i = 0; i < 2; i++) {
            Global.residualsPlot[i].setTitreGraphique(Global.titreG[i + 18]);
            Global.residualsPlot[i].setTitreX(Global.titreSx[i + 16]);
            Global.residualsPlot[i].setTitreY(Global.titreSy[i + 15]);          
			//System.out.println("i = " + i + " Data.stat[7][i+2] = " + Data.stat[7][i+2] + " Data.stat[6][i+2] = " + Data.stat[6][i+2] + " max = " + max);
            if (Data.stat[8][i+2] >100) Global.residualsPlot[i].setDecimalsurX(0); // stat[8][i+2] = Etendue E puis V. Avant setDecimalsurX(0) -> pas de décilmale sur axe des X. Avec setDecimalsurX(1) -> décimales SI NECESSAIRE. Modif 2020.
		    else Global.residualsPlot[i].setDecimalsurX(2);  // En compélent de autre méthode dans Plot.java. Modif 2021.
            // setDecimalsurX(0) -> pas de décilmale sur axe des X. Avec setDecimalsurX(1) -> décimales. Modif 2021.
            // ajusteExtremas de Plot.java: selon valeur booléenne (vrai ou faux) applique ou non l'ajustement 
            // surX, sur X et met ou non le minimum à zéro sur Y et sur Y  
            if (Data.stat[8][1] > 50) Global.residualsPlot[i].setDecimalsurY(0); // stat[8][1] = Etendue CPUE (comme substitut de résidus CPUE; 50 au lieu de 100 car résidus et valeurs positives et négartives). Avant setDecimalsurX(0) -> pas de décilmale sur axe des X. Avec setDecimalsurX(1) -> décimales SI NECESSAIRE. Modif 2020.
		    else Global.residualsPlot[i].setDecimalsurY(2);  // Modif 2021.
			//System.out.println("Data.stat[7][1] = " + Data.stat[7][1]);                        
            Global.residualsPlot[i].ajusteExtremas(false, false, false, false);  // seconde valeur boolénne changée de true en false. Modification 2020
            // Cette modification évite un écrasement des valeurs de Y autours de l'axe des X.
            Global.residualsPlot[i].setXcutYat(0.0d);
        }

        /**
         * *********************Three-variate plot ******************************
         * ********(or bi-variate + prediction when CPUE = f(E) or f(V)**********
         ***********************************************************************/
         
        double pasx, pasy, minix, miniy, maxix, maxiy, xx, yy;
		double[] estim1 = new double[nim+1];
		double[] estim2 = new double[nim+1];
		double[] tabxx = new double[nim+1];
		double[] MSE = new double[5];
		double[] MSY = new double[5];
        DecimalFormat df = new DecimalFormat("0.0");
		if (((Global.numero_modele > 5) || (Global.numero_modele < 2)) && (Global.numero_modele != 33))  // Exclusion modèles CPUE = f(V)
        {
            double[] ext = Stat1.Extremas(Data.getF());
            minix = ext[0];
            if (minix > 0) {
                minix = 0;
            }
            maxix = ext[1];
            pasx = (maxix - minix) / nim;

            ext = Stat1.Extremas(Data.getV());
            miniy = ext[0];
            maxiy = ext[1];
            pasy = (maxiy - miniy) / 4.0; // Le pas de y (en fait V) est de 1/4 de l'intervalle de variation de V.
			//System.out.println("minix = " + minix + " maxix = " + maxix + " pasx = " +  pasx + " miniy = " + miniy + " maxiy = " + maxiy + " pasy = " +  pasy);
            xx = minix;
            yy = miniy;
            PlotSerie[] pvs = new PlotSerie[8];
            Global.variatePlot[0] = new Plot();
            Global.variatePlot[1] = new Plot();
            pvs[0] = new PlotSerie("Weighted effort (E)", Data.getF(), "CPUE", Data.getPue());       // Graphique CPUE observée vs E
            pvs[1] = new PlotSerie("Weighted effort (E)", Data.getF(), "Catch (Y)", Data.getYexp()); // Graphique Y observée vs E
            pvs[0].setCouleur(Color.black);
            pvs[1].setCouleur(Color.black);
            Global.variatePlot[0].setValeurs(pvs[0]);
            if ((Global.numero_modele == 0) || (Global.numero_modele ==1)  || (Global.numero_modele ==6)) {
            Global.variatePlot[0].setTitreGraphique("Function CPUE=f(E) (E weighted if relevant)");
            }	
            else {Global.variatePlot[0].setTitreGraphique("Function CPUE=f(V & E) (E weighted if relevant)");}
            Global.variatePlot[1].setValeurs(pvs[1]);
            if ((Global.numero_modele == 0) || (Global.numero_modele ==1)  || (Global.numero_modele ==6)) {
            Global.variatePlot[1].setTitreGraphique("Function Y=f(E) (E weighted if relevant)");  
            }	
            else {Global.variatePlot[1].setTitreGraphique("Function Y=f(V & E) (E weighted if relevant)");}
            pvs[0].setFigure(2); // pvs[0] = graphique CPUE = f()
            pvs[1].setFigure(2); // pvs[1] = graphique Y = f()
            pvs[0].setMark(2);   // Rajoute des petits carrés sur ligne du graphique.
            pvs[1].setMark(3);   // Rajoute des petits triangles sur ligne du graphique.
			// for (int j = 0; j < 4; j++) System.out.println("j= " + j + "; par_alors[j]= " + par_alors[j]);
            for (int j = 0; j < 5; j++) {		    // Boucle des j: j=0 Vmini; j=2 Vmiddle; j=4 Vmaxi (ou Predicted CPUE & Y si CPUE=f(E)); j=1 et j=2 rien ne se passe (sinon estimations inutilisées de CPUE et Y);

				MSE[j]= EquationModele.minimum_fonction(yy,par_alors);
				MSY[j]= MSE[j] * EquationModele.fonction_modele(MSE[j],yy,yy,par_alors); // Calcul des MSY à partir de paramètres non-jackknife pour valeurs remarquables de V (Y = CPUE * E).
				//System.out.println("j= " + j + "; MSE[j]= " + MSE[j]); // Calcul des MSE pour valeurs remarquables de V.
				//System.out.println("j= " + j + "; MSY[j]= " + MSY[j]); // Calcul des MSY pour valeurs remarquables de V.
				Global.AllPredictedNegative[j] = 0;
				for (int i = 0; i < (nim+1); i++) { // Attention la longueur de la boucle = nim (nb data retenues) mais les valeurs sont calculées pour un pas fixe de E (pasx)
                	//                    fonction_modele(double ff, double vv, double vba, double par[])
                    estim1[i] = EquationModele.fonction_modele(xx, yy, yy, par_alors); // Valeur prédites des CPUEs; xx = effort; yy = V.
                    estim2[i] = xx * estim1[i];
					if (estim1[i] <= 0) Global.AllPredictedNegative[j] = Global.AllPredictedNegative[j] + 1;                                       // Valeur predites des Ys.
					//System.out.println("j = " + j + " i = " + i + " estim1[i] = " + estim1[i] + " AllPredictedNegative[j] == " + Global.AllPredictedNegative[j]);
                    tabxx[i] = xx;
					//System.out.println("i = " + i + " estim1[i] = " + estim1[i] + " estim2[i] = " + estim2[i] + " yy = " + yy);
                    xx = xx + pasx;
                }
                if (j == 0) {
                    if ((Global.numero_modele != 0) && (Global.numero_modele !=1)  && (Global.numero_modele !=6)) { // Elimine modèles CPUE = f(E)
                    // pvs[2] <--> Vmini de graphique CPUE = f()
                    // pvs[3] <--> Vmini de graphique Y = f()
                    pvs[2] = new PlotSerie("Weighted effort (E)", tabxx, "Vmini: " + df.format(yy).toString(), estim1); // Ligne CPUE pour Vmin
                    pvs[3] = new PlotSerie("Weighted effort (E)", tabxx, "Vmini: " + df.format(yy).toString(), estim2); // Ligne Y pour Vmin
                    pvs[2].setCouleur(Color.blue);
                    pvs[3].setCouleur(Color.blue);
                    pvs[2].setFigure(2);
                    pvs[3].setFigure(2);
					Global.variatePlot[0].setValeurs(pvs[2]);
					Global.variatePlot[1].setValeurs(pvs[3]);                    }
                } else if (j == 4) {
                    if ((Global.numero_modele == 0) || (Global.numero_modele ==1)  || (Global.numero_modele ==6)) { // Modeles CPUE=f(V)
                    	// pvs[4] <--> CPUE de graphique CPUE = f()
                        // pvs[5] <--> Y de graphique Y = f()
						pvs[4] = new PlotSerie("Weighted effort (E)", tabxx, "Predicted CPUE", estim1);
                    	pvs[5] = new PlotSerie("Weighted effort (E)", tabxx, "Predicted Y", estim2);
						Global.variatePlot[0].setValeurs(pvs[4]);
          				Global.variatePlot[1].setValeurs(pvs[5]);
                    	pvs[4].setFigure(2);
                    	pvs[5].setFigure(2);
					}
                    else {
                    	// pvs[4] = Vmaxi de graphique CPUE = f()
                        // pvs[5] = Vmaxi de graphique Y = f()
                    	pvs[4] = new PlotSerie("Weighted effort (E)", tabxx, "Vmaxi: " + df.format(yy).toString(), estim1);
                    	pvs[5] = new PlotSerie("Weighted effort (E)", tabxx, "Vmaxi: " + df.format(yy).toString(), estim2);
                    	Global.variatePlot[0].setValeurs(pvs[4]);
                    	Global.variatePlot[1].setValeurs(pvs[5]);
                    	pvs[4].setFigure(2);
                    	pvs[5].setFigure(2);
                    }
				} else if (j == 2 && ((Global.numero_modele == 10) || (Global.numero_modele == 21) || (Global.numero_modele == 23) || (Global.numero_modele == 24) || (Global.numero_modele == 25) || (Global.numero_modele == 27) || (Global.numero_modele >= 30))) // Modèles non-monotoniques
					{  // Modèles mixtes où CPUE = f(V) en dôme (fonction quadratique ou Ricker)
                    pvs[6] = new PlotSerie("Weighted effort (E)", tabxx, "Vmiddle:" + df.format(yy).toString(), estim1);
                    pvs[7] = new PlotSerie("Weighted effort (E)", tabxx, "Vmiddle:" + df.format(yy).toString(), estim2);
                    pvs[6].setCouleur(Color.green);
                    pvs[7].setCouleur(Color.green);
                    pvs[6].setFigure(2);
                    pvs[7].setFigure(2);
					Global.variatePlot[0].setValeurs(pvs[6]);
                    Global.variatePlot[1].setValeurs(pvs[7]);
                }
                xx = minix;
                yy = yy + pasy;

            }
            for (int i = 0; i < 2; i++) {
            Global.variatePlot[i].setMinAxeX(0.0);
            Global.variatePlot[i].setMinAxeY(0.0);
            Global.variatePlot[i].ShowLegend = true; 
            if (Data.stat[8][2] > 100) Global.variatePlot[i].setDecimalsurX(0); // Si étendue > 100. Avant setDecimalsurX(0) -> pas de décilmale sur axe des X. Avec setDecimalsurX(1) -> décimales SI NECESSAIRE. Modif 2020.
    		else Global.variatePlot[i].setDecimalsurX(2);  // Modif 2021.
			//System.out.println("i = " + i + " Data.stat[7][2] dans Modele.java= " + Data.stat[7][2]);
			}
			// Ce qui suit est hors boucle car ordres inverse de CPUE et Y dans Datat.stat et Global.variatePlot[i]
			if (Data.stat[8][0] > 100) Global.variatePlot[1].setDecimalsurY(0);  // Si étendue > 100.
			else Global.variatePlot[1].setDecimalsurY(2);  // Modif 2021
			if (Data.stat[8][1] > 100) Global.variatePlot[0].setDecimalsurY(0);  // Si étendue > 100.
			else Global.variatePlot[0].setDecimalsurY(2);  // Modif 2021
			
			Global.message$[5] = "";
			for (int j = 0; j < 5; j++) {
				if (Global.AllPredictedNegative[j] >= Modele.nim) 
				    Global.message$[5] = "WARNING: \nAt least one full set of predicted values of CPUE=f(E,V) and Y=f(E,V)\ncorresponding to Vmin or Vmax (or Vmiddle if the CPUE=f(V) is not\nmonotonic) presents only negative values.\n\n";  
			}
		}   
   /************ Traitement des modèles CPUE = f(V) ****************/		
		else 
        {
            double[] ext = Stat1.Extremas(Data.getVbar());
            miniy = ext[0]; // Minimum de V (axe des X)
            maxiy = ext[1];
			pasy = (maxiy - miniy) / nim;
			//System.out.println("miniy = " + miniy + " maxiy = " + maxiy + " pasy = " +  pasy);
			yy = miniy;
			xx = 0.0;  // Effort, non utilisé dans EquationModele.fonction_modele(xx, yy, yy, par_alors)
  			PlotSerie[] pvs = new PlotSerie[8];
            Global.variatePlot[0] = new Plot();
			Global.variatePlot[1] = null;       // Elimine graphique Y = f(V) au cas où il reste en mémoire d'un ajustement antérieur de Y = f(E,V) ou Y = f(E)
            pvs[0] = new PlotSerie("Weighted environment (V)", Data.getVbar(), "CPUE", Data.getPue()); // Données observées
            pvs[0].setCouleur(Color.black);
            Global.variatePlot[0].setValeurs(pvs[0]);
            Global.variatePlot[0].setTitreGraphique("Function CPUE=f(V) (V lagged if relevant)");
            pvs[0].setFigure(2);
            pvs[0].setMark(2);      
			for (int i = 0; i < (nim+1); i++) {				   
				 //           fonction_modele(double ff, double vv, double vba, double par[])
                 estim1[i] = EquationModele.fonction_modele(xx, yy, yy, par_alors);
				 //System.out.println("i = " + i + " estim1[i] = " + estim1[i] + " xx = " + xx + " yy = " + yy);
                 tabxx[i] = yy;
                 yy = yy + pasy;
            }
			pvs[4] = new PlotSerie("Weighted environment (V)", tabxx, "CPUE", estim1); // Predicted CPUE (fitted curve).
          	Global.variatePlot[0].setValeurs(pvs[4]);
            pvs[4].setFigure(2);
			yy = yy + pasy;
			Global.variatePlot[0].setMinAxeX(miniy-(Data.stat[8][3] / 10)); // miniy est bien minimum de V sur axe des X; Data.stat[8][3] est l'étendue de V			
			//System.out.println("Dans Modele.java miniy = " + miniy + " miniy-(Data.stat[8][3] / 10) = " + (miniy-(Data.stat[8][3] / 10)) );
			//Global.variatePlot[0].setMaxAxeX(maxiy+(Data.stat[8][3] / 10));
			if (Data.stat[8][3] > 100) Global.variatePlot[0].setDecimalsurX(0); // Si étendue > 100. Avant setDecimalsurX(0) -> pas de décilmale sur axe des X. Avec setDecimalsurX(1) -> décimales SI NECESSAIRE. Modif 2020.
			else Global.variatePlot[0].setDecimalsurX(2);  // Modif 2021.
			if (Data.stat[8][1] > 100) Global.variatePlot[0].setDecimalsurY(0); // Si étendue > 100.
			else Global.variatePlot[0].setDecimalsurY(2);  // Modif 2021
			Global.variatePlot[0].setMinAxeY(0.0);
            Global.variatePlot[0].ShowLegend = true; 
        }
    }
    
	 /*
     Donne les principaux résultats dans fenetre "Fit a model directly", sous fenetre de gauche "Modelization: Main results, y compris Akaike 
     */
    public static Object[][] getResult() {
    	nimTabFish = Data.getNbDataRetenue();
    	if (nimTabFish > 100) nimTabFish = 100;
        if (!Global.modelisationOk) {
            return null;
        }
        String[] param$ = {"     a    ", "     b    ", "     c    ", "     d    "};
        String[] title$ = {"Parameters  ", "Actual value ", "Initial value"};
        Object[][] data$ = new String[nbre_par + 12][3];
        DecimalFormat nf = new DecimalFormat(" 0.00000;-0.00000");
        DecimalFormat nf2 = new DecimalFormat(" 0.00;-0.00");
        NumberFormat numFormat = new DecimalFormat();
        numFormat = new DecimalFormat("0.####E0");
        for (int j = 0; j < 3; j++) {
            data$[0][j] = title$[j];
        }
        for (int i = 0; i < nbre_par; i++) {
            data$[i + 1][0] = param$[i];
            if ((par_alors[i] < 0.001 && par_alors[i] > 0) || par_alors[i] > 999.999 || (par_alors[i] > -1.0E-3 && par_alors[i]<0)|| par_alors[i] < -999.999) data$[i + 1][1] = numFormat.format(par_alors[i]);
            else data$[i + 1][1] = nf.format(par_alors[i]); // Valeur paramètre estimé fin Marquardt
            if ((par_init[i] < 0.001 && par_init[i] > 0) || par_init[i] > 999.999 || (par_init[i] > -1.0E-3 && par_init[i]<0)|| par_init[i] < -999.999) data$[i + 1][2] = numFormat.format(par_init[i]);
            else data$[i + 1][2] = nf.format(par_init[i]); // Valeur paramètre initiale.
        }
        data$[nbre_par + 1][0] = "Nb. of data used to fit";        // Dans ce qui suit, la seconde dimension de data$ indique qu'il s'agit soit d'un titre ([0]) soit de la valeur correspondante ([0]). Commentaire 2020.
        data$[nbre_par + 1][1] = Integer.toString(nim);            // nim = nombre observations
        data$[nbre_par + 2][0] = "Nb. of degrees of freedom";
        data$[nbre_par + 2][1] = Integer.toString(nim - nbre_par);
        data$[nbre_par + 3][0] = "Coef. of determination R²";
        data$[nbre_par + 3][1] = nf2.format(r);
        
        // System.out.println("r : "+r+" et globalr : "+Global.coeff_determination);  // r = Global.coeff_determination ligne 111

        Global.fF = Global.fF(Global.coeff_determination, (Global.nbre_param - 1), getNbDataRetenue());
        //System.out.println("fF Model = " + Global.fF);
        data$[nbre_par + 4][0] = "Fisher test: F(" + (Global.nbre_param -1) + "; " + (getNbDataRetenue() - Global.nbre_param) +")"; // Contrairement à p dans fF de Global.java dernière methode, nbre_param inclus la constante (a en général). D'où la soustraction de 1. 
                               // F(x,y) Donne entre parenthèses les valeurs du nombre d'observation et de paramètres du modèle pour le test de Fisher sur R2
        if ((Global.fF < 0.001 && Global.fF > 0) || Global.fF> 999.999) data$[nbre_par + 4][1] = numFormat.format(Global.fF);
        else data$[nbre_par + 4][1]  = nf.format( Global.fF);
        data$[nbre_par+5][0]= "Fisher test: F p-value";
     	if (Global.fF < (Global.TableFisher_p_05 [nimTabFish- Global.nbre_param - 1][Global.nbre_param - 1 - 1])) {// The last -1 term is here to compensate the fact that table indices start at 0 and not at 1.
     	   data$[nbre_par+5][1]= " >0.05";
     	   Global.fFsignif$ =  " p>0.05"; // Do not use p>0.05 because this is not compatible with the path of the html graph file.
        }
     	else 
     	if (Global.fF >= Global.TableFisher_p_05 [nimTabFish - Global.nbre_param - 1][Global.nbre_param - 1 - 1] && Global.fF < Global.TableFisher_p_01 [nimTabFish - Global.nbre_param - 1][Global.nbre_param - 1 -1]) {
     	   data$[nbre_par+5][1]= " <0.05"; 
     	   Global.fFsignif$ =  " p<0.05";
        }
     	else { 
     	   data$[nbre_par+5][1]= " <0.01";
     	   Global.fFsignif$ =  " p<0.01"; 
     	} 
       	data$[nbre_par + 6][0] = "Corrected R²                      ";
        data$[nbre_par + 6][1] = nf2.format(Global.corrected_R2);
        data$[nbre_par + 7][0] = "AIC                            ";
        if ((aic < 0.001 && aic > 0) || aic > 999.999 || (aic > -1.0E-3 && aic < 0)|| aic < -999.999) data$[nbre_par +7][1] = numFormat.format(aic);
        else data$[nbre_par +7][1] = nf.format(aic);
        data$[nbre_par + 8][0] = "Corrected AIC                  ";
        if ((aic_corr < 0.001 && aic_corr > 0) || aic_corr > 999.999 || (aic_corr > -1.0E-3 && aic_corr < 0)|| aic_corr < -999.999) data$[nbre_par +8][1] = numFormat.format(aic_corr);
        else data$[nbre_par +8][1] = nf.format(aic_corr);
        data$[nbre_par + 9][0] = "BIC                            ";
        if ((bic < 0.001 && bic > 0) || bic > 999.999 || (bic > -1.0E-3 && bic < 0)|| bic < -999.999) data$[nbre_par +9][1] = numFormat.format(bic);
        else data$[nbre_par +9][1] = nf.format(bic);      
        data$[nbre_par + 10][0] = "CPUE Variance                 ";
        if ((variance_pue < 0.001 && variance_pue > 0)|| variance_pue > 999.999) data$[nbre_par +10][1] = numFormat.format(variance_pue);
        else data$[nbre_par +10][1] = nf.format(variance_pue);   
        data$[nbre_par + 11][0] = "Residual Variance              ";
        if ((sompar < 0.001 && sompar > 0)  || sompar > 999.999) data$[nbre_par +11][1] = numFormat.format(sompar);
        else data$[nbre_par +11][1] = nf.format(sompar);           

        return data$;

    }

    /*
     Donne les résultats détaillés
     */
    public static Object[][] getYearResult() {
        if (!Global.modelisationOk) {
            return null;
        }
        String[] title$ = {"Year/season", "Observed CPUE", "Fitted CPUE", "Residuals"};
        Object[][] data$ = new String[nim + 1][4];
        DecimalFormat nf = new DecimalFormat(" 0.000000;-0.000000");
        DecimalFormat nf0 = new DecimalFormat("0");
        double[] p = Data.getPue();
        double[] y = Data.getYears();
        for (int j = 0; j < 4; j++) {
            data$[0][j] = title$[j];
        }

        for (int i = 0; i < nim; i++) {
            data$[i + 1][0] = nf0.format(y[i]);
            data$[i + 1][1] = nf.format(p[i]);
            data$[i + 1][2] = nf.format(fittedpue[i]);
            data$[i + 1][3] = nf.format(res[i]);
        }

        return data$;

    }

    private static void initVariable() {

        nim = 0;
        nbre_par = 0;
        lambda = 16;
        riter = 0;
        iter = 0;
        ritertot = 0;
        val_init = 0;
        val_fin = 0;
        ecart = 0;
        r = 0;
        sompar = 0;
        variance_pue = 0;
        yy = 0;

        ptmp = new double[nparmax + 2];
        covar = new double[nparmax + 1][nparmax + 1];

        hessien = new double[nparmax + 1][nparmax + 1];
        covpy = new double[nparmax + 2];
        par_alors = new double[nparmax + 2];
        racineh = new double[nparmax + 2];
        derivee = new double[nparmax + 2];
        delta = new double[nparmax + 2];
        par_init = new double[nparmax + 2];
        par_inter = new double[nparmax + 2];
        spar = new double[nparmax + 2];
        par = new double[nparmax + 2];
        dpar = new double[nparmax + 2];
        sompari = new double[nparmax + 2];
        //f,v,vbar,pue,res,fittedpue;
        resiter = new double[maxrit];
        derfonc = new double[61][nparmax + 1];

    }

    /*private static void saveResult(){
     String [] param$={"a","b","c","d"};
     try
     {
     PrintWriter out=new PrintWriter(new FileOutputStream("stat.txt",true));
     out.println(RechercheModele.getEquation());
     out.println(nim + " exploitée " + Global.nb_classes_exploitees+" recrutement  " +Global.recruitment_age + " debut "+ Global.begin_influence_period + " fin " +Global.end_influence_period);
     for(int i=0;i<nbre_par;i++)
     out.println(param$[i] +"  " +par_alors[i] + "  " + par_init[i]);
     out.println("r� " +   r);
     out.println("");
     out.close();
     }
     catch(IOException ieo)
     {}

     }
     */
}
