/**
 * Titre : Climprod<p>
 * Gestion de petite fenêtre de message d'erreur ou de warning
 * (panneau stop rouge).
 */
package fr.ird.climprod;

import java.awt.Dimension;
import java.awt.GraphicsDevice;
import javax.swing.JOptionPane;
import javax.swing.JFrame;
import java.awt.Toolkit;

public class MsgDialogBox extends JFrame {   //Pas g�nial du tout mais permet de

    private int reponse;                            //r�cup�rer l'icone climprod

    public MsgDialogBox(int typeBoite, String message, int messageType, JFrame parent) {
    // typeBoite: 0 avec bouton OK; 1 avec boutons Yes et No.
   /* 
     GraphicsDevice gd = parent.getGraphicsConfiguration().getDevice();
        int xscr = gd.getDisplayMode().getWidth();
        int yscr = gd.getDisplayMode().getHeight();

        Dimension size = this.getSize();
	  
        int y = (yscr - size.height)/2;
		int x = (xscr - size.width)/2;
        this.setLocation(x,y);
        System.out.println("OWWW "+x+" : "+y); 
    */
        
        this.setIconImage(Toolkit.getDefaultToolkit().createImage(MsgDialogBox.class.getResource("resources/images/Climprod.jpg")));
        reponse = -1;
        int ico = valMessage(messageType);

        if (typeBoite == 0) {
            JOptionPane.showMessageDialog(parent, message, "Climprod: Message", ico);
            reponse = 0;

        } else if (typeBoite == 1) {
            Object[] options = {"Yes", "No"};
            reponse = JOptionPane.showOptionDialog(parent, message, "Climprod: Message", JOptionPane.DEFAULT_OPTION, ico,
                    null, options, options[0]);
        } else if (typeBoite == 2) {
            Object[] options = {"Yes", "No", "Cancel"};
            reponse = JOptionPane.showOptionDialog(parent, message, "Climprod: Message", JOptionPane.DEFAULT_OPTION, ico,
                    null, options, options[0]);
        }
    }

    public boolean Ok() {
        return reponse == 0;
    }

    public boolean No() {
        return reponse == 1;
    }

    public boolean Cancel() {
        return reponse == 2;
    }

    private int valMessage(int index) {
        String[] typeMessage = new String[]{"ERROR_MESSAGE", "INFORMATION_MESSAGE",
            "WARNING_MESSAGE", "QUESTION_MESSAGE", "PLAIN_MESSAGE"};
        if (index < 0 || index > 4) {
            return -1;
        }
        try {
            Class cl = JOptionPane.class;
            return cl.getField(typeMessage[index]).getInt(cl);
        } catch (Exception e) {
            return -1;
        }

    }
}
