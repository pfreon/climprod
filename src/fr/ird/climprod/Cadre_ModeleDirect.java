/**
 * Titre : Climprod<p>
 * Gestion fenetre du menu "Fit a model directly "
 */
package fr.ird.climprod;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import java.awt.event.*;
import java.io.InputStream;
import javax.swing.table.TableColumn;
import java.util.Vector;

public class Cadre_ModeleDirect extends JFrame {

    JPanel jPanChoix = new JPanel();
    JPanel jPanCmdChoix = new JPanel();
    JPanel jPanBiologie = new JPanel();
    JPanel jPanResults = new JPanel();

    JRadioButton chkE = new JRadioButton();
    JRadioButton chkV = new JRadioButton();
    JRadioButton chkEVCatchability = new JRadioButton();
    JRadioButton chkEVAbundance = new JRadioButton();
    JRadioButton chkEVBoth = new JRadioButton();
    ButtonGroup group = new ButtonGroup();
  //JButton cmdE = new JButton();
    //JButton cmdV = new JButton();
    // JButton cmdEVCatchability = new JButton();
    //JButton cmdEVAbundance = new JButton();
    // JButton cmdEVBoth = new JButton();
    JList<String> jlstChoix = new JList<String>();

    JScrollPane jScrollPane1 = new JScrollPane();
    JScrollPane jScrollPane2 = new JScrollPane();
    JScrollPane jScrollPane3 = new JScrollPane();
    JScrollPane jScrollPane4 = new JScrollPane();

    GridBagLayout gridBagLayout1 = new GridBagLayout();
    GridBagLayout gridBagLayout2 = new GridBagLayout();

    JLabel jlblExploitedYears = new JLabel();
    JLabel jlblRecrutement = new JLabel();
    JLabel jlblBegining = new JLabel();
    JLabel jlblEnd = new JLabel();
    JLabel lblModele = new JLabel();
    JComboBox<String> jcboExploitedYears = new JComboBox<String>();
    JComboBox<String> jcboRecrutement = new JComboBox<String>();
    JComboBox<String> jcboBegining = new JComboBox<String>();
    JComboBox<String> jcboEnd = new JComboBox<String>();

    JTable jTableModelisation = new JTable();
    JTable jTableValidation = new JTable();
    DefaultListModel<String> modele = new DefaultListModel<String>();
    JTextArea jTextAreaInfoModel = new JTextArea();

    JToolBar jToolBar1 = new JToolBar();
    JButton cmdFit = new JButton();
    JButton cmdFitted = new JButton();
    JButton cmdTVariate = new JButton();
    JButton cmdJackniffe = new JButton();
    JButton cmdEnd = new JButton();
    JButton cmdPredict = new JButton();

    //JPopupMenu popup=new JPopupMenu();
    // JMenuItem menuPlotFitted = new JMenuItem("Observed-Fitted and Residuals CPUE plots");
    //JMenuItem menuPlotJacknife = new JMenuItem("Jacknife Plots");
    // JMenuItem menuPlotThreeVariate = new JMenuItem("CPUE=f() & Y=() plots");
    int nbFit;
    Vector<Cadre_SplitPlot> vPlot = new Vector<Cadre_SplitPlot>();

    public Cadre_ModeleDirect() {
        try {
			Global.MSY_MSE_OK=false;
			Modele.Flag_additive_model_fitted = false;
			Global.environmental_influence = "";
			Global.relationCPU_E=0;
			Global.relationCPU_V=0;
            nbFit = 0;
            initWindow();
            UtilCadre.Size(this, 60, 65); // 80, 85 -> 35, 40 Facteur de réduction fenetre "Fit a model directly" Test 2020
            UtilCadre.Centrer(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initWindow() throws Exception {
        this.setIconImage(Toolkit.getDefaultToolkit().createImage(Cadre_ModeleDirect.class.getResource("resources/images/Climprod.jpg")));

        this.setTitle("Climprod: Fit a model directly");
        this.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                this_windowClosing(e);
            }
        });
        this.getContentPane().setLayout(gridBagLayout2);
        jPanChoix.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)), "Select a model"));
        jPanChoix.setLayout(gridBagLayout1);
        jPanCmdChoix.setLayout(new GridLayout(5, 2));
        chkE.setText("Simple models CPUE=f(E)");
        chkE.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                chkE_actionPerformed(e);
            }
        });

        chkV.setText("Simple models CPUE=f(V)");
        chkV.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                chkV_actionPerformed(e);
            }
        });
        chkEVCatchability.setText("Mixed models CPUE=f(E,V), where V influence catchability");
        chkEVCatchability.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                chkEVCatchability_actionPerformed(e);
            }
        });
        chkEVAbundance.setText("Mixed models CPUE=f(E,V), where V influence abundance");
        chkEVAbundance.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                chkEVAbundance_actionPerformed(e);
            }
        });
        chkEVBoth.setText("Mixed models CPUE=f(E,V), where V influence abundance & catchability");
        chkEVBoth.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                chkEVBoth_actionPerformed(e);
            }
        });

        jScrollPane2.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)), "Selected model information"));
        jPanBiologie.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)), "Age structure (in number of past year-classes, being them seasonal or annual)"));
        jPanBiologie.setLayout(new GridLayout(4, 2));
        jlblExploitedYears.setText("Number of significantly exploited years classes");
        jlblRecrutement.setText("Age at recruitment");
        jlblBegining.setText("Age at the beginning of environmental influence");
        jlblEnd.setText("Age at the end of environmental influence");
        cmdFit.setText("Fit the selected model");

        cmdFit.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                cmdFit_actionPerformed(e);
            }
        });
        lblModele.setText("Model fitting:");
        jScrollPane3.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)), "Modelization: Main results"));
        jTextAreaInfoModel.setLineWrap(true);
        jTextAreaInfoModel.setWrapStyleWord(true);
        jTextAreaInfoModel.setEditable(false);
        jlstChoix.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                jlstChoix_valueChanged(e);
            }
        });
        this.addWindowListener(new java.awt.event.WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                Global.CadreModeleDirect = null;
            }
        });
        jPanResults.setLayout(new GridLayout(1, 2, 5, 0));
    //gridLayout3.setColumns(2);
        //gridLayout3.setHgap(5);

        jScrollPane4.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)), "Validation: Jackknife main results"));
        this.getContentPane().add(jPanChoix, new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 0, 0, 5), 0, 0));

        jPanChoix.add(jPanCmdChoix, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 5), 0, 0));
        group.add(chkE);
        group.add(chkV);
        group.add(chkEVAbundance);
        group.add(chkEVCatchability);
        group.add(chkEVBoth);
        jPanCmdChoix.add(chkE, null);
        jPanCmdChoix.add(chkV, null);
        jPanCmdChoix.add(chkEVAbundance, null);
        jPanCmdChoix.add(chkEVCatchability, null);
        jPanCmdChoix.add(chkEVBoth, null);
        jPanChoix.add(jScrollPane1, new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 100, 0));
        jScrollPane1.getViewport().add(jlstChoix, null);
        jPanChoix.add(jScrollPane2, new GridBagConstraints(0, 1, 2, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 2, 2), 0, 67));
        jScrollPane2.getViewport().add(jTextAreaInfoModel, null);
        this.getContentPane().add(jPanBiologie, new GridBagConstraints(0, 1, 2, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(0, 5, 0, 5), 0, 0));

        String[] item1 = QuestionReponse.getItemQuestion("nb_classes_exploitees");
        String[] item2 = QuestionReponse.getItemQuestion("begin_influence_period");
        for (String item : item1) {
            jcboRecrutement.addItem(item);
            jcboExploitedYears.addItem(item);
        }
        for (String item : item2) {
            jcboBegining.addItem(item);
            jcboEnd.addItem(item);
        }

        if (Global.nb_classes_exploitees < 1) {
            jcboExploitedYears.setSelectedIndex(0);
        } else {
            jcboExploitedYears.setSelectedIndex(Global.nb_classes_exploitees - 1);
        }
        if (Global.recruitment_age < 1) {
            jcboRecrutement.setSelectedIndex(0);
        } else {
            jcboRecrutement.setSelectedIndex(Global.recruitment_age - 1);
        }

        if (Global.begin_influence_period < 1) {
            jcboBegining.setSelectedIndex(0);
        } else {
            jcboBegining.setSelectedIndex(Global.begin_influence_period);
        }
        if (Global.end_influence_period < 1) {
            jcboEnd.setSelectedIndex(0);
        } else {
            jcboEnd.setSelectedIndex(Global.end_influence_period);
        }

        jPanBiologie.add(jlblExploitedYears, null);
        jPanBiologie.add(jcboExploitedYears, null);
        jPanBiologie.add(jlblRecrutement, null);
        jPanBiologie.add(jcboRecrutement, null);
        jPanBiologie.add(jlblBegining, null);
        jPanBiologie.add(jcboBegining, null);
        jPanBiologie.add(jlblEnd, null);
        jPanBiologie.add(jcboEnd, null);
        jPanResults.add(jScrollPane3, null);
        jPanResults.add(jScrollPane4, null);
        jScrollPane4.getViewport().add(jTableValidation, null);
        jScrollPane3.getViewport().add(jTableModelisation, null);
        this.getContentPane().add(cmdFit, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(2, 5, 0, 5), 0, 0));
        this.getContentPane().add(lblModele, new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(10, 10, 0, 8), 0, 0));

        this.getContentPane().add(jPanResults, new GridBagConstraints(0, 4, 2, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 100));
        this.getContentPane().add(jToolBar1, new GridBagConstraints(0, 5, 2, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 5, 5, 5), 0, 0));

 		// popup.add(menuPlotFitted);
        // popup.addSeparator();
        /// popup.add(menuPlotThreeVariate);
        /// popup.add(menuPlotJacknife);
        cmdFitted.setIcon(new ImageIcon(Cadre_ModeleDirect.class.getResource("resources/images/FittedPlot.gif")));
        cmdJackniffe.setIcon(new ImageIcon(Cadre_ModeleDirect.class.getResource("resources/images/JACKNIFFE.gif")));
        cmdTVariate.setIcon(new ImageIcon(Cadre_ModeleDirect.class.getResource("resources/images/TVariate.gif")));
        cmdEnd.setIcon(new ImageIcon(Cadre_ModeleDirect.class.getResource("resources/images/End.gif")));
        cmdPredict.setIcon(new ImageIcon(Cadre_ModeleDirect.class.getResource("resources/images/Prediction.gif")));
        cmdFitted.setToolTipText("Obs.-Fit. and Res. CPUE plots");
        cmdTVariate.setToolTipText("CPUE=f() & Y=() plots");
        cmdJackniffe.setToolTipText("Jackknife plots");
        cmdEnd.setToolTipText("Close (ESC)");
        int condition = JComponent.WHEN_IN_FOCUSED_WINDOW;
        KeyStroke keyStroke2 = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
        InputMap inputMap2 = cmdEnd.getInputMap(condition);
        ActionMap actionMap2 = cmdEnd.getActionMap();
        inputMap2.put(keyStroke2, keyStroke2.toString());
        actionMap2.put(keyStroke2.toString(), new AbstractAction() {            
            @Override
            public void actionPerformed(ActionEvent arg0) {
                cmdEnd.doClick();
            }
        });
        cmdPredict.setToolTipText("Prediction");
        jToolBar1.add(cmdFitted, null);
        jToolBar1.add(cmdTVariate, null);
        jToolBar1.add(cmdJackniffe, null);
        jToolBar1.addSeparator();
        jToolBar1.add(cmdPredict, null);
        jToolBar1.addSeparator();
        jToolBar1.addSeparator();
        jToolBar1.add(cmdEnd, null);
        jToolBar1.setFloatable(false);

        cmdFitted.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                cmdFitted_actionPerformed(e);
            }
        });

        cmdTVariate.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                cmdTVariate_actionPerformed(e);
            }
        });

        cmdJackniffe.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                cmdJackniffe_actionPerformed(e);
            }
        });
        cmdEnd.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                cmdEnd_actionPerformed(e);
            }
        });

        cmdPredict.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                cmdPredict_actionPerformed(e);
            }
        });

        chkE.setSelected(true);
        
        /*Gestion message dans cadre de droite de "Current known facts" de fenêtre principale
        lors de selection directe de modeles. Attribution de valeurs aux variables environmental_influence
        et envir_preponderant en fonction du type de modèle selectioné.
        ATTENTION, NE PASSE PAS PAR ICI (DONC PAS DE MISE A JOUR) SI ON NE RE-CLIQUE PAS SUR LA CATEGORIE
        DE MODELE POUR METTRE A JOUR LE TYPE D'INFLUENCE DE L'ENVIRONNEMENT ET SUR LE MODELE POUR METTRE
        A JOUR LA VARIABLE Global.envir_preponderant QUI NE DONNERA UN COMMENTAIRE QUE POUR LES MODELES
        CPUE=f(E) OU CPUE=f(V).
        ejecutar al abrir la ventana.
        */
        displayList(RechercheModele.getlisteModele(0));
        EnvironmentsetEnabled(false, false);
        Global.environmental_influence = Global.influenceEnv[0];
        Global.envir_preponderant = 1;
		//System.out.println("Flag 0 ModeleDirect.java Global.envir_preponderant = " + Global.envir_preponderant + " Global.environmental_influence = " + Global.environmental_influence);
        /*fin ejecutar*/
    }
    
    void chkE_actionPerformed(ActionEvent e) {  // Case CPUE=f(E).
        displayList(RechercheModele.getlisteModele(0));
        EnvironmentsetEnabled(false, false);
        Global.environmental_influence = Global.influenceEnv[0];
        Global.envir_preponderant = 1;
		//System.out.println("Flag 1 ModeleDirect.java Global.envir_preponderant = " + Global.envir_preponderant + " Global.environmental_influence = " + Global.environmental_influence);
    }

    void chkV_actionPerformed(ActionEvent e) {  // Case CPUE=f(V).
        displayList(RechercheModele.getlisteModele(1));
        EnvironmentsetEnabled(true,true);
        Global.environmental_influence = Global.influenceEnv[0];
        Global.envir_preponderant = 2;
		//System.out.println("Flag 2 ModeleDirect.java Global.envir_preponderant = " + Global.envir_preponderant + " Global.environmental_influence = " + Global.environmental_influence);
    }

    void chkEVCatchability_actionPerformed(ActionEvent e) {  // Case CPUE=f(E,V) effect on catchabilty.
        displayList(RechercheModele.getlisteModele(2));
        EnvironmentsetEnabled(true, false);
        Global.environmental_influence = Global.influenceEnv[2];
        Global.envir_preponderant = -1;
		//System.out.println("Flag 3 ModeleDirect.java Global.envir_preponderant = " + Global.envir_preponderant + " Global.environmental_influence = " + Global.environmental_influence);
    }

    void chkEVAbundance_actionPerformed(ActionEvent e) {  // Case CPUE=f(E,V) effect on abundance.
        displayList(RechercheModele.getlisteModele(3));
        EnvironmentsetEnabled(true, true);
        Global.envir_preponderant = -1;
        Global.environmental_influence = Global.influenceEnv[1];
		//System.out.println("Flag 4 ModeleDirect.java Global.envir_preponderant = " + Global.envir_preponderant + " Global.environmental_influence = " + Global.environmental_influence);
    }

    void chkEVBoth_actionPerformed(ActionEvent e) {  // Case CPUE=f(E,V) effect on both.
        displayList(RechercheModele.getlisteModele(4));
        EnvironmentsetEnabled(true, true);
        Global.envir_preponderant = -1;
        Global.environmental_influence = Global.influenceEnv[3];
		//System.out.println("Flag 5 ModeleDirect.java Global.envir_preponderant = " + Global.envir_preponderant + " Global.environmental_influence = " + Global.environmental_influence);
    }

    void cmdFitted_actionPerformed(ActionEvent e) {  // INUTILISE ????.
        String windowTitre = "Model fitting: " + RechercheModele.getEquation() + " nbFit(fitting nb " + nbFit + ")";
		//System.out.println("Flag 6 ModeleDirect.java");
        if (openWindow(windowTitre)) {
            return;
        }
        MsgDialogBox msg;
        Cadre_SplitPlot dlgSp;
        if (Global.modelisationOk) {
            dlgSp = new Cadre_SplitPlot(Global.fittedCpuePlot, true);
            dlgSp.setTitle(windowTitre);
            UtilCadre.Centrer(dlgSp);
            dlgSp.setVisible(true);
            vPlot.add(dlgSp);
                
        } else {
            msg = new MsgDialogBox(0, "In order to display these plots\n you have to fit a model", 0, Global.CadreMain);
        }

    }

    ;
    void cmdTVariate_actionPerformed(ActionEvent e) {
        String windowTitre = "Model fitting: " + RechercheModele.getEquation() + "(fitting nb " + nbFit + ") ";
        if (openWindow(windowTitre)) {
            return;
        }
        MsgDialogBox msg;
        Cadre_SplitPlot dlgSp;
        if (Global.modelisationOk) {
            dlgSp = new Cadre_SplitPlot(Global.variatePlot);
            dlgSp.setTitle(windowTitre);
            UtilCadre.Centrer(dlgSp);
            dlgSp.setVisible(true);
            vPlot.add(dlgSp);
        } else {
            msg = new MsgDialogBox(0, "In order to display these plots\n you have to fit a model", 0, Global.CadreMain);
        }
    }

    ;
    void cmdJackniffe_actionPerformed(ActionEvent e) {
		//System.out.println("Flag cmdJackniffe_actionPerformed(ActionEvent e) dans ModeleDirect.java");         
        String windowTitre = "Model fitting: " + RechercheModele.getEquation() + "(fitting nb " + nbFit + ")  ";
        if (openWindow(windowTitre)) {
            return;
        }
        MsgDialogBox msg;
        Cadre_SplitPlot dlgSp;
		if (Global.validationOk) {
            dlgSp = new Cadre_SplitPlot(Global.jackknifePlot);
            dlgSp.setTitle(windowTitre);
            if (Global.nbre_param > 3) {
                UtilCadre.Size(dlgSp, 80, 90);
            }
            UtilCadre.Centrer(dlgSp);
            dlgSp.setVisible(true);
            vPlot.add(dlgSp);
        } else {
            msg = new MsgDialogBox(0, "In order to display these plots\n you have to fit a model", 0, Global.CadreMain);
        }

    }

    ;

    void cmdPredict_actionPerformed(ActionEvent e) {
        MsgDialogBox msg;
        if (Global.modelisationOk) {
            if (Global.test_jackknife) {
                Cadre_Prediction dlg = new Cadre_Prediction(this);
                dlg.setTitle("Climprod: Prediction for the fitted model " + RechercheModele.getEquation());
                dlg.setModal(true);
                dlg.setVisible(true);
            } else {
                msg = new MsgDialogBox(0, "The model is not validated (bad jackknife test)\n You can't use it for prediction. ", 0, Global.CadreMain);
            }
        } else {
            msg = new MsgDialogBox(0, "In order to do prediction\n you have to fit a model.", 0, Global.CadreMain);
        }
    }

    void cmdEnd_actionPerformed(ActionEvent e) {
        close();

    }

    ;

/*
    Affiche la liste des mod�les
*/

private void displayList(String[] liste) {

        jTextAreaInfoModel.setText("");
        modele = new DefaultListModel<String>();

        if (liste != null) {
            for (String element : liste) {
                modele.addElement(element);
            }
        }
        jlstChoix.setModel(modele);
    }

    /*
     Sélectionne un modèle
     */
    void jlstChoix_valueChanged(ListSelectionEvent e) {
        String equation = (String) jlstChoix.getSelectedValue();
        if (equation != null) {
            RechercheModele.select(equation);
            Global.relationCPU_E = RechercheModele.getNumCpue_ERelation();
            Global.relationCPU_V = RechercheModele.getNumCpue_VRelation();
            ModelInformation();
        }
    }

    /*
     Affiche les informations sur le mod�le s�lectionn�.
     */
    private void ModelInformation() {
        jTextAreaInfoModel.setText("");
        InputStream file = Cadre_ModeleDirect.class.getResourceAsStream("resources/models/" + RechercheModele.getModelInformation());
        if (file == null) {
            return;
        }
   // String file=Configuration.helpPathName+Configuration.fileSep+RechercheModele.getModelInformation();
        // if(file.equals("")) return;
        try {

            String[] dataLine;
            ReadFileText rft = new ReadFileText(file);
            dataLine = rft.getLines();
            //System.out.println("help "+dataLine.length);
            if (dataLine == null) {
                jTextAreaInfoModel.setText("Help file " + file + " not found.");
            } else {
                for (String line : dataLine) {
                    if (line.trim().length() == 0) {
                        jTextAreaInfoModel.append("\n\n");
                    } else {
                        jTextAreaInfoModel.append(line + " ");
                    }
                }
                jTextAreaInfoModel.setCaretPosition(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void EnvironmentsetEnabled(boolean a, boolean b) {

        jcboRecrutement.setEnabled(a);
        jcboBegining.setEnabled(b);
        jcboEnd.setEnabled(b);
        jlblRecrutement.setEnabled(b);
        jlblBegining.setEnabled(b);
        jlblEnd.setEnabled(b);
    }

    void cmdFit_actionPerformed(ActionEvent e) {
      try {
        MsgDialogBox msg;
        if (jlstChoix.getSelectedIndex() == -1) {
            msg = new MsgDialogBox(0, "Please select a model", 0, Global.CadreMain);
            return;
        }
        if (Global.CadreFittedPlots != null) {
            Global.CadreFittedPlots.dispose();
        }
        if (Global.CadreResidualPlots != null) {
            Global.CadreResidualPlots.dispose();
        }
		//System.out.println("Dans  void cmdFit_actionPerformed Global.CadreJackniffePlots = " + Global.CadreJackniffePlots);
        if (Global.CadreJackniffePlots != null) {
            Global.CadreJackniffePlots.dispose();
        }
        if (Global.CadreThreeVariatePlots != null) {
            Global.CadreThreeVariatePlots.dispose();
        }
        if (Global.CadreMSPlots != null) {
            Global.CadreMSPlots.dispose();
        }
        if (Global.CadreResultats != null) {
            Global.CadreResultats.dispose();
        }
        Global.recruitment_age = -1;
        Global.begin_influence_period = -1;
        Global.end_influence_period = -1;
        
        Global.modelisationOk = false;
        Global.nb_classes_exploitees = jcboExploitedYears.getSelectedIndex() + 1;
        if (jcboRecrutement.isEnabled()) {
            Global.recruitment_age = jcboRecrutement.getSelectedIndex() + 1;
            Global.begin_influence_period = jcboBegining.getSelectedIndex();
            Global.end_influence_period = jcboEnd.getSelectedIndex();
        }

/***********************************************************
Any change in age warnings related to ages here must be
repeated in the QuestionReponse.java file, cases 17, 52, 57 & 58.
************************************************************
*/
        //System.out.println("Global.recruitment_age: " + Global.recruitment_age + "Global.nb_classes_exploitees: " + Global.nb_classes_exploitees + " Global.begin_influence_period: " + Global.begin_influence_period + "Global.end_influence_period: " + Global.end_influence_period);
		if (Global.nb_classes_exploitees > 5 && Global.nb_classes_exploitees < 9) {
			 //msg = new MsgDialogBox(0, "WARNING! \nA stock with more than 5 significantly exploited year-classes is not ideal for applying \nsurplus production models that make use of the transition prediction approach (past-effort-averaging) of \nFox (1975). This method is used in CLIMPROD to fit data on non-equilibrium conditions. It simulates the \nequilibrium by using a weighted average of fishing effort (E). \nIf interanual variability of E is too large, the results are likely to be uncertain.", 0, Global.CadreMain);
             throw new OnError("WARNING! \nA stock with more than 5 significantly exploited year-classes is not ideal for applying \nsurplus production models that make use of the transition prediction approach (past-effort-averaging) of \nFox (1975). This method is used in CLIMPROD to fit data on non-equilibrium conditions. It simulates the \nequilibrium by using a weighted average of fishing effort (E). \nIf interanual variability of E is too large, the results are likely to be uncertain.");
        }
        if (Global.nb_classes_exploitees > 8) {
                     throw new OnError("A stock with too many significantly exploited year-classes is not a situation compatible with the use of \nsurplus production models that make use of the transition prediction approach (past-effort-averaging) \nofFox (1975). This method is used in CLIMPROD to fit data on non-equilibrium conditions. It simulate the \nequilibrium by using a weighted average of fishing effort (E). \nIt is recommanded to stop here your research or revise your selection of number(s) if necessary.");
        }
        if (Global.end_influence_period < Global.begin_influence_period) {
           throw new OnError("Age at the end of environmental influence must be larger than or equal to \nage at the begining of environmental influence. \nPlease modify your selection of number(s)");
        }
        if (Global.begin_influence_period > (Global.recruitment_age + Global.nb_classes_exploitees)) {
           throw new OnError("If the age at the begining of environmental influence occurs \nafter the age of the last exploited year class \n(that is age at recruitment + number of significantly exploited year classes - 1) \nthen there is no point in using a model incorporating an environmental variable. \nPlease modify your selection of number(s)");
        }     
        if (Global.end_influence_period > (Global.recruitment_age + Global.nb_classes_exploitees - 1)) {
           msg = new MsgDialogBox(0, "If the age at the end of environmental influence occurs \nafter the age of the last exploited year class \n(that is age at recruitment + number of significantly exploited year classes - 1) \nthen only its influence on exploited year classes will be taken into account.", 0, Global.CadreMain);
        }    
        if (Global.recruitment_age >= 0 && Global.begin_influence_period >=0 && Global.end_influence_period < (Global.recruitment_age + Global.nb_classes_exploitees)){
        	if ((Global.end_influence_period - Global.begin_influence_period) >= 5){
                   throw new OnError ("WARNING! \nToo many year classes influenced by the environnement (V). \n\nOwing to a recruitment at age " + Global.recruitment_age  + " and " + Global.nb_classes_exploitees + " exploited year classes, and the environmental influence beginning at age " + Global.begin_influence_period + " and ending at age " + Global.end_influence_period + ", \nresults in the need of averaging the environnement effect on " + (Global.end_influence_period - Global.begin_influence_period + 1) + " year classes.");  
        	}
        }
		if (Global.recruitment_age >= 0 && Global.begin_influence_period >=0 && Global.end_influence_period >= (Global.recruitment_age + Global.nb_classes_exploitees)){           	
              if ((Global.recruitment_age + Global.nb_classes_exploitees - Global.begin_influence_period) > 5) {
				   throw new OnError ("WARNING! \nToo many year classes influenced by the environnement (V). \n\nOwing to a recruitment at age " + Global.recruitment_age  + " and " + Global.nb_classes_exploitees + " exploited year classes, and the environmental influence beginning at age " + Global.begin_influence_period + " and ending at age " + Global.end_influence_period + ", \nresults in the need of averaging the environnement effect on " + (Global.recruitment_age + Global.nb_classes_exploitees - Global.begin_influence_period) + " year classes.");   
              }                
        } 
      } catch (Exception ee) {
         	 new MsgDialogBox(0, ee.getMessage(), 0, Global.CadreMain);
         	 }
        if (Global.begin_influence_period == -1) Global.begin_influence_period = 0; // Change for consistency in loops of models with effect on catchability only
        if (Global.end_influence_period == -1) Global.end_influence_period = 0;     // Change for consistency in loops of models with effect on catchability only
        Global.numero_modele = RechercheModele.getNumero();
        Global.typeModele = RechercheModele.getType();
        Modele.Estimer();
        lblModele.setText("Model fitting: " + RechercheModele.getEquation() + " (Marquardt algorithm)");
        renseignerTable(jTableModelisation, Modele.getResult());
        Validation.valide_modele();
		//System.out.println("Flag valide_modele() dans Cadre_ModeleDirect.java ligne 615");
        renseignerTable(jTableValidation, Validation.getParamResult());
        nbFit++;
                            
		MsgDialogBox msg;
		if ((Global.numero_modele < 6 && Global.numero_modele > 1) || Global.numero_modele == 20 || Global.numero_modele == 33) {// Modèles CPUE=f(V) et modèle exponentiel additif
			msg = new MsgDialogBox(0, "MSY and MS-E graphs are not justified or available for this model.\nPlease continue.", 0, this);
			for (int n=1; n<5;n++) Global.message$[n] = ""; 
			for (int n=6; n<14;n++) Global.message$[n] = "";
		}
		if (Global.message$[13] != "") // The estimate of the initial value of at least one of the parameter is equal to infinity.
			msg = new MsgDialogBox(0, Global.message$[13], 0, Global.CadreMain);
		if (Global.message$[0] != "") // Likely problem in Jackknife computation.
			msg = new MsgDialogBox(0, Global.message$[0], 0, Global.CadreMain);
		if (Global.message$[6] != "") // At least one of the central values of MSE is larger than 1.0E10 for noteworthy V values 
			msg = new MsgDialogBox(0, Global.message$[6], 0, Global.CadreMain);  
		else if (Global.message$[10] != "") // At least one of the central values of MSE is negative for noteworthy V values 
			msg = new MsgDialogBox(0, Global.message$[10], 0, Global.CadreMain);            
		else if (Global.message$[3] != "") // All noteworthy values of MSY and/or MSE upper limits at 95% \nare null or negative 
			msg = new MsgDialogBox(0, Global.message$[3], 0, Global.CadreMain);
		else	 
		{
			if (Global.message$[5] != "") // At least one set of predicted values of CPUE=f(E,V) and Y=f(E,V) corresponding to Vmin or Vmax presents only negative values.
				msg = new MsgDialogBox(0, Global.message$[5], 0, Global.CadreMain);
			else {
			if (Global.message$[4] != "") // All noteworthy values of MSY and MSE lower limits at 95% are null or negative. 
				msg = new MsgDialogBox(0, Global.message$[4], 0, Global.CadreMain);
				else {
					if (Global.message$[1] != "") // All noteworthy values of MSE lower limit at 95% are null or negative. 					
						msg = new MsgDialogBox(0, Global.message$[1], 0, Global.CadreMain);            
					if (Global.message$[7] != "") // Please note that some MSY central values for at least one of the noteworthy V values are unexpectidly negative.
                		msg = new MsgDialogBox(0, Global.message$[7], 0, Global.CadreMain);
					if (Global.message$[2] != "") // All noteworthy values of MSY lower limit at 95% are null or negative.
						msg = new MsgDialogBox(0, Global.message$[2], 0, Global.CadreMain);
					if (Global.message$[8] != "")      // Please note that the width of the 95% confidence interval of MSY central values for at least one of the noteworthy V values is larger than the corresponding MSY value.
                		msg = new MsgDialogBox(0, Global.message$[8], 0, Global.CadreMain);
                    if (Global.message$[9] != "")      // Please note that the width of the 95% confidence interval of MSE central values for at least one of the noteworthy V values is larger than the corresponding MSE value.;					  
                		msg = new MsgDialogBox(0, Global.message$[9], 0, Global.CadreMain);
					if (Global.message$[11] != "")// Please note that the MSY central value for the mean V value is 10 times larger than the maximum observed catch value.
						msg = new MsgDialogBox(0, Global.message$[11], 0, Global.CadreMain);
					if (Global.message$[12] != "")// Please note that the MSE central value for the mean V value is 5 times larger than the maximum observed fishing effort value.
						msg = new MsgDialogBox(0, Global.message$[12], 0, Global.CadreMain);
				}            
    		}
		}
  
        /* if(Global.nb_classes_exploitees==0)
         {
         msg=new MsgDialogBox(0,"Please provide the number of significantly/n exploited year classes",0);
         return;
         }
         */

        /*if(Global.recruitment_age==0)
         {
         msg=new MsgDialogBox(0,"Please provide the age at recrutement",0);
         return;
         }*/

        /* if(Global.begin_influence_period==-1)
         {
         msg=new MsgDialogBox(0,"Please provide the age age at the begining of environmental influence",0);
         return;
         }*/

        /*if(Global.end_influence_period==-1)
         {
         msg=new MsgDialogBox(0,"Please provide the age age/n at the end of environmental influence",0);
         return;
         }*/
          //popup.setBorderPainted(true);
        //popup.show(cmdFit,cmdFit.getWidth()-popup.getWidth(),0);
    }

    void this_windowClosing(WindowEvent e) {
        close();
    }

    private void renseignerTable(JTable t, Object[][] res) {
        if (res == null) {
            return;
        }
        int c = res[0].length;
        int l = res.length - 1;
        String[] column = new String[c];
        for (int j = 0; j < c; j++) {
            column[j] = (String) res[0][j];
        }

        Object[][] dat = new Object[l][c];
        for (int i = 0; i < l; i++) {
            System.arraycopy(res[i + 1], 0, dat[i], 0, c);

        }
        ModeleDeTableStat mdt = new ModeleDeTableStat(dat, column);
        t.setModel(mdt);
        TableColumn tmcolumn;
        int nbc = t.getColumnModel().getColumnCount();
        for (int i = 0; i < nbc; i++) {
            tmcolumn = t.getColumnModel().getColumn(i);
            tmcolumn.setMinWidth(50);
        }

    }

    private boolean openWindow(String windowTitre) {
        for (Object plot : vPlot) {
            Cadre_SplitPlot dlgSp = (Cadre_SplitPlot) plot;
            if (dlgSp != null) {
                if (dlgSp.getTitle().equals(windowTitre)) {
                    dlgSp.requestFocus();
                    if (dlgSp.getState() == Cadre_SplitPlot.ICONIFIED) {
                        dlgSp.setState(Cadre_SplitPlot.NORMAL);
                    }
                    return true;
                }
            }
        }
        return false;

    }

    private void close() {
        for (Object plot : vPlot) {
            Cadre_SplitPlot dlgSp = (Cadre_SplitPlot) plot;
            if (dlgSp != null) {
                dlgSp.dispose();
            }
        }
        vPlot.clear();
        nbFit = 0;
        this.dispose();
    }
}
