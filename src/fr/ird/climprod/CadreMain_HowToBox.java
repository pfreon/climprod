// Fenetre Help ->How to get started du menu principal pour présenter copyright FAO-IRD.
package fr.ird.climprod;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;

public class CadreMain_HowToBox extends JDialog implements ActionListener {


  JPanel jPanHowTo = new JPanel();
  JPanel jPanCmd = new JPanel();
 // JPanel jPanImage = new JPanel();
  JPanel jPanText = new JPanel();
  JButton cmdOK = new JButton();
 // JLabel imageIcone = new JLabel();
  //ImageIcon imageIcon;
  JTextArea jTextAreaCopyRight = new JTextArea();

  BorderLayout borderLayout1 = new BorderLayout();
  //BorderLayout borderLayout2 = new BorderLayout();
  FlowLayout flowLayout1 = new FlowLayout();
  FlowLayout flowLayout2 = new FlowLayout();
  GridLayout gridLayout1 = new GridLayout();
 // String product = "Climprod";
 // String version = "1.0";


  public CadreMain_HowToBox(Frame parent) {
    super(parent);
    enableEvents(AWTEvent.WINDOW_EVENT_MASK);
    try {
      jbInit();
    }
    catch(Exception e) {
      e.printStackTrace();
    }
   // imageIcone.setIcon(imageIcon);
    pack();
  }

  private void jbInit() throws Exception  {
    //imageIcon = new ImageIcon(getClass().getResource("Climprod.ico"));
    this.setTitle("How to get started");
    setResizable(false);

    jPanHowTo.setLayout(borderLayout1);
    jPanCmd.setLayout(flowLayout1);
    //jPanImage.setLayout(flowLayout1);
   // jPanImage.setBorder(new EmptyBorder(10, 10, 10, 10));
   // gridLayout1.setRows(4);
   // gridLayout1.setColumns(1);
    jTextAreaCopyRight.setText(
   "HOW TO GET STARTED (full details in Help -> Documentation -> Tutorial):\n\n" +
   "You need first open a data file containing your data in four columns in a csv format using SEMICOLONS (;) as " +
   "columns separators and FULL STOPS (PERIODS)  as a decimal separators in numbers. The heading of the columns are: " +
   "Year/season;Catches;Effort;Environment. Please note that from an ecological point of view it is often better " +
   "to consider that the start of the fishing season is not the 1st of January and the same applied for your environmental " +
   "variable. If the environment influences the catchability, the fishery and environmental values in a row must cover " +
   "roughly the same months of the year (the higher the autocorrelations in series are, the rougher can be the matching). " +
   "Furthermore, if there are several spawning seasons per year, your time interval can be shorter than " +
   "12 months. In that case, please entre decimal values of years (e.g. 2000.0 and 200.5 for two semestral " +
   "seasons). Please do not incorporate artificial lag between the fishery and environmental data in the same row.\n\n" +

   "Historical files of CLIMPROD had the extension .CLI and the names of the first and second columns were " + 
   "'Years' and 'Production' respectively. You can still use directly those old files including the EXAMPLE.CLI " +
   "file available in your CLIMPROD directory. If you need to create a new data " +
   "file you can either do it by using the sub-menu 'Create a new CLIMPROD data file' in the " +
   "main menu 'File', or create it using a spreadsheet software (e.g. Excel, OpenOffice Calc) " +
   "and save it under the .csv format using semicolons as separators. You will be able to import directly " +
   "this .csv file in CLIMPROD but you can also choose to modify its extension in .CLI.\n\n" +

   "After opening a data file, CLIMPROD will spread out your entry data plus the computed CPUE, and " +
   "display tables of basic statistics on the four variables. From this point you can proceed in two " +
   "different ways:\n\n1) You can make use of the sub-menu 'Select the appropriate model and fit it' in " +
   "the 'Modelization' main menu. This is the recommended option. The expert system will ask you a " +
   "number of questions, some them based on graphics or statistical results, in order to select " +
   "the most appropriate model and try to validate it. You can tick the 'Trace all the procedure' " +
   "facility in the main menu 'Option' although this talkative option (full transparency of the expert-system) " +
   "is not recommended for a first use of CLIMPROD. \n\n2) You can decide to select yourself a model by using the sub-menu " +
   "'Fit a model directly' in the menu 'Modelization', which supposes that you are familiar with surplus production " +
   "models including an environmental variable. Before doing this you can explore yourself your data " +
   "set using the first three sub-menus of the 'Plots & graphs' main menu. After fitting a model, you " +
   "will be able to display additional graphs related to the validation of the model.\n\n" +

   "At the end of any of the two ‘Modelization’ options you will be able to use the ‘Display the results " +
   "tables’ related to modelization and validation. If the model was validated, you will also access to " +
   "the sub-menu ‘Intent prediction (allowed if good jackknife test)’. In any case you will be allowed to build and display a " +
   "history html & graphs directory by using the corresponding sub-menu in the 'Files' main menu.\n\n");
    jTextAreaCopyRight.setLineWrap(true);
    jTextAreaCopyRight.setWrapStyleWord(true);
    jTextAreaCopyRight.setPreferredSize(new Dimension(890, 610));
    jTextAreaCopyRight.setEditable(false);
    jTextAreaCopyRight.setMargin(new Insets(5, 5, 5, 5));
    jTextAreaCopyRight.setFont(new java.awt.Font("Serif", 0, 14)); // Fenetre Help ->"How to get started" du menu principal. Avant 0, 11. Modif 2020.
    jPanText.setLayout(flowLayout1);
    jPanText.setBorder(new EmptyBorder(10, 10, 10, 10));
    jPanText.add(jTextAreaCopyRight, null);

    cmdOK.setText("Ok");
    int condition = JComponent.WHEN_IN_FOCUSED_WINDOW;
    KeyStroke keyStrokep = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
    InputMap inputMapp = cmdOK.getInputMap(condition);
    ActionMap actionMapp = cmdOK.getActionMap();
    inputMapp.put(keyStrokep, keyStrokep.toString());
    actionMapp.put(keyStrokep.toString(), new AbstractAction() {            
        @Override
        public void actionPerformed(ActionEvent arg0) {
            cmdOK.doClick();
        }
    });
    cmdOK.addActionListener(this);
    jPanCmd.add(cmdOK, null);

    jPanHowTo.add(jPanText, BorderLayout.CENTER);

    jPanHowTo.add(jPanCmd, BorderLayout.SOUTH);
    this.getContentPane().add(jPanHowTo, null);
  }

  protected void processWindowEvent(WindowEvent e) {
    if (e.getID() == WindowEvent.WINDOW_CLOSING) {
      cancel();
    }
    super.processWindowEvent(e);
  }

  void cancel() {
    dispose();
  }

  public void actionPerformed(ActionEvent e) {
    if (e.getSource() == cmdOK) {
      cancel();
    }
  }
}