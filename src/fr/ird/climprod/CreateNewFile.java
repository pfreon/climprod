package fr.ird.climprod;

/**
 * <p>Titre : </p>
 * <p>Description : </p>
 * <p>Copyright : Copyright (c) 2006</p>
 * <p>Société : </p>
 * @author non attribué
 * @version 1.0
 * Creation of a new data set. Comment 2020.
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;

import javax.swing.table.TableColumn;

import javax.swing.filechooser.FileFilter;

public class CreateNewFile extends JDialog{
  private BorderLayout borderLayout1 = new BorderLayout();
  private JPanel jPanData = new JPanel();
  private JPanel jPanCmd=new JPanel();
  private JButton cmdOk=new JButton("Save");
  private JButton cmdCancel=new JButton("Cancel (ESC)");
  private JScrollPane jScrollPane1 = new JScrollPane();
  private JTable jTableData = new JTable();
  private String[] title$={"Year/season","Catches","Effort","Environment"};
  private Object[][] data$=new String[100][4];

  private int choice;
  int NbObservations=0;
  final int Ok_Option=1;
  final int Cancel_Option=2;
  final int MaxObservations=100;
  private ModeleDeTableEdition mdt;
  private JFrame parent;

  public CreateNewFile(JFrame parent) {
      super(parent);
      this.parent=parent;
      choice=0;
      try {
        initWindow();
        this.setSize(500,400);
        UtilCadre.Centrer(this);
      }
      catch(Exception e) {
      e.printStackTrace();
      }
  }
  private void initWindow() throws Exception {
    jScrollPane1.getViewport().add(jTableData, null);
    jPanData.add(jScrollPane1, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0,GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(5, 2, 5, 2), 0, 200));
    //Assignations des variables de GridBagConstraints(gridx, gridy, gridwidth, gridheight, 1.0, 1.0, anchor, fill, insets, 0, 0)
    //Assignations des variables de Insets(int top, int left, int bottom, int right)    
    this.getContentPane().add(jPanData, BorderLayout.CENTER);

    jPanCmd.add(cmdOk);
    jPanCmd.add(cmdCancel);
    int condition = JComponent.WHEN_IN_FOCUSED_WINDOW;
    KeyStroke keyStrokep = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
    InputMap inputMapp = cmdCancel.getInputMap(condition);
    ActionMap actionMapp = cmdCancel.getActionMap();
    inputMapp.put(keyStrokep, keyStrokep.toString());
    actionMapp.put(keyStrokep.toString(), new AbstractAction() {            
        @Override
        public void actionPerformed(ActionEvent arg0) {
            cmdCancel.doClick();
        }
    });
    this.getContentPane().add(jPanCmd, BorderLayout.SOUTH);

    cmdOk.addActionListener(new java.awt.event.ActionListener() {

      public void actionPerformed(ActionEvent e) {
         cmdOk_actionPerformed(e);
      }
    });
    cmdCancel.addActionListener(new java.awt.event.ActionListener() {

      public void actionPerformed(ActionEvent e) {
        cmdCancel_actionPerformed(e);
      }
    });

    //Initialisation de la table de saisie

    for(int i=0;i<MaxObservations;i++)
    {
        for(int j=0;j<4;j++)
            data$[i][j]="";
    }

    mdt=new ModeleDeTableEdition(data$,title$);
    jTableData.setModel(mdt);
    jTableData.setColumnSelectionAllowed(false);
    jTableData.setRowSelectionAllowed(false);
    jTableData.setRowHeight(25);
   // jTableData.setTableHeader(new JTableHeader tableHeader());
    TableColumn tmcolumn = null;
    floatTextField ftf =new floatTextField();
    JTextField jt=new JTextField();
    
    int nbc=jTableData.getColumnModel().getColumnCount();
    jTableData.getColumnModel().getColumn(0).setCellEditor(new DefaultCellEditor(new floatTextField())); // Before (new integerTextField() in order to allow inputs of fractions of years. Modif 2021
    jTableData.getColumnModel().getColumn(0).setMinWidth(70);
    for (int i = 1; i < nbc; i++) {
      tmcolumn = jTableData.getColumnModel().getColumn(i);
      tmcolumn.setMinWidth(70);
      tmcolumn.setCellEditor(new DefaultCellEditor(new floatTextField()));
    }
  /* jTableData.getSelectionModel().addListSelectionListener(new ListSelectionListener () {
    public void valueChanged(ListSelectionEvent e) {
        int rowIndex = jTableData.getSelectedRow();
        int colIndex = jTableData.getSelectedColumn();
//jTableData.setDefaultEditor(jTableData.setCellEditor(new TableCellEditor({})));

        //label.setText(SheetTableModel.getCellName(rowIndex, colIndex));
       // editor.setText(model.getLiteralValueAt(rowIndex, colIndex));
      //  editor.requestFocusInWindow();
       // MsgDialogBox msg=new MsgDialogBox(0,rowIndex+"   "+ colIndex,0);
    }
    });*/

  }
  void cmdOk_actionPerformed(ActionEvent e) {
   /*
    Contr�le validit� de la saisie ( Donn�es manquantes Lignes vides)
   */
   int k=0;
   boolean endFile=false;
   NbObservations=0;
   try{
     for(int i=0;i<MaxObservations;i++){
       k=0;
       for(int j=0;j<4;j++){
         if(data$[i][j].toString().trim().length()==0) k++;
       }
       //System.out.println(k+"  "+ endFile);
       if(endFile && k!=4){
          throw new OnError("Empty row(s) before row " + (i+1));
       }

       if(k!=0 && k!=4) {
          throw new OnError("Invalid data, row " + (i+1));
       }
       endFile=(k==4);
       if(endFile && NbObservations==0) NbObservations=i;
     }

     JFileChooser d=new JFileChooser();
     d.setCurrentDirectory(new File(Global.datafilePath));
     d.setDialogTitle("Save a data file");

     //*********************************
     String nomFichier="";

    //*************************************************************************

    d.setFileFilter(new FileFilter()	{
      public boolean accept(File f){
              return f.getName().toLowerCase().endsWith(".cli") || f.isDirectory();
     }
     public String getDescription(){
             return "Data file (*.cli)";
     }
        });
    d.setSelectedFile(new File(nomFichier));
    int res=d.showSaveDialog(this);
    if (res==JFileChooser.APPROVE_OPTION){
          nomFichier=d.getSelectedFile().getAbsolutePath();
          if (nomFichier!=null){
            //Contr�le et correction eventuelle de l'extension: ".cli" obligatoirement
            if(!nomFichier.endsWith(".cli")){
              if(nomFichier.indexOf(".")==-1)
                nomFichier=nomFichier+".cli";
              else
                nomFichier=nomFichier.substring(0,nomFichier.indexOf("."))+".cli";
            }
            // Le nom de fichier existe-il d�j�, si oui veut-on �craser le fichier existant?
            File fexist=new File(nomFichier);
            if(fexist.exists()){
              MsgDialogBox msg=new MsgDialogBox(1,"Do you want to overwritting the existing file?",2, this.parent);
              if(msg.No()) return;
            }

            try
            {this.saveDataFile(nomFichier);
               mdt.updateIsSave();
            }
            catch(IOException ec)
                { MsgDialogBox msg = new MsgDialogBox(0,"Unexpected error "  + ec.getMessage(),0, this.parent);
            choice=Cancel_Option;
            cancel();
                }

          }
      }
   }
   catch(OnError ex)
   {
     MsgDialogBox msg=new MsgDialogBox(0,ex.getMessage(),0, this.parent);
   }
  }

  void cmdCancel_actionPerformed(ActionEvent e) {
      choice=Cancel_Option;
      if(mdt.isChanged()){
        MsgDialogBox msg=new MsgDialogBox(1,"Do you want to stop without recording updates?",2, this.parent);
              if(msg.No()) return;
      }
      cancel();
  }
  private void cancel() {
    this.setCursor(Cursor.getDefaultCursor());
    dispose();
}

private void saveDataFile(String fileName) throws IOException
{
          PrintWriter out=new PrintWriter(new FileWriter(fileName));
          out.println(title$[0]+";"+ title$[1]+";"+title$[2]+";"+title$[3]);
          for(int i=0;i<NbObservations;i++){
                  out.println(data$[i][0]+";"+data$[i][1]+";"+data$[i][2]+";"+data$[i][3]);
          }

          out.close();
}

}