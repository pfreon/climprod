
/*
 * Titre : Climprod<p>
 * Tracer de graphiques dont l'axe des X représente une variable qualitative.
 * Utilisé pour histogrammes du jackknife uniquement.
 */
package fr.ird.climprod;

import java.awt.font.*;
import java.awt.geom.*;

import java.awt.*;

import java.awt.datatransfer.*;

public class PlotModal extends Plot {

    public PlotModal() {
        super();
        super.typePlot = 4;
    }

    /*
     * Ajoute une série de données au graphique représentant des valeurs.
     * La série est ajoutée au vecteur ListeSerie.
     * Après l'ajout la méthode détermine les nouveaux extremas du graphique
     * @param PlotSerie s.
     */
    @Override
    public void setValeurs(PlotSerie s) {
	//System.out.println("Flag PlotModal setValeurs()");
        if (nbSerie == 0) {
            extremasSeries[0] = 1;
            extremasSeries[1] = s.getSize();
            titre[0] = s.getNameX();
            extremasSeries[2] = s.getMinY();
            extremasSeries[3] = s.getMaxY();
            extremasAxes[0] = 0;
            titre[1] = s.getNameY();
            pas[0] = 1;
            etiquettes = s.getEtiquettes();
            nbBarPlot = (int) extremasSeries[1];
        } else {
            if (nbBarPlot < s.getSize()) {
                nbBarPlot = (int) extremasSeries[1];
            }
            extremasSeries[2] = Math.min(extremasSeries[2], s.getMinY());
            extremasSeries[3] = Math.max(extremasSeries[3], s.getMaxY());
        }

        extremasAxes[1] = (float) (extremasSeries[1] + 1);
        pas[1] = Math.abs((extremasSeries[3] - extremasSeries[2]) / 10);
        if (pas[1] > 1) {
            pas[1] = Math.round(pas[1] + 0.5);
        }
        ListeSerie.add(s);
        if (CouleursParDefault) {
            couleursParDefault(s);
        }

        nbSerie++;
        super.ajusteExtrema(2, 3);
    }
    /*
     * Trace les axes du graphique.
     *@param : Graphics g, le contexte graphique.
     */

    @Override
    public void TraceAxes(Graphics g) { // Appelé pour histogrammes.
        if (etiquettes == null) {
            return;
        }
        double x1, y1, xg, px;
        double x2, y2, yg, py;
        double maxx;
        int grad;
        double nb;
        Line2D.Double line;
        String eti;
        double[] posLeg = new double[2];
        Font fcurrent = g.getFont();
        FontMetrics fm;
        //Dimension d=panel.getSize();
        Graphics2D g2 = (Graphics2D) g;
        Shape[] shapes = new Shape[1];

        if (dimPlot.width < dimPlot.height) {
            grad = (int) (dimPlot.width * 0.01);
        } else {
            grad = (int) (dimPlot.height * 0.01);
        }
		//for (int i = 0; i < 4; i++) System.out.println("PlotModal.java TraceAxes(Graphics g) i = " + i + " extremasAxes[i]= " + extremasAxes[i] + " paramEchelle[i] = " + paramEchelle[i]); 
		// Les 4 lignes suivantes apparaissent aussi dans Plot.java avec différence dans y2.
        x1 = (extremasAxes[0] * paramEchelle[0] - paramEchelle[2]);
        x2 = (extremasAxes[1] * paramEchelle[0] - paramEchelle[2]);
        y1 = ((extremasAxes[2] + origine[0]) * paramEchelle[1] + paramEchelle[3]);
        y2 = ((extremasAxes[3] + origine[0]) * paramEchelle[1] + paramEchelle[3]);
        line = new Line2D.Double(x1, y1, x2, y1);
        g2.draw(line);
        posLeg[0] = x2;
        posLeg[1] = y2;
        fm = g.getFontMetrics(feti);
        py = fm.getHeight();
        double py2 = py / 2;
        double dec = 0;

        nb = extremasAxes[0] + 1;
        g.setFont(feti);
        for (int i = 0; i < nbBarPlot; i++) {
            xg = (nb * paramEchelle[0] - paramEchelle[2]);
            line = new Line2D.Double(xg, y1 - grad, xg, y1 + grad);
            g2.draw(line);
            if (i % pas[0] == 0) {
                eti = etiquettes[i];
                px = fm.stringWidth(eti) / 2;
                g.drawString(eti, (int) (xg - px), (int) (y1 + grad + py + dec));
                dec = py2 - dec;
            }
            nb = nb + 1;

        }
		TextLayout textT2;
        TextLayout textTl = new TextLayout(titre[0], getPoliceTitre('x'), new FontRenderContext(null, false, false));
        textTl.draw(g2, (float) (x1 + (x2 - x1) / 2 - (textTl.getBounds().getWidth()) / 2), (float) (y1 + grad + 2 * py + (textTl.getBounds().getHeight())));

        textTl = new TextLayout(titre[2], getPoliceTitre('g'), new FontRenderContext(null, false, false));
        textTl.draw(g2, (float) (x1 + (x2 - x1) / 2 - (textTl.getBounds().getWidth()) / 2), (float) (y2 - 4 * (textTl.getBounds().getHeight())));
		textT2 = new TextLayout(titre[3], getPoliceTitre('g'), new FontRenderContext(null, false, false));
		textT2.draw(g2, (float) (x1 + (x2 - x1) / 2 - (textT2.getBounds().getWidth()) / 2), (float) (y2 - 2 * (textT2.getBounds().getHeight())));
        if (ShowLegend) {
            traceLegende(g, posLeg);
        }
		// Les 4 lignes suivantes apparaissent ci-dessus avec differences et aussi dans Plot.java.

       x1 = ((extremasAxes[0] + origine[1]) * paramEchelle[0] - paramEchelle[2]);
        x2 = ((extremasAxes[1] + origine[1]) * paramEchelle[0] - paramEchelle[2]);
        y1 = (extremasAxes[2] * paramEchelle[1] + paramEchelle[3]);
        y2 = (extremasAxes[3] * paramEchelle[1] + paramEchelle[3]);
        line = new Line2D.Double(x1, y1, x1, y2);
        g2.draw(line);
    	//line=new Line2D.Double(x1-grad,y1,x1+grad,y1);
        //g2.draw(line);
        nb = extremasAxes[2];
        // nb=extremasAxes[2]+pas[1];
        maxx = 0;
        while (nb <= extremasAxes[3] + 0.0001) {
            yg = (nb * paramEchelle[1] + paramEchelle[3]);
            line = new Line2D.Double(x1 - grad, yg, x1 + grad, yg);
            g2.draw(line);
            eti = getFormat(nb, 'y');
            px = fm.stringWidth(eti) + 2;
            if (px > maxx) {
                maxx = px;
            }

            g.drawString(eti, (int) (x1 - grad - px), (int) (yg));
            nb = nb + pas[1];
        }
        int w = dimPlot.width;
        textTl = new TextLayout(titre[1], getPoliceTitre('y'), new FontRenderContext(null, false, false));
        AffineTransform textAt = new AffineTransform();
        g2.setFont(getPoliceTitre('y'));
        textAt.setToTranslation(x1 - grad - maxx - (float) textTl.getBounds().getHeight(), (y2 + (y1 - y2 + (float) textTl.getBounds().getWidth()) / 2));
        g2.transform(textAt);
        textAt.setToRotation(3 * Math.PI / 2.0);
        g2.transform(textAt);
        g2.drawString(titre[1], 0, 0);

        g.setFont(fcurrent);
    }

    /*
     * Affecte de nouvelles étiquettes à l'axe
     * des X.
     */
    public void setEtiquettes(String[] etiq) {
        if (etiq.length < nbBarPlot) {
            // do nothing
        } else {
            etiquettes = etiq;
        }

    }

    /*
     * Exporte l'ensemble des données du graphique dans le presse papier.
     * Le graphique peut être reconstitué ainsi sous Excel.
     */
    public void exportClipboard() {

        StringBuilder sbf = new StringBuilder();
        Clipboard system;
        StringSelection stsel;
        sbf.append("\t");
        for (String etiquette : etiquettes) //ligne de titre
        {
            sbf.append(etiquette).append("\t");
        }
        sbf.append("\n");
        for (Object serie : ListeSerie) {
            PlotSerie s = (PlotSerie) serie;
            double[] vy = s.getDataY();
            sbf.append(s.getNameY()).append("\t");
            for (int j = 0; j < vy.length; j++) {
                sbf.append(vy[j]).append("\t");
            }
            sbf.append("\n");
        }
        stsel = new StringSelection(sbf.toString());
        system = Toolkit.getDefaultToolkit().getSystemClipboard();
        system.setContents(stsel, stsel);

    }
    /*
     Affecte le pas sur l'axe X du graphique.Ne retient que la partie entière
     (Si le pas est superieur à l'étendue sur X ou <0 pas d'affectation.)
     @param double le pas p.
     */

    @Override
    public void setpasX(double p) {
        if (p < extremasAxes[1] && p > 0) {
            pas[0] = Math.floor(p);
        }
    }

    private String[] etiquettes;
    private int nbBarPlot;

}
