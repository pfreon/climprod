/**
 * Titre : Climprod<p>
 * Description :
 * <p>
 * Copyright : Copyright (c) T. L<p>
 * Soci�t� :
 * <p>
 * @author T. L
 * @version 1.0
 */
package fr.ird.climprod;

public class EquationModele {

    /*
     * Calcul de la moyenne sur un tableau d'entiers i.
     * @param int[]i.
     * @return un double, la moyenne.
     */
    static public double fonction_modele(double ff, double vv, double vba, double par[]) { // vv = V; vba = V moyenne pondérée.
        double puec = 0;
        double tmp, tmp1, tmp2;
        int numeromod = Global.numero_modele;
        //System.out.println("numero" + numeromod);
        switch (numeromod) {
            case 0:   // CPUE=a+b.E
                puec = par[0] + par[1] * ff;
                break;
            case 1:   // CPUE=a.exp(b.E)
                puec = par[0] * Math.exp(par[1] * ff);
                break;
            case 2:   // CPUE=a+b.V
                puec = par[0] + par[1] * vba;
                break;
            case 3:   // CPUE=a.V^b
                puec = par[0] * Math.pow(vba, par[1]);
                break;
            case 4:   // CPUE=a+b.V^c
                puec = par[0] + par[1] * Math.pow(vba, par[2]);
                break;
            case 5:   // CPUE=a+b.V+c.V^2. avant CPUE=a.V+b.V^2+c. Commentaire 2020
                puec = par[0] + par[1] * vba + par[2] * (vba * vba);
                break;
            case 6:   // CPUE=(a+b.E)^(1/(c-1))
                tmp = par[0] + par[1] * ff;
                if (tmp > 0) {
                    puec = Math.pow(tmp, 1.0 / (par[2] - 1));
                } else {
                    puec = -Math.pow(-tmp, 1.0 / (par[2] - 1));
                }
                break;
            case 7:   // CPUE=a.V^b+c.E
                puec = par[0] * Math.pow(vba, par[1]) + par[2] * ff;
                break;
			case 34:   // CPUE=a.V.exp(b.V)+c.E ABONDANCE
                puec = par[0] * vba * Math.exp(par[1] * vba) + (par[2] * ff);
                break;
		    /*case 35:   // CPUE=a+b.V.exp(c.V)+d.E (Idem modèle 34 CPUE=a.V.exp(b.V)+c.E mais avec constante. Ajustement pas toujours réaliste, bénéfice minime, paramètre 'a' souvent non significatif. Parfois pb d'ajustement (ne converge pas ou MSY et MSE remarquables = 0 ou < 0.
				 puec = par[0] + par[1]* vba * Math.exp(par[2]*vba) + (par[3] * ff);
				break;*/
			case 35:   // CPUE=a.V.exp(b.V).exp(c.E) ABONDANCE
				puec = par[0] * vba * Math.exp(par[1] * vba)*Math.exp(par[2] * ff);
				break;
			case 37:   // CPUE=a.V.exp(b.V)+c.(a.V.exp(b.V))^2).E  // CAPTURABILITE. Le modèle CPUE=a.V.exp(b.V)+c.(V.exp(b.V))^2).E donne résultats identiques mais parfois ne converge pas (R² = 0).
				puec = par[0] * vv * Math.exp(par[1] * vv) + par[2] *(par[0]*vv * Math.exp(par[1]*vv))* (par[0]*vba * Math.exp(par[1]*vba)) * ff; //puec = par[0]* vv * Math.exp(par[1] * vv) + par[2] *(vv * Math.exp(par[1]*vv))* (vba * Math.exp(par[1]*vba)) * ff; // 
				break;
			case 36:   // CPUE=a.V.exp(b.V).exp(c.V.exp(b.V).E) // CAPTURABILITE
				puec = par[0] * vv * Math.exp(par[1] * vv) * Math.exp(par[2] * vba * Math.exp(par[1]*vba)* ff);
				break;  
			/* case 38:   // CPUE=a.V.exp(b.V).exp(c.V.exp(d.V).E) // ABONDANCE et/ou MIXTE. Pb: ajustement difficile et souvent irréaliste.
				puec = par[0] * vv * Math.exp(par[1] * vv) * Math.exp(par[2] * vv * Math.exp(par[3]*vv)* ff);
				break; */				          
            case 8:   // CPUE=a+b.V+c.E
                puec = par[0] + par[1] * vba + par[2] * ff;
                break;
            case 9:   // CPUE=a.V+b.E
                puec = par[0] * vba + par[1] * ff;
                break;
            case 10:   // CPUE=a+b.V+c.V^2+d.E
                puec = par[0] + par[1] * vba + par[2] * (vba * vba) + par[3] * ff;
                break;
            case 11:   // CPUE=a.V^b.exp(c.E)
                puec = par[0] * Math.pow(vba, par[1]) * Math.exp(par[2] * ff);
                break;
            case 12:   // CPUE=(a+b.V).exp(c.E) 
                puec = (par[0] + par[1] * vba) * Math.exp(par[2] * ff);
                break;
            case 13:   // CPUE=a.V.exp(b.E)
                puec = par[0] * vba * Math.exp(par[1] * ff);
                break;
			case 33:   // CPUE = a V exp(bV)
                puec = par[0] * vba * Math.exp(par[1] * vba);
                break;
            case 14: // CPUE=a+b.V+c.(a+b.V)^2.E MODELE CAPTURABILITE #22
                puec = par[0] + par[1] * vv + par[2] * (par[0] + par[1] * vv) * (par[0] + par[1] * vba) * ff;
                break;
            case 15:   // CPUE=a.V^b+c.V^(2.b).E MODELE CAPTURABILITE
                tmp1 = Math.pow(vv, par[1]);
                tmp2 = Math.pow(vba, par[1]);
                puec = par[0] * tmp1 + par[2] * tmp1 * tmp2 * ff;
                break;
            case 16:   // CPUE=a.V+b.V^2.E MODELE CAPTURABILITE
                puec = par[0] * vv + par[1] * vv * vba * ff;
                break;
            case 17:   // CPUE=a.V^b.exp(c.E.V^b) MODELE CAPTURABILITE
                tmp1 = Math.pow(vv, par[1]);
                tmp2 = Math.pow(vba, par[1]);
                puec = par[0] * tmp1 * Math.exp(par[2] * tmp2 * ff);
                break;
            case 18:   // CPUE=(a+b.V).exp(c.(a+b.V).E) MODELE CAPTURABILITE
                puec = (par[0] + par[1] * vv)
                        * Math.exp(+par[2] * (par[0] + par[1] * vba) * ff);
                break;
            case 19:   // CPUE=a.V.exp(b.V.E) MODELE CAPTURABILITE
                puec = par[0] * vv * Math.exp(par[1] * vba * ff);
                break;
            case 20:   // CPUE=a.exp(b.E)+c.V+d MODELE ABONDANCE ADDITIF (impossible reformuler en a+bV+c exp(d E), cf commentaire dans Model.java, initialisation)
                puec = par[0] * Math.exp(par[1] * ff) + par[2] * vba + par[3];
                break;
            case 21:   // CPUE=(a.V+b.V^2).exp(c.E)  MODELE ABONDANCE
                puec = (par[0] * vba + par[1] * (vba * vba)) * Math.exp(par[2] * ff);
                break;
            case 22:   // CPUE=((a.V^b)+d.E)^(1/(c-1)) avant ((a.V^b)+c.E)^(1/(d-1)). MODIF 2020.
                tmp = par[0] * Math.pow(vba, par[1]) + par[3] * ff;
                if (tmp > 0) {
                    puec = Math.pow(tmp, 1.0 / (par[2] - 1));
                } else {
                    puec = -Math.pow(-tmp, 1.0 / (par[2] - 1));
                }
                break;
            case 23:  // CPUE = ((a.V+b.V^2)^(c-1)+d.E)^(1/(c-1)) avant ((a.V+b.V^2)^(d-1)+c.E)^(1/(d-1)) MODELE ABONDANCE
                tmp = par[0] * vba + par[1] * (vba * vba);
                if (tmp > 0) {
                    tmp = Math.pow(tmp, par[2] - 1) + par[3] * ff;
                } else {
                    tmp = -Math.pow(-tmp, par[2] - 1) + par[3] * ff;
                }
                if (tmp > 0) {
                    puec = Math.pow(tmp, 1. / (par[2] - 1)); 
                } else {
                    puec = -Math.pow(-tmp, 1. / (par[2] - 1)); 
                }
                break;
            case 24:   // CPUE=a.V.(b-c.V)+d.V^2.(b-c.V)^2.E MODELE CAPTURABILITE
                tmp = par[1] - par[2] * vv;
                puec = par[0] * vv * tmp
                        + par[3] * vv * vba * tmp * (par[1] - par[2] * vba) * ff;
                break;
            case 25:   // CPUE=a.V.(1+b.V).exp(c.V.(1+b.V).E) MODELE CAPTURABILITE
                puec = par[0] * vv * (1 + par[1] * vv) * Math.exp(par[2] * vba * (1 + par[1] * vba) * ff);
                break;
            case 26:   // CPUE=a.V^(b+c)+d.V^(2.b).E MODELE CAPTURABILITE + ABONDANCE 
                tmp1 = Math.sqrt(vba * vv);
                puec = par[0] * Math.pow(tmp1, par[1] + par[2]) + par[3] * Math.pow(vv*vba, par[1]) * ff;
                break;
            case 27:   // CPUE=a.V^(1+b)+c.V^(2+b)+d.V^(2.b).E MODELE LINEAIRE QUADRATIQUE CAPTURABILITE + ABONDANCE.  
                tmp1 = Math.pow(vv, par[1]);
                tmp2 = Math.pow(vba, par[1]);
                puec = par[0] * tmp1 * vba + par[2] * (vv * vba) * tmp1 + par[3] * tmp1 * tmp2 * ff; // Formulation vérifiée (X^(a+b) = X^a . X^b)
                break;
            case 28:   // CPUE=a.V^b.exp(c.V^d.E) MODELE ABONDANCE
                puec = par[0] * Math.pow(vba, par[1]) * Math.exp(par[2] * Math.pow(vba, par[3]) * ff);
                break;
            case 29:   // CPUE=a.V^b.exp(c.V^d.E) MODELE CAPTURABILITE + ABONDANCE
                tmp1 = Math.sqrt(vba * vv);
                puec = par[0] * Math.pow(tmp1, par[1]) * Math.exp(par[2] * Math.pow(vba, par[3]) * ff);
                break;
            case 30:  //  CPUE=(a.V^(1+b)+c.V^(2+b)).exp(d.V^b.E) MODELE EXP. QUADRATIQUE-PUISSANCE CAPTURABILITE + ABONDANCE 
                tmp1 = Math.pow(vv, par[1]);
                tmp2 = Math.pow(vba, par[1]);
                // AVANT: puec = (par[1] * tmp1 * vba + par[0] * tmp1 * (vba * vba)) * Math.exp(par[3] * tmp2 * ff);
			    puec = (par[0] * tmp1 * vba + par[2] * (vv * vba) * tmp1) * Math.exp(par[3] * tmp2 * ff); // Formulation vérifiée (X^(a+b) = X^a . X^b)
                break;
        }
        return (puec);
    }

    static public double minimum_fonction(double vv, double par[]) { // En fait il s'agit de la valeur maximale de E(ff) de la fonction Y=f(E) ou Y=f(E,V)
        double ff = 0, aa, bb, cc;									 // On calcule donc la valeur de E(ff) qui annule la dérivée de la fonction Y.
        int numeromod = Global.numero_modele;
        switch (numeromod) {
            case 0:  // CPUE=a+b.E
                ff = -0.5 * par[0] / par[1];
                break;
            case 1:  // CPUE=a.exp(b.E)
                ff = -1.0 / par[1];
                break;
            case 2:  // CPUE=a+b.V
                ff = 0;
                break;
            case 3:  // CPUE=a+b.V
                ff = 0;
                break;
            case 4:  // CPUE=a+b.V^c
                ff = 0;
                break;
            case 5:  // CPUE=a+b.V+c.V^2
                ff = 0;
                break;
            case 6:  // CPUE=(a+b.E)^(1/(c-1))
                ff = -par[0] * (par[2] - 1) / (par[1] * par[2]);
                break;
            case 7:   // CPUE=a.V^b+c.E
                ff = -0.5 * par[0] * Math.pow(vv, par[1]) / par[2];
                break;
			case 34:   // CPUE=a.V.exp(b.V)+c.E
				ff = - 0.5 * par[0] * vv * Math.exp(par[1]*vv)/ par[2]; // ff = - par[0] * vv * Math.exp(par[1]*vv)/ 2 * par[2];
                break;
			/*case 35:   // CPUE=a+b.V.exp(c.V)+d.E (Idem modèle 34 CPUE=a.V.exp(b.V)+c.E mais avec constante. Ajustement pas toujours réaliste, bénéfice minime, paramètre 'a' souvent non significatif. Parfois pb d'ajustement (ne converge pas ou MSY et MSE remarquables = 0 ou < 0.
				ff = -0.5 * (par[1]* vv * Math.exp(par[2]*vv))/ par[3];
                break; */
            case 8:  // CPUE=a+b.V+c.E
                ff = -0.5 * (par[0] + par[1] * vv) / par[2];
                break;
			case 35:   // CPUE=a.V.exp(b.V).exp(c.E)
				ff = -1.0 / par[2];
				break; 
			case 37:   // CPUE=a.V.exp(b.V)+c.(aV.exp(b.V))^2).E  // Capturabilité
				ff = -1 /(2 * par[2] * par[0] * vv * Math.exp(par[1] * vv)); // ff = -0.5 * par[2] * par[0] * vv * Math.exp(par[1] * vv); 
				break;
			case 36:   // CPUE=a.V.exp(b.V).exp(c.V.exp(b.V).E)
				ff = -1 /(par[2] * vv * Math.exp(par[1]*vv)); 
			break;
			/*case 38:   // CPUE=a.V.exp(b.V).exp(c.V.exp(d.V).E) // ABONDANCE et MIXTE?
				ff = -1 /(par[2] * vv * Math.exp(par[3]*vv)); 
				break; */
            case 9:  // CPUE=a.V+b.E
                ff = -0.5 * par[0] * vv / par[1];
                break;
            case 10: // CPUE=a+b.V+c.V^2+d.E
                ff = -0.5 * (par[0] + par[1] * vv + par[2] * (vv * vv)) / par[3];
                break;
            case 11: // CPUE=a.V^b.exp(c.E)
                ff = -1.0 / par[2];
                break;
            case 12: // CPUE=(a+b.V).exp(c.E)
                ff = -1.0 / par[2];
                break;
            case 13: // CPUE=a.V.exp(b.E)
                ff = -1.0 / par[1];
                break;
			case 33:   // CPUE = a V exp(bV)
				ff = 0;
				break;
            case 14: // CPUE=a+b.V+c.(a+b.V)^2.E
                ff = -0.5 / (par[2] * (par[0] + par[1] * vv));
                break;
            case 15: // CPUE=a.V^b+c.V^(2.b).E MODELE CAPTURABILITE
                ff = -0.5 * par[0] * Math.pow(vv, par[1]) / (par[2] * Math.pow(vv, par[1] * 2));
                break;
            case 16: // CPUE=a.V+b.V^2.E MODELE CAPTURABILITE
                ff = -0.5 * par[0] / (par[1] * vv);
                break;
            case 17: // CPUE=a.V^b.exp(c.V^b.E) MODELE CAPTURABILITE
                ff = -1.0 / (par[2] * Math.pow(vv, par[1]));
                break;
            case 18: // CPUE=(a+b.V).exp(c.(a+b.V).E) MODELE CAPTURABILITE
                ff = -1.0 / (par[2] * (par[0] + par[1] * vv));
                break;
            case 19: // CPUE=a.V.exp(b.V.E) MODELE CAPTURABILITE
                ff = -1.0 / (par[1] * vv);
                break;
            case 20: // CPUE=a.exp(b.E)+c.V+d
                ff = 0.0;
                /*
                 aa = par[1];
                 bb = par[2];
                 cc = par[3]*vv+par[4];
                 ff = minimis2(f0,aa,bb,cc);
                 */
                break;
            case 21: // CPUE=(a.V+b.V^2).exp(c.E)
                ff = -1.0 / par[2];
                break;
            case 22: // CPUE=((a.V^b)+d.E)^(1/(c-1))
                ff = -(par[0] * Math.pow(vv, par[1])) * (par[2] - 1) / (par[3] * par[2]);
                break;
            case 23: // CPUE=((a.V+b.V^2)^(c-1)+d.E)^(1/(c-1))
				 ff = -Math.pow(par[0] * vv + par[1] * (vv * vv), par[2] - 1) * (par[2] - 1) / (par[3] * par[2]);
                break;
            case 24: // CPUE=a.V.(b-c.V)+d.V^2.(b-c.V)^2.E
                ff = 0.5 * par[0] / (-par[3] * vv * (par[1] - par[2] * vv));
                break;
            case 25: // CPUE=a.V.(1+b.V).exp(c.V.(1+b.V).E)
                ff = -1.0 / (par[2] * vv * (1 + par[1] * vv));
                break;
            case 26: // CPUE=a.V^(b+c)+d.V^(2.b).E
                ff = -0.5 * par[0] * Math.pow(vv, par[2] - par[1]) / par[3];
                break;
            case 27: // CPUE=a.V^(1+b)+c.V^(2+b)+d.V^(2.b).E
                ff = -0.5 * (par[0] + par[2] * vv) / (par[3] * Math.pow(vv, par[1]));
                break;
            case 28: // CPUE=a.V^b.exp(c.V^d.E)
                ff = -1.0 / (par[2] * Math.pow(vv, par[3]));
                break;
            case 29: // CPUE=a.V^b.exp(c.V^d.E)
                ff = -1.0 / (par[2] * Math.pow(vv, par[3]));
                break;
            case 30: // CPUE=(a.V^(1+b)+c.V^(2+b)).exp(d.V^b.E)
                ff = -1.0 / (par[3] * Math.pow(vv, par[1]));
                break;
        }
        return (ff);
    }
}
