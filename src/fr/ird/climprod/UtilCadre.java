/**
 * Titre : Climprod<p>
 * Controle taille fenetre question au debut des questions du menu 
 * "Select the appropriate model and fit it".
 */
package fr.ird.climprod;

import java.awt.*;
import javax.swing.*;

public class UtilCadre 
{

    static public void Centrer(JFrame f) 
   {
        GraphicsDevice gd = Global.CadreMain.getGraphicsConfiguration().getDevice();
        int xscr = gd.getDisplayMode().getWidth();  // Obtient la résolution en largeur de l'écran de l'ordi utilisé (idem lignes infra)
        int yscr = gd.getDisplayMode().getHeight(); // Obtient la résolution en hauteur de l'écran 
        int xoffset = gd.getConfigurations()[0].getBounds().x;
        int yoffset = gd.getConfigurations()[0].getBounds().y;
        // System.out.println("Résolution écran x et y ligne 15 UtilCadre: " + xscr +" "+yscr); // Test 2020
        // System.out.println("Centrer offset ligne 17 UtilCadre, xoffset et yoffset = " + xoffset +" "+yoffset); // Test 2020
        Dimension size = f.getSize();

        int y = (yscr - size.height) / 2;
        int x = (xscr - size.width) / 2; 
		f.setLocation(x+xoffset, y+yoffset); // f.setLocation(500,150) = Test 2020; 
        // System.out.println("Hauteur fenêtre Select (size.height): " + size.height + "  Largeur (size.width): " + size.width);
		// System.out.println("Coordonnées xy point en haut à gauche de fenêtre Select the appropriate model et fen^tre Fit directely ligne 25 UtilCadre.java: x = " + x + "  xoffset = "+ xoffset + "  y = " + y + "  yoffset = " + xoffset);  // Test 2020
    }

    static public void Centrer(JDialog f) 
    {
        GraphicsDevice gd = Global.CadreMain.getGraphicsConfiguration().getDevice();
        int xscr = gd.getDisplayMode().getWidth();  //Obtient la résolution en largeur de l'écran de l'ordi utilisé (supra et infra)
        int yscr = gd.getDisplayMode().getHeight(); // Obtient la résolution en hauteur de l'écran  
        int xoffset = gd.getConfigurations()[0].getBounds().x;
        int yoffset = gd.getConfigurations()[0].getBounds().y;

        //Dimension screenSize = f.getToolkit().getScreenSize();
        Dimension size = f.getSize();
        int y = (yscr - size.height) / 2;
        int x = (xscr - size.width) / 2;
	    f.setLocation(x+xoffset, y+yoffset); // "Coordonnées xy point en haut à gauche de fenêtre nom modèle ajustement et description 
		// f.setLocation(500, 60); // Test 2020.
        // System.out.println("Hauteur fenêtre Nom, Resultat ajustement et description modele (size.height): " + size.height + "  Largeur (size.width): " + size.width);
   		// System.out.println("Coordonnées xy point en haut à gauche de fenêtre nom modèle ajustement et description ligne 40 UtilCadre.java: x = " + x + " + xoffset = " + xoffset + "  y = " + y + " + yoffset = " + yoffset );  // Test 2020 
    }

    /*
     Positionne un  controle Frame en bas � droite
     @param JFrame le Frame � positionner
     */
    static public void bottomRight(JFrame f) 
    {
        Dimension screenSize = f.getToolkit().getScreenSize();
        Dimension size = f.getSize();
        int y = (screenSize.height - size.height);
        int x = (screenSize.width - size.width);
	    // System.out.println(" int x ligne 57 UtilCadre.java JFrame= " + x + " int y = " + y); // Test 2020
        f.setLocation(x, y);
    }

    static public void Size(JFrame f, int sx, int sy) 
    {
    	//Dimensionne le controle en % de taille de l'ecran
        //sx et sy doivent etre compris entre 10 et 100
        if ((sx > 100) || (sx < 10)) 
	{
        sx = 100;
        }
        if ((sy > 100) || (sy < 10)) 
        {
            sy = 100;
        }
        // System.out.println("Facteurs réduction fenêtre principale ou Question (JFrame) par rapport à max, sx et sy ligne 73 UtilCadre.java = " + sx + " " + sy); // Test 2020

        //Dimension screenSize = f.getToolkit().getScreenSize();
        //int y = (screenSize.height * sy/100);
        //int x = (screenSize.width * sx/100);
        //GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        
	GraphicsDevice gd = Global.CadreMain.getGraphicsConfiguration().getDevice();
        
     /* for (int aa=0; aa<gd.getConfigurations().length; aa++){
            System.out.println("BOUNDS : "+gd.getConfigurations()[aa].getBounds());
        }*/
        
		int x = gd.getDisplayMode().getWidth() * sx / 100;  // Test 2020 intialement /100 ->190. Affecte largeur cadre principal ou Select (alternative if (x > 1366) x = 1366;)
        int y = gd.getDisplayMode().getHeight() * sy / 100; // Test 2020 intialement /100 ->195. Affecte hauteur cadre principal
		// System.out.println("Taille fenêtre principale ou Select (JFrame) int x et y ligne 88 UtilCadre.java = " + x + " " + y + " sx = " + sx + " sy = " + sy); // Test 2020
        f.setSize(x, y);
		//System.out.println("f.setSize(x, y) ligne 88 UtilCadre.java = " + f); // Test 2020
    }

    static public void Size(JDialog f, int sx, int sy, boolean screen) 
    {
    	//Dimensionne le controle en % de taille du container parent
        //screen false ou de l'�cran screen true
        //sx et sy doivent �tre compris entre 10 et 100
        if ((sx > 100) || (sx < 10)) 
        {
            sx = 100;
        }
        if ((sy > 100) || (sy < 10)) 
        {
            sy = 100;
        }
        //System.out.println(" Facteurs réduction fenêtre par rapport à max JDialog, sx et sy ligne 105 UtilCadre.java = " + sx + " " + sy); // Test 2020

        Dimension dialogSize = null;
        GraphicsDevice gd = Global.CadreMain.getGraphicsConfiguration().getDevice();
        
        int xscr = gd.getDisplayMode().getWidth();  // Obtient la résolution en largeur de l'écran de l'ordi utilisé (idem supra)
        int yscr = gd.getDisplayMode().getHeight(); // Obtient la résolution en hauteur de l'écran (idem)
        // System.out.println("Résolution écran xscr et yscr ligne 112 JDialog UtilCadre.java = " + xscr + " int y = " + yscr); // Test 2020
        /* if (screen) 
        {dialogSize = f.getToolkit().getScreenSize()
        } else 
        {dialogSize = f.getParent().getSize();
        }*/
        // Dimension
        int y = (yscr * sy / 100); 
        int x = (yscr * sx / 100);
        //System.out.println("Facteurs réduction fenêtre principale ou Question (JDialog) par rapport à max, sx et sy ligne 119 UtilCadre.java = " + sx + " " + sy); // Test 2020
        f.setSize(x, y);
		//System.out.println("Taille fenêtre int x et y ligne 121 JDialog UtilCadre.java = " + x + " " + y); // Test 2020
    }

    static public void leftResize(JFrame f, int w, int h) 
    {
        Size(f, w, h);
        
        GraphicsDevice gd = Global.CadreMain.getGraphicsConfiguration().getDevice();
        int xoffset = gd.getConfigurations()[0].getBounds().x;
        int yoffset = gd.getConfigurations()[0].getBounds().y;
        // System.out.println("xoffset et yoffset ligne 130 leftResize UtilCadre.java = " +xoffset+" "+yoffset); // Test 2020
        f.setLocation(0 + xoffset, 0 + yoffset);
        f.repaint();
    }

    static public void rightResize(JFrame f, int w, int h) 
    {
        Size(f, w, h);

        //Dimension screenSize = f.getToolkit().getScreenSize();
        //f.setLocation(screenSize.width - size.width, 0);
        Dimension size = f.getSize();
        GraphicsDevice gd = Global.CadreMain.getGraphicsConfiguration().getDevice();
        int xoffset = gd.getConfigurations()[0].getBounds().x;
        int yoffset = gd.getConfigurations()[0].getBounds().y;
        //GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();

        int width = gd.getDisplayMode().getWidth();
        //f.setLocation((width - (size.width/2))+xoffset, 0+yoffset); // Coordonnées x,y du coins supérieur gauche de fenêtre graphiques. width = largeur écran ordi utilisé; size.width = largeur fenêtre graphique. Test  2020
		f.setLocation((width - size.width)+xoffset, 0+yoffset); // Coordonnées xy point en haut à gauche de fenêtre graphique lors de questions sur graphiques
		// f.setLocation(500,0) en remplacement provisoire de ligne au-dessus Test 2020 
		// System.out.println("Coordonnées xy point en haut à gauche de fenêtre graphique lors de questions sur graphiques ligne 151 rightResize UtilCadre.java: width = " + width + "   size.width = " + size.width + "  xoffset = "+ xoffset + "  yoffset = " + xoffset);  // Test 2020

     }

}
