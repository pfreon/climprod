/**
 * Titre : Climprod<p>
 *
 */
package fr.ird.climprod;

import javax.swing.JPanel;
import javax.swing.filechooser.FileFilter;
import java.io.*;
import javax.swing.JFileChooser;
import java.awt.*;
import java.awt.event.*;

public class PanelPlot extends JPanel
        implements ComponentListener, MouseMotionListener {

    public PanelPlot() {
        super();
        addComponentListener(this);
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseExited(MouseEvent e) {
                setCursor(Cursor.getDefaultCursor());
            }
        }
        );
        addMouseMotionListener(this);

        //posx=0;
        //posy=0;
    }

    @Override
    public void componentResized(ComponentEvent evt) {
        if (Gra != null) {
            Gra.Echelle(this);
        }
        update();
    }

    @Override
    public void componentMoved(ComponentEvent evt) {
    }

    @Override
    public void componentHidden(ComponentEvent evt) {
    }

    @Override
    public void componentShown(ComponentEvent evt) {
    }

    @Override
    public void mouseDragged(MouseEvent evt) {
        //int x=evt.getX();
        //int y=evt.getY();
    }

    @Override
    public void mouseMoved(MouseEvent evt) {
        if (Gra != null) {
            if (Gra.ShowEtiquettePointee) {
                // posx=evt.getX();
                // posy =evt.getY();
                //Point p= evt.getPoint();
                // System.out.println(evt.getX()+" "+p.getX()+" " +evt.getY()+" "+p.getY());
                setCursor(Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
                Gra.showEtiquettePointee(evt.getX(), evt.getY());
            } else {
                repaint();
            }
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        if (Gra != null) {
            Image im;
            Graphics gi, gi2;
            Dimension d = getSize();
            im = createImage(d.width, d.height);
            gi = im.getGraphics();
            gi2 = im.getGraphics();
            Gra.traceMap(gi);
            Gra.TraceAxes(gi2);
            super.paintComponent(g);
            g.drawImage(im, 0, 0, null);
            gi.dispose();
        }
    }

    public void update() {
        if (Gra != null) {
            repaint();
        }
    }

    /**
     * Enregistre l'image du graphique au format JPEG. Le nom donné au fichier
     * est celui du titre du graphique.
     */
    public void saveImage() {
        if (Gra == null) {
            return;
        }
        Dimension taille = this.getSize();

        Image picture = createImage(taille.width, taille.height);
        Graphics g = picture.getGraphics();
        Graphics g2 = picture.getGraphics();
        Gra.traceMap(g);
        Gra.TraceAxes(g2);
        //picture=createImage(getPreferredSize().width,getPreferredSize().height);
        String titre = Gra.getTitreGraphique();

        //
        String fileSep = System.getProperty("file.separator");
        if (fileSep.equals("/")) {
            fileSep = "//";
        }
        // String path=Configuration.workingDirectory+Configuration.fileSep+"ClimprodImages";
        String path = System.getProperty("user.home") + fileSep + ".climprod" + fileSep + "ClimprodImages";
        File fPath = new File(path);
        if (!fPath.exists()) {
            fPath.mkdir();
        }
        //titre=path+Configuration.FileSep+titre+".jpg";
        JFileChooser d = new JFileChooser();
        d.setDialogTitle("Save the plot as an image");
        d.setCurrentDirectory(new File(path));
        d.setSelectedFile(new File(titre + ".jpg"));
        d.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                return f.getName().toLowerCase().endsWith(".jpg") || f.isDirectory();
            }

            @Override
            public String getDescription() {
                return "Image jpg";
            }
        });

        int res = d.showSaveDialog(this);
        if (res == JFileChooser.APPROVE_OPTION) {
            titre = d.getSelectedFile().getAbsolutePath();
            if (titre == null) {
                titre = Gra.getTitreGraphique() + ".jpg";
            }
            ExporterImage expI = new ExporterImage(picture, titre);
            expI.write();
        }

        g.dispose();

    }

    /*
     * Retourne le graphique "attach�" au Panel.
     */
    public Plot getPlot() {
        return Gra;
    }

    /*
     * Affecte un graphique au panel.
     */
    public void setPlot(Plot plot) {
        if (plot == null) {
            return;
        }
        if (plot.getNbSerie() == 0) {
            MsgDialogBox msg = new MsgDialogBox(0, "Unable to display the plot (axe X or axe Y <= 0) for this model and data set.", 0, Global.CadreMain);
            return;
        }
        Gra = plot;
        try {
            Gra.Echelle(this);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private float cx;
    private float cy;
    private float decx;
    private float decy;
    //private float posx;
    //private float posy;
    private float minx, miny, maxx, maxy;
    private int nbseries;
    private Plot Gra;

}
