/**
 * Titre : Climprod<p>
 *
 */
package fr.ird.climprod;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.InputStream;

public class Cadre_Help extends JDialog {

    JScrollPane jScrollPane1 = new JScrollPane();
    JPanel jPanel1 = new JPanel();
    JButton cmdClose = new JButton();
    BorderLayout borderLayout1 = new BorderLayout();
    JTextArea jTextAreaHelp = new JTextArea();

    public Cadre_Help(Frame parent, String file) {
        super(parent);
        try {
            initWindow();
            String[] dataLine;
            InputStream inHelp = Cadre_Help.class.getResourceAsStream("resources/help/" + file);
            if (inHelp == null) {
                jTextAreaHelp.setText("Help file " + file + " not found.");
            } else {
                ReadFileText rft = new ReadFileText(inHelp);
                dataLine = rft.getLines();
                for (int i = 0; i < dataLine.length; i++) {
                    if (dataLine[i].trim().length() == 0) {
                        jTextAreaHelp.append("\n\n");
                    } else {
                        jTextAreaHelp.append(dataLine[i] + " ");
                    }
                }
            }
            jTextAreaHelp.setCaretPosition(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initWindow() throws Exception {

        this.getContentPane().add(jScrollPane1, BorderLayout.CENTER);
        this.getContentPane().add(jPanel1, BorderLayout.SOUTH);
        this.setModal(true);
        this.setSize(350, 700);
        this.setTitle("Climprod: Help ");

        jPanel1.setLayout(borderLayout1);
        jTextAreaHelp.setLineWrap(true);
        jTextAreaHelp.setWrapStyleWord(true);
        jTextAreaHelp.setEditable(false);
        jTextAreaHelp.setFont(new java.awt.Font("SansSerif", 0, 12));
        jScrollPane1.getViewport().add(jTextAreaHelp, null);

        cmdClose.setText("Close (ESC)");
        KeyStroke keyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
        int condition = JComponent.WHEN_IN_FOCUSED_WINDOW;
        InputMap inputMap = cmdClose.getInputMap(condition);
        ActionMap actionMap = cmdClose.getActionMap();
        inputMap.put(keyStroke, keyStroke.toString());
        actionMap.put(keyStroke.toString(), new AbstractAction() {            
            @Override
            public void actionPerformed(ActionEvent arg0) {
                cmdClose.doClick();
            }
        });
        jPanel1.add(cmdClose, BorderLayout.CENTER);

        cmdClose.addActionListener(new java.awt.event.ActionListener() {

            public void actionPerformed(ActionEvent e) {
                cancel();
            }
        });

    }

    void cancel() {
        this.dispose();
    }

}
