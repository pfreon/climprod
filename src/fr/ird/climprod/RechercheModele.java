
/**
 * Titre : Climprod<p>
 */
package fr.ird.climprod;

import java.util.StringTokenizer;
import java.util.Hashtable;
import java.util.HashMap;
import java.io.InputStream;

public class RechercheModele {		       //Variables dans fichier ListeModele.csv:
    private static int nbData=0;
    private static int[] ordre;                //Ordre de sélection des modèles lors de la recherche
    private static String [] equation;         //Equation litterale
    private static String [] help;             //Fichier d'aide associé
    private static int [] type;                //Type de modèle: Effort = 1 Climat = 2 Mixte = 3
    private static int [] affichage;           //Ordre d'affichage dans la liste
    private static String [] infl_Env;         //Influence de l'environnement
    private static int [] nbre_param;          //Nbre de paramètre du modèle
    private static int [] forme1;              //Forme équation sur param 1
    private static int [] forme2;              //Forme équation sur param 2
    private static boolean[] teste;            //Vrai si modèle testé.
    private static String[] listeModeleE;
    private static String[] listeModeleV;
    private static String[] listeModeleEVAbundance;
    private static String[] listeModeleEVCatchability;
    private static String[] listeModeleEVBoth;
    //private static Hashtable htNum=new Hashtable();

    private static int numEnCours;
    private static int indexEnCours;
    private static boolean foundModel=false;

    final static int lineaire=1;
    final static int exponentiel=2;
    final static int general=3;
    final static int power=4;
    final static int quadratique=5;
    final static int exponentiel_additif=6;

    final static int abondance=1;
    final static int prise=2;
    final static int both=3;

    final static int effort=1;
    final static int climat=2;
    final static int mixte=3;

 /*
  Assure la lecture du fichier des questions et initialise l'ensemble des variables.
  @param un String repr�sentant le nom "compl�t" du fichier listemodele.cvs.
*/

  public static void initScript(InputStream file) //Lecture du fichier listeModeles.csv qui contient tous les modèles proposés par CLIMPROD
 //  public static void initScript(String file)
  {
      try
      {
        HashMap<String, String> htTemp=new HashMap<String, String>();
        boolean doublon=false;
        String[] dataLine;

        ReadFileText rft=new ReadFileText(file);
        dataLine=rft.getLines();
        nbData=dataLine.length;
		//System.out.println("nbData = " + nbData);
        String[] mE=new String[nbData];
        String[] mV=new String[nbData];
        String[] mEVA=new String[nbData];
        String[] mEVC=new String[nbData];
        String[] mEVB=new String[nbData];
        int ie=0,iv=0,ieva=0,ievc=0,ievb=0;
        equation=new String[nbData];
        help=new String[nbData];
        ordre=new int[nbData];
        type=new int[nbData];
        affichage=new int[nbData];

        infl_Env=new String[nbData];
        nbre_param=new int[nbData];
        teste=new boolean[nbData];
        forme1=new int[nbData];
        forme2=new int[nbData];
        for (int i=0;i<dataLine.length;i++)
        {
             teste[i]=false;
             StringTokenizer d=new StringTokenizer (dataLine[i],";");
             int num=Integer.parseInt(d.nextToken().trim());
             ordre[num]=i;
             //htNum.put(d.nextToken().trim(),Integer.toString(i));
             type[i]=Integer.parseInt(d.nextToken().trim());
             affichage[i]=Integer.parseInt(d.nextToken().trim());
             equation[i]=(d.nextToken().trim());
             doublon=true;  //pour prendre en compte les �quations en doubles
             if(htTemp.get(equation[i]) ==null)
             {
                  htTemp.put(equation[i],Integer.toString(i));
                  doublon=false;
             }
             nbre_param[i]=Integer.parseInt(d.nextToken().trim());
             help[i]=(d.nextToken().trim().toUpperCase());
             forme1[i]=Integer.parseInt(d.nextToken().trim());
             forme2[i]=Integer.parseInt(d.nextToken().trim());
             int index=Integer.parseInt(d.nextToken().trim());
             //String[] influence={"","abundance","catchability","both"};
             infl_Env[i]=Global.influenceEnv[index];
             if(!doublon)
             {
             switch (index)
             {
                case 0:
                    if( type[i]==1)
                    {
                      mE[ie]=equation[i]; // Modèles simples Effort seul
                      ie=ie+1;
                    }
                    else
                    {
                      mV[iv]=equation[i]; // Modèles simples Environnement seul
                      iv=iv+1;
                    }
                    break;
                case 1:
                    mEVC[ievc]=equation[i]; // Modèles mixte Effort + Environnement sur Capturabilité
                    ievc=ievc+1;
                    break;
                case 2:
                    mEVA[ieva]=equation[i]; // Modèles mixte Effort + Environnement sur Abondance
                    ieva=ieva+1;
                    break;
                case 3:
                    mEVB[ievb]=equation[i]; // Modèles mixte Effort + Environnement sur les deux (both)
                    ievb=ievb+1;
                    break;
             }
            }
            listeModeleE=new String[ie];
            listeModeleV=new String[iv];
            listeModeleEVAbundance=new String[ieva];
            listeModeleEVCatchability=new String[ievc];
            listeModeleEVBoth=new String[ievb];
            for(int c=0;c<ie;c++)
                listeModeleE[c]=mE[c];
            for(int c=0;c<iv;c++)
                listeModeleV[c]=mV[c];
            for(int c=0;c<ieva;c++)
                listeModeleEVAbundance[c]=mEVA[c];
            for(int c=0;c<ievc;c++)
                listeModeleEVCatchability[c]=mEVC[c];
            for(int c=0;c<ievb;c++)
                listeModeleEVBoth[c]=mEVB[c];
			//System.out.println(type[i]+"  "+affichage[i]+" "+equation[i]+" "+nbre_param[i]+"  "+help[i]+"  "+infl_Env[i]);
        }
      }
      catch(Exception e)
      {
         MsgDialogBox msg=new MsgDialogBox(0,"Invalid list model file: "+e.getMessage(),0, Global.CadreMain);
      }
}

/*
  Sélectionne un modèle en fonction des parametres connus (Propose en fonction de l'état du
processus un modèle s'il existe)
*/
public static void select(){
int  newIndex=-1;
int i=0;
foundModel=false;
			//System.out.println("On recherche d'abord un modèle Global.relationCPU_E = " + Global.relationCPU_E + " Global.relationCPU_V = " + Global.relationCPU_V );
   if(Global.relationCPU_E !=2 && Global.relationCPU_E !=6 && Global.cpue_sous_sur_production==1 && Global.relationCPU_V != 0)
       {
       Global.relationCPU_E = 7; // empêche de trouver un modèle si cpue_sous_sur_production lorsque surexploition et que CPUE=f(E) non-exponentielle dans case 5: et 8: du fichier TexteRegles.java
		    //System.out.println("Dans RechercheModele.java premier if, Global.relationCPU_E = "+Global.relationCPU_E);
       }
     if(Global.relationCPU_E ==2 && Global.cpue_sous_sur_production ==1 && Global.relationCPU_V !=0)
       {
       Global.relationCPU_E = 6; // force modèle exponentiel additif dans case 5: et 8: du fichier TexteRegles.java
		    //System.out.println("Dans RechercheModele.java second if, Global.relationCPU_E = "+Global.relationCPU_E);
       }
do
{
        int j=ordre[i];
       //System.out.println(type[j]+"  "+Global.typeModele+"***"+forme1[j]+"  "+Global.relationCPU_E+"***"+forme2[j]+"  "+Global.relationCPU_V+"***"+infl_Env[j]+"  "+(Global.environmental_influence));
       //System.out.println(forme1[j]+"  "+Global.relationCPU_E);
       //System.out.println(forme2[j]+"  "+Global.relationCPU_V);
       //System.out.println(infl_Env[j]+"  "+(Global.environmental_influence));
       //System.out.println("************************");
        if(teste[j]==false && type[j]==Global.typeModele && forme1[j]==Global.relationCPU_E && forme2[j]==Global.relationCPU_V && infl_Env[j].equals(Global.environmental_influence))
        {
              indexEnCours=j;//i;
              foundModel=true;
             // System.out.println("Equ "+equation[j]);

        }
        i=i+1;
       //System.out.println(foundModel+"  "+ i+"  "+nbData);
}
while(foundModel==false && i<nbData);
//System.out.println("On a trouv� un mod�le dont CPUE=f(E) est " + RechercheModele.getCpue_ERelation()+" et CPUE=f(V) est " +RechercheModele.getCpue_VRelation() );
}

/*
  S�lectionne le mod�le correspondant � l'�quation pass�e en param�tre
  @param un String L'�quation du mod�le recherch�
*/
public static void select(String formule){
  foundModel=false;
//System.out.println("On recherche un mod�le " + Global.relationCPU_E+" " +Global.relationCPU_V );
  for (int i=0;i<nbData;i++)
  {
    if(formule.equals(equation[i]))
    {
          indexEnCours=i;
          foundModel=true;
          return;
    }
  }

}


/*
  Donne la forme de l'�quation
  @return : String la forme litrerale de la relation.
*/
public static String getEquation(){
      if(foundModel)
          return equation[indexEnCours];
      else
          return "No selected model";

}

/*
  Donne le rang d'affichage du mod�le
  @return : int la forme litrerale de la relation.
*/
public static int getNumero(){
      if(foundModel)
          return affichage[indexEnCours];
      else
          return -1;

}


/*
  Donne la forme de la relation entre la CPUE et V
  @return : String la forme litrerale de la relation.
*/

public static String getCpue_VRelation(){
      if(foundModel)
          switch(forme2[indexEnCours])
          {
                 case lineaire:
                    return "linear";
                 case general:
                    return "power";
                 case power:
                    return "power";
                 case quadratique:
                    return "dome-shaped";
                  default:
                    return "";
          }
      else
          return "No selected model";

}
/*
  Donne la forme de la relation entre la CPUE et V
  @return : int l'entier repr�sentant la forme de la relation.
*/

public static int  getNumCpue_VRelation(){
      if(foundModel)
          return(forme2[indexEnCours]);
       else
          return -1;

}
/*
Indique si un mod�le est s�lectionn�
@return boolean true si un mod�le est s�lectionn�
*/
public static boolean modelIsSelected(){
      return foundModel;
}

/*
  Donne la forme de la relation entre la CPUE et E
  @return : String la forme litterale de la relation.
*/

public static String getCpue_ERelation(){
      if(foundModel)
          switch(forme1[indexEnCours])
          {
                 case lineaire:
                    return "linear (Graham-Shaefer)";
                 case general:
                    return "generalized (Pella & Tomlinson)";
                 case exponentiel:
                    return "exponential (Fox)";

                 default:
                    return "";
          }
      else
          return "No selected model";

}

/*
  Donne la forme de la relation entre la CPUE et E
  @return : int l'entier repr�sentant la forme de la relation.
*/

public static int getNumCpue_ERelation(){
      if(foundModel)
          return(forme1[indexEnCours]);
       else
          return -1;

}

public static int getType(){
  if(foundModel)
          return type[indexEnCours];
      else
          return -1;
}

public static String getModelInformation(){
  if(foundModel)
          return help[indexEnCours];
      else
          return "";
}

public static void reset(){
    foundModel=false;
    for(int i=0;i<nbData;i++)
          teste[i]=false;
}

/*
    Donne la liste des  mod�les correspondant au type pass� en param�tre
    @param  un entier typeModele (0:cpue=f(e),1:cpue=f(v),2:cpue=f(e,v) abondance,3:cpue=f(e,v) capturabilité,4:cpue=f(e,v) les deux
    @return un String[] la liste des mod�les ou null ;
*/

public static String[] getlisteModele(int typeModele){
  switch (typeModele)
  {
      case 0:
          return listeModeleE;
      case 1:
          return listeModeleV;
      case 2:
          return listeModeleEVAbundance;
      case 3:
          return listeModeleEVCatchability;
      case 4:
          return listeModeleEVBoth;
      default:
          return null;
   }
}

/*
 Donne l'influence de l'environnement pout le modele s�lectionn�
 @return un String ou null si pas de mod�le s�lectionn�.
*/
public static String getInfluenceEnvironment(){
 if(foundModel)
          return infl_Env[indexEnCours];
      else
          return null;
}
}