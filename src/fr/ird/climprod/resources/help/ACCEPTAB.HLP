Is this an acceptable model?

These two graphs present pseudo three-dimensional relationships between
the two independent variables fishing effort (E) and environment (V), and:

-the production (Y) on the upper panel;
-the catch per unit of effort (CPUE) on lower panel.

The two lines of each graph correspond to minimum and maximum observed
values of V. Note that from these graphs it is not possible to estimate
the goodness of the fit. If your fishery is on a non-equilibrium situation
(transitional state) with respect to E (several exploited year classes and
an increasing effort, for instance) and/or with respect to V (influence of
environment over several years) some points may be located outside the
two lines even in the case of a good fit. Look at the next graphs and
next statistical results for goodness-of-fit estimation.

In the case of the additive model: CPUE = a exp(b E) + c V + d, surprising
results may be observed, such as an exponential growth of the production
when effort is increasing, which is not a realistic predictive model 
(nevertheless such a model may be used to describe the historical data).

Answer YES if the general shape of the function corresponds to a reasonable
modelization in your case.
