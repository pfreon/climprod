MODEL #32 CPUE = a V^(b + c) + d V^(2b) E

This is a surplus production model combining: on the one hand the
linear model (Graham-Schaefer model) between the CPUE and the fishing
effort E, and on the other hand a power relationship between the CPUE
and the environmental variable V. This latter variable is supposed to
influence both the catchability coefficient and the abundance of
the stock (surplus production) through a power relationship. Because
the same model with a linear relationship between the CPUE and V (instead
of power one) would result in an overparameterized model, this model can
be also used as a proxy for this case due to the flexibility of the power
relationship.

Please note that theoretically such an equation can be used even when
the influences of the environment on abundance and catchability have
opposite signs. In practice, owing to the transition prediction
approach and the simplifications retained in the formula, this
particular case cannot be reasonably modelized using CLIMPROD.
In case of identical signs of environmental influences, the most 
favorable cases are obtained when the weighting factors are the 
same for environmental influence on abundance and on catchability.
This condition is fulfiled when the environmental influence on
abundance concerns all the exploited year-classes. In such cases with n
exploited year classes and a recruitment at age tr, the answers given 
to the questions "Age at the beginning of environmental influence?"
and "Age at the end of environmental influence?" must be tr and tr+n-1 
respectively.

After fitting the model, if the coefficient b is not significantly
different from zero, this suggests that the influence of V could be
only on the abundance. If such is your belief, you can try to fit
the model: 

   #10 CPUE = a V^b + c E.

If the coefficient c is not significantly different from zero, this
suggests that the influence of V could be only on the catchability
coefficient. If such is your belief, you can try to fit the model:

   #24 CPUE = a V^b + c V^(2b) E.

These shifts of model can be done by clicking on "Modelization" and
then on "Fit a model directly" in the main menu of CLIMPROD. 
