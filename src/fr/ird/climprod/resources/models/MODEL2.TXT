MODEL #2 CPUE = (a + bE)^(1/(c-1))

This is the conventional generalized surplus production model, usually
named "Pella and Tomlinson model", in its simplified version due to the
re-parameterization of Fox (1975). Because CLIMPROD makes use of the Fox's 
equilibrium approximation approach for transitional states by weighting the
past fishing efforts, this model and its fitting are strictly equivalent
to those of the PRODFIT model Fox (1975). It is more flexible than the
linear or exponential models but presents an additional parameter (c).

If c = 2, this model is strictly identical to the linear Schaefer model:

   #0 CPUE = a + b E. 

Thus, after fitting this model, if 1.6 < c < 2.5, the CLIMPROD expert-
system retains the Schaefer model which presents the advantage of more
degrees of freedom.

If c = 1, this model is strictly identical to the exponential Fox model:

   #1 CPUE = a exp(b E). 

Thus, after fitting this model, if 0.6 < c < 1.4, the CLIMPROD expert-
system retains the Fox model which presents the advantage of more degrees
of freedom.

If you are presently using the mode "Fit a model directly" of CLIMPROD, it
is recommended that you use the same shift of model and compare the relevance
of the shift by using the AIC and BIC results. The model with the lowest
criterion value is the most suitable.

This model does not take into account any environmental influence on the
production (white noise).

Reference:
FOX, W:W., 1975.- Fitting the generalized stock production model by least squares
and eqUllibnum approximation. Fish. Bull. (U.s.), 73 (1): 23-36.
