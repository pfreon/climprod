MODEL #15 CPUE = a exp(bE) + cV + d

This is a surplus production model combining, in an additive way, on the one 
hand an exponential relationship between the CPUE and the fishing effort 
E, and on the other hand a linear relationship between the CPUE and the
environmental variable V. This latter variable is supposed to only influence
the stock abundance (surplus production) and not the catchability 
coefficient.

It is suggested to use it when the user suspects that at high levels of
exploitation the stock is still able to present a high variability in
the CPUE according to fluctuations of V.

Owing to the additive combination mentioned above, in some instances the
model having the best fit can show an abnormal behaviour: when the
fishing effort E increases, the resulting production (Y = CPUE * E) may
increase exponentialy and MSY & MSE values tend to infinity (plot not available). 

In such a case, the solution is to use a regression under constraint on 
parameter b (option not available in this CLIMPROD  version) or to use 
one of the following multiplicative models:

   #13 CPUE = a V exp(b E) 

   #14 CPUE = (a + b V) exp(c E).

If you are presently using the mode "Select the appropriate model and fit it"
of CLIMPROD, you can do these shifts of model by clicking on "Modelization" and
then on "Fit a model directly" in the main menu of CLIMPROD. 
