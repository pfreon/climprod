MODEL #12 CPUE = aV exp(bV) + cE

This is a surplus production model combining on the one hand a linear
relationship between the CPUE and the fishing effort E (Graham-Schaefer model), 
and on the other hand a non-linear relationship between the CPUE and the environmental 
variable V. The shape of this non-linear relationship is quite flexible (it is in fact 
the same equation as the one used by Ricker to describe stock-recruitment relationships).

Variable V is supposed to only influence the abundance of the
stock (surplus production) and not the catchability coefficient.

This kind of non-monotonic equation is useful when the relationship between 
CPUE and V is shaped (optimal central value). The graph MSY versus V must 
OBVIOUSLY be parabolic. If not, the data-set does not justify such a model
and it is recommanded to use a model with less parameters and more degree
of freedom as for instance models:

   #8 CPUE = a+bV+cE or 

   #10 CPUE = aV^b+cE. 

Moreover, when using these models in a non-equilibrium condition for the variable
V (transitional state*) the user must carefully check the temporal stability
of the variable V on the time-series graph. In particular, it would be
nonsense to allow the program to average two V values with on located one
on the left-hand side of the parabola and the other on the right-hand side.

If the fit is not good, it is suggested to fit the following model  where the
relationship between CPUE and V is quadratic (parabola): 

      #11 CPUE = a + bV + cV^2 + dE.

If you are presently using the mode "Select the appropriate model and fit it"
of CLIMPROD, you can do these shifts of model by clicking on "Modelization" and
then on "Fit a model directly" in the main menu of CLIMPROD. 

Then it is highly recommanded to assess the relevance of this shift to model #11
by using the AIC and BIC results available in CLIMPROD. The model with the lowest
criterion value is the most suitable.

*That is when V is averaged, which is the case when the age at beginning
and/or the end of environmental influence is different from 1.
