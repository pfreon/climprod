MODEL #22 CPUE = a + bV + c(a+bV)^2 E 

This is a surplus production model combining on the one hand a linear
relationship between the CPUE and the fishing effort E, and on the other
hand a linear relationship between the CPUE and the environmental variable V.
This latter variable is supposed to only influence the catchability
coefficient and not the stock abundance (surplus production). 

After fitting this model, if the value of the parameter a is not 
significantly different from zero, it is suggested to try the 
following model which has no constant term but more degrees of freedom: 

   #23 CPUE = a V + b V^2 E.

If you are presently using the mode "Select the appropriate model and fit it"
of CLIMPROD, you can do this shift of model by clicking on "Modelization" and
then on "Fit a model directly" in the main menu of CLIMPROD.

Then it is highly recommanded to assess the relevance of this shift
of model by using the AIC and BIC results available in CLIMPROD. The
model with the lowest criterion value is the most suitable.
