MODEL #28 CPUE = (a + b V) exp(c (a + bV) E)

This is a surplus production model combining on the one hand an 
exponential relationship (Fox model) between the CPUE and the fishing
effort E and on the other hand a linear relationship between the CPUE 
and the environmental variable V. This latter variable is supposed to
only influence the catchability coefficient and not the stock abundance 
(surplus production).

After fitting this model, if the value of the parameter a is not
significantly different from zero, it is suggested to try the
following model which has no constant term but more degrees of
freedom:

   #27 CPUE = a V exp(b V E).

Then it is highly recommanded to assess the relevance of this shift of
model by using the AIC and BIC results available in CLIMPROD. The model
with the lowest criterion value is the most suitable.
