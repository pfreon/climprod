
/**
 * Titre : Climprod<p>
 * Permet de presenter au sein d'une meme fenetre plusieurs graphiques. 
 * Utilise lors des premiers graphiques de time series et bivariate plots
 * presentes au debut des questions du menu "Select the appropriate model and fit it".
 */
package fr.ird.climprod;

import javax.swing.JFrame;
import java.awt.*;

public class Cadre_SplitPlot extends JFrame {
  private GridLayout gridLayout1 = new GridLayout();
  private Plot[] plots;
  private PanelPlot[] panPlots;
  private boolean  vertical=true;
  private int horizontal=0;


  public Cadre_SplitPlot(Plot [] p) {
    try {
      plots=p;
      initWindow();
     // UtilCadre.Size(this,70,70);
    //  UtilCadre.bottomRight(this);
    }
    catch(Exception e) {
      e.printStackTrace();
    }
  }

  public Cadre_SplitPlot(Plot  p) {
    try {
      plots=new Plot[1];
      plots[0]=p;
      initWindow();
     // UtilCadre.Size(this,70,70);
     // UtilCadre.bottomRight(this);
    }
    catch(Exception e) {
      e.printStackTrace();
    }
  }

   public Cadre_SplitPlot(Plot [] p,boolean orientationVerticale) {
    try {
      plots=p;
      vertical=orientationVerticale;
      initWindow();
     // UtilCadre.Size(this,70,70);
     // UtilCadre.bottomRight(this);
    }
    catch(Exception e) {
      e.printStackTrace();
    }
  }




  private void initWindow() throws Exception {
    this.setIconImage(Toolkit.getDefaultToolkit().createImage(Cadre_SplitPlot.class.getResource("resources/images/Climprod.jpg")));

    int nbLig=0;
    int nbCol=0;
    int nbCase=plots.length;
    if(nbCase==1)
    {
        nbLig=1;
        nbCol=1;
    }
    else if(nbCase%2==0 && nbCase!=2)
    {
        nbLig=nbCase/2;
        nbCol=nbLig;
    }
    else if(nbCase>4)
    {
        if(nbCase%3==0)
        {
          nbLig=3;
          nbCol=nbCase/3;
        }
        else
        {
          nbLig=(nbCase+1)/2;
          nbCol=nbLig-1;
        }
    }
    else
    {
        if(vertical)
        {
          nbCol=1;
          nbLig=nbCase;
        }
        else
        {
          nbCol=nbCase;
          nbLig=1;
        }
    }
    gridLayout1.setRows(nbLig);
    gridLayout1.setColumns(nbCol);
    gridLayout1.setHgap(3);
    gridLayout1.setVgap(3);
    this.getContentPane().setLayout(gridLayout1);
    panPlots=new PanelPlot[nbCase];
    for (int i=0;i<nbCase;i++)
    {
          panPlots[i]=new PanelPlot();
          panPlots[i].setPlot(plots[i]);
          this.getContentPane().add(panPlots[i]);
    }
    UtilCadre.Size(this,70,70);
  }
}