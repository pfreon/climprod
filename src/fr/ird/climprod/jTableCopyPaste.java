
/**
 * Titre : GenLev<p>
 * Cette classe dote la class JTables de fonctionnalit� de copier-coller.
 * Le format de donn�es copi�es dans le presse papier est compatible avec le format Excel.
 * Source : d'apr�s JavaWorld rubrique type 77.
 */
package fr.ird.climprod;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.awt.datatransfer.*;
import java.util.*;

public class jTableCopyPaste implements ActionListener
{
  private Clipboard system;
  private JTable jTable1 ;
  private boolean copyAutorise;
  private boolean pasteAutorise;


public jTableCopyPaste(JTable myJTable,boolean bcopy,boolean bpaste)
 {
   jTable1 = myJTable;
   copyAutorise=bcopy;
   pasteAutorise=bpaste;
    // Combinaison de touche ctrl_c pour la copie.
   KeyStroke copy = KeyStroke.getKeyStroke(KeyEvent.VK_C,ActionEvent.CTRL_MASK,false);
   jTable1.registerKeyboardAction(this,"Copy",copy,JComponent.WHEN_FOCUSED);

   // Combinaison de touche ctrl_v pour le coller
   KeyStroke paste = KeyStroke.getKeyStroke(KeyEvent.VK_V,ActionEvent.CTRL_MASK,false);
   jTable1.registerKeyboardAction(this,"Paste",paste,JComponent.WHEN_FOCUSED);

   system = Toolkit.getDefaultToolkit().getSystemClipboard();

}

/**
* Retourne la table sur laquelle se font les op�rations.
*/
public JTable getJTable() {
  return jTable1;
}

/**
* D�termine  la table sur laquelle se font les op�rations.
*/
public void setJTable(JTable jTable1)
{
  this.jTable1=jTable1;
}

/**
* Gestion des op�rations de copier coller.
*/
public void actionPerformed(ActionEvent e)
{
if (e.getActionCommand().compareTo("Copy")==0 && copyAutorise)
{
  StringBuffer sbf=new StringBuffer();

  // S�lection de blocs continus.
  // D�sactiv�
  int numcols=jTable1.getSelectedColumnCount();
  int numrows=jTable1.getSelectedRowCount();
  int[] rowsselected=jTable1.getSelectedRows();
  int[] colsselected=jTable1.getSelectedColumns();
  /*
  if (!((numrows-1==rowsselected[rowsselected.length-1]-rowsselected[0] &&
    numrows==rowsselected.length) &&

    (numcols-1==colsselected[colsselected.length-1]-colsselected[0] &&
    numcols==colsselected.length)))
  {
    JOptionPane.showMessageDialog(null, "Invalid Copy Selection",
    "Selection invalide",
    JOptionPane.ERROR_MESSAGE);

    return;
  }
  */
  if(rowsselected[0]==0)  //si s�lection commence � la premi�re ligne on ajoute
  {                       //les t�tes de colonne.
    for (int j=0;j<numcols;j++)
    {
      sbf.append(jTable1.getColumnName(colsselected[j]));
      if (j<numcols-1) sbf.append("\t");
    }
    sbf.append("\n");
  }
  for (int i=0;i<numrows;i++)
  {
    for (int j=0;j<numcols;j++)
    {

      sbf.append(jTable1.getValueAt(rowsselected[i],colsselected[j]));
      if (j<numcols-1) sbf.append("\t");
    }
    sbf.append("\n");
  }

  StringSelection stsel = new StringSelection(sbf.toString());
  system = Toolkit.getDefaultToolkit().getSystemClipboard();
  system.setContents(stsel,stsel);
}


if (e.getActionCommand().compareTo("Paste")==0 && pasteAutorise)
{
  int startRow=(jTable1.getSelectedRows())[0];
  int startCol=(jTable1.getSelectedColumns())[0];
  try
  {
    String tout$= (String)(system.getContents(this).getTransferData(DataFlavor.stringFlavor));
    StringTokenizer st1=new StringTokenizer(tout$,"\n");
    for(int i=0;st1.hasMoreTokens();i++)
    {

      String ligne$=st1.nextToken();
      UtilStringTokenizer ust=new UtilStringTokenizer (ligne$,"\t"," ");
      ligne$=ust.getNewString();
      StringTokenizer st2=new StringTokenizer(ligne$,"\t");
      for(int j=0;st2.hasMoreTokens();j++)
      {
        String valeur$=(String)st2.nextToken();
        if (startRow+i< jTable1.getRowCount() &&
          startCol+j< jTable1.getColumnCount())
            jTable1.setValueAt(valeur$,startRow+i,startCol+j);

      }
    }
  }
  catch(Exception ex){ex.printStackTrace();}

  }
}
}
