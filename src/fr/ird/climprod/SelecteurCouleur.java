
/**
 * Titre : Climprod<p>
 */
package fr.ird.climprod;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.border.Border;

public class SelecteurCouleur extends JPanel
	//implements ComponentListener, MouseMotionListener
{

  private String[] Couleur$;
  private Color[] cpd;
  private Color selectedColor;
   JPanel pg;
  JComboBox<String> cboColor = new JComboBox<String>();
  JLabel lblColor = new JLabel();
  GridBagLayout gridBagLayout1 = new GridBagLayout();

  public SelecteurCouleur() {
    super();
    //addComponentListener(this);



    try {
      jbInit();
    }
    catch(Exception e) {
      e.printStackTrace();
    }
  }

  private void jbInit() throws Exception {

    cpd=new Color[7];
    cpd[0]=Color.black;
    cpd[1]=Color.red;
    cpd[2]=Color.green;
    cpd[3]=Color.yellow;
    cpd[4]=Color.blue;
    cpd[5]=Color.magenta;
    cpd[6]=Color.cyan;
    Couleur$=new String[7];
    Couleur$[0]="black";
    Couleur$[1]="red";
    Couleur$[2]="green";
    Couleur$[3]="yellow";
    Couleur$[4]="blue";
    Couleur$[5]="magenta";
    Couleur$[6]="cyan";
    for (int i=0;i<7;i++)
          cboColor.addItem(Couleur$[i]);

    this.setLayout(gridBagLayout1);
    lblColor.setBackground(Color.red);
    lblColor.setBorder(BorderFactory.createEtchedBorder());
    lblColor.setOpaque(true);
    //lblColor.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED,Color.red,Color.red,new Color(178, 0, 0),new Color(124, 0, 0)),BorderFactory.createEmptyBorder(0,5,0,0)));
    lblColor.setText("");

    lblColor.addMouseListener(new java.awt.event.MouseAdapter() {



      public void mouseClicked(MouseEvent e) {
        lblColor_mouseClicked(e);
      }
    });
    cboColor.addActionListener(new java.awt.event.ActionListener() {

      public void actionPerformed(ActionEvent e) {
        cboColor_actionPerformed(e);
      }
    });


    this.add(lblColor, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
            ,GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(11, 4, 13, 5), 121, 23));
    this.add(cboColor, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0
            ,GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, new Insets(11, 4, 13, 5), -3, 0));

    //selectedItem=0;
    cboColor.setSelectedIndex(0);
  }

  void cboColor_actionPerformed(ActionEvent e) {
        int i=cboColor.getSelectedIndex();
        if(i!=-1)
        {
          selectedColor=cpd[i];
          lblColor.setBackground(selectedColor);

        }
        cboColor.setVisible(false);
        lblColor.setVisible(true);
  }

  void lblColor_keyPressed(KeyEvent e) {

        cboColor.setVisible(true);
        lblColor.setVisible(false);
  }
/*
    Donne la couleur s�lectionn�e
    (Pas tr�s joli mais marche avec patteraction)
    @return Color le couleur s�lectionn�e
*/
  public Color getSelectedColor(){
     int i=cboColor.getSelectedIndex();
     if(i!=-1)
         selectedColor=cpd[i];
     return selectedColor;
  }

/*
    Selectionne une couleur
    @param Color la nouvelle couleur
    (Si la couleur ne correspond pas rien ne se passe)
*/
  public void setSelectedColor(Color c){
     String c$=c.toString();
     for(int i=0;i<cpd.length;i++)
     {
        if(c$.equals(cpd[i].toString()))
        {
          cboColor.setSelectedIndex(i);
         // System.out.println("seleceteur " + c$);
        }
    }
  }

/*
      Affecte un nouvelle liste de couleurs avec leur noms au controle
      Les 2 tableaux doivent avoir meme longueur
      @param color le tableau des couleurs, colorName le tableau des noms de couleurs
*/
  public void setColorList(Color [] color,String[] colorName){

      if(color.length!=colorName.length) return;

      cpd=new Color[color.length];
      Couleur$=new String[color.length];
      for(int i=0;i<color.length;i++)
      {
          cpd[i]=color[i];
          Couleur$[i]=colorName[i];
      }

  }







  void lblColor_mouseClicked(MouseEvent e) {

       cboColor.setVisible(true);
       lblColor.setVisible(false);

  }

  void cboColor_mouseClicked(MouseEvent e) {

  }

}