/**
 * Titre : Climprod<p>
 */
package fr.ird.climprod;

import java.util.StringTokenizer;
import java.util.Hashtable;
import java.util.HashMap;
import java.util.Vector;
import java.io.InputStream;

public class QuestionReponse {
	private static int FlagDoubleClick = 0;
    private static int nbData = 0;
    private static int[] rTrue;
    private static int[] rFalse;
    private static int[] regle;
    private static String[] script;
    private static String[] help;
    private static String[] name;
    private static String[][] item;
    private static HashMap<String, String> htNum = new HashMap<String, String>();
    private static HashMap<String, String> htName = new HashMap<String, String>();
    private static int[] type;
    private static int indexEnCours; //Index associé à la clé (Numéro de décision): index partant de 0 dans tableau Arbre-decision.csv
							         // Attention different de indexEnCours de TexteRegles.java.
    private static int numEnCours;                   //Numéro de décision
    private static String scriptEnCours;
    private static String[] itemEnCours;
    private static String helpEnCours;
    private static int[] selectedItem;
    private static Vector<Integer> raisonnement = new Vector<Integer>();
    //private static int etapeEnCours=0;

/*
Affiche avertissement sur usage double click. Addition 2020.
*/
public static void ClickWarning() { 
	MsgDialogBox msg;
        if (Global.double_click == null) Global.double_click = "ShowWarningDoubleClick";
	if (Global.double_click.compareTo("ShowWarningDoubleClick") != 0) 
		Global.dbl_click = 2; 
	else Global.dbl_click = 1; 
	if(Global.dbl_click == 1) 
		msg = new MsgDialogBox(0, "Please note that this version of CLIMPROD allows the use of the double click \non 'Yes', 'No', 'Don't know' and numbers to answer questions with no need\n to use the 'Next' button. But before doing this please make a simple click on \nthe selected answer button to make sure that you will not skip a message that \nmay pop up in the 'Message' box located at the bottom of the window. \n\nAlternatively you can still ignore the double click option and use the  'Next' button \nas in the earlier version of CLIMPROD.", 0, Global.CadreQuestion);
	}

    /*
     Assure la lecture du fichier des questions et initialise l'ensemble des variables de l'arbre de décision.
     @param un String repr�sentant le nom "compl�t" du fichier Arbre_decisions.cvs.
     */
    public static void initScript(InputStream file) // Lecture fichier Arbre_decisions.csv. Comment 2020
    {
        try {

            String[] dataLine;
            String tt;
            ReadFileText rft = new ReadFileText(file);
            dataLine = rft.getLines();   // Liste variables dans fichier Arbre_decisions.csv:
            nbData = dataLine.length;
            rTrue = new int[nbData];        // N°décision redirection si VRAI 
            rFalse = new int[nbData];       // N°décision redirection si FAUX  
            regle = new int[nbData];        // N° de règle appliquée
            script = new String[nbData];    // Question posée
            help = new String[nbData];      // Fichier d'aide .hlp
            name = new String[nbData];      // Variable associée à la question posée
            item = new String[nbData][];    // Différentes réponses numériques à questions sur âges (item [i][j])
            type = new int[nbData];         // Type de décision:
            		            	// 1= autres décisions que 2 ou 3 (validité données, identification type de modèle; 
					            	// 2=On cherche un modèle dans ListeModele.csv et on l'ajuste si on le trouve; 
						            // 3= relative à validation.
            selectedItem = new int[nbData]; 

            for (int i = 0; i < dataLine.length; i++) {
                StringTokenizer d = new StringTokenizer(dataLine[i], ";");
                tt = d.nextToken().trim();
                htNum.put(tt, Integer.toString(i));
                //htNum.put(d.nextToken().trim(),Integer.toString(i));
                rTrue[i] = Integer.parseInt(d.nextToken().trim());
                rFalse[i] = Integer.parseInt(d.nextToken().trim());
                regle[i] = Integer.parseInt(d.nextToken().trim());
                type[i] = Integer.parseInt(d.nextToken().trim());
                int k = d.countTokens();
                if (k != 0) {
                    name[i] = d.nextToken().trim();
                    htName.put(name[i], Integer.toString(i));
                    // Global.htVar.put(name[i],"-1");
                    script[i] = d.nextToken().trim();
                    help[i] = d.nextToken().trim().toUpperCase();

                    k = d.countTokens();
                    if (k != 0) {
                        item[i] = new String[k];
                        for (int j = 0; j < k; j++) {
                            item[i][j] = d.nextToken().trim();
                        }
                    } else {
                        k = 3;
                        item[i] = new String[3];
                        item[i][0] = "Yes";
                        item[i][1] = "No";
                        item[i][2] = "Don't know";
                    }
                } else {
                    name[i] = null;
                    script[i] = null;
                    help[i] = null;
                    item[i] = null;
                }
                /*System.out.println(i+"  " +tt+"  "+  regle[i]);
                 for(int l=0;l<k;l++)
                 System.out.print(item[i][l]+"  ");
                 System.out.println("******************");*/
            }
            activeDidacticiel(true);
        } catch (Exception e) {
            new MsgDialogBox(0, e.getMessage(), 0, Global.CadreMain);
        }
    }

    /*
     "Charge" la question en cours.
     */
    public static void loadQuestion() {
        //numEnCours=Integer.parseInt((String)htNum.get(number));
		if (FlagDoubleClick == 0) {
			//System.out.println("Flag1 ClickWarning in QR" + "FlagDoubleClick = " + FlagDoubleClick);
			QuestionReponse.ClickWarning();
			FlagDoubleClick = 1;
		}

        scriptEnCours = script[indexEnCours];
            if (script[indexEnCours] != null) {
                itemEnCours = new String[item[indexEnCours].length];
                for (int i = 0; i < itemEnCours.length; i++) {
                    itemEnCours[i] = item[indexEnCours][i];
                }
            } else {
                itemEnCours = null;
            }
         helpEnCours = help[indexEnCours];
    }


    /*
     Donne le num�ro de question en cours
     @return un entier le num�ro de question
     */
    public static int getNum() {
        return numEnCours;
    }


    /*
     Donne le numéro de règle  associé à la question.
     @return un entier le numéro de règle, ou -1 si pas de question associ�e.
     */
    public static int getNumRegle() {
      //System.out.println("Dans QuestionReponse.java numEnCours = " + numEnCours + " indexEnCours = " + indexEnCours);    	
        if (numEnCours >= 0) {
            return regle[indexEnCours];
        } else {
            return -1;
        }
    }


    /*
     Donne le script associé à la question.
     @return un String ou null si pas de script.
     */
    public static String getScript() {

        return scriptEnCours;
    }


    /*
     Donne les items réponse associés à la question.
     @return un String[], la liste des items ou null si d'items.
     */
    public static String[] getItem() {

        return itemEnCours;
    }


    /*
     Donne le nom du fichier d'aide associé à la question.
     @return un String ou null si pas de fichier.
     */
    public static String getHelp() {

        return helpEnCours;
    }


    /*
     Donne le numéro de l'item sélectionné en réponse à la question.
     @return un int ou -1 si pas de réponse valide.
     */
    public static int getReponse() {
            return selectedItem[indexEnCours];
    }


    /*
    Validate the answer number to the current question and the potential corresponding rule
    This method is used for questions which verify a rule directly without any item
    @param none
     */
    public static void setReponse() {
        boolean result = false;
        if (regle[indexEnCours] != -1) {
            result = TexteRegles.isTrue();
        }
        if (result) {
            selectedItem[indexEnCours] = 0;
        } else {
            selectedItem[indexEnCours] = 1;
        }
    }
    /*
    Validate the answer number to the current question and the potential corresponding rule
    @param int index which represents the selected answer item number to the question
     */
    public static void setReponse(int index, String reptxt, String questxt, String commentTxt) {
        MsgDialogBox msg; // Addition 2020.
        selectedItem[indexEnCours] = index;
        index = index + 1;
		//System.out.println("In QuestionReponse. java Answer to rule " + regle[indexEnCours] + " given to question " + numEnCours + " is  " + index);
        // build list questions for html
        //System.out.println("AAA question number : "+numEnCours);
        //System.out.println("AAA question : "+questxt);
        //System.out.println("BBB i guess answer is : "+reptxt);
        Global.questionDic.put(numEnCours, questxt); // Texte de la question.
        Global.answerDic.put(numEnCours, reptxt);    // Réponse à la question.
        //System.out.println("Dans QuestionReponse.java ligne 232,  numEnCours: " + numEnCours + " questxt: " + questxt + " reptxt: " + reptxt + " commentTxt: " + commentTxt);
        Global.warningDic.put(numEnCours, commentTxt);       
		//System.out.println("Dans QuestionReponse.java ligne 234, Global.warningDic: " + Global.warningDic + " commentTxt: " + commentTxt);

      try {
        switch (numEnCours) {
			case 0:
				System.out.println("Case 0 Global.double_click = " + Global.double_click);
				if(Global.double_click == "ShowWarningDoubleClick")
					throw new OnError("SPECIAL WARNING: \nPlease not that you can use the double click on 'Yes', 'No', 'Don't know'\nand other buttons to answer questions,\nbut before doing this please make a simple click on the button\nto make sure that you will not skip a message that\nmay pop up in the 'Message' box located at the bottom\nof the window.");
				break;
			case 106:
			    //System.out.println("Case 106 Global.double_click = " + Global.double_click);
                if (index == 1) Global.double_click = "ShowWarningDoubleClick"; // Boolean variable not use because string requested to read and write the configuration text file. Modif. 2020.
                else Global.double_click = "DontShowWarningDoubleClick";
                if(index == 3)
                throw new OnError("It is NOT compulsory to know the answer to this question,\nbut please have a look at the help file using the 'Help (H)' button\nby using the 'Next(N)' button. \nBy default CLIMPROD will consider that the answer is 'Yes'");
                break;	
            case 2:									// Suite à réorganisation ordre des questions posées. Modif. 2020.
                Global.changement_exploitation = index;
                if(Global.changement_exploitation==3)
                  throw new OnError("It is compulsory to know the answer to this question. \nPlease note that you can answer 'Yes' if the used \nfishing effort takes into account these changes. \nPlease have a look at the help file using the 'Help (H)' \nbutton if necessary.");
                break;
            case 3:                                 // Suite à réorganisation ordre des questions posées. Modif. 2020.
                Global.unite_standardisee = index;
                if(Global.unite_standardisee==3)
                  throw new OnError("It is compulsory to know the answer to this question. \nPlease have a look at the help file using the 'Help (H)' \nbutton if necessary.");
                break;
            case 4:
                Global.effet_delais_abundance_negligeable = index;
                if(Global.effet_delais_abundance_negligeable==3)
                  throw new OnError("It is compulsory to know the answer to this question. \nPlease have a look at the help file using the 'Help (H)' \nbutton if necessary.");
                break;
            case 5:
                Global.stock_unique = index;
                break;
            case 6:
                Global.metapopulation = index; // Avant stock_divise. Modif. 2020
                break;
            case 7:
                Global.sous_stock_isole = index;
                break;
            case 9:
                Global.under_and_over_exploited = index;
                break;
            case 10:
                Global.under_and_optimaly = index;
                if(Global.under_and_optimaly==3)
                  throw new OnError("It is compulsory to know the answer to this question. \nPlease have a look at the help file using the 'Help (H)' \nbutton if necessary.");
                break;
            case 11:
                Global.statistiques_anormales = index;
                if(Global.statistiques_anormales==3)
                  throw new OnError("It is NOT compulsory to know the answer to this question because next questions \nrelated to graphics presentation should substitute for it. But please have a look \nat the help file using the 'Help (H)' button if necessary before \nconfirming your answer by using the 'Next(N)' button.");
                break;
            case 12:
                Global.unstability = index;
                if(Global.unstability==3)
                  throw new OnError("It is compulsory to know the answer to this question. \nPlease have a look at the help file using the 'Help (H)' button if necessary.");
                break;
            case 13:
                Global.abnormal_points_dist = index;
                if(Global.abnormal_points_dist==3)
                  throw new OnError("It is compulsory to know the answer to this question. \nPlease have a look at the help file using the 'Help (H)' button if necessary.");
                break;
            case 14:
                Global.abnormal_points_scatt = index;
                if(Global.abnormal_points_scatt==3)
                  throw new OnError("It is compulsory to know the answer to this question. \nPlease have a look at the help file using the 'Help (H)' button if necessary.");
                break;
            case 15:
                Global.effort_increasing = index;
				if(Global.effort_increasing==3)
                  throw new OnError("It is NOT compulsory to know the answer to this question except if the answer to it is obvious. \nBut please have a look at the help file using the 'Help (H)' button if necessary \nbefore confirming your answer by using the 'Next(N)' button. \nBy default CLIMPROD will consider that the answer is 'No'");
                break;
            case 16:
                Global.independance = index;
                if(Global.independance==3)
                  throw new OnError("It is NOT compulsory to know the answer to this question except if the answer to it is obvious. \nBut please have a look at the help file using the 'Help (H)' button if necessary \nbefore confirming your answer by using the 'Next(N)' button. \nBy default CLIMPROD will consider that the answer is 'Yes'");
                break;
            case 17:
                Global.nb_classes_exploitees = index;
				if (Global.nb_classes_exploitees > 5 && Global.nb_classes_exploitees < 9) {
           		throw new OnError("WARNING! \nA stock with more than 5 significantly exploited year-classes is not ideal for applying \nsurplus production models that make use of the transition prediction approach (past-effort-averaging) of \nFox (1975) which constitues an improvent of Gulland (1961, 1969) methods. \nThis method is used in CLIMPROD to fit data on non-equilibrium conditions. It simulates the \nequilibrium by using a weighted average of fishing effort (E). \nIf interanual variability of E is too large, the results are likely to be uncertain. \nYou can still continue by clickin on the Next(N) button.");
        		}
        		if (Global.nb_classes_exploitees > 8) {
                     throw new OnError("WARNING! \nA stock with too many significantly exploited year-classes is not a situation compatible with the use of \nsurplus production models that make use of the transition prediction approach (past-effort-averaging) \nofFox (1975). This method is used in CLIMPROD to fit data on non-equilibrium conditions. It simulate the \nequilibrium by using a weighted average of fishing effort (E). \nIt is recommanded to stop here your research or revise your answer if necessary.");
        		}              Data.init_val();//Pour obtenir la relation en fct du d�calage
                break;
            case 18:
                Global.relationCPU_E = 0; //Initialisations pour prendre
                Global.relationCPU_V = 0;   //en compte le changement (si usage touche 'Previous')
                Global.recruitment_age = 0;    //  de la prépondérence
                Global.begin_influence_period = 0;  //de l'effort ou de l'environement
                Global.end_influence_period = 0;
                if (index == 1) { index = 2 ;}  // Modif 2020 pour faire en sorte que reponse "I don't know" branche sur prépondérance effort sans devoir tout modifier.  
                else{
                  if (index == 2) index = 1;
                }
                Global.envir_preponderant = index;
                if (index == 3) {
                    index = 1;
                    Global.typeModele = index;
                    throw new OnError("It is NOT compulsory to know the answer to this question except if the answer to it is obvious. \nBut please have a look at the help file using the 'Help (H)' button if necessary \nbefore confirming your answer by using the 'Next(N)' button. \nBy default CLIMPROD will consider that the answer is 'No'");
                }
                Global.typeModele = index;
                break;
            case 19:
                if(index==3) {
                  throw new OnError("It is compulsory to know the answer to this question. \nPlease have a look at the help file using the 'Help (H)' button if necessary.");
                }
                else Global.decreasing_relationship = index;
                break;
            case 20:  // Vide car traitement identique pour decisions 20 et 68.
            case 68:
                if (index == 1) {
                    Global.relationCPU_E = RechercheModele.lineaire;
                }if (index == 2) {
                    Global.relationCPU_E = RechercheModele.exponentiel;
                }  if (index == 3) {
                  index = 2;
                  throw new OnError("It is NOT compulsory to know the answer to this question except if the answer \nis obvious. By default CLIMPROD will consider that the answer is 'No'. \nBut please have a look at the help file using the 'Help (H)' button \nif necessary before confirming your answer by using the 'Next(N)' button. \nBy default CLIMPROD will consider that the answer is 'No'");
                }
                break;
            case 21:
            case 69:
                Global.pessimiste = index;
                break;
            case 22:
            case 70:
                Global.stock_deja_effondre = index;
                break;
            case 23:
            case 71:
                Global.lifespan = index;
                if (Global.lifespan < Global.nb_classes_exploitees) {
                   throw new OnError("There is a contradiction between your answer. \nYou declared a life span of " + Global.lifespan + " years and a number of exploited year-classes of " + Global.nb_classes_exploitees + " years. \nPlease revise your answer(s).");
                }
                break;
            case 24:
            case 72:
                Global.rapport_vie_exploitee_inferieur_deux = index;
                if (Global.rapport_vie_exploitee_inferieur_deux == 1 && (Global.lifespan / Global.nb_classes_exploitees) >= 2) {
                   throw new OnError("There is a contradiction between your answers. You declared: \n-a ratio lifespan / number of exploited year-classes < 2; \n-a life span of " + Global.lifespan + " years; \n-a number of exploited year-classes of " + Global.nb_classes_exploitees + " years. \nPlease revise your answer(s).");
                }
                else {
                if (Global.rapport_vie_exploitee_inferieur_deux == 2 && (Global.lifespan / Global.nb_classes_exploitees) < 2) {
                   throw new OnError("There is a contradiction between your answers. You declared: \n-a ratio lifespan / number of exploited year-classes >= 2; \n-a life span of " + Global.lifespan + " years; \n-a number of exploited year-classes of " + Global.nb_classes_exploitees + " years. \nPlease revise your answer(s).");
                }
                }
                break;
//            case 25: // Cas supprimé car doublon avec metapopulation
//            case 74: // Cas supprimé car doublon avec metapopulation
//                Global.stock_divise = index;
//                break;
            case 25:
            case 73:
                Global.reserves_naturelles = index;
                break;
            case 26:
            case 74:
                Global.premiere_reproduction_avant_recrutement = index;
                break;
            case 27:
            case 75:
                Global.fecondite_faible = index;
                break;
            case 28:
            case 76:
                Global.cpue_unstable = index;
                break;
            case 46:
            case 66:
                //String[] influence={"abundance","catchability","both"};//probl�me du both
                Global.environmental_influence = Global.influenceEnv[index];
                Global.typeModele = RechercheModele.mixte;
                break;
            case 47:
            case 67:
                Global.cpue_sous_sur_production = index;
                break;
            case 48:
            case 59:
                Global.linear_relationship = index;
                if (index == 1) {
                    Global.relationCPU_V = RechercheModele.lineaire;
                }
                if(Global.linear_relationship==3)
                  throw new OnError("It is NOT compulsory to know the answer to this question except if the answer \nis obvious. Please have a look at the help file using the 'Help (H)' button \nif necessary before confirming your answer by using the 'Next(N)' button. \nBy default CLIMPROD will consider that the answer is 'No'");
                break;
            case 49:
            case 60:
                Global.monotonic_relationship = index;
                if(Global.monotonic_relationship==3){
                  Global.relationCPU_V = RechercheModele.quadratique;
                  throw new OnError("It is NOT compulsory to know the answer to this question except if the answer \nis obvious. Please have a look at the help file using the 'Help (H)' button \nif necessary before confirming your answer by using the 'Next(N)' button. \nBy default CLIMPROD will consider that the answer is 'No'");
                }
                if (index == 1) {
                    Global.relationCPU_V = RechercheModele.general; // Modèle puissance
                }
                if(Global.monotonic_relationship==2){
                    Global.relationCPU_V = RechercheModele.quadratique;
                }
                System.out.println("Global.relationCPU_V = " + Global.relationCPU_V);
                break;

            case 51:
            case 56:
                Global.recruitment_age = index;
                break;
            case 52:
            case 57:
            /************************************************
			Any change in age warnings cases 52 & 58 must be 
			repeated in the Cadre_ModeleDirect.java file, 
			line 574.
			*************************************************
			*/
                Global.begin_influence_period = index - 1;
               	if (Global.begin_influence_period >= (Global.recruitment_age + Global.nb_classes_exploitees)) {
         				throw new OnError("If the age at the begining of environmental influence occurs \nafter the age of the last exploited year class \n(that is age at recruitment + number of significantly exploited year classes - 1) \nthen there is no point in using a model incorporating an environmental variable. \nPlease use the 'Previous' button to modify your answer(s)");
       				} 
       			if (Global.begin_influence_period == -1) Global.begin_influence_period = 0; // Change for consistency in loops of models with effect on catchability only         
                break;
            case 53:
            case 58:
                Global.end_influence_period = index - 1;
                if (Global.end_influence_period < Global.begin_influence_period) {
                        throw new OnError("Age at the end of environmental influence must be larger than or equal to \nage at the begining of environmental influence. \nPlease modify your answer(s), using the 'Previous' button when necessary.");
                }
		        if (Global.end_influence_period > (Global.recruitment_age + Global.nb_classes_exploitees - 1)) {
          				msg = new MsgDialogBox(0, "If the age at the end of environmental influence occurs \nafter the age of the last exploited year class \n(that is age at recruitment + number of significantly exploited year classes - 1) \nthen only its influence on exploited year classes will be taken into account. \nYou can still continue by clickin on the Next(N) button.", 0, Global.CadreQuestion);
        		}
		        if (Global.recruitment_age >= 0 && Global.begin_influence_period >=0 && Global.end_influence_period < (Global.recruitment_age + Global.nb_classes_exploitees)){
	        		if ((Global.end_influence_period - Global.begin_influence_period) >= 5){
                   		throw new OnError ("WARNING! \nToo many year classes influenced by the environnement (V). \n\nOwing to a recruitment at age " + Global.recruitment_age  + " and " + Global.nb_classes_exploitees + " exploited year classes, and the environmental influence beginning at age " + Global.begin_influence_period + " and ending at age " + Global.end_influence_period + ", \nresults in the need of averaging the environnement effect on " + (Global.end_influence_period - Global.begin_influence_period + 1) + " year classes." + "\nYou can still continue by clickin on the Next(N) button.");  
	        		}
		        }
				if (Global.recruitment_age >= 0 && Global.begin_influence_period >=0 && Global.end_influence_period >= (Global.recruitment_age + Global.nb_classes_exploitees)){           	
	                if ((Global.recruitment_age + Global.nb_classes_exploitees - Global.begin_influence_period) > 5) {
				   		throw new OnError ("WARNING! \nToo many year classes influenced by the environnement (V). \n\nOwing to a recruitment at age " + Global.recruitment_age  + " and " + Global.nb_classes_exploitees + " exploited year classes, and the environmental influence beginning at age " + Global.begin_influence_period + " and ending at age " + Global.end_influence_period + ", \nresults in the need of averaging the environnement effect on " + (Global.recruitment_age + Global.nb_classes_exploitees - Global.begin_influence_period) + " year classes." + "\nYou can still continue by clickin on the Next(N) button.");   
              		}                
		        }
		        if (Global.end_influence_period == -1) Global.end_influence_period = 0; // Change for consistency in loops of models with effect on catchability only         
                Data.init_val();
                break;
            case 95:
                Global.good_results = index;
                if(Global.good_results==3)
                  throw new OnError("It is compulsory to know the answer to this question. \nPlease have a look at the help file using the 'Help (H)' button if necessary.");
                break;
            case 96:
                Global.trend_residuals = index;
                if(Global.trend_residuals==3) {
                  throw new OnError("It is compulsory to know the answer to this question. \nPlease have a look at the help file using the 'Help (H)' button if necessary.");
                }
                break;
            case 97:
                Global.coeff_determination_instable = index;
                if(Global.coeff_determination_instable==3) {
                  throw new OnError("It is compulsory to know the answer to this question. \nPlease have a look at the help file using the 'Help (H)' button if necessary.");
                }
                break;
           case 99:
                Global.acceptable_graphs = index;
                if(Global.acceptable_graphs==3) {
                  Global.acceptable_graphs = 2;
                  throw new OnError("It is compulsory to know the answer to this question. \nPlease have a look at the help file using the 'Help (H)' button if necessary.");
				}
                break;
        }
	  }
	  catch (Exception e) {
         	 new MsgDialogBox(0, e.getMessage(), 0, Global.CadreMain);
         	 }
        if (regle[indexEnCours] != -1) {
            TexteRegles.isTrue();
        }

    }

    /*
     Decide what is next question
     @param int number index of the current answer
     */
    public static void nextQuestion(int index) {
        raisonnement.add(numEnCours);
        //indexEnCours=Integer.parseInt((String)htNum.get(Integer.toString(numEnCours)));
        if (index == 0) //MODIF ICI 0
        {
            numEnCours = rTrue[indexEnCours];
        } else {
            numEnCours = rFalse[indexEnCours];
        }
        System.out.println("La question en cours a le numéro :" + numEnCours);

        Object tp$ = htNum.get(Integer.toString(numEnCours));
        if (tp$ != null) {
            indexEnCours = Integer.parseInt((String) tp$);
        } else {
            indexEnCours = -1;
        }

        //System.out.println("IndexEnCours in QuestionReponse.java: " + indexEnCours);
    }

    /*
     Go back to previous question
     */
    public static int previousQuestion() {
        int k = raisonnement.size();
        if (k == 0) {
            return k;
        }
        // int etapeActuelle=QuestionReponse.getEtape();
        numEnCours = ((Integer) (raisonnement.lastElement()));
        indexEnCours = Integer.parseInt((String) htNum.get(Integer.toString(numEnCours)));

        //System.out.println("previous" + numEnCours);
        raisonnement.removeElementAt(k - 1);
        //System.out.println("Size" + raisonnement.size());
        return k - 1;
    }

    public static int getEtape() {
        if (indexEnCours >= 0) {
            //System.out.println("regle " + QuestionReponse.getNumRegle()+ " Etape " + type[indexEnCours] );
            return type[indexEnCours];
        } else {
            return -1;
        }

    }

    /*
     Init all answers to -1
     */
    public static void reset() {
        raisonnement.clear();
        for (int i = 0; i < selectedItem.length; i++) {
            selectedItem[i] = -1;
        }
        numEnCours = 0;
        indexEnCours = Integer.parseInt((String) htNum.get(Integer.toString(numEnCours)));
    }

    /*
     Return item for question named nameVar
     No control on nameVar
     (Used to get params of a specific question)
     */
    public static String[] getListScript() {

        return script;
    }

    public static String[] getItemQuestion(String nameVar) {
        int it = Integer.parseInt((String) htName.get(nameVar));
        if (script[it] != null) {
            String[] itemQuestion = new String[item[it].length];
            for (int i = 0; i < itemQuestion.length; i++) {
                itemQuestion[i] = item[it][i];
            }
            return itemQuestion;
        } else {
            return null;
        }
    }


    /*
     Activate tutorial
     @param boolean activate if true
     */
    public static void activeDidacticiel(boolean active) {
        reset();
    }

    /*
     Question to display on screen
     @return boolean
     */
    public static boolean displayQuestion() {
        String tc = TexteRegles.getComment();  // Commentaire dans "Current known facts" de la fenêtre principale.
        if (scriptEnCours != null) {
            return true;
        } else return (type[indexEnCours] == Global.fixationM && (!(tc == null) && !tc.equals("")));

    }

    /*
     Return number of already asked questions
     @return un entier  .
     */
    public static int getNbQuestion() {
        return raisonnement.size();

    }

}
