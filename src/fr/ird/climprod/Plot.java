/*
 * Titre : Climprod<p>
 * Definit et trace un graphique de type série chrono, bivarie, histogramme, etc..
 */
package fr.ird.climprod;

import java.awt.font.*;
import java.awt.geom.*;
import java.awt.image.*;

import java.util.Vector;
import java.awt.*;
import javax.swing.*;
import java.text.*;
import java.awt.datatransfer.*;

public class Plot implements Cloneable {

    public Plot() {
        int i;
        nbSerie = 0;
        serieHisto = false;
        ListeSerie = new Vector<PlotSerie>();
        fx = new Font("SansSerif", Font.PLAIN, 12);
        fy = new Font("SansSerif", Font.PLAIN, 12);
        fg = new Font("SansSerif", Font.BOLD, 14);
        fl = new Font("SansSerif", Font.PLAIN, 10);
        feti = new Font("SansSerif", Font.ITALIC, 10);

        valideSerie = new boolean[2];
        extremasAxes = new double[4];
        extremasSeries = new double[4];
        nbdecimalAxes = new int[2];
        notationE = new boolean[2];
        paramEchelle = new double[4];
        titre = new String[4];
        for (i = 0; i < 4; i++) {
            titre[i] = " ";
        }
        pas = new double[2];  // Pas des graduations et labels des axes.
        origine = new double[2];

        marges = new double[4];
        for (i = 0; i < 4; i++) {
            marges[i] = 2 / 10;
        }
        for (i = 0; i < 2; i++) {
            notationE[i] = false;
        }
        for (i = 0; i < 2; i++) {
            nbdecimalAxes[i] = 2;
            notationE[i] = false;
            origine[i] = 0;
        }
        ShowLegend = false;
        ShowEtiquettePointee = false;
        CouleursParDefault = false;

    }

    @Override
    public Object clone() {
        try {

            Plot p = (Plot) super.clone();
            // System.out.println(this.ListeSerie.toString() + " clone vecteur " +p.ListeSerie.toString());
            
            // The following commented line has been replaced by the 5 followings
            // to avoid unchecked cast
            //p.ListeSerie = (Vector<PlotSerie>) p.ListeSerie.clone();
            Vector<PlotSerie> ll = new Vector<PlotSerie>();
            for (int i = 0; i < p.ListeSerie.size(); i++) {
                ll.add(p.ListeSerie.get(i));
            }
            p.ListeSerie = ll;
            
            for (int i = 0; i < nbSerie; i++) {

                PlotSerie sc = (PlotSerie) p.ListeSerie.get(i);
                PlotSerie snew = (PlotSerie) sc.clone();
                p.ListeSerie.remove(i);
                p.ListeSerie.insertElementAt(snew, i);
                // System.out.println("Clone de la serie dans m�thode clone du plot " +sc.toString()+ " " +snew.toString());
            }
            // System.out.println("Clone du plot dans m�thode clone du plot  " +p.toString());
            return p;
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

    /*
     * Ajoute une série de données au graphique représentant des valeurs. La
     * série est ajoutée au vecteur ListeSerie. Après l'ajout la méthode
     * détermine les nouveaux extremas du graphique.
     * Ne concerne pas les Three-variate plot.
     * @param PlotSerie s.
     */
    public void setValeurs(PlotSerie s) {
    	//System.out.println("Flag PlotSerie s, ligne 100 Plot.java"); // test 2020
       	int divX = 10;
        if (nbSerie == 0) { // Ce qui suit ne concerne pas les histogrammes. Pas d'effet si résultat < 0 alors que l'axe concerné est forcé d'être coupé par l'autre à 0. 
							// Le if suivant filtre les variables X pour éliminer V et Years
			if (s.getMinX() != Data.stat[6][3] && s.getMaxX() != Data.stat[7][3] && s.getMinX() != Stat1.min(Data.year) && s.getMaxX() != Stat1.max(Data.year))
			extremasSeries[0] = 0.0;
			else if (s.getMinX() != Stat1.min(Data.year) && s.getMaxX() != Stat1.max(Data.year))  // Si années
			extremasSeries[0] = s.getMinX()-((s.getMaxX() - s.getMinX())/20); //Pour éloigner valeur Xmin de l'axe des Y Modif. 2020
			else extremasSeries[0] = s.getMinX();
			if (s.getMinX() != Stat1.min(Data.year) && s.getMaxX() != Stat1.max(Data.year))
				extremasSeries[1] = s.getMaxX()+((s.getMaxX() - s.getMinX())/20); //Pour éloigner valeur Xmax du bout de l'axe des X. Modif 2020
			else  extremasSeries[1] = s.getMaxX();
			//System.out.println("Plot Java ligne 113 nbSerie = " + nbSerie + " extremasSeries[0] = " + extremasSeries[0] + " extremasSeries[1] = " + extremasSeries[1]);
       	     titre[0] = s.getNameX();

			if (s.getMinY() != Data.stat[6][3] && s.getMaxY() != Data.stat[7][3] && s.getMinY() != Stat1.min(Data.year) && s.getMaxY() != Stat1.max(Data.year) && s.getMinY() > 0) // s.getMinY()>0 prevents setting to zero residuals (and possibly V when negative)
				extremasSeries[2] = 0.0;
			else extremasSeries[2] = s.getMinY()-((s.getMaxY() - s.getMinY())/20); //Pour éloigner valeur Ymin de l'axe des X Modif. 2020
			//System.out.println("Plot Java ligne 119 Stat1.min(Modele.res) = " + Stat1.min(Modele.res));
            extremasSeries[3] = s.getMaxY()+((s.getMaxY() - s.getMinY())/20); //Pour éloigner valeur Ymax du sommet de l'axe des Y. Modif 2020
            titre[1] = s.getNameY();
        } 
        else {
            extremasSeries[0] = Math.min(extremasSeries[0], s.getMinX());
            extremasSeries[1] = Math.max(extremasSeries[1], s.getMaxX());
            extremasSeries[2] = Math.min(extremasSeries[2], s.getMinY());
            extremasSeries[3] = Math.max(extremasSeries[3], s.getMaxY());
        }
		//System.out.println("Plot Java ligne 126 extremasSeries[2] = " + extremasSeries[2] + " extremasSeries[3] = " + extremasSeries[3]);
        if (s.getFigure() == Histogrammes) {   // Pour figures histogrammes.
            extremasSeries[0] = 1;
            extremasSeries[1] = s.getSize();
            extremasAxes[0] = 0;
            extremasAxes[1] = (double) (extremasSeries[1] + 0.5);
			//System.out.println("extremasAxes[1] = " + extremasAxes[1]);
            pas[0] = 1; // Pas des graduations et labels axe des X.
            serieHisto = true;
        } else if (!serieHisto) {              // Pour autres figures.
			// extremasSeries[0]=extremasSeries[0]*.9;
            // extremasSeries[2]=extremasSeries[2]*1.1;
            pas[0] = Math.abs((extremasSeries[1] - extremasSeries[0]) / 20); // Le pas sur l'axe des X (pas[0]  est égal à 1/20ème del'étendue, arondi à unité entière. Commentaire 2021
            if (pas[0] > 1) {
                pas[0] = Math.round(pas[0] - 0.5);
				//System.out.println("Ligne 143 Plot.java, calcul pas[0]. extremasSeries[1]= " + extremasSeries[1]+" extremasSeries[0]= " + extremasSeries[0] + " pas[0]= " + pas[0]);  // test 2020
            }
            ajusteExtrema(0, 1);
        }
		pas[1] = Math.abs((extremasSeries[3] - extremasSeries[2]) / 10); // Le pas sur l'axe des Y (pas[1]  est égal à 1/10ème del'étendue, arrondi à unité entière.  Commentaire 2020
		//System.out.println("Ligne 149 Plot.java, calcul pas[1]. extremasSeries[3]= " + extremasSeries[3]+" extremasSeries[2]= " + extremasSeries[2] + " pas[1]= " + pas[1]);  // test 2020
		if (pas[1] > 1) {
            pas[1] = Math.round(pas[1] + 0.5);
        }
        ajusteExtrema(2, 3);
        // attempt to solve problematic graph drawing MS-E versus V
        // with example.cli and "a V exp(b E)" direct model fit
        // the PAS is equal to zero because the values are constant...
        if (pas[0] == 0.0) {  // Should not be pas[1] for the Y axis? In any case PAS not equal to zero when MSY or MSY = cte because confidence interval is considered
           System.out.println("We reject the serie because of the STEP");
            return;
        }
        ListeSerie.add(s);
        if (CouleursParDefault) {
            couleursParDefault(s);
        }

        nbSerie++;

    }

    /*
     * Calcule l'echelle sur l'objet BufferedImag
     *
     * @param BufferedImag Scr, laquelle doit être tracé le graphique.
     */
    public void Echelle(BufferedImage Scr) {
        panel = null;
        dimPlot = new Dimension(Scr.getWidth(), Scr.getHeight());
        Echelle scale = new Echelle(dimPlot, extremasAxes, marges, 1);
        paramEchelle[0] = scale.getCx();
        paramEchelle[1] = scale.getCy();
        paramEchelle[2] = scale.getDecx();
        paramEchelle[3] = scale.getDecy();
    }

    /*
     * Calcule l'échelle sur l'objet JPanel
     *
     * @param JPanel Scr, le conteneur sur lequel doit être tracé le graphique.
     */
    public void Echelle(JPanel Scr) {

        panel = (JPanel) Scr;
        dimPlot = panel.getSize();
        Echelle scale = new Echelle(dimPlot, extremasAxes, marges, 1);
        paramEchelle[0] = scale.getCx();
        paramEchelle[1] = scale.getCy();
        paramEchelle[2] = scale.getDecx();
        paramEchelle[3] = scale.getDecy();
    }

    /*
     * Trace les axes du graphique.
     *
     * @param : Graphics g, le contexte graphique.
     */
    public void TraceAxes(Graphics g) { // Appelé dans 'Select the appropriate model and fi it' et dans 'Fit a model directly' mais uniquement après click sur Plots etc.
										// Rappelé ensuite lors de passage de la souris sur le graphique.
    	//System.out.println("Flag TraceAxes");
        int i;
        double x1, y1, xg, px;
        double x2, y2, yg, py;
        double[] posLeg = new double[2];

        double maxx;
        int grad;
        double nb;
        Line2D.Double line;
        String eti;
        Font fcurrent = g.getFont();
        FontMetrics fm;
        // Dimension d=panel.getSize();
        Graphics2D g2 = (Graphics2D) g;
        Shape[] shapes = new Shape[1];

        if (dimPlot.width < dimPlot.height) {
            grad = (int) (dimPlot.width * 0.01);
        } else {
            grad = (int) (dimPlot.height * 0.01);
        }
		//for (i = 0; i < 4; i++) System.out.println("Plot.java TraceAxes(Graphics g) i = " + i + " extremasAxes[i]= " + extremasAxes[i] + " paramEchelle[i] = " + paramEchelle[i]); 
		// Les 4 lignes suivantes apparaissent aussi dans PlotModal.java avec différence dans y2.
	    x1 = (extremasAxes[0] * paramEchelle[0] - paramEchelle[2]);
        x2 = (extremasAxes[1] * paramEchelle[0] - paramEchelle[2]);
        y1 = ((extremasAxes[2] + origine[0]) * paramEchelle[1] + paramEchelle[3]);
        y2 = ((extremasAxes[3]) * paramEchelle[1] + paramEchelle[3]);//ici16/7
		//System.out.println("Plot.java TraceAxes(Graphics g) x1 = " + x1 + " x2 = " + x2 + " y1 = " + y1 + " y2 = " + y2);
        line = new Line2D.Double(x1, y1, x2, y1);
        g2.draw(line);
        posLeg[0] = x2 + grad; //x sup coin gauche

        fm = g.getFontMetrics(feti);
        py = fm.getHeight();
        double py2 = py / 2;
        double dec = 0;
		nb = extremasAxes[0]; // Avant: nb = extremasAxes[0] + pas[0] donc pas d'écriture de la valeur de la première graduation sur axe des X;
        g.setFont(feti);
        while (nb <= extremasAxes[1] + 0.0001) {
            xg = (nb * paramEchelle[0] - paramEchelle[2]);
            line = new Line2D.Double(xg, y1 - grad, xg, y1 + grad);
            g2.draw(line);
            eti = getFormat(nb, 'x');
            px = fm.stringWidth(eti) / 2;
            g.drawString(eti, (int) (xg - px), (int) (y1 + grad + py + dec));
            nb = nb + pas[0];
            dec = py2 - dec;
        }
        TextLayout textTl;  // The following lines are not called for the jackknife plots. Instead, PlotModal.java is used. Comment 2021;
        if (titre[0].trim().length() != 0) {  // Titre axe X.
            textTl = new TextLayout(titre[0], getPoliceTitre('x'), new FontRenderContext(null, false, false));
            textTl.draw(g2, (float) (x1 + (x2 - x1) / 2 - (textTl.getBounds().getWidth()) / 2), (float) (y1 + grad + 2 * py + (float) (textTl.getBounds().getHeight())));
        }
        if (titre[2].trim().length() != 0) {  // Titre du graphique.
            textTl = new TextLayout(titre[2], getPoliceTitre('g'), new FontRenderContext(null, false, false));
            textTl.draw(g2, (float) (x1 + (x2 - x1) / 2 - (textTl.getBounds().getWidth()) / 2), (float) (y2 - 2 * (textTl.getBounds().getHeight())));
        }
		x1 = ((extremasAxes[0] + origine[1]) * paramEchelle[0] - paramEchelle[2]);
		//System.out.println("extremasAxes[0] 2 = " + extremasAxes[0]);
        x2 = ((extremasAxes[1] + origine[1]) * paramEchelle[0] - paramEchelle[2]);
        y1 = (extremasAxes[2] * paramEchelle[1] + paramEchelle[3]);
        y2 = (extremasAxes[3] * paramEchelle[1] + paramEchelle[3]);
        line = new Line2D.Double(x1, y1, x1, y2);
        g2.draw(line);
        posLeg[1] = dimPlot.height / 2;
        if (ShowLegend) {
            traceLegende(g, posLeg);
        }
        //line=new Line2D.Double(x1-grad,y1,x1+grad,y1);
        //g2.draw(line);
        // nb=extremasAxes[2]+pas[1];
        nb = extremasAxes[2];
        maxx = 0;
        while (nb <= extremasAxes[3] + 0.0001) {
            yg = (nb * paramEchelle[1] + paramEchelle[3]);
            line = new Line2D.Double(x1 - grad, yg, x1 + grad, yg);
            g2.draw(line);
            eti = getFormat(nb, 'y');
            px = fm.stringWidth(eti) + 2;
            if (px > maxx) {
                maxx = px;
            }

            g.drawString(eti, (int) (x1 - grad - px), (int) (yg));
            nb = nb + pas[1];
        }
        //g.drawString(Integer.toString(count),250,250);
        int w = dimPlot.width;
        if (titre[1].trim().length() != 0) {  // Titre axe Y.
            textTl = new TextLayout(titre[1], getPoliceTitre('y'), new FontRenderContext(null, false, false));
            AffineTransform textAt = new AffineTransform();
            g2.setFont(getPoliceTitre('y'));
            textAt.setToTranslation(x1 - grad - maxx - (float) textTl.getBounds().getHeight(), (y2 + (y1 - y2 + (float) textTl.getBounds().getWidth()) / 2));
            g2.transform(textAt);
            textAt.setToRotation(3 * Math.PI / 2.0);
            g2.transform(textAt);
            g2.drawString(titre[1], 0, 0);
        }
        g.setFont(fcurrent);
        //g.dispose();
        g2.dispose();
    }

    /*
     * DEFINITION DES MARGES DU GRAPHIQUE. 2020
     */
    public void setMarges(double[] newMarges) {
        if (newMarges.length != 4) {
            return;
        }
        for (int i = 0; i < 4; i++) {
            marges[i] = newMarges[i];
        }
        if (panel != null) {
            this.Echelle(panel);
        }
    }

    /*
     * Donne l'ensemble des séries du graphique
     *
     * @return Vector le vecteur contenant les listes
     */
    public Vector getListeSeries() {
        return ListeSerie;
    }

    /*
     * Donne le type du graphique.
     *
     * @return int le type du grahique.
     */
    public int getType() {
        return typePlot;
    }

    /*
     * Donne le coefficient sur l'axe X du graphique.
     *
     * @return double le coefficient.
     */
    public double getCx() {
        return paramEchelle[0];
    }

    /*
     * Donne le coefficient sur l'axe Y du graphique.
     *
     * @return double le coefficient.
     */

    public double getCy() {
        return paramEchelle[1];
    }

    /*
     * Donne le décalage sur l'axe X du graphique.
     *
     * @return double le décalage.
     */

    public double getDecx() {
        return paramEchelle[2];
    }

    /*
     * Donne le décalage sur l'axe X du graphique.
     *
     * @return double le décalage.
     */

    public double getDecy() {
        return paramEchelle[3];
    }

    /*
     * Donne le maximun de la série sur l'axe X du graphique.
     *
     * @return double le maximun de la série sur X.
     */
    public double getMaxSerieX() {
        return extremasSeries[1];
    }

    /*
     * Donne le minimun de la série sur l'axe X du graphique.
     *
     * @return double le minimun de la série sur X.
     */
    public double getMinSerieX() {
		//System.out.println("extremasSeries[0] de getMinSerieX() = " + extremasSeries[0]);
        return extremasSeries[0];
    }

    /*
     * Donne le maximun de la série sur l'axe Y du graphique.
     *
     * @return double le maximun de la série sur Y.
     */
    public double getMaxSerieY() {
        return extremasSeries[3];
    }

    /*
     * Donne le minimun de la série sur l'axe Y du graphique.
     *
     * @return double le minimun de la série sur Y.
     */
    public double getMinSerieY() {
        return extremasSeries[2];
    }

    /*
     * Donne le maximun sur l'axe X du graphique.
     *
     * @return double le maximum sur X.
     */
    public double getMaxAxeX() {
        return extremasAxes[1];
    }

    /*
     * Donne le minimun sur l'axe X du graphique.
     *
     * @return double le miniimum sur X.
     */
    public double getMinAxeX() {
		//System.out.println("extremasSeries[0] de getMinAxeX() = " + extremasSeries[0]);
        return extremasAxes[0];
    }

    /*
     * Donne le maximun sur l'axe Y du graphique.
     *
     * @return double le maximum sur Y.
     */
    public double getMaxAxeY() {
        // System.out.println("getMaxAxeY() extremasAxes[3] (graphique antérieur), ligne 425 Plot.java = " + extremasAxes[3]); // test 2020
        return extremasAxes[3];   // Maximum étiquette axe Y (au moins pour les 4 time series plots du menu principal "Plots"). 2020
    }
    /*
     * Donne le minimun sur l'axe Y du graphique.
     *
     * @return double le minimum sur Y.
     */
    public double getMinAxeY() {
        return extremasAxes[2];
    }

    /*
     * Donne le pas sur l'axe Y du graphique.
     *
     * @return double le pas sur Y.
     */
    public double getpasY() {
   	   // System.out.println("getpasY(), pas[1](graphique antérieur), ligne 444 Plot.java = " + pas[1]); // test 2020    
        return pas[1];         // Pas axe Y. 2020
     }
    /*
     * Donne le pas sur l'axe X du graphique.
     *
     * @return double le pas sur X.
     */
    public double getpasX() {
        return pas[0];
    }

    /*
     * Affecte le pas sur l'axe Y du graphique. (Si le pas est superieur à
     * l'étendue sur Y ou <0 pas d'affectation.) @param double le pas
     *
     * p.
     */
    public void setpasY(double p) {
        if (p < extremasAxes[3] && p > 0) {
            pas[1] = p;
        }
    }

    /*
     * Affecte le pas sur l'axe X du graphique. (Si le pas est superieur à
     * l'étendue sur X ou <0 pas d'affectation.) @param double le pas
     *
     * p.
     */
    public void setpasX(double p) {  // Used in Modele.java (ex: Global.fittedCpuePlot[i].setpasX(5.0d)), Validation.java and Data.java.
        if (p < extremasAxes[1] && p > 0) {
            pas[0] = p;
			//System.out.println("Ligne 496 Plot.java, calcul pas[0]. extremasSeries[1]= " + extremasSeries[1]+" extremasSeries[0]= " + extremasSeries[0] + " pas[0]= " + pas[0]);  // test 2020
        }
    }

    /*
     * Affecte le maximiun sur l'axe X du graphique.
     *
     * @param double le maximum ext.
     */
    public void setMaxAxeX(double ext) {
        extremasAxes[1] = ext;
    }

    /*
     * Affecte le minimiun sur l'axe X du graphique.
     *
     * @param double le minimum ext.
     */
    public void setMinAxeX(double ext) {
		//System.out.println("extremasSeries[0] de setMinAxeX() = " + extremasSeries[0]); // = 0 puis <0 avant plot CPUE=f();.
		extremasAxes[0] = ext;
    }

    /*
     * Affecte le maximiun sur l'axe Y du graphique.
     *
     * @param double le maximum ext.
     */
    public void setMaxAxeY(double ext) {
        extremasAxes[3] = ext;
        // System.out.println("setMaxAxeY() extremasAxes[3], ligne 503 Plot.java = " + extremasAxes[3]); // test 2020
    }

    /*
     * Affecte le minimiun sur l'axe Y du graphique.
     *
     * @param double le minimum ext.
     */
    public void setMinAxeY(double ext) {
        extremasAxes[2] = ext;
    }

    /*
     * Donne le nombre de séries du graphique.
     *
     * @return un int, le nombre de séries .
     */
    public int getNbSerie() {
        return nbSerie;
    }

    /*
     * Affecte le titre du graphique.
     *
     * @param String s, le titre.
     */
	public void setTitreGraphique(String s) {
        titre[2] = s;
        }
	public void setTitreGraphique1(String s) {
        titre[2] = s;
	    }
	public void setTitreGraphique2(String s) {
		titre[3] = s;
		}
    

    /*
     * Affecte le titre du graphique sur X.
     *
     * @param String s, le titre.
     */
    public void setTitreX(String s) {
        titre[0] = s;
    }

    /*
     * Affecte le titre sur Y.
     *
     * @param String s, le titre.
     */
    public void setTitreY(String s) {
        titre[1] = s;
    }

    /*
     * Donne le titre du graphique.
     *
     * @return String le titre.
     */
    public String getTitreGraphique() {
        return titre[2];
    }

    /*
     * Donne le titre sur X.
     *
     * @return String le titre.
     */
    public String getTitreX() {
        return titre[0];
    }

    /*
     * Donne le titre sur Y.
     *
     * @return String le titre.
     */
    public String getTitreY() {
        return titre[1];
    }

    /*
     * Donne le panel sur lequel le graphique est affiché.
     *
     * @return JPanel le panel.
     */
    public JPanel getPanel() {
        return panel;
    }

    /*
     * Donne les polices de caractères sur le titre,l'axe X ou l'axe Y.
     *
     * @param char t (si t='x' affectation sur axe X,t='y" sur axe T,t='g' sur
     * graphique.
     * @return Font, la police utilisée.
     */
    public Font getPoliceTitre(char t) {
        Font f = new Font("SansSerif", Font.ITALIC, 12);
        switch (t) {
            case 'x':
                f = fx;
                break;
            case 'y':
                f = fy;
                break;
            case 'g':
                f = fg;
                break;
        }
        return f;
    }

    /*
     * Affecte les polices de caractères sur le titre,l'axe X ou l'axe Y.
     *
     * @param char t (si t='x' affectation sur axe X,t='y" sur axe T,t='g' sur
     * graphique.
     * @param Font, la nouvelle police.
     */
    public void setPoliceTitre(char t, Font f) {
        switch (t) {
            case 'x':
                fx = f;
                break;
            case 'y':
                fy = f;
                break;
            case 'g':
                fg = f;
                break;
        }
    }

    /*
     * Donne la polices de caractères utilisée pour les graduations
     *
     * @return Font, la police utilisée.
     */
    public Font getPoliceGraduation() {
        return feti;
    }

    /*
     * Affecte la polices de caractères pour les graduations
     *
     * @param Font, la nouvelle police.
     */
    public void setPoliceGraduation(Font f) {
        feti = f;
    }

    /*
     * Retourne un boolean vrai si la notation scientifique est utilisée sur
     * l'axe X
     *
     * @return boolean.
     */
    public boolean getNotationEsurX() {
        return notationE[0];
    }

    /*
     * Retourne un boolean vrai si la notation scientifique est utilisée sur
     * l'axe Y
     *
     * @return boolean.
     */
    public boolean getNotationEsurY() {
        return notationE[1];
    }

    /*
     * Affecte l'affichage au format scientifique à vrai ou faux sur l'axe X.
     *
     * @param boolean b.
     */
    public void setNotationEsurX(boolean b) {
        notationE[0] = b;
    }

    /*
     * Affecte l'affichage au format scientifique à vrai ou faux sur l'axe Y.
     *
     * @param boolean b.
     */
    public void setNotationEsurY(boolean b) {
        notationE[1] = b;
    }

    /*
     * Donne le nombre de decimales utilisees pour l'affichage sur l'axe X
     *
     * @return int le nombre de decimales.
     */
    public int getDecimalsurX() {
        return nbdecimalAxes[0];
    }

    /*
     * Donne le nombre de decimales utilisees pour l'affichage sur l'axe Y
     *
     * @return int le nombre de decimales.
     */
    public int getDecimalsurY() {
        return nbdecimalAxes[1];
    }

    /*
     * Affecte le nombre de decimales utilisees pour l'affichage sur l'axe X. Ce
     * nombre est limité à 9.
     *
     * @param int n, le nombre de decimales.
     */
    public void setDecimalsurX(int n) {
        if (n < 10) {
            nbdecimalAxes[0] = n;
        }
    }

    /*
     * Affecte le nombre de decimales utilisées pour l'affichage sur l'axe Y. Ce
     * nombre est limité à 9.
     *
     * @param int n, le nombre de decimales.
     */
    public void setDecimalsurY(int n) {
        if (n < 10) {
            nbdecimalAxes[1] = n;
        }
    }

    /*
     * Détermine sur quelle valeur l'axe X coupe l'axe Y. Cette valeur doit être
     * comprise dans les limites de l'axe Y.
     *
     * @param double f, la valeur où se fait l'intersection.
     */
    public void setXcutYat(double f) {
        if ((f >= extremasAxes[2]) && (f <= extremasAxes[3])) {
            origine[0] = f - extremasAxes[2];
        }
    }

    /*
     * Détermine sur quelle valeur l'axe Y coupe l'axe X. Cette valeur doit être
     * comprise dans les limites de l'axe X.
     *
     * @param double f, la valeur où se fait l'intersection.
     */
    public void setYcutXat(double f) {
        if ((f >= extremasAxes[0]) && (f <= extremasAxes[1])) {
            origine[1] = f - extremasAxes[0];
        }
    }

    /*
     * Donne la valeur sur l'axe Y de l'intersection avec l'axe X.
     *
     * @return double,la valeur où se fait l'intersection.
     */
    public double getXcutYat() {
        return extremasAxes[2] + origine[0];
    }

    /*
     * Donne la valeur sur l'axe X de l'intersection avec l'axe Y.
     *
     * @return double,la valeur où se fait l'intersection.
     */
    public double getYcutXat() {
        return extremasAxes[0] + origine[1];
    }

    /*
     * Détermine le format pour l'affichage.
     *
     * @return String, la valeur formatée.
     */
    protected String getFormat(double val, char t) {
        String decimal = "0.";
        String nbdec = "0";
        String s;

        double exp;
        int nb;
        boolean e;
        int i = 0;
        if (t == 'x') {
            nb = nbdecimalAxes[0];
            e = notationE[0];
        } else if (t == 'y') {
            nb = nbdecimalAxes[1];
            e = notationE[1];
        } else {
            return "0";
        }

        while (i < nb) {
            decimal = decimal + nbdec;
            i++;
        }
        if (i == 0) {
            decimal = "0";
        }
        DecimalFormat df = new DecimalFormat(decimal);
        if (val == 0) {
            return "0";
        } else if (!e) {
            return df.format(val);
        } else {
            exp = val;
        }
        i = 0;
        if (Math.abs(val) > 1) {
            s = "e";
            while (Math.abs(exp) >= 10) {
                exp = exp / 10;
                i = i + 1;
            }
        } else {
            s = "e-";
            while (Math.abs(exp) < 1) {
                exp = exp * 10;
                i = i + 1;
            }
        }
        if (i > 9) {
            s = s + Integer.toString(i);
        } else {
            s = s + "0" + Integer.toString(i);
        }

        return df.format(exp) + s;
    }

	// La fonction qui suit est supprimée car jamais appellée. Commentaire 2021.
	/*    private double Graduation(double xmin, double xmax, int nint) {
        int i, k;
        double[] q = {1.0, 2.0, 5.0, 10.0, 20.0};
        double[] r = {1.41421, 3.162278, 7.071068};
        double s = 0.00002;
        double[] u = new double[3];
        double p, pl, lp, pas, g1, j1, t1, g2, j2, t2, xinf, xsup;
        /*double xmin=0.0;
         double xmax=10.0;
         int nint=10;*/
/*        int mfix = 0;

        p = (xmax - xmin) / (double) nint;

        pl = Math.log(p) / Math.log(10);
        lp = pl;
        if (p < 1) {
            lp = lp - 1;
        }
        s = p / 10.0 * lp;
        //System.out.println(p + "*"+ pl +"**" + lp+"***"+s)	;

        k = 3;
        for (i = 0; i < 3; i++) {
            u[i] = q[i] + s;
            //if(mfix==0) u[i]=r[i];
            if (s < u[i] && k == 3) {
                k = i;
            }
        }
        i = 0;
        if (k == 3) {
            i = 3;
        }

        pas = q[i] * Math.pow(10.0, lp);
        g1 = xmin / pas;
        j1 = g1;
        if (g1 < 0) {
            j1 = j1 - 1;
        }
        t1 = Math.abs(j1 + 1.0 - g1);
        if (t1 < s) {
            j1 = j1 + 1;
        }
        xinf = pas * j1;
        g2 = xmax / pas;
        j2 = g2 + 1.0;
        if (g2 < -1) {
            j2 = j2 - 1;
        }
        t2 = Math.abs(g2 + 1.0 - j2);
        if (t2 < s) {
            j2 = j2 - 1;
        }
        xsup = pas * j2;
        if (xinf > xmin) {
            xinf = xmin;
        }
        if (xsup < xmax) {
            xsup = xmax;
        }

        //System.out.println(xinf + "*"+ xsup +"**" + pas)	;
        return pas;
    }
	*/
    /*
     * Trace le graphique.
     *
     * @param Graphics g, le contexte graphique.
     */
    public void traceMap(Graphics g) {
        int i;
        double x1, y1, x2, y2, bottom;
        Line2D.Double line;
        Graphics2D g2 = (Graphics2D) g;
        Color oldColor = g2.getColor();
        //System.out.println(oldColor);
        BasicStroke oldStroke = (BasicStroke) g2.getStroke();
        PlotSerie s;
        for (int j = 0; j < ListeSerie.size(); j++) {
            s = (PlotSerie) ListeSerie.get(j);
            double[] vx;
            double[] vy;
            int typefigure = s.getFigure();
            int typemark = s.getMark();

            if ((s.getMinX() >= extremasAxes[0] && s.getMaxX() <= extremasAxes[1] && s.getMinY() >= extremasAxes[2] && s.getMaxY() <= extremasAxes[3]) == false) {
				//System.out.println("s.getMinX() = " + s.getMinX() + " extremasAxes[0] = " + extremasAxes[0]);
                LimitePlot lp = new LimitePlot(s.getDataX(), s.getDataY(), extremasAxes);
                vx = lp.getXs();
                vy = lp.getYs();
                if (vx == null) {
                    typefigure = -1;
                }

            } else {
                vx = s.getDataX();
                vy = s.getDataY();
                //System.out.println("***lg serie**"+vx.length);
            }
            switch (typefigure) {
                case NuageDePoints:  // Nuage de points X Y  (pour bivariate plots et résidus). 2020

                    Color[] currentColor = s.getCouleurPoints();
                    if (typemark == 0) {
                        typemark = 1;
                    }
                    for (int k = 0; k < vx.length; k++) {
                        vx[k] = vx[k] * paramEchelle[0] - paramEchelle[2];
                        vy[k] = vy[k] * paramEchelle[1] + paramEchelle[3];
                    }
                    traceMark(g2, vx, vy, typemark, currentColor);
                    break;
                case Courbes:
                    g2.setColor(s.getCouleur());
                    g2.setStroke(s.getBasicStroke());
       				vx[0] = vx[0] * paramEchelle[0] - paramEchelle[2];
                    vy[0] = vy[0] * paramEchelle[1] + paramEchelle[3];
                    for (i = 1; i < vx.length; i++) {
                        vx[i] = vx[i] * paramEchelle[0] - paramEchelle[2];
                        vy[i] = vy[i] * paramEchelle[1] + paramEchelle[3];
                        line = new Line2D.Double(vx[i - 1], vy[i - 1], vx[i], vy[i]);
                        g2.draw(line);
				  //x1=x2;
                        //y1=y2;

                    }
                    g2.setStroke(oldStroke);
                    currentColor = s.getCouleurPoints();
                    traceMark(g2, vx, vy, typemark, currentColor);
                    break;
                case CourbesModales: // graphiques dont l'axe des X représente une variable qualitative (histogrammes jackknife)
					//System.out.println("\na FLAG CourbesModales LIGNE 989 Plot.java"); // N'apparait pas... . Test 2020
                    g2.setColor(s.getCouleur());
                    g2.setStroke(s.getBasicStroke());
                    x1 = vx[0] * paramEchelle[0] - paramEchelle[2];
                    y1 = vy[0] * paramEchelle[1] + paramEchelle[3];
                    Rectangle2D.Double point = new Rectangle2D.Double(x1 - 1, y1 - 1, 2, 2);
                    g2.draw(point);
                    for (i = 1; i < vx.length; i++) {
                        if (vx[i] == (vx[i - 1] + 1)) {
                            x2 = vx[i] * paramEchelle[0] - paramEchelle[2];
                            y2 = vy[i] * paramEchelle[1] + paramEchelle[3];
                        } else {
                            x2 = x1;
                            y2 = y1;
                        }
                        point = new Rectangle2D.Double(x1 - 1, y1 - 1, 2, 2);
                        g2.draw(point);
                        line = new Line2D.Double(x1, y1, x2, y2);
                        g2.draw(line);
                        point = new Rectangle2D.Double(x2 - 1, y2 - 1, 2, 2);
                        g2.draw(point);
                        x1 = x2;
                        y1 = y2;
                    }
                    break;
                case Histogrammes:
                    g2.setColor(s.getCouleur());
                    g2.setStroke(s.getBasicStroke());
                    double nb = (double) (extremasAxes[0] + 0.5);
                    double width = (double) (((nb + 1) * paramEchelle[0] - paramEchelle[2]) - (nb * paramEchelle[0] - paramEchelle[2]));
                    //bottom=  (extremasAxes[2]*paramEchelle[1]+paramEchelle[3]);
                    double xcuty = this.getXcutYat() * paramEchelle[1] + paramEchelle[3];
                    Rectangle2D rect;
                    for (i = 0; i < vx.length; i++) {
                        x1 = (double) (nb * paramEchelle[0] - paramEchelle[2]);
                        y1 = vy[i] * paramEchelle[1] + paramEchelle[3];
                        if (xcuty >= y1) {
                            rect = new Rectangle2D.Double(x1, y1, width, (xcuty - y1));
                        } else {
                            rect = new Rectangle2D.Double(x1, xcuty, width, y1 - xcuty);
                        }
                        g2.draw(rect);
                        //g2.fill(rect);
                        nb = nb + 1;
                    }
                    g2.setStroke(oldStroke);
                    break;
                case Barres:
                    g2.setColor(s.getCouleur());
                                //((extremasAxes[2]+origine[0])*paramEchelle[1]+	paramEchelle[3]);
                    //float xcuty=this.getXcutYat();
                    xcuty = this.getXcutYat() * paramEchelle[1] + paramEchelle[3];

                    for (i = 0; i < vx.length; i++) {
                        x1 = vx[i] * paramEchelle[0] - paramEchelle[2];
                        y1 = vy[i] * paramEchelle[1] + paramEchelle[3];
                        if (y1 >= xcuty) {
                            line = new Line2D.Double(x1, xcuty, x1, y1);
                        } else {
                            line = new Line2D.Double(x1, y1, x1, xcuty);
                        }
                        g2.draw(line);
                    }
                    break;
                default:
                    break;
            }
        }
        g2.setColor(oldColor);
        g2.dispose();
    }

    /*
     * Affiche les coordonnées du point "pointé" par la souris. Attention : la
     * série doit être de type nuage de points.
     */
    public void showEtiquettePointee(double x, double y) {
        if (!ShowEtiquettePointee) {
            return;
        }
        if (panel == null) {
            return;
        }
        Dimension d = panel.getSize();
        String patch = "";
        Graphics g = panel.getGraphics();

        FontMetrics fm;
        Font fcurrent = g.getFont();

        PlotSerie s = null;
        boolean found = false;
        for (int i = 0; i < nbSerie; i++) {
            s = (PlotSerie) ListeSerie.get(i);
            if (s.getFigure() == 1) //  a priori une seule s�rie "nuage"
            {
                found = true;
                break;
            }
        }
        if (!found) {
            return;
        }
        g.setFont(feti);
        fm = g.getFontMetrics(feti);
        int py = (int) (fm.getHeight());
        Color cf = panel.getForeground();
        g.setColor(panel.getBackground());
    	// Rectangle2D.Float point=new Rectangle2D.Float(4.0f,(float)(d.height-py),(float)(d.width*0.95f), (float)py);
        //g2.draw(point);
        g.fillRect(4, d.height - (2 * py), (int) (d.width * .95), py);
        g.setColor(cf);
        String[] etiquettes = s.getEtiquettes();
        double[] sx = s.getDataX();
        double[] sy = s.getDataY();
        double vx = (x + paramEchelle[2]) / paramEchelle[0];
        double vy = (y - paramEchelle[3]) / paramEchelle[1];
        int res = 0;//UtilIndex.FoundNearest(sx,sy,vx,vy);

        if (res != -1) {
            patch = etiquettes[res] + "  abs.: " + sx[res] + "  ord.: " + sy[res];
        }

        g.drawString(patch, 3, (d.height - py));
        panel.repaint(0, (d.height - py), d.width, py);
        g.setFont(fcurrent);
        g.dispose();

    }

    /*
     *
     */
    public void exportClipboard(int numSerie) {
        if (numSerie > nbSerie - 1 || numSerie < 0) {
            return;
        }
        StringBuilder sbf = new StringBuilder();
        Clipboard system;
        StringSelection stsel;
        PlotSerie s = (PlotSerie) ListeSerie.get(numSerie);
        double[] vx = s.getDataX();
        double[] vy = s.getDataY();
        String[] eti = s.getEtiquettes(); //Attention pas de vérification
        sbf.append("" + "\t").append(s.getNameX()).append("\t").append(s.getNameY()).append("\n");
        for (int i = 0; i < vx.length; i++) {
            String line = eti[i] + "\t" + vx[i] + "\t" + vy[i] + "\n";
            sbf.append(line);
        }
        stsel = new StringSelection(sbf.toString());
        system = Toolkit.getDefaultToolkit().getSystemClipboard();
        system.setContents(stsel, stsel);

    }

    /*
     * Ajuste les extremas des axes de façon automatique de telle sorte que
     * l'extremité des axes correspondent à une graduation.
     */
    protected void ajusteExtrema(int i, int j) { // différent de ajuste Extremas (pluriel) ci-dessous.
		//System.out.println("Flag ajusteExtrema (au singulier)");
        int p = 0;
        if (j == 3) {
            p = 1;
        }
        if (Math.abs(pas[p]) < 1.e-5) {
            return;
        }
        // Par defaut les extrêmes des axes sont égaux au extrêmes des séries correspondantes. Commentaire 2021.
        extremasAxes[i] = extremasSeries[i];  // Définition extremasAxes minima < 1; i=0 min X et i=2 min Y. Commentaire 2020
        extremasAxes[j] = extremasSeries[j];  // Définition extremasAxes maxima < 1; j=1 max X et j=3 max Y. Commentaire 2020
        
        if (Math.abs(extremasAxes[i]) > 1) {
            extremasAxes[i] = Math.round(extremasAxes[i] - 0.5); // Définition extremasAxes > 1 (=arrondi de valeur). Commentaire 2020
        }
        if (Math.abs(extremasAxes[j]) > 1) {
            extremasAxes[j] = Math.round(extremasAxes[j] + 0.5);
        }
		//System.out.println("ajusteExtrema(), ligne 1133 Plot.java, i="+ i + " extremasAxes[i]= " + extremasAxes[i]);
		//System.out.println("ajusteExtrema(), ligne 1133 Plot.java, j="+ j + " extremasAxes[j]= " + extremasAxes[j]);
        double adj = extremasAxes[i];
        int z = 0;
        while (adj < extremasAxes[j]) {
            adj = adj + pas[p];
  		//z=z+1;
            // if(z>100)
            // {
            //   System.out.println(adj + "pas" + pas[p]+ "  "+extremasAxes[j]);
            //   return;
		//  }
        }
        extremasAxes[j] = (double) adj;

    }

    /*
     * Ajuste les extremas des axes de telle sorte que les graduations soient
     * des multiples de 10. (PF 2020: c'est un pb pour graphique résidus car amplitude divisée 2 !!!)
     */
    public void ajusteExtremas(boolean surX, boolean surY, boolean minzeroSurX, boolean minzeroSurY) { // différent de ajuste Extrema (singulier) ci-dessus.
        // Selon valeur booléenne (vrai ou faux) applique ou non l'ajustement surX, sur X et met ou non le minimum à zéro sur Y et sur Y 
		//System.out.println("Flag ajusteExtremas() au pluriel, ligne 1163 Plot.java"); // test 2020
        long[] bornes = {1, 10, 100, 1000, 10000};
        int deb;
        int fin;
        int signe = 0;
        if (surX && surY) {
            deb = 0;
            fin = 4;
        } else if (surX) {
            deb = 0;
            fin = 2;
        } else if (surY) {
            deb = 2;
            fin = 4;
        } else {
			//System.out.println("ajusteExtremas(), ligne 1204 Plot.java surX = " + surX + " surY = " + surY);
            return;
        }
		//System.out.println("ajusteExtremas(), ligne 1207 Plot.java deb = " + deb + " fin = " + fin);
        for (int i = deb; i < fin; i++) {
			//System.out.println("i " + i + "OLD extremasSeries[i] " + extremasSeries[i]);
            double extabs = Math.abs(extremasSeries[i]); // extabs pour extension absolue. i = 0 min X; i = 1 max X
			//System.out.println("ajusteExtremas(), ligne 1211 Plot.java i =" + i + " extremasSeries[i]=" + extremasSeries[i]);
            int index = 4;
            if (extabs < 1) {
                index = 0;
            } else if (extabs < 100) { // 2020 Pourquoi < 10 et < 100 n'existent pas?
                index = 1;
            } else if (extabs < 10000) {
                index = 2;
            } else if (extabs < 1000000) {
                index = 3;
            }
            // System.out.println("\najusteExtremas(), ligne 1193 Plot.java, index = " + index); // test 2020
            double newext = 0;
            double modulo = extremasSeries[i] % bornes[index]; // % = modulo = reste de la division extremasSeries / bornes. Commentaire 2020  
			//System.out.println("ajusteExtremas(), ligne 1225 Plot.java  modulo initial= " + modulo); // test 2020
            if (extremasSeries[i] < 0) {
                modulo = bornes[index] + modulo;
                // System.out.println("ajusteExtremas(), extremasSeries[i] < 0 ligne 1196 Plot.java, i= " + i + " index= " + index + " bornes[index]= " + bornes[index] + " modulo final= " + modulo); // test 2020
            }
       // else
            // modulo=bornes[index]-(modulo);

            if ((i % 2) == 0) { // % = Modulo reste de la division entière i / 2. Ici 0 si i pairs et 1 sinon.
                //newext=extremasSeries[i]-(modulo);
                if (modulo == 0) {
                    newext = extremasSeries[i] - (bornes[index]);
                } else {
                    newext = extremasSeries[i] - (modulo);
                }
            } else {
                //  newext=extremasSeries[i]+(modulo);
                newext = extremasSeries[i] + bornes[index] - (modulo);
            }
            if (extremasSeries[i] >= 0 && newext < 0) {
                extremasAxes[i] = 0;
            } else {
                extremasAxes[i] = newext;
			//System.out.println("Flag dans Plot.java ligne 1230 newext = " + newext);               
            }
			//System.out.println(" NEW extremasSeries[i] " +extremasAxes[i]);
        }
        //System.out.println();
        if (minzeroSurX && extremasAxes[0] > 0) {
            extremasAxes[0] = 0;
        }
        if (minzeroSurY && extremasAxes[2] > 0) {
            extremasAxes[2] = 0;
        }
		//System.out.println("Dans Plot.java ligne 1240 extremasAxes[0] = " + extremasAxes[0] + "extremasAxes[1] = " + extremasAxes[1]);      
        pas[0] = (extremasAxes[1] - extremasAxes[0]) / 10; // extremasAxes[0] min. axe X et extremasAxes[1] max axe X. 2020.
        pas[1] = (extremasAxes[3] - extremasAxes[2]) / 10; // extremasAxes[2] min. axe Y et extremasAxes[3] max axe Y. 2020.
		// System.out.println("ajusteExtremas(), ligne 1229 Plot.java pas[1]= " + pas[1] + " extremasAxes[2]= " + extremasAxes[2] + " extremasAxes[3]= " + extremasAxes[3]); // test 2020
		//System.out.println("Flag Plot.java ajusteExtremas. pas[0] = " + pas[0] + " pas[1] = " + pas[1]);
        if (pas[0] > 10) {
            this.setDecimalsurX(0);
        }
        if (pas[1] > 10) {
            this.setDecimalsurY(0);
        }       
        //if(surX) ajusteExtrema(0,1);
        //if(surX) ajusteExtrema(2,3);
		//System.out.println("Dans Plot.java ligne 1270 extremasAxes[0] = " + extremasAxes[0] + "extremasAxes[1] = " + extremasAxes[1]);
    }

    public void pasAutoX(int nbGraduation) {
        float[] bornes = {1, 10, 100, 1000, 10000, 100000, 1000000};
        if (nbGraduation <= 0) {
            return;
        }
        double etendue = (extremasAxes[1] - extremasAxes[0]) / nbGraduation;  // 2020
        if (etendue <= 1) {
            pas[0] = etendue;
        }

    }

    /*
     Tracé du point (figuré et couleur)
     */
    protected void traceMark(Graphics2D g2, double[] vx, double[] vy, int typemark, Color[] currentColor) {
        double x1, y1;
        Line2D.Double l1, l2, l3;
        for (int i = 0; i < vx.length; i++) {
            x1 = vx[i];//*paramEchelle[0]-paramEchelle[2];
            y1 = vy[i];//*paramEchelle[1]+paramEchelle[3];
            g2.setColor(currentColor[i]);
            switch (typemark) {
                case 1:
                    l1 = new Line2D.Double(x1 - 4, y1, x1 + 4, y1);
                    g2.draw(l1);
                    l2 = new Line2D.Double(x1, y1 + 4, x1, y1 - 4);
                    g2.draw(l2);
                    break;
                case 2:
                    Rectangle2D.Double point = new Rectangle2D.Double(x1 - 2, y1 - 2, 4d, 4d);
                    g2.fill(point);
                    break;
                case 3:
                    for (int k = 0; k < 3; k++) {
                        int l = 3 - (1 * k);
                        l1 = new Line2D.Double(x1 - l, y1 + l, x1 + l, y1 + l);
                        g2.draw(l1);
                        l2 = new Line2D.Double(x1 + l, y1 + l, x1, y1 - l);
                        g2.draw(l2);
                        l3 = new Line2D.Double(x1, y1 - l, x1 - l, y1 + l);
                        g2.draw(l3);
                    }
                    break;
            }
        }
    }


    /*
     Trace la légende (à optimiser)
     @param le contexte graphique g et les coordonnées du coin superieur gauche
     */
    protected void traceLegende(Graphics g, double[] position) {

  		// Dimension d=panel.getSize();
        Color[] colorL = new Color[nbSerie];
        Color[][] colorP = new Color[nbSerie][1];
        int numLegend = 0;
        int[] mark = new int[nbSerie];
        int[] typeG = new int[nbSerie];
        double[][] positionLigne = new double[nbSerie][2];
        double[] positionMarkX = new double[1];
        double[] positionMarkY = new double[1];
        BasicStroke[] currentStroke = new BasicStroke[nbSerie];
        TextLayout[] textTl = new TextLayout[nbSerie];
        Font fcurrent = g.getFont();
        Color ccurrent = g.getColor();
        double textWidth = 0;
        double textHeight = 0;
        double longueurSegment = 0;
        //
        for (int i = 0; i < nbSerie; i++) {

            PlotSerie s = (PlotSerie) ListeSerie.get(i);
            if (s.getLegende().length() != 0) {
                currentStroke[numLegend] = s.getBasicStroke();
                typeG[numLegend] = s.getFigure();
                colorL[numLegend] = s.getCouleur();
                colorP[numLegend][0] = s.getDefaultCouleurPoints();
                mark[numLegend] = s.getMark();
                textTl[numLegend] = new TextLayout(s.getLegende(), s.getFont(), new FontRenderContext(null, false, false));
                double cW = textTl[numLegend].getBounds().getWidth();
                if (cW > textWidth) {
                    textWidth = cW;
                }
                double cH = textTl[numLegend].getBounds().getHeight();
                if (cH > textHeight) {
                    textHeight = cH;
                }
                numLegend = numLegend + 1;
            }
        }
        if (numLegend == 0) {
            return;//Tous les textes de legendes "blancs"
        }
        int cadreX = (int) position[0];
        int cadreY = (int) (position[1] - textHeight);
        double largeur = (dimPlot.width * .98) - position[0];
        double dec = position[0] * 0.01;
        position[0] = position[0] + dec;
        int cadreW = (int) (largeur + 2 * dec);
        if (textWidth > largeur) {
            return;
        } else {
            longueurSegment = position[0] + (largeur - textWidth) * .85;
        }

        positionMarkX[0] = position[0] + (longueurSegment - position[0]) / 2;

        //     System.out.println("lpanel" + d.width+ "  Largeur "+largeur+" texte largeur "  +textWidth + " Longueur Seg " +longueurSegment+ "Pos0 " + position[0]);
        Graphics2D g2 = (Graphics2D) g;
        BasicStroke oldStroke = (BasicStroke) g2.getStroke();
        for (int i = 0; i < numLegend; i++) {
            g2.setColor(colorL[i]);
            g2.setStroke(currentStroke[i]);
            Line2D.Double line = new Line2D.Double(position[0], position[1], longueurSegment, position[1]);
            g2.draw(line);
            positionMarkY[0] = position[1];
            traceMark(g2, positionMarkX, positionMarkY, mark[i], colorP[i]);
            g2.setColor(ccurrent);

            textTl[i].draw(g2, (float) (position[0] + largeur - textWidth), (float) (position[1] + (textHeight / 2)));
            position[1] = position[1] + (float) (textHeight);
        }
        g2.setStroke(oldStroke);

        int cadreH = (int) (position[1] + (float) (textHeight / 2) - cadreY);
        g2.drawRect(cadreX, cadreY, cadreW, cadreH);

    }

    public Image createImage(int width, int height) {

        BufferedImage picture = new BufferedImage(width, height, BufferedImage.TYPE_INT_BGR);

        /* for(int i=0;i<picture.getWidth();i++)
         for(int j=0;j<picture.getHeight();j++)
         picture.setRGB(i,j, Color.white.getRGB());*/
        Graphics g = picture.getGraphics();
        Graphics g2 = picture.getGraphics();
        g.fillRect(0, 0, picture.getWidth(), picture.getHeight());
        g.setColor(Color.black);

        g2.fillRect(0, 0, picture.getWidth(), picture.getHeight());
        g2.setColor(Color.black);
        //g.setXORMode();
        this.Echelle(picture);
        this.traceMap(g);
        this.TraceAxes(g2);
        //System.out.println(picture.getClass());
        return (Image) picture;
    }

    protected void couleursParDefault(PlotSerie s) {
        Color[] cpd = new Color[17];
        cpd[0] = Color.red;
        cpd[1] = Color.blue;
        cpd[2] = Color.yellow;
        cpd[3] = Color.green;
        cpd[4] = Color.cyan;
        cpd[5] = Color.magenta;
        cpd[6] = Color.orange;
        cpd[13] = Color.pink;
        cpd[8] = new Color(0, 10, 128);
        cpd[9] = new Color(128, 0, 0);
        cpd[10] = new Color(0, 128, 0);
        cpd[11] = new Color(0, 128, 148);
        cpd[12] = new Color(177, 128, 98);
        cpd[7] = new Color(217, 255, 222);
        cpd[14] = new Color(255, 226, 34);
        cpd[14] = new Color(192, 192, 172);
        cpd[15] = Color.black;
        cpd[16] = Color.white;
        if (nbSerie < 17) {
            s.setCouleur(cpd[nbSerie]);
        } else {
            s.setCouleur(cpd[0]);
        }
    }

    final int NuageDePoints = 1; // 2020
    final int Courbes = 2;
    final int Histogrammes = 3;
    final int CourbesModales = 4;
    final int Barres = 5;

    private boolean serieHisto;

    protected int typePlot;
    protected int nbSerie;									 //nbre de series
    protected Vector<PlotSerie> ListeSerie;						 //vecteur de la liste de series (en [0] axe X,en[1] axe Y)
    protected double[] extremasAxes;  			 //Extremas d�finis sur X et Y
    protected double[] extremasSeries;  			 //Extremas d�finis sur X et Y

    protected double[] paramEchelle;
    private Font fx, fy, fg, fl;							   //Police sur X,Y,Titre
    protected Font feti;							    //Police sur etiquete X,Y

    protected String[] titre;							 	 //Titres sur X,Y,Graphique
    private double[] marges;								 	 //Tableau des marges du graphique en % [0]=gauche,[1]=droite,[2]=haut,[3]=bas (float entre 0 et 1)
    protected double[] pas;								  //Valeur du pas (à l'orgine (max-min)/10)
    private boolean[] valideSerie;      //True si s�ries X [0] et Y [1] d�finies.
    protected JPanel panel;
    protected Dimension dimPlot;

    private int[] nbdecimalAxes;
    private boolean[] notationE;
    protected double[] origine;
    public boolean ShowLegend;
    public boolean ShowEtiquettePointee;
    public boolean CouleursParDefault;

}

class Echelle {

    public Echelle(Dimension d, double extremas[], double marge[], int Quad) {
        int i;
        double[] Marge = new double[4];
        for (i = 0; i < 4; i++) {
            Marge[i] = marge[i];										//Chaque marge doit �tre comprise entre 0+ et 20%
            if (Math.abs(Marge[i]) > 0.2 || Marge[i] <= 0) {
                Marge[i] = (double) 0.2;
            }
        }

        cx = d.width;
        cy = d.height;
        switch (Quad) {
            case 2:
                cy = cy / 2;
                break;
            case 3:
                cx = cx / 2;
                break;
            case 4:
                cx = cx / 2;
                cy = cy / 2;
                break;
        }
        Marge[0] = Marge[0] * cx;
        Marge[1] = Marge[1] * cx;
        Marge[2] = Marge[2] * cy;
        Marge[3] = Marge[3] * cy;

        if ((extremas[1] - extremas[0]) != 0) {
            cx = (cx - Marge[0] - Marge[1]) / Math.abs(extremas[1] - extremas[0]);
        }
        if ((extremas[2] - extremas[3]) != 0) {
            cy = (cy - Marge[2] - Marge[3]) / Math.abs(extremas[3] - extremas[2]);
        }
        Decx = extremas[0] * cx - Marge[0];
        Decy = extremas[2] * cy + (d.height - Marge[2]);
        cy = -cy;
    //}
    }

    public double getCx() {
        return cx;
    }

    public double getCy() {
        return cy;
    }

    public double getDecx() {
        return Decx;
    }

    public double getDecy() {
        return Decy;
    }

    //private float[] extremas = new float[4];
    private double cx;
    private double cy;
    private double Decx;
    private double Decy;
}

class LimitePlot {

    public LimitePlot(double[] x, double[] y, double[] ext) {
        double[][] tmp = new double[x.length][2];
        int k = 0;
        //System.out.println(ext[0]+" "+ext[1]+" "+ ext[2]+" " +ext[3]+"**************************************");
        for (int i = 0; i < x.length; i++) {
            if (x[i] >= ext[0] && x[i] <= ext[1] && y[i] >= ext[2] && y[i] <= ext[3]) {
                tmp[k][0] = x[i];
                tmp[k][1] = y[i];
                //System.out.println(x[i]+" "+y[i]);
                k = k + 1;
            }
        }
        if (k > 0) {
            xs = new double[k];
            ys = new double[k];
            for (int i = 0; i < k; i++) {
                xs[i] = tmp[i][0];
                ys[i] = tmp[i][1];
            }

        }
    }

    public double[] getXs() {
        return xs;
    }

    public double[] getYs() {
        return ys;
    }
    private double[] xs;
    private double[] ys;
}
