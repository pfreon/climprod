
/**
 * Titre : Climprod<p>
 * Description : <p>
 * Copyright : Copyright (c) <p>
 * Soci�t� : <p>
 * @author
 * @version 1.0
 * Formate un dossier html pour conserver l'historique des données, résultats, graphiques et questions/réponses.
 */
package fr.ird.climprod;
import java.util.Hashtable;
import java.util.Enumeration;
public class WriteHtml {

  public WriteHtml() {
  }

  static public String enteteHtml(String titre,String cssClass,String menu){
      String Head="<HTML>\n  <HEAD>\n";
      if(titre!=null)
        Head=Head+"   <TITLE>"+ titre+"   </TITLE>\n";
     if(cssClass!=null)
        Head=Head+ cssClass+" \n";
     
     Head = Head+"</HEAD>\n";
     Head = Head+ "<BODY>\n";
     
     if(menu!=null)
         Head=Head+menu + "\n";
     
      return Head;
  }
  static public String getBaliseImage(String url, String width,String height,int bordure)
  {
      String w="";
      String h="";
      if(width.trim()!="") w=" WIDTH = " + width;
      if(height.trim()!="") h=" HEIGHT = " + height;
      return "<IMG SRC=\"" + url+ "\"" + w + h + " BORDER=" +bordure + ">";

  }
   static public String getBaliseAncre(String url,String libelle, String classe)
  {
      String c="";

      if(classe !=null) c=" CLASS= " + classe;

      return "<A HREF =\"" + url +"\"" + c+ ">"+libelle+"</A>";

  }

   static public String getTableau(Hashtable paramList,boolean titreLigne,String[] titreColonne,String[][] valeur){

      String width="";
      String border="";
      String fontSize="";
      String caption="";
      String cellpading="";
      String cellspacing="";
      String titreCol="";
      String listeLigne="";
      String TDDeb="<TD>";
      String TDFin="</TD>";
      String THDeb="<TH>";
      String THFin="</TH>";
      String TRDeb="<TR>";
      String TRFin="</TR>";
      String table="";
      String TLDeb="";
      String TLFin="";
      if(titreLigne)
      {
          TLDeb="<TH>";
          TLFin="</TH>";
      }
      Enumeration ek=paramList.keys();

     while(ek.hasMoreElements())
     {
          String param=(String) ek.nextElement();
          String value=(String)paramList.get(param);
          if(param.equals("CAPTION"))
              caption="<CAPTION>"+ value + "</CAPTION>";
          else if (param.equals("BORDER"))
              border=" BORDER=" + value + " ";
          else if (param.equals("WIDTH"))
              width=" WIDTH= " + value + " ";
          else if (param.equals("CELLPADDING"))
              cellpading="CELLPADDING " + value + " ";
          else if (param.equals("CELLSPACING"))
              cellspacing=" CELLSPACING= " + value + " ";
          else if(param.equals("FONTSIZE") )
              fontSize="<FONT SIZE= " + value+">";
          else if(param.equals("TH") )
              THDeb="<TH CLASS=" + value+">";
          else if(param.equals("TR") )
              TRDeb="<TR CLASS=" + value+">";
          else if(param.equals("TD") )
              TDDeb="<TD CLASS=" + value+">";
          else if(param.equals("figureTitreLigne") )
              TLDeb="<TH CLASS=" + value+">";
          else
              return null;

     }


     for(int i=0;i<titreColonne.length;i++)
          titreCol=titreCol+THDeb + fontSize + titreColonne[i]+THFin;


      for(int i=0;i<valeur.length;i++)
      {
        listeLigne=listeLigne+"<TR>\n";
        listeLigne=listeLigne+TLDeb+ fontSize +valeur[i][0]+TLFin+"\n";
        for(int j=1;j<valeur[i].length;j++)
           listeLigne=listeLigne+"  <TD>" + fontSize + valeur[i][j]+"</TD>";
        listeLigne=listeLigne+"\n</TR>\n";
       }

      table="<TABLE "+ border+ width+">\n";
      table=table+ caption+"\n";
      table=table+ titreCol+"\n";
      table=table+listeLigne+"\n </TABLE>";

      return table;





  }
  static public String getTableau(String titre,String classTableau,String[][] classCellule,String[][] valeur){
    String classe="";
    String listeLigne="";
    String table="";
    for(int i=0;i<valeur.length;i++)
    {
        listeLigne=listeLigne+"<TR>\n";

        for(int j=0;j<valeur[i].length;j++){
           classe=" CLASS="+classCellule[i][j]+">";
           listeLigne=listeLigne+"  <TD " + classe+  valeur[i][j]+"</TD>";
        }
        listeLigne=listeLigne+"\n</TR>\n";
     }
     if(classTableau !=null)
          table="<TABLE CLASS="+ classTableau+">\n";
     else
          table="<TABLE>\n";
     if(titre !=null)
          table=table+ "<CAPTION>"+ titre + "</CAPTION>"+"\n";

     table=table+listeLigne+"\n </TABLE>";

     return table;

  }
  static public String getTableau(String titre,String classTableau,String[][] valeur){
    String listeLigne="";
    String table="";
    for(int i=0;i<valeur.length;i++)
    {
        listeLigne=listeLigne+"<TR>\n";

        for(int j=0;j<valeur[i].length;j++){
           listeLigne=listeLigne+"  <TD> "+ valeur[i][j]+"</TD>";
        }
        listeLigne=listeLigne+"\n</TR>\n";
     }
     if(classTableau !=null)
          table="<TABLE CLASS="+ classTableau+">\n";
     else
          table="<TABLE>\n";
     if(titre !=null)
          table=table+ "<CAPTION>"+ titre + "</CAPTION>"+"\n";

     table=table+listeLigne+"\n </TABLE>";

     return table;

  }
  /*
  * Permet de constituer un menu a l'aide d'un tableau HTML de 2 lignes maximum
  * La premi�re ligne facultative comporte le titre, la seconde la suite de liens
  * permettant l'acc�s � d'autres pages
  *  Aucun controle sur les variables pass�es en param�tres
  */

  static String horizontalMenu(String titreMenu,String classTitre,String attributTableau,String[] classCellule,String[] caption,String[] ancre,String[] classBalise){
    String listeMenu="";

    if(titreMenu !=null)
    {
      listeMenu=listeMenu+"<TR> \n";
      if(classTitre!=null)
        listeMenu=listeMenu+"  <TD CLASS=" + classTitre+" COLSPAN ="+ancre.length+">"+ titreMenu +"</TD>\n</TR>\n";
      else
        listeMenu=listeMenu+"  <TD COLSPAN ="+ancre.length+">"+ titreMenu +"</TD>\n</TR>\n";
    }
    listeMenu=listeMenu+"<TR>\n";
        if(classBalise!=null)
        {
          for(int i=0;i<ancre.length;i++){
              String balise=getBaliseAncre(ancre[i],caption[i],classBalise[i]);
              String classe=" CLASS="+classCellule[i]+">";
              listeMenu=listeMenu+"  <TD "+ classe+" "+balise +"</TD>";
          }
        }
        else
        {
           for(int i=0;i<ancre.length;i++){
              String balise=getBaliseAncre(ancre[i],caption[i],null);
              String classe=" CLASS="+classCellule[i]+">";
              listeMenu=listeMenu+"  <TD "+ classe+" "+balise +"</TD>";
          }
        }


        listeMenu=listeMenu+"\n</TR>\n";
    //<CENTER><H1> CLIMPROD </H1></CENTER>\n
    return "<TABLE"+attributTableau+">\n" +listeMenu+ "</TABLE>\n";

  }

}