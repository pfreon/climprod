/**
 * Titre : Climprod<p>
 * Constitute un rapport de l'historique des données, graphiques et tableau de résultats 
 * et celui des questions/réponses en lien avec WriteHtml.java.
 */
package fr.ird.climprod;
import javax.swing.*;
import java.awt.*;
import java.util.Hashtable;
import java.util.Vector;
import java.io.*;
import java.awt.event.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import javax.swing.border.TitledBorder;
public class RapportHtml extends JDialog {
    
    private JFrame parent;
    static private String fileSep;
    static private String path;
    static private String folderName;
    
    public RapportHtml(JFrame parent, String currentFacts) {
        super(parent);
        this.parent = parent;
        choice = 0;
        folderList = null;
        facts = currentFacts;
		//System.out.println("RapportHtml Ligne 33 FlagNewHtmlFolder = " + Global.FlagNewHtmlFolder);
        fileSep = System.getProperty("file.separator");
        if (fileSep.equals("/")) {
            fileSep = "//";
        }
        path = System.getProperty("user.home") + fileSep + ".climprod" + fileSep + "Html&GraphsFolder";
        File fPath = new File(path);
        File[] dd = fPath.listFiles();

        // default report name : basename of datafilename
        File file = new File(Global.nom_fichier);
        String defaultName = file.getName();

        cboFile.addItem(defaultName);
        
        if (!fPath.exists()) {
            fPath.mkdir();
        } else {
            for (int i = 0; i < dd.length; i++) {
                // if this is a directory and it's not the default one (already added first)
                if (dd[i].isDirectory() && ! dd[i].getName().equals(defaultName)) {
                    cboFile.addItem(dd[i].getName());
                }
            }
        }

        try {
            initWindow();
            this.setSize(500, 400);
            UtilCadre.Centrer(this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initWindow() throws Exception {

        this.getContentPane().setLayout(new BorderLayout());

        jPanCombo.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)), "Select or create a new directory"));
        jPanChk.setBorder(new TitledBorder(BorderFactory.createEtchedBorder(Color.white, new Color(142, 142, 142)), "Checked element are added in the directory"));

        cboFile.setEditable(true);
        jTextInfo.setBackground(new Color(204, 204, 204));
        jTextInfo.setText("If you select an existing directory\nall HTML directory files of \nthis directory will be overwritten\nalthough this is not always\nreflected in the file date.");
        jTextInfo.setFont(new java.awt.Font("Dialog", 2, 13));

        this.getContentPane().add(jPan, BorderLayout.CENTER);
        this.getContentPane().add(jPanCmd, BorderLayout.SOUTH);
        jPanChk.setLayout(new GridLayout(13, 1));
        jPan.setLayout(new GridLayout(1, 2));
        jPan.add(jPanCombo);
        jPan.add(jPanChk);
        jPanCmd.add(cmdOk);
        jPanCmd.add(cmdCancel);
        jPanCombo.add(cboFile, BorderLayout.NORTH);
        jPanCombo.add(jTextInfo, BorderLayout.SOUTH);
        //Options accessibles
        for (int i = 0; i < 5; i++) {
            bEnable[i] = (Global.nom_fichier != null);
        }
        bEnable[5] = bEnable[0] && Global.modelisationOk;
        bEnable[6] = bEnable[5];  // Residuals CPUEs vs E and vs V
        bEnable[8] = bEnable[5];
		bEnable[7] = bEnable[0] && Global.validationOk; // Jackknife plot
		bEnable[9] = Global.validationOk && (((Global.numero_modele > 5) || (Global.numero_modele < 2)) && (Global.numero_modele != 20) && (Global.numero_modele != 33));
        bEnable[10] = bEnable[5];
        bEnable[11] = bEnable[7];
        // bEnable[12] = to enable questions and answers
		bEnable[12] = bEnable[0] && (Global.changement_exploitation != -1); // Modif 2020.
        // to create index.html
        bEnable[13] = bEnable[0];
        for (int i = 0; i < chkInclude.length; i++) {
			//System.out.println("chkInclude.length = " + chkInclude.length);
            chkInclude[i] = new JCheckBox(caption[i], bEnable[i]);
            chkInclude[i].setEnabled(bEnable[i]);
            jPanChk.add(chkInclude[i], i);
        }
        
        int condition = JComponent.WHEN_IN_FOCUSED_WINDOW;
        KeyStroke keyStrokeo = KeyStroke.getKeyStroke(KeyEvent.VK_ASTERISK, 0);
        InputMap inputMapo = cmdOk.getInputMap(condition);
        ActionMap actionMapo = cmdOk.getActionMap();
        inputMapo.put(keyStrokeo, keyStrokeo.toString());
        actionMapo.put(keyStrokeo.toString(), new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                cmdOk.doClick();
            }
        });
        cmdOk.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                cmdOk_actionPerformed(e);
            }
        });

        //int condition = JComponent.WHEN_IN_FOCUSED_WINDOW;
        KeyStroke keyStrokep = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
        InputMap inputMapp = cmdCancel.getInputMap(condition);
        ActionMap actionMapp = cmdCancel.getActionMap();
        inputMapp.put(keyStrokep, keyStrokep.toString());
        actionMapp.put(keyStrokep.toString(), new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                cmdCancel.doClick();
            }
        });
        cmdCancel.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                cmdCancel_actionPerformed(e);
            }
        });

    }

    public int getResult() {

        while (choice == 0) {
        }
        return choice;

    }

    public String[] getFolderName() {
        if (choice == 1) {
            return folderList;
        }
        return null;

    }

    /**
     *Titres des onglets et des fichiers html et effacement fichiers existants
     */
    @SuppressWarnings("unchecked")
    void cmdOk_actionPerformed(ActionEvent e) {

        String[] fileName = {"MainResult.html", "DataAndStatisticalTable.html", "TimePlot.html", "HistogrPlot.html",
            "BivariatePlot.html", "FittedPlot.html", "ResidualPlot.html", "JackknifePlot.html", "VariatePlot.html",
            "MS_Plot.html", "Model.html", "Validation.html","QuestionsAnswers.html", "index.html"};
        String[] caption = {"Main Results", "Data", "Time graphs", "Histogram graphs",
            "Bivariate plots", "CPUE residual analysis graphs", "Residual CPUE vs E & V plots", "Jackknife bar graphs", "CPUE=f() & Y=() tri-variate graphs",
            "MSY & MSE vs V graphs", "Modelization", "Validation", "Questions, answers & warnings"};
        String folderName = "";
        Hashtable listFile = new Hashtable();
        for (int i = 0; i < fileName.length; i++) {
            listFile.put(fileName[i], Integer.toString(i));
        }
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        folderName = "" + cboFile.getSelectedItem();
        if (folderName.trim().equals("")) {
            folderName = "defaultFolder";
        }
        Global.FlagNewHtmlFolder = true;
		//System.out.println("Ligne 194 FlagNewHtmlFolder =  " + Global.FlagNewHtmlFolder);
        String folderPath = path + fileSep + folderName;
        File fPath = new File(folderPath);
        if (!fPath.exists()) {
            fPath.mkdir();
        } else {
            File[] dd = fPath.listFiles();  // Liste des 33 fichiers (13 *.html et 20 *.jpg) pré-existant dans répertoire
            for (int i = 0; i < dd.length; i++) {
				//System.out.println("Ligne 199 dd.length =  " + dd.length + " dd[i] = " + dd[i] +" i = " + i + " listFile.containsKey(dd[i].getName();
                    dd[i].delete();
            }
        }
  
        int c = chkInclude.length;

        Vector vAncre = new Vector();
        Vector vCaption = new Vector();
        String[] htmlFile = new String[14];
        String[] htmlText = new String[14];
        String htmlMenu;
        String htlmEntete;
		//System.out.println("RapportHtml ligne 214 c = " + c);
        for (int i = 0; i < c; i++) {
            htmlText[i] = null;
            bEnable[i] = chkInclude[i].isEnabled() && chkInclude[i].isSelected();
            if (bEnable[i]) {
                htmlFile[i] = fileName[i];
                vAncre.add(htmlFile[i]);
                vCaption.add(caption[i]);
            } else {
                htmlFile[i] = null;
            }
        }
        if (bEnable[c]) //Gestion de l'index
        {
            htmlFile[c] = fileName[c];
        } else {
            htmlFile[c] = null;
        }
      //vAncre.add("/");
        //vCaption.add("End");
        int n = vCaption.size();
        caption = new String[n];
        vCaption.copyInto(caption);
        String[] ancre = new String[n];
        vAncre.copyInto(ancre);

        String cssClass = "<STYLE TYPE=\"text/css\">\n";
        // cssClass =cssClass+"TD.M1 {border:solid 1;background-color:red;font-weight:bold;font-size:xx-small}\n";
        cssClass = cssClass + "TABLE.T2 {background-color:blue}\n";
        cssClass = cssClass + "TABLE.T1 {border: solid 1}\n";
        cssClass = cssClass + "TD.M1 {border:solid 0; font-weight:bold;font-size:small;text-align:center}\n";
        cssClass = cssClass + "TD.C1 {border:solid 1;background-color:silver;font-weight:bold;font-size:xx-small}\n";
        cssClass = cssClass + "TD.C2 {border:solid 1;background-color:silver;font-weight:bold;font-size:xx-small}\n";
        cssClass = cssClass + "TD.C3 {border:solid 1;background-color:white;font-weight:normal;font-size:xx-small}\n";
        cssClass = cssClass + "TD.C5 {color:white;font-weight:normal;font-size:large}\n";
        cssClass = cssClass + "TH.A0 {background-color:silver}\n";
        cssClass = cssClass + "A.C4 {color:white;text-decoration:none}";

        cssClass = cssClass + "</STYLE>\n";

        String[] cssCellule = new String[n];
        String[] cssAncre = new String[n];
        for (int i = 0; i < n; i++) {
            cssCellule[i] = "M1";
            cssAncre[i] = "C4";
        }
        String attributsTableau = " CLASS=T2  BORDER='0'  CELLSPACING='2' CELLPADDING='4'  WIDTH='100%'  ";
        htmlMenu = WriteHtml.horizontalMenu("CLIMPROD", "C5", attributsTableau, cssCellule, caption, ancre, cssAncre);
        //htmlMenu=WriteHtml.horizontalMenu(null,"C5",attributsTableau,cssCellule,caption,ancre,cssAncre);
        htlmEntete = WriteHtml.enteteHtml("Climprod - Folder name: " + folderName, cssClass, htmlMenu);

        if (bEnable[0]) {
            htmlText[0] = htlmEntete.replace("HREF =\""+htmlFile[0]+"\"","HREF =\""+htmlFile[0]+"\" style=\"font-weight:bold;font-size:16px;color:red;\"") + "\n\n ";
            htmlText[0] = htmlText[0] + "<H4>" + caption[0] + "</H4>\n";
            htmlText[0] = htmlText[0] + "<PRE>" + facts + "</PRE>";
            htmlText[0] = htmlText[0] + "</BODY>\n</HTML>";
        }
        if (bEnable[1]) {
            htmlText[1] = htlmEntete.replace("HREF =\""+htmlFile[1]+"\"","HREF =\""+htmlFile[1]+"\" style=\"font-weight:bold;font-size:16px;color:red;\"") + "\n\n ";
            // htmlText[1]=htmlText[1]+"<H4> Main Results </H4>\n <CENTER>\n";
            htmlText[1] = htmlText[1] + makeDataTable();
            htmlText[1] = htmlText[1] + "\n </CENTER>\n</BODY>\n</HTML>";
        }
        if (bEnable[2]) {
            htmlText[2] = htlmEntete.replace("HREF =\""+htmlFile[2]+"\"","HREF =\""+htmlFile[2]+"\" style=\"font-weight:bold;font-size:16px;color:red;\"") + "\n\n <CENTER>\n";
            htmlText[2] = htmlText[2] + this.makePlot(Global.timePlot, true, folderPath);
            htmlText[2] = htmlText[2] + "\n</CENTER>\n</BODY>\n</HTML>";
        }
        if (bEnable[3]) {
            htmlText[3] = htlmEntete.replace("HREF =\""+htmlFile[3]+"\"","HREF =\""+htmlFile[3]+"\" style=\"font-weight:bold;font-size:16px;color:red;\"") + "\n\n <CENTER>\n";
            htmlText[3] = htmlText[3] + this.makePlot(Global.distriPlot, true, folderPath);
            htmlText[3] = htmlText[3] + "\n</CENTER>\n</BODY>\n</HTML>";
        } 
        if (bEnable[4]) {
            htmlText[4] = htlmEntete.replace("HREF =\""+htmlFile[4]+"\"","HREF =\""+htmlFile[4]+"\" style=\"font-weight:bold;font-size:16px;color:red;\"") + "\n\n <CENTER>\n";
            htmlText[4] = htmlText[4] + this.makePlot(Global.scatterPlot, true, folderPath);
            htmlText[4] = htmlText[4] + "\n  </CENTER>\n</BODY>\n</HTML>";
        }
        if (bEnable[5]) {
            htmlText[5] = htlmEntete.replace("HREF =\""+htmlFile[5]+"\"","HREF =\""+htmlFile[5]+"\" style=\"font-weight:bold;font-size:16px;color:red;\"") + "\n\n <CENTER>\n";
            htmlText[5] = htmlText[5] + this.makePlot(Global.fittedCpuePlot, true, folderPath);
            htmlText[5] = htmlText[5] + "\n </CENTER>\n</BODY>\n</HTML>";
        }
        if (bEnable[6]) {
            htmlText[6] = htlmEntete.replace("HREF =\""+htmlFile[6]+"\"","HREF =\""+htmlFile[6]+"\" style=\"font-weight:bold;font-size:16px;color:red;\"") + "\n\n <CENTER>\n";
            htmlText[6] = htmlText[6] + this.makePlot(Global.residualsPlot, true, folderPath);
            htmlText[6] = htmlText[6] + "\n  </CENTER>\n</BODY>\n</HTML>";
        }
        if (bEnable[7]) {
            htmlText[7] = htlmEntete.replace("HREF =\""+htmlFile[7]+"\"","HREF =\""+htmlFile[7]+"\" style=\"font-weight:bold;font-size:16px;color:red;\"") + "\n\n <CENTER>\n";
            htmlText[7] = htmlText[7] + this.makePlot(Global.jackknifePlot, true, folderPath);
            htmlText[7] = htmlText[7] + "\n  </CENTER>\n</BODY>\n</HTML>";
        }
        if (bEnable[8]) {
            htmlText[8] = htlmEntete.replace("HREF =\""+htmlFile[8]+"\"","HREF =\""+htmlFile[8]+"\" style=\"font-weight:bold;font-size:16px;color:red;\"") + "\n\n <CENTER>\n";
            htmlText[8] = htmlText[8] + this.makePlot(Global.variatePlot, true, folderPath);
            htmlText[8] = htmlText[8] + "\n  </CENTER>\n</BODY>\n</HTML>";
        }
        if (bEnable[9]) {
            htmlText[9] = htlmEntete.replace("HREF =\""+htmlFile[9]+"\"","HREF =\""+htmlFile[9]+"\" style=\"font-weight:bold;font-size:16px;color:red;\"") + "\n\n <CENTER>\n";
            htmlText[9] = htmlText[9] + this.makePlot(Global.msyPlot, true, folderPath);
            htmlText[9] = htmlText[9] + "\n  </CENTER>\n</BODY>\n</HTML>";
        }
        if (bEnable[10]) {
            htmlText[10] = htlmEntete.replace("HREF =\""+htmlFile[10]+"\"","HREF =\""+htmlFile[10]+"\" style=\"font-weight:bold;font-size:16px;color:red;\"") + "\n\n ";
            htmlText[10] = htmlText[10] + "<H4> Modelisation:Detailled Results </H4>\n <CENTER>\n";
            htmlText[10] = htmlText[10] + makeModeleTable();
            htmlText[10] = htmlText[10] + "\n  </CENTER>\n</BODY>\n</HTML>";
        }
        if (bEnable[11]) {
            htmlText[11] = htlmEntete.replace("HREF =\""+htmlFile[11]+"\"","HREF =\""+htmlFile[11]+"\" style=\"font-weight:bold;font-size:16px;color:red;\"") + "\n\n ";
            htmlText[11] = htmlText[11] + "<H4> Validation:Detailled Results </H4>\n <CENTER>\n";
            htmlText[11] = htmlText[11] + makeValidationTable();
            htmlText[11] = htmlText[11] + "\n  </CENTER>\n</BODY>\n</HTML>";
        }
        if (bEnable[12]) {         
            htmlText[12] = htlmEntete.replace("HREF =\""+htmlFile[12]+"\"","HREF =\""+htmlFile[12]+"\" style=\"font-weight:bold;font-size:16px;color:red;\"") + "\n\n ";
            htmlText[12] = htmlText[12] + "<H4> Questions, answers &amp; warning</H4>\n <CENTER>\n";
            htmlText[12] = htmlText[12] + makeQuestionsAnswers();
            //System.out.println("Flag if (bEnable[11])dans RapportHtml.java ligne 328, makeQuestionsAnswers()= " + makeQuestionsAnswers()); // Modif. 2020.
            htmlText[12] = htmlText[12] + "\n  </CENTER>\n</BODY>\n</HTML>";
        }
        if (bEnable[13]) {
            String quest[] = QuestionReponse.getListScript();
            htmlText[13] = htlmEntete.replace("HREF =\""+htmlFile[13]+"\"","HREF =\""+htmlFile[13]+"\" style=\"font-weight:bold;font-size:16px;color:red;\"") + "\n ";
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"); 
            Date date = new Date();
            htmlText[13] = htmlText[13] + "Generated at "+dateFormat.format(date); // Date et heure affichées à l'ouverture du fichier html d'acceuil.
            htmlText[13] = htmlText[13] + "\n </BODY>\n</HTML>\n ";
        }

        c = htmlText.length;
		//System.out.println("RapportHtml ligne 346 c = " + c);
        for (int i = 0; i < c; i++) {
            if (bEnable[i]) {
                try {
                    this.saveHtmlFile((folderPath + fileSep + htmlFile[i]), htmlText[i].replace("²","&sup2;"));
                } catch (IOException ec) {
                    MsgDialogBox msg = new MsgDialogBox(0, "Unexpected error " + ec.getMessage(), 0, this.parent);
                    choice = 2;
                    cancel();
                }
            }
        }

        fPath = new File(path);
        File[] dd = fPath.listFiles();
        Vector vList = new Vector();
        for (int i = 0; i < dd.length; i++) {
            if (dd[i].isDirectory()) {
                vList.add(dd[i].getAbsolutePath() + fileSep + "index.html");
            }
        }
        folderList = new String[vList.size()];
        vList.copyInto(folderList);
        MsgDialogBox msg = new MsgDialogBox(0, "Files successfully saved in the directory\n" + folderPath, 1, this.parent);
        choice = 1;
        cancel();
    }  
       
    void cmdCancel_actionPerformed(ActionEvent e) {
        choice = 2;
        cancel();
    }
    
    private void makeImage(String titre, Plot p) { // Images graphiques disponibles dans classeur du path; titre = chemin complet addresse et nom fichier

		//System.out.println("RapportHtml.java ligne 383, titre: " + titre); // titre obtenu dans makeImage
        ExporterImage expI = new ExporterImage(p.createImage(450, 450), titre); // 450 x 450 pixels = tous fichiers plots
        expI.write();   // Enregistrement fichiers .jpg

    }
    /*
	 *Création  contenu onglet Questions & Réponses. Commentaire 2020
	*/
	
    private String makeQuestionsAnswers(){   
        String resultHTML = "<ul style='text-align:left;'>";
        //System.out.println("Flag private String makeQuestionsAnswers() dans RapportHtml.java ligne 393, resultHTML = " + resultHTML); // Modif. 2020
        // ugly way to make tings ordered
        for (int i = 0; i < 200; i++){
        //System.out.println("RapportHtml.java ligne 396; i = " + i + "Global.warningDic.get(i) = " + Global.warningDic.get(i));
            if (Global.questionDic.containsKey(i)){
                resultHTML += "<li>";
                resultHTML += Global.questionDic.get(i).replace('²','2')+" ";
                resultHTML += "<b style='color:blue;'>"+Global.answerDic.get(i)+"</b>";
                if (Global.warningDic.containsKey(i)){
                	//System.out.println("Flag if (Global.warningDic.containsKey(i)) dans RapportHtml.java ligne 400, i = " + i + " Global.warningDic.containsKey(i) = " + Global.warningDic.containsKey(i)); // Modif. 2020
                    if (!Global.warningDic.get(i).equals("")){
                        resultHTML += "<i style='color:red;'>. Warning: "+Global.warningDic.get(i)+"</i>";
                    } 

                }
                resultHTML += "</li>\n";
            }
        }

        resultHTML += "</ul>";
        return resultHTML;
    }

    private String makeDataTable() {

        String cssClass = "<STYLE TYPE=\"text/css\">\n";

        cssClass = cssClass + "TH.A0 {background-color:silver}\n";
        cssClass = cssClass + "TH.A1 {background-color:yellow}\n";
        cssClass = cssClass + "TD.C1 {border:solid 1;background-color:silver;font-weight:bold;font-size:xx-small}\n";
        cssClass = cssClass + "TD.C2 {border:solid 1;background-color:silver;font-weight:bold;font-size:xx-small}\n";
        cssClass = cssClass + "TD.C3 {border:solid 1;background-color:white;font-weight:normal;font-size:xx-small}\n";
        cssClass = cssClass + "TABLE.T1 {border: solid 1}\n";
        cssClass = cssClass + "</STYLE>\n";

        String[] htmlTab = new String[4];

        for (int i = 0; i < 3; i++) {
            htmlTab[i] = "";
        }

        Object[][] res = Data.getDataTable();
        if (res == null) {
            return null;
        }

        int c = res[0].length;
        int l = res.length;
        String[][] dat = new String[l][c];
        String[][] classe = new String[l][c];
        for (int i = 0; i < l; i++) {
            for (int j = 0; j < c; j++) {
                dat[i][j] = (String) res[i][j];
            }
        }
        for (int j = 0; j < c; j++) {
            classe[0][j] = "C1";
        }
        for (int i = 0; i < l; i++) {
            classe[i][0] = "C2";
        }
        for (int i = 1; i < l; i++) {
            for (int j = 1; j < c; j++) {
                classe[i][j] = "C3";
            }
        }

        htmlTab[0] = WriteHtml.getTableau("Data", "T1", classe, dat);

        res = Data.getStatistics();
        c = res[0].length;
        l = res.length;
        dat = new String[l][c];
        classe = new String[l][c];
        for (int i = 0; i < l; i++) {
            for (int j = 0; j < c; j++) {
                dat[i][j] = (String) res[i][j];
            }
        }
        for (int j = 0; j < c; j++) {
            classe[0][j] = "C1";
        }
        for (int i = 0; i < l; i++) {
            classe[i][0] = "C2";
        }
        for (int i = 1; i < l; i++) {
            for (int j = 1; j < c; j++) {
                classe[i][j] = "C3";
            }
        }

        htmlTab[1] = WriteHtml.getTableau("Statistics", "T1", classe, dat);

        res = Data.getCorrelationTable();
        c = res[0].length;
        l = res.length;
        dat = new String[l][c];
        classe = new String[l][c];
        for (int i = 0; i < l; i++) {
            for (int j = 0; j < c; j++) {
                dat[i][j] = (String) res[i][j];
            }
        }
        for (int j = 0; j < c; j++) {
            classe[0][j] = "C1";
        }
        for (int i = 0; i < l; i++) {
            classe[i][0] = "C2";
        }
        for (int i = 1; i < l; i++) {
            for (int j = 1; j < c; j++) {
                classe[i][j] = "C3";
            }
        }

        htmlTab[2] = WriteHtml.getTableau("Correlation", "T1", classe, dat);

        htmlTab[3] = "<TABLE cellpadding=2 cellspacing=20 >\n<TR>";
        htmlTab[3] = htmlTab[3] + "<TD align=center rowspan=2 >\n" + htmlTab[0] + "\n</TD>";
        htmlTab[3] = htmlTab[3] + "<TD align=center >\n" + htmlTab[1] + "\n</TD>";
        htmlTab[3] = htmlTab[3] + "</TR>\n<TR>";
        htmlTab[3] = htmlTab[3] + "<TD align=center >\n" + htmlTab[2] + "\n</TD>";
        htmlTab[3] = htmlTab[3] + "</TR>\n</TABLE>";

        return htmlTab[3];

    }

    private String makeValidationTable() {

        String cssClass = "<STYLE TYPE=\"text/css\">\n";

        cssClass = cssClass + "TH.A0 {background-color:silver}\n";
        cssClass = cssClass + "TH.A1 {background-color:yellow}\n";
        cssClass = cssClass + "TD.C1 {border:solid 1;background-color:silver;font-weight:bold;font-size:xx-small}\n";
        cssClass = cssClass + "TD.C2 {border:solid 1;background-color:silver;font-weight:bold;font-size:xx-small}\n";
        cssClass = cssClass + "TD.C3 {border:solid 1;background-color:white;font-weight:normal;font-size:xx-small}\n";
        cssClass = cssClass + "TABLE.T1 {border: solid 1}\n";
        cssClass = cssClass + "</STYLE>\n";

        String[] htmlTab = new String[5];

        for (int i = 0; i < 3; i++) {
            htmlTab[i] = "";
        }

        Object[][] res = Validation.getParamResult();
        if (res == null) {
            return null;
        }

        int c = res[0].length;
        int l = res.length;
        String[][] dat = new String[l][c];
        String[][] classe = new String[l][c];
        for (int i = 0; i < l; i++) {
            for (int j = 0; j < c; j++) {
                dat[i][j] = (String) res[i][j];
            }
        }
        for (int j = 0; j < c; j++) {
            classe[0][j] = "C1";
        }
        for (int i = 0; i < l; i++) {
            classe[i][0] = "C2";
        }
        for (int i = 1; i < l; i++) {
            for (int j = 1; j < c; j++) {
                classe[i][j] = "C3";
            }
        }

        htmlTab[0] = WriteHtml.getTableau("", "T1", classe, dat);

        res = Validation.getMS_YResult();
        c = res[0].length;
        l = res.length;
        dat = new String[l][c];
        classe = new String[l][c];
        for (int i = 0; i < l; i++) {
            for (int j = 0; j < c; j++) {
                dat[i][j] = (String) res[i][j];
            }
        }
        for (int j = 0; j < c; j++) {
            classe[0][j] = "C1";
        }
        for (int i = 0; i < l; i++) {
            classe[i][0] = "C2";
        }
        for (int i = 1; i < l; i++) {
            for (int j = 1; j < c; j++) {
                classe[i][j] = "C3";
            }
        }

        htmlTab[1] = WriteHtml.getTableau("", "T1", classe, dat);

        res = Validation.getMS_EResult();
        c = res[0].length;
        l = res.length;
        dat = new String[l][c];
        classe = new String[l][c];
        for (int i = 0; i < l; i++) {
            for (int j = 0; j < c; j++) {
                dat[i][j] = (String) res[i][j];
            }
        }
        for (int j = 0; j < c; j++) {
            classe[0][j] = "C1";
        }
        for (int i = 0; i < l; i++) {
            classe[i][0] = "C2";
        }
        for (int i = 1; i < l; i++) {
            for (int j = 1; j < c; j++) {
                classe[i][j] = "C3";
            }
        }

        htmlTab[2] = WriteHtml.getTableau("", "T1", classe, dat);

        res = Validation.getYearResult();
        c = res[0].length;
        l = res.length;
        dat = new String[l][c];
        classe = new String[l][c];
        for (int i = 0; i < l; i++) {
            for (int j = 0; j < c; j++) {
                dat[i][j] = (String) res[i][j];
            }
        }
        for (int j = 0; j < c; j++) {
            classe[0][j] = "C1";
        }
        for (int i = 0; i < l; i++) {
            classe[i][0] = "C2";
        }
        for (int i = 1; i < l; i++) {
            for (int j = 1; j < c; j++) {
                classe[i][j] = "C3";
            }
        }

        htmlTab[3] = WriteHtml.getTableau("", "T1", classe, dat);

        htmlTab[4] = "<TABLE cellpadding=2 cellspacing=20 >\n<TR>";
        htmlTab[4] = htmlTab[4] + "<TD align=center >\n" + htmlTab[0] + "\n</TD>";
        htmlTab[4] = htmlTab[4] + "<TD align=center rowspan=3>\n" + htmlTab[3] + "\n</TD>";
        htmlTab[4] = htmlTab[4] + "</TR>\n<TR>";
        htmlTab[4] = htmlTab[4] + "<TD align=center >\n" + htmlTab[1] + "\n</TD>";
        htmlTab[4] = htmlTab[4] + "</TR>\n<TR>";
        htmlTab[4] = htmlTab[4] + "<TD align=center >\n" + htmlTab[2] + "\n</TD>";
        htmlTab[4] = htmlTab[4] + "</TR>\n</TABLE>";

        return htmlTab[4];

    }

    private String makeModeleTable() {

        String cssClass = "<STYLE TYPE=\"text/css\">\n";

        cssClass = cssClass + "TH.A0 {background-color:silver}\n";
        cssClass = cssClass + "TH.A1 {background-color:yellow}\n";
        cssClass = cssClass + "TD.C1 {border:solid 1;background-color:silver;font-weight:bold;font-size:xx-small}\n";
        cssClass = cssClass + "TD.C2 {border:solid 1;background-color:silver;font-weight:bold;font-size:xx-small}\n";
        cssClass = cssClass + "TD.C3 {border:solid 1;background-color:white;font-weight:normal;font-size:xx-small}\n";
        cssClass = cssClass + "TABLE.T1 {border: solid 1}\n";
        cssClass = cssClass + "</STYLE>\n";

        Object[][] res = Modele.getYearResult();
        if (res == null) {
            return null;
        }

        int c = res[0].length;
        int l = res.length;
        String[][] dat = new String[l][c];
        String[][] classe = new String[l][c];
        for (int i = 0; i < l; i++) {
            for (int j = 0; j < c; j++) {
                dat[i][j] = (String) res[i][j];
            }
        }
        for (int j = 0; j < c; j++) {
            classe[0][j] = "C1";
        }
        for (int i = 0; i < l; i++) {
            classe[i][0] = "C2";
        }
        for (int i = 1; i < l; i++) {
            for (int j = 1; j < c; j++) {
                classe[i][j] = "C3";
            }
        }

        return WriteHtml.getTableau("", "T1", classe, dat);

    }

    private String makePlot(Plot[] p, boolean orientationVerticale, String folderpath) {
        /*if (p == null){
            System.out.println("ON a un gagnant");
            return "";
        }*/
        int nbCase = p.length;
        String[] titre = new String[nbCase];
        int nbLig = 0;
        int nbCol = 0;

        if (nbCase == 1) {
            nbLig = 1;
            nbCol = 1;
        } else if (nbCase == 3) {
            nbLig = 2;
            nbCol = 2;
        } else if (nbCase % 2 == 0 && nbCase != 2) {
            nbLig = nbCase / 2;
            nbCol = nbLig;
        } else if (nbCase > 4) {
            if (nbCase % 3 == 0) {
                nbLig = 3;
                nbCol = nbCase / 3;
            } else {
                nbLig = (nbCase + 1) / 2;
                nbCol = nbLig - 1;
            }
        } else {
            if (orientationVerticale) {
                nbCol = 1;
                nbLig = nbCase;
            } else {
                nbCol = nbCase;
                nbLig = 1;
            }
        }

        String[] temp = new String[nbCase];
        String[][] baliseImage = new String[nbLig][nbCol];
        for (int i = 0; i < nbLig; i++) {
            for (int j = 0; j < nbCol; j++) {
                baliseImage[i][j] = "";
            }
        }
        for (int i = 0; i < nbCase; i++) { // Images graphiques
            if (p[i] != null){			
			String titreCorr  = p[i].getTitreGraphique();
 	          	int cc  = titreCorr.length();
                        if (titreCorr.indexOf("%") > 0) titreCorr =  titreCorr.substring(0,(titreCorr.indexOf("%")+12));  // In order to shorten the title of the coefficient jackknife plot file.      
                titre[i] = titreCorr + ".jpg"; // Noms fichiers image jpg                
				//System.out.println("RapportHtml.java ligne 770, titre [i]: " + titre[i] + " i = " + i);
				makeImage(folderpath + fileSep + titre[i], p[i]); 
                temp[i] = WriteHtml.getBaliseImage("./" + titre[i], "500", "500", 1);
            }
        }        
        int k = 0;
        for (int i = 0; i < nbLig; i++) {
            for (int j = 0; j < nbCol; j++) {
                baliseImage[i][j] = temp[k];
                k++;
                if (k >= nbCase) {
                    break;
                }
            }
        }
        String htmlTab = "";
        htmlTab = htmlTab + WriteHtml.getTableau(null, null, baliseImage);
        return htmlTab;

    }

/* INUTILISE. Commentaire 2020   
		private void saveHtmlFile(String[] fileName, String[] htmlCode) throws IOException { 
        for (int i = 0; i < fileName.length; i++) {
            if (!fileName[i].equals("")) {
                PrintWriter out = new PrintWriter(new FileWriter(fileName[i]));
                out.println(htmlCode[i]);
                out.close();
            }
        }
    }*/

    private void saveHtmlFile(String fileName, String htmlCode) throws IOException { // Nom des fichiers .html (tables et graphiques)
        PrintWriter out = new PrintWriter(new FileWriter(fileName)); // Enregistrement fichiers .html
        out.println(htmlCode); 										 // Enregistrement fichiers .html
        out.close();

    }

    @Override
    protected void processWindowEvent(WindowEvent e) {
        if (e.getID() == WindowEvent.WINDOW_CLOSING) {
            cancel();
        }
        super.processWindowEvent(e);
    }

    private void cancel() {
        this.setCursor(Cursor.getDefaultCursor());
        dispose();
    }

	//private String fileSep;
    //private String path;
    private String facts;
    private String htmlText = "";
    private String[] folderList;
    private boolean[] bEnable = new boolean[14];
    private int choice;
    final int Ok_Option = 1;
    final int Cancel_Option = 2;

    JCheckBox[] chkInclude = new JCheckBox[13];
    String[] caption = {"Current known facts", "Data & statistical tables",
        "Time plots", "Histogram plots", "Bivariate plots", "CPUE residual analysis graphs",
        "Res f(E) vs V or Res f(V) vs E", "Jackknife plots", "CPUE=f() & Y=f() plots",
        "MSY & MSE plots", "Modelization: Detailed results",
        "Validation: Detailed results", "Questions, answers & warnings"};

    JPanel jPan = new JPanel();
    JPanel jPanCombo = new JPanel();
    JPanel jPanChk = new JPanel();
    JPanel jPanCmd = new JPanel();
    JComboBox<String> cboFile = new JComboBox<String>();
    JButton cmdOk = new JButton("Ok (*)");
    JButton cmdCancel = new JButton("Cancel (ESC)");
    JTextArea jTextInfo = new JTextArea();

}
