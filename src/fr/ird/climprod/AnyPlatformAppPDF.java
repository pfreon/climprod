package fr.ird.climprod;

// Permet l'ouverture d'un fichier pdf quelle que soit la plateforme (Linux, Mac, Windows).

import java.awt.Desktop;
import java.io.File;

public class AnyPlatformAppPDF {
    
    public static void mainPDF(String PDFpathfilename) {
      try {
       File pdfFile = new File(PDFpathfilename);
        if (pdfFile.exists()) {

            if (Desktop.isDesktopSupported()) {
                Desktop.getDesktop().open(pdfFile);
            } else {
                System.out.println("Awt Desktop is not supported!");
            }
        } else {
            System.out.println("PDF file does not exist!");
            MsgDialogBox msg = new MsgDialogBox(0, "PDF file not found.\nPlease check if there are pdf files in your directory\nC:/Program Files/CLIMPROD_5.0/Documents/\nIf not please uninstall and resinstall CLIMPROD\nand accept the default location of the software.\nYour data files will not be lost.", 0, Global.CadreMain);
        }
      } catch (Exception ex) {
        ex.printStackTrace();
      }
    }
} 

